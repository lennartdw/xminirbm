DWAMF_DIR := ./dwamf
OUT_DIR = .
BUILD_DIR = $(OUT_DIR)/build

LIBS = -lgsl -lgslcblas -lquadmath -lm -lpthread -lboost_system -lboost_filesystem
GPP = g++
# OPTIMIZED
#GPP_OPTIONS = -O3 -Wall -std=gnu++14
# OPTIMIZED AND PARALLELIZED
GPP_OPTIONS = -O3 -Wall -std=gnu++14 -fopenmp
# DEBUGGING
#GPP_OPTIONS = -g -Wall -std=gnu++14
# DEBUGGING AND PROFILING
#GPP_OPTIONS = -g -Wall -std=gnu++14 -pg
# OPTIMIZED AND PROFILING
#GPP_OPTIONS = -O3 -Wall -std=gnu++14 -pg
PROG_NAME=xminirbm

PREFIX = $(GPP_OPTIONS) -I"$(DWAMF_DIR)" -D"EIGEN_DONT_PARALLELIZE" -D"_RANDOM_GSL_MACROS"

OBJS = $(BUILD_DIR)/$(PROG_NAME).o $(BUILD_DIR)/datasets/prototypes.o $(BUILD_DIR)/util/test.o $(BUILD_DIR)/dwamf/util/files.o $(BUILD_DIR)/dwamf/util/strings.o $(BUILD_DIR)/dwamf/util/Monitor.o $(BUILD_DIR)/dwamf/util/ProgressMonitor.o $(BUILD_DIR)/dwamf/util/LogManager.o $(BUILD_DIR)/dwamf/util/bitwise.o $(BUILD_DIR)/dwamf/func/types.o $(BUILD_DIR)/dwamf/func/sets.o $(BUILD_DIR)/dwamf/func/matrices.o $(BUILD_DIR)/dwamf/conf/Configuration.o $(BUILD_DIR)/dwamf/rand/numbers.o $(BUILD_DIR)/dwamf/rand/numbers-gsl.o $(BUILD_DIR)/dwamf/rand/RealProbabilityDistribution.o $(BUILD_DIR)/dwamf/rand/IndexDistribution.o $(BUILD_DIR)/dwamf/rand/sets.o $(BUILD_DIR)/dwamf/rand/statistics.o

$(OUT_DIR)/$(PROG_NAME): $(OBJS) $(DWAMF_DIR)/conf/*.hpp $(DWAMF_DIR)/data/*.hpp $(DWAMF_DIR)/func/*.hpp $(DWAMF_DIR)/util/*.hpp
	echo "Building $(PROG_NAME) ..."
	$(GPP) $(PREFIX) -o $@ $(OBJS) $(LIBS)

$(BUILD_DIR)/$(PROG_NAME).o: $(PROG_NAME).cpp analysis/DistributionAnalyzer.hpp analysis/CategoricalDistributionAnalyzer.hpp analysis/CrossEntropyAnalyzer.hpp analysis/ProbabilityAnalyzer.hpp analysis/TotalCorrelationAnalyzer.hpp datasets/Classification.hpp datasets/ImageBW.hpp datasets/prototypes.hpp datasets/BinaryGaussianMixtureDistribution.hpp experiments/Classification.hpp experiments/RBM.hpp experiments/Sampling.hpp experiments/Transformation.hpp experiments/Utilities.hpp networks/BinaryRBM.hpp $(DWAMF_DIR)/rand/MonteCarloSampler.hpp $(DWAMF_DIR)/rand/IndexDistribution.hpp networks/ExactBinaryRBM.hpp networks/UnsupervisedMachine.hpp platforms/UnsupervisedPlatform.hpp util/headers.hpp util/test.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/util/test.o: util/test.cpp util/test.hpp datasets/ImageBW.hpp networks/BinaryRBM.hpp networks/ExactBinaryRBM.hpp networks/UnsupervisedMachine.hpp platforms/UnsupervisedPlatform.hpp $(DWAMF_DIR)/util/bitwise.hpp $(DWAMF_DIR)/data/DataIO.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/datasets/prototypes.o: datasets/prototypes.cpp datasets/prototypes.hpp datasets/ImageBW.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/func/types.o: $(DWAMF_DIR)/func/types.cpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/func/matrices.o: $(DWAMF_DIR)/func/matrices.cpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/func/sets.o: $(DWAMF_DIR)/func/sets.cpp $(DWAMF_DIR)/func/types.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)
	
$(BUILD_DIR)/dwamf/util/files.o: $(DWAMF_DIR)/util/files.cpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/util/strings.o: $(DWAMF_DIR)/util/strings.cpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/util/bitwise.o: $(DWAMF_DIR)/util/bitwise.cpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/util/Monitor.o: $(DWAMF_DIR)/util/Monitor.cpp $(DWAMF_DIR)/util/Monitor.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/util/ProgressMonitor.o: $(DWAMF_DIR)/util/ProgressMonitor.cpp $(DWAMF_DIR)/util/ProgressMonitor.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/util/LogManager.o: $(DWAMF_DIR)/util/LogManager.cpp $(DWAMF_DIR)/util/LogManager.hpp $(DWAMF_DIR)/util/globals.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/conf/Configuration.o: $(DWAMF_DIR)/conf/Configuration.cpp $(DWAMF_DIR)/conf/Configuration.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/rand/numbers-gsl.o: $(DWAMF_DIR)/rand/numbers-gsl.cpp $(DWAMF_DIR)/rand/numbers.hpp $(DWAMF_DIR)/rand/numbers-gsl-macros.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/rand/numbers.o: $(DWAMF_DIR)/rand/numbers.cpp $(DWAMF_DIR)/rand/numbers.hpp $(DWAMF_DIR)/rand/numbers-gsl-macros.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/rand/RealProbabilityDistribution.o: $(DWAMF_DIR)/rand/RealProbabilityDistribution.cpp $(DWAMF_DIR)/rand/RealProbabilityDistribution.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/rand/IndexDistribution.o: $(DWAMF_DIR)/rand/IndexDistribution.cpp $(DWAMF_DIR)/rand/IndexDistribution.hpp $(DWAMF_DIR)/rand/RealProbabilityDistribution.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/rand/sets.o: $(DWAMF_DIR)/rand/sets.cpp $(DWAMF_DIR)/rand/sets.hpp $(DWAMF_DIR)/func/sets.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

$(BUILD_DIR)/dwamf/rand/statistics.o: $(DWAMF_DIR)/rand/statistics.cpp $(DWAMF_DIR)/rand/statistics.hpp
	$(GPP) $(PREFIX) -c -o $@ $< $(LIBS)

.PHONY: clean
clean:
	rm -f $(BUILD_DIR)/*.o $(BUILD_DIR)/datasets/*.o $(BUILD_DIR)/util/*.o $(BUILD_DIR)/dwamf/conf/*.o $(BUILD_DIR)/dwamf/func/*.o $(BUILD_DIR)/dwamf/rand/*.o $(BUILD_DIR)/dwamf/util/*.o
