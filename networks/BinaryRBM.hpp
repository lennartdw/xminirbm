/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Network model: RBM with binary variables.
 */

#ifndef _XMINIRBM_NETWORKS_BINARYRBM
#define _XMINIRBM_NETWORKS_BINARYRBM

#include "data/DataIO.hpp"
#include "rand/MonteCarloSampler.hpp"
#include "rand/sets.hpp"
#include "util/bitwise.hpp"

#include "../util/headers.hpp"
#include "UnsupervisedMachine.hpp"

#define UNIT_TO_STRING(_unit_) (ISNAN(_unit_) ? "?" : ((_unit_) < 0.5 ? "0" : "1"))

template<typename Scalar>
class BinaryRBM : public UnsupervisedMachine<Scalar>,
                  public MonteCarloSampler< Array<Scalar, Dynamic, Dynamic> >
{
    private:
        bool useExactHiddenProbabilities = false;
        bool usePersistentCD = false;
        int defaultCDOrder;

        // current states of the Markov chains of the Gibbs sampler
        // row index is chain index, column index is visible unit index
        int mcSamplerNumChains = 1;
        Array<Scalar, Dynamic, Dynamic> mcSamplerStates;
        Array<Scalar, Dynamic, Dynamic> mcSamplerStatesHidden;

        bool useAdaptiveCDOrder = false;
        size_t numSamplesACDE;
        size_t minNumSamplesACDE = 1000;
        bool isStoringSamplesACDE = false;
        int minStepsBeforeUpdateACDE = 1; // minimal number of epochs between ACDE estimates
        int stepsBeforeUpdateCountACDE = 0;

        Scalar cThresholdACDE = 5.0;
        vector< Array<Scalar, Dynamic, Dynamic> > samplesACDE;
        Array<Scalar, Dynamic, Dynamic> samplesACDEMean;
        Scalar multiplierACDE = 1.0;
        function<Scalar(const Array<Scalar, Dynamic, Dynamic>&)> acfReductionACDE = [](const Array<Scalar, Dynamic, Dynamic> &acfValue) -> Scalar {
                return acfValue.mean();
            }; // default reduction is towards mean integrated autocorrelation time ("miact")

    protected:
        Matrix<Scalar, Dynamic, 1> biasV;
        Matrix<Scalar, Dynamic, 1> biasH;
        Matrix<Scalar, Dynamic, Dynamic> weights;

        Scalar lastLoss = NAN;

        virtual void calculateBatchAverages(Matrix<Scalar, Dynamic, 1> &avgV, Matrix<Scalar, Dynamic, 1> &avgH, Matrix<Scalar, Dynamic, Dynamic> &avgVH, const Array<Scalar, Dynamic, Dynamic> &batchV, const Array<Scalar, Dynamic, Dynamic> &batchH)
        {
            avgV = batchV.colwise().mean();
            avgH = batchH.colwise().mean();

            avgVH.resize(this->getNumVisible(), this->getNumHidden());
            for (int j = 0; j < this->getNumHidden(); ++j)
            {
                for (int i = 0; i < this->getNumVisible(); ++i)
                {
                    avgVH(i, j) = (batchV.col(i) * batchH.col(j)).mean();
                }
            }
        }

        /**
         * Calculate the conditional probabilities
         *     p[s, j] := P(H_j = 1 | V^(s))
         * for a set of samples V = { V^(s) } of the visible units.
         */
        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic, EigenOptions> probHGivenV(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &V) const
        {
            Array<Scalar, Dynamic, Dynamic, EigenOptions> sigmoidH = -V.matrix() * weights;
            sigmoidH.matrix().rowwise() -= biasH.transpose();
            sigmoidH = Array<Scalar, Dynamic, Dynamic, EigenOptions>::Ones(V.rows(), getNumHidden()) + sigmoidH.exp();
            sigmoidH = sigmoidH.inverse();
            return sigmoidH;
        }

        /**
         * Calculate the conditional probabilities
         *     p[s, j] := P(V_i = 1 | H^(s))
         * for a set of samples H = { H^(s) } of the hidden units.
         */
        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic, EigenOptions> probVGivenH(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &H) const
        {
            Array<Scalar, Dynamic, Dynamic, EigenOptions> sigmoidV = -H.matrix() * weights.transpose();
            sigmoidV.matrix().rowwise() -= biasV.transpose();
            sigmoidV = Array<Scalar, Dynamic, Dynamic, EigenOptions>::Ones(H.rows(), getNumVisible()) + sigmoidV.exp();
            sigmoidV = sigmoidV.inverse();
            return sigmoidV;
        }

        /**
         * Calculate the conditional probabilities
         *     p[k, i] = p(V_i = v[k]_i | H = h)
         * for all samples v in V and one specific H = h.
         */
        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic> probVGivenH(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &V, const long int h)
        {
            // cf. [220225]
            Array<Scalar, 1, Dynamic> sVec = bitwise_product(this->weights, h) + this->biasV;
            Array<Scalar, 1, Dynamic> rVec = sVec.exp();
            rVec += Array<Scalar, 1, Dynamic>::Ones(this->getNumVisible());
            Array<Scalar, Dynamic, Dynamic> unnormedLogProbs = V.rowwise() * sVec;
            return unnormedLogProbs.exp().rowwise() / rVec;
        }

    public:
        BinaryRBM(int numVisible, int numHidden, int cdOrder,
                  bool persistentCD = false,
                  Scalar stddevWeights = 1.0e-2,
                  Scalar stddevBiasV = 1.0e-1,
                  Scalar stddevBiasH = 1.0e-1)
        {
            biasV.resize(numVisible);
            biasH.resize(numHidden);
            weights.resize(numVisible, numHidden);

            initializeWeights(stddevWeights, stddevBiasV, stddevBiasH);

            defaultCDOrder = cdOrder;
            usePersistentCD = persistentCD;
        }

        BinaryRBM() : BinaryRBM(0, 0, 0) {}

        virtual ~BinaryRBM() {}

        void activateAdaptiveCDOrder(size_t numSamples, Scalar multiplier = 1.0, int minStepsBeforeUpdate = 1, Scalar cThreshold = 5.0, size_t minNumSamples = 1000)
        {
            useAdaptiveCDOrder = true;
            numSamplesACDE = numSamples;
            minNumSamplesACDE = minNumSamples;
            cThresholdACDE = cThreshold;
            multiplierACDE = multiplier;

            minStepsBeforeUpdateACDE = minStepsBeforeUpdate;
            stepsBeforeUpdateCountACDE = 0;

            samplesACDE.clear();
            samplesACDEMean = Array<Scalar, Dynamic, Dynamic>::Zero(mcSamplerStates.rows(), mcSamplerStates.cols());
        }

        template<typename EigenMatrixOrArray>
        static bool constructStates(EigenMatrixOrArray &dataset, const string &source, const string &rowSeparator = "\n")
        {
            vector<string> rowStrings = string_split(source, rowSeparator, true);
            if (rowStrings.size() <= 0 || rowStrings[0].length() <= 0)
            {
                dataset.resize(0, 0);
                return true;
            }
            else
            {
                dataset.resize(rowStrings.size(), rowStrings[0].length());
                for (long int row = 0; row < dataset.rows(); ++row)
                {
                    if ((long int) rowStrings[row].length() != dataset.cols())
                    {
                        STDLOG.printf_err("Failed to convert string to binary dataset: Row %ld has different number of columns than row 0.\n", row);
                        return false;
                    }

                    for (long int col = 0; col < dataset.cols(); ++col)
                    {
                        char ch = rowStrings[row][col];
                        if (ch == '0')
                        {
                            dataset(row, col) = 0;
                        }
                        else if (ch == '1')
                        {
                            dataset(row, col) = 1;
                        }
                        else
                        {
                            STDLOG.printf_err("Failed to convert string to binary dataset: Invalid character '%c' at row %ld, col %ld.\n", ch, row, col);
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        void deactivateAdaptiveCDOrder()
        {
            useAdaptiveCDOrder = false;
            isStoringSamplesACDE = false;
            samplesACDE.clear();
        }

        /**
         * Transform the weights and biases such that the new RBM
         * assigns probabilities p'(x) = p(flipped_x), where
         * flipped_x is obtained by swapping 0's and 1's in x,
         * e.g., 0010011 -> 1101100 .
         */
        virtual void flipVisibleUnits()
        {
            // cf. [211111]
            biasV = -biasV;
            biasH = biasH + weights.colwise().sum().transpose();
            weights = -weights;
        }

        virtual Scalar getAdaptiveCDOrderCThreshold() const
        {
            return cThresholdACDE;
        }

        virtual Scalar getAdaptiveCDOrderMultiplier() const
        {
            return multiplierACDE;
        }

        virtual size_t getAdaptiveCDOrderNumSamples() const
        {
            return numSamplesACDE;
        }

        virtual const Matrix<Scalar, Dynamic, 1> &getBiasHidden() const
        {
            return biasH;
        }

        virtual const Matrix<Scalar, Dynamic, 1> &getBiasVisible() const
        {
            return biasV;
        }

        virtual int getCDOrder() const
        {
            return defaultCDOrder;
        }

        /**
         * Return the current sample (state) of the Markov chain
         * Monte Carlo samplr.
         */
        virtual Array<Scalar, Dynamic, Dynamic> getCurrentSample()
        {
            return mcSamplerStates;
        }

        virtual Array<Scalar, Dynamic, Dynamic> getCurrentSampleHidden()
        {
            return mcSamplerStatesHidden;
        }

        virtual int getNumHidden() const
        {
            return biasH.size();
        }

        virtual int getNumVisible() const override
        {
            return biasV.size();
        }

        /**
         * Generate a binary RBM with the visible and hidden
         * units swapped.
         */
        virtual BinaryRBM<Scalar> getTransposedMachine() const
        {
            BinaryRBM<Scalar> transposedRBM(biasH.size(), biasV.size(), defaultCDOrder);
            transposedRBM.biasV = this->biasH;
            transposedRBM.biasH = this->biasV;
            transposedRBM.weights = this->weights.transpose();

            transposedRBM.useExactHiddenProbabilities = false;
            transposedRBM.mcSamplerNumChains = this->mcSamplerNumChains;

            return transposedRBM;
        }

        virtual const Matrix<Scalar, Dynamic, Dynamic> &getWeights() const
        {
            return weights;
        }

        using MonteCarloSampler< Array<Scalar, Dynamic, Dynamic> >::initialize;

        /**
         * Initialize the Markov chain Monte Carlo sampler
         * by activating all visible units of all chains
         *  with 50 % probability.
         */
        virtual void initialize() override
        {
            cout << "[rbm] initializing RBM Monte-Carlo sampler with " << mcSamplerNumChains << " chains" << endl;

            mcSamplerStates.resize(mcSamplerNumChains, getNumVisible());
            mcSamplerStatesHidden.resize(mcSamplerNumChains, getNumHidden());

            if (useAdaptiveCDOrder)
            {
                // reset estimator for adaptive CD order
                updateAdaptiveCDOrder();
            }

            for (int chain = 0; chain < mcSamplerNumChains; ++chain)
            {
                for (int i = 0; i < getNumVisible(); ++i)
                {
                    if (randd() < 0.5)
                    {
                        mcSamplerStates(chain, i) = 0;
                    }
                    else
                    {
                        mcSamplerStates(chain, i) = 1;
                    }
                }

                for (int j = 0; j < getNumHidden(); ++j)
                {
                    mcSamplerStatesHidden(chain, j) = NAN;
                }
            }
        }

        virtual void initializeMonteCarloSampler(size_t numChains = 1, size_t burnInSteps = 0)
        {
            this->mcSamplerNumChains = numChains;
            this->initialize(burnInSteps);
        }

        virtual void initializeWeights(Scalar stddevWeights = 1.0e-2, Scalar stddevBiasV = 1.0e-1, Scalar stddevBiasH = 1.0e-1)
        {
            for (int j = 0; j < getNumHidden(); ++j)
            {
                biasH(j) = rand_gauss() * stddevBiasH;

                for (int i = 0; i < getNumVisible(); ++i)
                {
                    if (j == 0)
                    {
                        biasV(i) = rand_gauss() * stddevBiasV;
                    }
                    weights(i, j) = rand_gauss() * stddevWeights;
                }
            }

        }

        virtual bool isMonteCarloSamplerInitialized() const
        {
            return (mcSamplerStates.rows() > 0);
        }

        virtual bool isUsingAdaptiveCDOrder() const
        {
            return this->useAdaptiveCDOrder;
        }

        virtual bool isUsingPersistentContrastiveDivergence() const
        {
            return this->usePersistentCD;
        }

        /**
         * Load an RBM with the given ID into the provided
         * reference from its weight
         * and bias files in the specified directory.
         * Returns 0 upon success and a nonzero error code
         * otherwise.
         */
        static int loadFromFile(BinaryRBM &rbm, const string &directory, const string &id, int contrastiveDivergenceOrder = 0, bool persistentCD = false, bool adaptiveCDOrder = false)
        {
            // weight matrix
            Matrix<Scalar, Dynamic, Dynamic> weights;
            if (DataIO::importMatrix(directory + "/" + id + "-weights.txt", weights, to_number<Scalar>) != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Failed to import weight matrix.\n");
                cout << endl; // error message sometimes skipped otherwise
                return -5; // no weights found
            }

            // visible biases
            Matrix<Scalar, Dynamic, 1> biasV;
            if (DataIO::importMatrix(directory + "/" + id + "-vbias.txt", biasV, to_number<Scalar>) != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Failed to import visible biases.\n");
                return -6; // no biases found
            }

            // hidden biases
            Matrix<Scalar, Dynamic, 1> biasH;
            if (DataIO::importMatrix(directory + "/" + id + "-hbias.txt", biasH, to_number<Scalar>) != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Failed to import hidden biases.\n");
                return -6; // no biases found
            }

            if (biasH.size() == weights.cols() && biasV.size() == weights.rows())
            {
                rbm = BinaryRBM(weights.rows(), weights.cols(), contrastiveDivergenceOrder);

                rbm.weights = weights;
                rbm.biasV = biasV;
                rbm.biasH = biasH;

                // cout << "WEIGHTS: " << rbm.weights << endl;
                // cout << "BIAS_V: " << rbm.biasV << endl;
                // cout << "BIAS_H: " << rbm.biasH << endl;
                rbm.usePersistentCD = persistentCD;
                if (persistentCD)
                {
                    // try to restore the sampler state
                    string source;
                    if (DataIO::importText(directory + "/" + id + "-vmcstate.txt", source) == DWAMF_DATAIO_SUCCESS)
                    {
                        if (BinaryRBM<Scalar>::constructStates(rbm.mcSamplerStates, source) && rbm.mcSamplerStates.cols() == rbm.getNumVisible())
                        {
                            rbm.mcSamplerNumChains = rbm.mcSamplerStates.rows();
                        }
                        else
                        {
                            STDLOG.printf_wrn("Found sampler state source file, but the data do not represent a valid sampler state.\n");
                        }
                    }
                    else
                    {
                        STDLOG.printf_wrn("Failed to import sampler state.\n");
                    }
                }

                if (adaptiveCDOrder)
                {
                    // use adaptive CD order
                    // restore state
                    Configuration params("", directory + "/" + id + "-acde.txt");
                    rbm.activateAdaptiveCDOrder(params.getValueSize("adaptive_cd_num_samples"), params.getValueReal("adaptive_cd_multiplier", 1.0), params.getValueInt("adaptive_cd_min_epoch_step", 1), params.getValueReal("adaptive_cd_iact_c", 5.0), params.getValueSize("adaptive_cd_min_num_samples", 1000));
                    rbm.defaultCDOrder = params.getValueInt("cd_order", 1);
                    rbm.stepsBeforeUpdateCountACDE = params.getValueInt("adaptive_cd_epoch_step_count", 0);
                }

                return 0; // import successful
            }
            else
            {
                STDLOG.printf_err("Dimensions of weight matrix and bias vectors do not match.\n");
                return -10; // invalid dimensions
            }
        }

        using MonteCarloSampler< Array<Scalar, Dynamic, Dynamic> >::next;

        /**
         * Advance the state of the Markov chain Monte Carlo sampler
         * by one step.
         */
        virtual void next() override
        {
            mcSamplerStatesHidden = randomHGivenV(mcSamplerStates);
            mcSamplerStates = randomVGivenH(mcSamplerStatesHidden);

            if (isStoringSamplesACDE)
            {
                // add current sample to collection
                samplesACDE.push_back(mcSamplerStates);

                samplesACDEMean += (mcSamplerStates - samplesACDEMean) / samplesACDE.size();

                if (samplesACDE.size() >= numSamplesACDE)
                {
                    // update default CD order
                    updateAdaptiveCDOrder();
                }
            }
        }

        virtual Scalar loss() const override
        {
            return lastLoss;
        }

        virtual Scalar loss(const Array<Scalar, Dynamic, Dynamic> &V) override
        {
            Array<Scalar, Dynamic, Dynamic> nextH = randomHGivenV(V);

            // calculate data averages
            Matrix<Scalar, Dynamic, 1> datAvgV;
            Matrix<Scalar, Dynamic, 1> datAvgH;
            Matrix<Scalar, Dynamic, Dynamic> datAvgVH;
            calculateBatchAverages(datAvgV, datAvgH, datAvgVH, V, nextH);

            // evaluate contribution to loss from data averages
            lastLoss = -(weights.transpose() * datAvgVH).trace();
            lastLoss -= biasV.transpose() * datAvgV;
            lastLoss -= biasH.transpose() * datAvgH;

            // estimate model averages
            Array<Scalar, Dynamic, Dynamic> nextV = V;
            randomCDSample(nextV, nextH, nextV, nextH, defaultCDOrder, false, false);

            Matrix<Scalar, Dynamic, 1> modAvgV;
            Matrix<Scalar, Dynamic, 1> modAvgH;
            Matrix<Scalar, Dynamic, Dynamic> modAvgVH;
            calculateBatchAverages(modAvgV, modAvgH, modAvgVH, nextV, nextH);

            // evaluate contribution to loss from model averages
            lastLoss += (weights.transpose() * modAvgVH).trace();
            lastLoss += biasV.transpose() * modAvgV;
            lastLoss += biasH.transpose() * modAvgH;

            return lastLoss;
        }

        void randomCDSample(Array<Scalar, Dynamic, Dynamic> &sampleV, Array<Scalar, Dynamic, Dynamic> &sampleH, const Array<Scalar, Dynamic, Dynamic> &initialV, const Array<Scalar, Dynamic, Dynamic> &initialH, int order, bool useFinalVisibleProbabilities = false, bool useFinalHiddenProbabilities = false)
        {
            sampleV = initialV;
            sampleH = initialH;
            for (int k = 0; k < order - 1; ++k)
            {
                sampleV = randomVGivenH(sampleH);
                sampleH = randomHGivenV(sampleV);
            }

            if (useFinalVisibleProbabilities)
            {
                sampleV = probVGivenH(sampleH);
            }
            else
            {
                sampleV = randomVGivenH(sampleH);
            }

            if (useFinalHiddenProbabilities)
            {
                sampleH = probHGivenV(sampleV);
            }
            else
            {
                sampleH = randomHGivenV(sampleV);
            }
        }

        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic, EigenOptions> randomHGivenV(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &V) const
        {
            Array<Scalar, Dynamic, Dynamic, EigenOptions> randH(V.rows(), getNumHidden());
            Array<Scalar, Dynamic, Dynamic, EigenOptions> probs = probHGivenV(V);

            const size_t numEntries = randH.size();
            Scalar *randHDataPtr = randH.data();
            Scalar *probsDataPtr = probs.data();
            for (size_t k = 0; k < numEntries; ++k)
            {
                if (randd() < probsDataPtr[k])
                {
                    randHDataPtr[k] = 1;
                }
                else
                {
                    randHDataPtr[k] = 0;
                }
            }

            return randH;
        }

        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic, EigenOptions> deprecated_randomHGivenV(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &V) const
        {
            Array<Scalar, Dynamic, Dynamic, EigenOptions> randH(V.rows(), getNumHidden());
            Array<Scalar, Dynamic, Dynamic, EigenOptions> probs = probHGivenV(V);
            for (int j = 0; j < getNumHidden(); ++j)
            {
                for (int s = 0; s < V.rows(); ++s)
                {
                    if (randd() < probs(s, j))
                    {
                        randH(s, j) = 1;
                    }
                    else
                    {
                        randH(s, j) = 0;
                    }
                }
            }

            return randH;
        }

        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic, EigenOptions> randomVGivenH(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &H) const
        {
            Array<Scalar, Dynamic, Dynamic, EigenOptions> randV(H.rows(), getNumVisible());
            Array<Scalar, Dynamic, Dynamic, EigenOptions> probs = probVGivenH(H);

            const size_t numEntries = randV.size();
            Scalar *randVDataPtr = randV.data();
            Scalar *probsDataPtr = probs.data();
            for (size_t k = 0; k < numEntries; ++k)
            {
                if (randd() < probsDataPtr[k])
                {
                    randVDataPtr[k] = 1;
                }
                else
                {
                    randVDataPtr[k] = 0;
                }
            }

            return randV;
        }

        template<int EigenOptions>
        Array<Scalar, Dynamic, Dynamic, EigenOptions> deprecated_randomVGivenH(const Array<Scalar, Dynamic, Dynamic, EigenOptions> &H) const
        {
            Array<Scalar, Dynamic, Dynamic, EigenOptions> randV(H.rows(), getNumVisible());
            Array<Scalar, Dynamic, Dynamic, EigenOptions> probs = probVGivenH(H);
            for (int i = 0; i < getNumVisible(); ++i)
            {
                for (int s = 0; s < H.rows(); ++s)
                {
                    if (randd() < probs(s, i))
                    {
                        randV(s, i) = 1;
                    }
                    else
                    {
                        randV(s, i) = 0;
                    }
                }
            }

            return randV;
        }

        virtual Array<Scalar, Dynamic, Dynamic> reconstruct(const Array<Scalar, Dynamic, Dynamic> &samples) const override
        {
            Array<Scalar, Dynamic, Dynamic> randH = randomHGivenV(samples);
            return randomVGivenH(randH);
        }

        virtual int saveToFile(const string &directory, const string &id) const
        {
            // weight matrix
            int r = DataIO::exportMatrix(directory + "/" + id + "-weights.txt", weights);
            if (r != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Failed to save weight matrix of binary RBM.\n");
                return r;
            }

            // visible bias
            r = DataIO::exportMatrix(directory + "/" + id + "-vbias.txt", biasV);
            if (r != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Failed to save visible biases of binary RBM.\n");
                return r;
            }

            // hidden bias
            r = DataIO::exportMatrix(directory + "/" + id + "-hbias.txt", biasH);
            if (r != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Failed to save hidden biases of binary RBM.\n");
                return r;
            }

            // sampler state
            if (isMonteCarloSamplerInitialized())
            {
                r = DataIO::exportText(directory + "/" + id + "-vmcstate.txt", BinaryRBM<Scalar>::statesToString(mcSamplerStates));
                if (r != DWAMF_DATAIO_SUCCESS)
                {
                    STDLOG.printf_err("Failed to save sampler state of the binary RBM.\n");
                    return r;
                }
            }

            // adaptive CD state
            if (useAdaptiveCDOrder)
            {
                Configuration params;
                params.setValue("cd_order", defaultCDOrder);
                params.setValue("adaptive_cd_num_samples", numSamplesACDE);
                params.setValue("adaptive_cd_min_num_samples", minNumSamplesACDE);
                params.setValue("adaptive_cd_iact_c", cThresholdACDE);
                params.setValue("adaptive_cd_multiplier", multiplierACDE);
                params.setValue("adaptive_cd_min_epoch_step", minStepsBeforeUpdateACDE);
                params.setValue("adaptive_cd_epoch_step_count", stepsBeforeUpdateCountACDE);

                if (! params.saveToFile(directory + "/" + id + "-acde.txt"))
                {
                    STDLOG.printf_err("Failed to save adaptive CD order estimator state of the binary RBM.\n");
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }
            }

            return DWAMF_DATAIO_SUCCESS;
        }

        virtual void setBiasHidden(const Matrix<Scalar, Dynamic, 1> &biasH)
        {
            this->biasH = biasH;
        }

        virtual void setBiasVisible(const Matrix<Scalar, Dynamic, 1> &biasV)
        {
            this->biasV = biasV;
        }

        virtual void setMonteCarloSamplerState(const Array<Scalar, Dynamic, Dynamic> &state)
        {
            if (mcSamplerStates.rows() > state.rows() || mcSamplerStates.cols() != state.cols())
            {
                STDLOG.printf_err("Invalid dimensions of provided state: expected at least %ld rows (chains) of exactly %ld entries (visible units).\n", mcSamplerStates.rows(), mcSamplerStates.cols());
                return;
            }

            // pick random subset of rows from the provided state
            // as sampler states
            vector<size_t> selectedRows = rand_subset(state.rows(), mcSamplerStates.rows());
            for (long int k = 0; k < mcSamplerStates.rows(); ++k)
            {
                mcSamplerStates.row(k) = state.row(selectedRows[k]);
            }

            // reset estimator for adaptive CD
            if (useAdaptiveCDOrder)
            {
                updateAdaptiveCDOrder();
            }
        }

        virtual void setWeights(const Matrix<Scalar, Dynamic, Dynamic> &weights)
        {
            this->weights = weights;
        }

        /**
         * Obtain a string representation of the provided dataset.
         */
        template<typename Derived>
        static string statesToString(const DenseBase<Derived> &dataset, const string &stateSeparator = "\n", const string &unitSeparator = "")
        {
            if (dataset.rows() <= 0 || dataset.cols() <= 0)
            {
                return "";
            }

            string str = "";
            for (long int row = 0; row < dataset.rows(); ++row)
            {
                string rowStr = UNIT_TO_STRING(dataset(row, 0));
                for (long int col = 1; col < dataset.cols(); ++col)
                {
                    rowStr += unitSeparator + UNIT_TO_STRING(dataset(row, col));
                }

                if (row > 0)
                {
                    str += stateSeparator;
                }
                str += rowStr;
            }

            return str;
        }

        virtual int trainingStepCD(const Array<Scalar, Dynamic, Dynamic> &batchV, Scalar learningRate, int cdOrder, Scalar weightDecay, bool monitorProgress = true)
        {
            const int batchSize = batchV.rows();
            Scalar learningRatePerSample = learningRate / batchSize;

            // cout << "CD training step with batch of size " << batchSize << endl;

            if (useAdaptiveCDOrder)
            {
                if ((++stepsBeforeUpdateCountACDE) >= minStepsBeforeUpdateACDE)
                {
                    // activate storage of Markov-chain samples
                    isStoringSamplesACDE = true;
                    // an update of the CD order will be triggered
                    // once enough samples have been collected
                    // cout << "[---] collecting samples for adaptive CD order estimates" << endl;
                }
                else
                {
                    // cout << "[---] adaptive CD order is turned on, but skipping sample collection for this training step" << endl;
                }
            }

            // evaluate data averages
            Array<Scalar, Dynamic, Dynamic> nextH;
            if (useExactHiddenProbabilities)
            {
                nextH = this->probHGivenV(batchV);
            }
            else
            {
                nextH = randomHGivenV(batchV);
            }

            Matrix<Scalar, Dynamic, 1> datAvgV;
            Matrix<Scalar, Dynamic, 1> datAvgH;
            Matrix<Scalar, Dynamic, Dynamic> datAvgVH;
            calculateBatchAverages(datAvgV, datAvgH, datAvgVH, batchV, nextH);

            // evaluate contribution to loss from data averages
            lastLoss = -(weights.transpose() * datAvgVH).trace();
            lastLoss -= biasV.transpose() * datAvgV;
            lastLoss -= biasH.transpose() * datAvgH;

            // estimate model averages
            Array<Scalar, Dynamic, Dynamic> nextV;
            if (usePersistentCD)
            {
                // using persistent contrastive divergence
                if (! isMonteCarloSamplerInitialized())
                {
                    initializeMonteCarloSampler(batchSize);
                }

                if (mcSamplerNumChains != batchSize)
                {
                    STDLOG.printf_inf("The number of chains of the Monte-Carlo sampler (%d) does not match the mini-batch size (%d). Will adjust the number of chains accordingly.\n", mcSamplerNumChains, batchSize);

                    // prune or re-sample from existing chains
                    // to adjust state
                    if (batchSize < mcSamplerNumChains)
                    {
                        mcSamplerStates = mcSamplerStates.topRows(batchSize);
                    }
                    else
                    {
                        Array<Scalar, Dynamic, Dynamic> newSamplerStates(batchSize, getNumVisible());
                        int oldChainIndex = 0;
                        for (int newChainIndex = 0; newChainIndex < batchSize; ++newChainIndex)
                        {
                            if (oldChainIndex >= mcSamplerNumChains)
                            {
                                next(cdOrder);
                                oldChainIndex = 0;
                            }

                            newSamplerStates.row(newChainIndex) = mcSamplerStates.row(oldChainIndex);
                            ++oldChainIndex;
                        }

                        mcSamplerStates = newSamplerStates;
                    }
                    mcSamplerNumChains = batchSize;
                }

                // advance Monte-Carlo sampler for cdOrder steps
                next(cdOrder);

                // extract model samples
                nextV = mcSamplerStates;
                if (useExactHiddenProbabilities)
                {
                    nextH = this->probHGivenV(nextV);
                }
                else
                {
                    nextH = randomHGivenV(nextV);
                }
            }
            else
            {
                // using (ordinary/vanilla) contrastive divergence
                if (useExactHiddenProbabilities)
                {
                    // used probabilities before,
                    // generate samples now
                    nextH = randomHGivenV(batchV);
                }

                randomCDSample(nextV, nextH, batchV, nextH, cdOrder, false, useExactHiddenProbabilities);
            }

            Matrix<Scalar, Dynamic, 1> modAvgV;
            Matrix<Scalar, Dynamic, 1> modAvgH;
            Matrix<Scalar, Dynamic, Dynamic> modAvgVH;
            calculateBatchAverages(modAvgV, modAvgH, modAvgVH, nextV, nextH);

            // evaluate contribution to loss from model averages
            lastLoss += (weights.transpose() * modAvgVH).trace();
            lastLoss += biasV.transpose() * modAvgV;
            lastLoss += biasH.transpose() * modAvgH;

            // update weights and biases
            if (weightDecay > 0.0)
            {
                Scalar gamma = (Scalar) (1.0 - weightDecay);
                weights *= gamma;
                biasV *= gamma;
                biasH *= gamma;
            }
            weights += learningRatePerSample * (datAvgVH - modAvgVH);
            biasV += learningRatePerSample * (datAvgV - modAvgV);
            biasH += learningRatePerSample * (datAvgH - modAvgH);

            return 0; // no errors
        }

        virtual int trainingStep(const Array<Scalar, Dynamic, Dynamic> &batchV, Scalar learningRate = 0.1, Scalar weightDecay = 0.0, bool monitorProgress = true) override
        {
            return trainingStepCD(batchV, learningRate, defaultCDOrder, weightDecay, monitorProgress);
        }

        /**
         * Estimate the integrated autocorrelation time
         * of the present model from the acquired Markov-chain
         * samples and update the default CD order
         * accordingly.
         */
        void updateAdaptiveCDOrder()
        {
            if (samplesACDE.size() > 0)
            {
                cout << "[rbm] estimate integrated autocorrelation time from " << samplesACDE.size() << " samples" << endl;

                Scalar estimatedACT = MonteCarloSampler< Array<Scalar, Dynamic, Dynamic> >::estimateIntegratedAutocorrelationTime(
                        samplesACDE,
                        samplesACDEMean,
                        acfReductionACDE,
                        samplesACDE.size() - minNumSamplesACDE,
                        cThresholdACDE
                    );

                cout << "      correlation-time estimate: " << estimatedACT << endl;
                if (ISNAN(estimatedACT))
                {
                    STDLOG.printf_wrn("Default CD order will not be updated because correlation-time estimated evaluated to NAN.\n");
                }
                else
                {
                    defaultCDOrder = ROUND(estimatedACT * multiplierACDE);
                    if (defaultCDOrder < 1)
                    {
                        defaultCDOrder = 1;
                    }

                    cout << "      new CD order: " << defaultCDOrder << endl;
                }

                // deactivate storing of samples for the rest of the epoch
                isStoringSamplesACDE = false;
                stepsBeforeUpdateCountACDE = 0;
            }

            // samples and sample mean have been modified;
            // will be discarded
            samplesACDE.clear();
            samplesACDEMean = Array<Scalar, Dynamic, Dynamic>::Zero(mcSamplerStates.rows(), mcSamplerStates.cols());
        }
};

#endif
