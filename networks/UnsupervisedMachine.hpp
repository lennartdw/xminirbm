/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Abstract framework for unsupervised network architectures.
 */

#ifndef _XMINIRBM_NETWORKS_UNSUPERVISEDMACHINE
#define _XMINIRBM_NETWORKS_UNSUPERVISEDMACHINE

template<typename Scalar>
class UnsupervisedMachine
{
    public:
        /**
         * Obtain the number of visible units,
         * i.e., the dimension of the input space.
         */
        virtual int getNumVisible() const = 0;

        /**
         * Obtain the last estimate of the loss function.
         */
        virtual Scalar loss() const = 0;

        /**
         * Estimate the loss from the provided samples.
         */
        virtual Scalar loss(const Array<Scalar, Dynamic, Dynamic> &batch) = 0;

        /**
         * Calculate first-order reconstructions of the
         * provided samples. Usually, this means that
         * each sample is mapped (possibly stochastically)
         * to a hidden representation (encoding step),
         * from which a new sample in the visible space
         * is generated in turn (decoding step).
         */
        virtual Array<Scalar, Dynamic, Dynamic> reconstruct(const Array<Scalar, Dynamic, Dynamic> &samples) const = 0;

        /**
         * Perform a training step using the provided minibatch
         * and learning rate.
         */
        virtual int trainingStep(const Array<Scalar, Dynamic, Dynamic> &batch, Scalar learningRate, Scalar weightDecay = 0.0, bool monitorProgress = true) = 0;
};

#endif
