/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Network model: RBM with binary variables and exact
 *     training (computing the full partition function).
 */

#ifndef _XMINIRBM_NETWORKS_EXACTBINARYRBM
#define _XMINIRBM_NETWORKS_EXACTBINARYRBM

#include "data/DataIO.hpp"
#include "util/bitwise.hpp"

#include "../util/headers.hpp"
#include "BinaryRBM.hpp"
#include "../analysis/DistributionAnalyzer.hpp"

#pragma omp declare reduction (+: Matrix<float, Dynamic, 1>: omp_out += omp_in) initializer(omp_priv = Matrix<float, Dynamic, 1>::Zero(omp_orig.size()))
#pragma omp declare reduction (+: Array<float, Dynamic, 1>: omp_out += omp_in) initializer(omp_priv = Array<float, Dynamic, 1>::Zero(omp_orig.size()))
#pragma omp declare reduction (+: Matrix<float, 1, Dynamic>: omp_out += omp_in) initializer(omp_priv = Matrix<float, 1, Dynamic>::Zero(omp_orig.size()))
#pragma omp declare reduction (+: Matrix<float, Dynamic, Dynamic>: omp_out += omp_in) initializer(omp_priv = Matrix<float, Dynamic, Dynamic>::Zero(omp_orig.rows(), omp_orig.cols()))

#pragma omp declare reduction (+: Matrix<double, Dynamic, 1>: omp_out += omp_in) initializer(omp_priv = Matrix<double, Dynamic, 1>::Zero(omp_orig.size()))
#pragma omp declare reduction (+: Array<double, Dynamic, 1>: omp_out += omp_in) initializer(omp_priv = Array<double, Dynamic, 1>::Zero(omp_orig.size()))
#pragma omp declare reduction (+: Matrix<double, 1, Dynamic>: omp_out += omp_in) initializer(omp_priv = Matrix<double, 1, Dynamic>::Zero(omp_orig.size()))
#pragma omp declare reduction (+: Matrix<double, Dynamic, Dynamic>: omp_out += omp_in) initializer(omp_priv = Matrix<double, Dynamic, Dynamic>::Zero(omp_orig.rows(), omp_orig.cols()))

template<typename Scalar>
class ExactBinaryRBM : public BinaryRBM<Scalar>
{
    private:
        Scalar partitionFunction = NAN; // updated with every training step
    // NOTE:
    // important protected variables from inherited from Binary RBM:
    // Matrix<Scalar, Dynamic, 1> biasV;
    // Matrix<Scalar, Dynamic, 1> biasH;
    // Matrix<Scalar, Dynamic, Dynamic> weights;

    public:
        ExactBinaryRBM(int numVisible, int numHidden,
                       Scalar stddevWeights = 1.0e-2,
                       Scalar stddevBiasV = 1.0e-1,
                       Scalar stddevBiasH = 1.0e-1) : BinaryRBM<Scalar>(numVisible, numHidden, 0, false, stddevWeights, stddevBiasV, stddevBiasH)
        {
        }

        ExactBinaryRBM(const BinaryRBM<Scalar> &baseRBM) : ExactBinaryRBM(baseRBM.getNumVisible(), baseRBM.getNumHidden())
        {
            this->weights = baseRBM.getWeights();
            this->biasV = baseRBM.getBiasVisible();
            this->biasH = baseRBM.getBiasHidden();
        }

        ExactBinaryRBM() : ExactBinaryRBM(0, 0) {}

        virtual ~ExactBinaryRBM() {}

        /**
         * Analyze the distribution of the visible units
         * using the provided distribution analyzers.
         * The full state distribution will also be stored
         * in the specified array 'stateDistribution' if the
         * pointer is valid (not NULL).
         */
        void analyzeDistributionVisible(const vector<DistributionAnalyzer<Scalar>*> &analyzers, Matrix<Scalar, Dynamic, 1> *stateDistribution = NULL, bool monitorProgress = true)
        {
            const long int numStatesV = ((long int) 1) << this->getNumVisible();
            partitionFunction = 0.0;
            if (stateDistribution != NULL)
            {
                stateDistribution->resize(numStatesV);
            }

            #ifndef _OPENMP
            ProgressMonitor monitor;
            if (monitorProgress) monitor.start();
            long int progressUpdateSteps = MIN(numStatesV / 10000, 50000);
            long int progressUpdateCounter = 0;
            #endif

            #pragma omp parallel for schedule(guided)
            for (long int v = 0; v < numStatesV; ++v)
            {
                #ifndef _OPENMP
                if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
                {
                    monitor.update((real_t) v / numStatesV);
                    progressUpdateCounter = 0;
                }
                #endif

                Scalar expAV = EXP(bitwise_masked_sum(this->biasV, v));
                Array<Scalar, 1, Dynamic> rVec =  bitwise_product(v, this->weights) + this->biasH.transpose();
                rVec = rVec.exp();
                rVec += Array<Scalar, 1, Dynamic>::Ones(this->getNumHidden());
                Scalar pprob = expAV * rVec.prod();

                Array<Scalar, 1, Dynamic> sample(this->getNumVisible());
                sample = integer_digits(sample, v, 2);

                #pragma omp critical(PROCESS_ANALYZERS)
                {
                    for (auto &analyzer : analyzers)
                    {
                        analyzer->processSample(sample, pprob);
                    }

                    partitionFunction += pprob;
                    if (stateDistribution != NULL)
                    {
                        (*stateDistribution)(v) = pprob;
                    }
                }
            }

            #ifndef _OPENMP
            if (monitorProgress) monitor.end();
            #endif

            // normalize all distributions
            if (stateDistribution != NULL)
            {
                *stateDistribution /= partitionFunction;
            }

            for (auto &analyzer : analyzers)
            {
                analyzer->normalize();
            }

            // cout << "Z = " << partitionFunction << endl;
            //
        }

        /**
         * Similar to the above analyzeDistributionVisible
         * method, but restricting the analysis to the visible
         * states present in the reference distribution,
         * at the expense of requiring computation of the
         * partition function independently in addition.
         * Implementation for states as binary digit sequences (integers).
         *
         * NOTE: This method does not make use of the actual probabilities
         *     of the reference distribution; only the list of states
         *     is used. The reference probabilities themselves are to be
         *     included in the distribution analyzers if needed.
         */
        void analyzeDistributionVisible(const vector<DistributionAnalyzer<Scalar>*> &analyzers, const unordered_map<unsigned long int, Scalar> &refStateDist, Matrix<Scalar, Dynamic, 1> *stateDistribution = NULL, bool monitorProgress = true)
        {
            if (stateDistribution != NULL)
            {
                stateDistribution->resize(refStateDist.size());
            }

            ProgressMonitor monitor;
            if (monitorProgress) monitor.start();
            long int progressUpdateSteps = MIN(refStateDist.size() / 10000, 50000);
            long int progressUpdateCounter = 0;
            long int totalStateCounter = 0;

            for (const auto &refState : refStateDist)
            {
                if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
                {
                    monitor.update((real_t) totalStateCounter / refStateDist.size());
                    progressUpdateCounter = 0;
                }

                const unsigned long int v = refState.first;

                Scalar expAV = EXP(bitwise_masked_sum(this->biasV, v));
                Array<Scalar, 1, Dynamic> rVec =  bitwise_product(v, this->weights) + this->biasH.transpose();
                rVec = rVec.exp();
                rVec += Array<Scalar, 1, Dynamic>::Ones(this->getNumHidden());
                Scalar pprob = expAV * rVec.prod();

                if (stateDistribution != NULL)
                {
                    (*stateDistribution)(v) = pprob;
                }

                Array<Scalar, 1, Dynamic> sample(this->getNumVisible());
                sample = integer_digits(sample, v, 2);

                for (auto &analyzer : analyzers)
                {
                    analyzer->processSample(sample, pprob);
                }

                ++totalStateCounter;
            }

            if (monitorProgress) monitor.end();

            if (ISNAN(partitionFunction))
            {
                updatePartitionFunction();
            }

            // normalize all distributions
            if (stateDistribution != NULL)
            {
                *stateDistribution /= partitionFunction;
            }

            for (auto &analyzer : analyzers)
            {
                analyzer->normalize(partitionFunction);
            }

            // cout << "Z = " << partitionFunction << endl;
        }

        /**
         * Similar to the above analyzeDistributionVisible
         * method, but restricting the analysis to the visible
         * states present in the reference distribution,
         * at the expense of requiring computation of the
         * partition function independently in addition.
         * Implementation for states as strings.
         *
         * NOTE: This method does not make use of the actual probabilities
         *     of the reference distribution; only the list of states
         *     is used. The reference probabilities themselves are to be
         *     included in the distribution analyzers if needed.
         */
        bool analyzeDistributionVisible(const vector<DistributionAnalyzer<Scalar>*> &analyzers, const unordered_map<string, Scalar> &refStateDist, bool monitorProgress = true)
        {
            cout << "        cycle samples" << endl;

            ProgressMonitor monitor;
            if (monitorProgress) monitor.start();
            long int progressUpdateSteps = MIN(refStateDist.size() / 10000, 50000);
            long int progressUpdateCounter = 0;
            long int totalStateCounter = 0;

            for (const auto &refStateEntry : refStateDist)
            {
                if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
                {
                    monitor.update((real_t) totalStateCounter / refStateDist.size());
                    progressUpdateCounter = 0;
                }

                Array<Scalar, 1, Dynamic> sample;
                if (BinaryRBM<Scalar>::constructStates(sample, refStateEntry.first))
                {
                    Scalar expAV = EXP((sample.matrix() * this->biasV)(0,0));
                    Array<Scalar, 1, Dynamic> rVec =  sample.matrix() * this->weights + this->biasH.transpose();
                    rVec = rVec.exp();
                    rVec += Array<Scalar, 1, Dynamic>::Ones(this->getNumHidden());
                    Scalar pprob = expAV * rVec.prod();

                    for (auto &analyzer : analyzers)
                    {
                        analyzer->processSample(sample, pprob);
                    }

                    ++totalStateCounter;
                }
                else
                {
                    STDLOG.printf_err("The state key '%s' could not be translated into a valid reference state. Distribution analysis will be aborted.\n", refStateEntry.first.c_str());
                    return false;
                }
            }

            if (monitorProgress) monitor.end();

            if (ISNAN(partitionFunction))
            {
                cout << "        update partition function" << endl;
                updatePartitionFunction();
            }

            // normalize analyzers
            for (auto &analyzer : analyzers)
            {
                analyzer->normalize(partitionFunction);
            }

            return true;
        }

        /**
         * Analyze the distribution of the visible units for the
         * states in the provided reference distribution
         * utilizing the given distribution of the hidden units.
         *
         * NOTE: This method does not make use of the actual probabilities
         *     of the reference distribution; only the list of states
         *     is used. The reference probabilities themselves are to be
         *     included in the distribution analyzers if needed.
         */
        bool analyzeDistributionVisibleFromHidden(const vector<DistributionAnalyzer<Scalar>*> &analyzers, const unordered_map<string, real_t> &refStateDist, const vector<real_t> &hiddenStateDist, bool monitorProgress = true)
        {
            // construct all reference states
            Array<Scalar, Dynamic, Dynamic, RowMajor> refStates(refStateDist.size(), this->getNumVisible());
            size_t refStateCounter = 0;
            for (auto &refStateEntry : refStateDist)
            {
                Array<Scalar, 1, Dynamic> state;
                if (BinaryRBM<Scalar>::constructStates(state, refStateEntry.first))
                {
                    if (state.rows() == 1 && state.cols() == this->getNumVisible())
                    {
                        refStates.row(refStateCounter) = state.row(0);
                        ++refStateCounter;
                    }
                    else
                    {
                        STDLOG.printf_err("The dimensions of the reference state generated from the state key '%s' (%ldx%ld) do not match the expected dimensions(1x%ld). Distribution analysis will be aborted.\n", refStateEntry.first.c_str(), state.rows(), state.cols(), this->getNumVisible());
                        return false;
                    }
                }
                else
                {
                    STDLOG.printf_err("The state key '%s' could not be translated into a valid reference state. Distribution analysis will be aborted.\n", refStateEntry.first.c_str());
                    return false;
                }
            }
            // cout << "constructed reference states" << endl;

            // cycle hidden states
            // to calculate probabilities of all reference states
            Array<Scalar, Dynamic, 1> refStateModelProbs = Array<Scalar, Dynamic, 1>::Zero(refStates.rows());
            const long int numStatesH = ((long int) 1) << this->getNumHidden();

            #ifndef _OPENMP
            ProgressMonitor monitor;
            if (monitorProgress) monitor.start();
            long int progressUpdateSteps = MIN(numStatesH / 10000, 50000);
            long int progressUpdateCounter = 0;
            #endif

            #pragma omp parallel for reduction(+:refStateModelProbs) schedule(guided)
            for (long int h = 0; h < numStatesH; ++h)
            {
                #ifndef _OPENMP
                if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
                {
                    monitor.update((real_t) h / numStatesH);
                    progressUpdateCounter = 0;
                }
                #endif

                Array<Scalar, Dynamic, 1> refStateProbsGivenH = this->probVGivenH(refStates, h).rowwise().prod();
                refStateModelProbs += refStateProbsGivenH * hiddenStateDist[h];
            }

            #ifndef _OPENMP
            if (monitorProgress) monitor.end();
            #endif
            // cout << "cycled hidden states" << endl;

            // run analyzers on reference states
            for (size_t refStateIndex = 0; refStateIndex < refStateDist.size(); ++refStateIndex)
            {
                for (auto &analyzer : analyzers)
                {
                    analyzer->processSample(refStates.row(refStateIndex), refStateModelProbs(refStateIndex));
                }
            }
            // cout << "processed samples through analyzers" << endl;

            // NOTE: model probabilities are already normalized,
            // hence normalization correction for the analyzers
            // must be disabled:
            for (auto &analyzer : analyzers)
            {
                analyzer->normalize(1.0);
            }

            return true;
        }

        Scalar getPartitionFunction() const
        {
            if (ISNAN(partitionFunction))
            {
                updatePartitionFunction();
            }
            return partitionFunction;
        }

        static int loadFromFile(ExactBinaryRBM &rbm, const string &directory, const string &id)
        {
            // load base RBM
            int r = BinaryRBM<Scalar>::loadFromFile(rbm, directory, id);
            if (r)
            {
                // an error occured
                return r;
            }

            // load meta data (if available)
            loadMetaDataFromFile(rbm, directory, id);

            return 0; // successful
        }

        static void loadMetaDataFromFile(ExactBinaryRBM &rbm, const string &directory, const string &id)
        {
            // load parameters from meta data file
            string metaFileName = directory + "/" + id + "-meta.txt";
            if (file_exists(metaFileName))
            {
                cout << "[x2rbm] scanning for meta data in file '" << metaFileName << "'" << endl;
                Configuration params(id.c_str(), metaFileName);
                rbm.partitionFunction = params.getValueReal("partition-function", NAN);
                if (! ISNAN(rbm.partitionFunction))
                {
                    cout << "        retrieved partition function: " << rbm.partitionFunction << endl;
                }
            }
            else
            {
                STDLOG.printf_wrn("No meta-data file found for machine '%s' in directory '%s'.\n", id.c_str(), directory.c_str());
            }
        }

        virtual Scalar loss(const Array<Scalar, Dynamic, Dynamic> &V) override
        {
            // evaluate loss contribution from (Einstein)
            //     -a_i x_i
            Matrix<Scalar, Dynamic, 1> batchVA = V.matrix() * this->biasV;
            this->lastLoss = -batchVA.mean();


            // evaluate loss contribution from (Einstein)
            //     -sum_j log[ 1 + exp(x_i w_ij + b_j) ]
            Array<Scalar, Dynamic, Dynamic> sumHExpData = V.matrix() * this->weights + this->biasH.transpose().replicate(V.rows(), 1);
            sumHExpData = sumHExpData.exp();
            sumHExpData += Array<Scalar, Dynamic, Dynamic>::Ones(V.rows(), this->getNumHidden());
            sumHExpData = sumHExpData.log();
            this->lastLoss -= sumHExpData.colwise().mean().sum();

            // contribution to loss from term
            //     log Z
            if (ISNAN(partitionFunction))
            {
                // update
                updatePartitionFunction();
            }
            this->lastLoss += LOG(partitionFunction);

            return this->lastLoss;
        }

        virtual int saveToFile(const string &directory, const string &id) const override
        {
            // save base machine
            int r = BinaryRBM<Scalar>::saveToFile(directory, id);
            if (r != DWAMF_DATAIO_SUCCESS)
            {
                // an error occured
                return r;
            }

            // save meta data
            if (saveMetaDataToFile(directory, id) != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_wrn("Failed to export meta data, but basic machine configuration has been saved.\n");
            }

            return r;
        }

        int saveMetaDataToFile(const string &directory, const string &id) const
        {
            cout << "[x2rbm] export meta data" << endl;
            Configuration params;
            if (! ISNAN(this->partitionFunction))
            {
                params.setValue("partition-function", this->partitionFunction);
            }

            if (params.size() > 0)
            {
                string metaFileName = directory + "/" + id + "-meta.txt";
                if (params.saveToFile(metaFileName))
                {
                    cout << "        saved " << params.size() << " meta parameters to file '" << metaFileName << "'" << endl;
                    return DWAMF_DATAIO_SUCCESS;
                }
                else
                {
                    STDLOG.printf_err("Could not save %ld meta parameters to file '%s'.\n", params.size(), metaFileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }
            }
            else
            {
                cout << "        no meta data available" << endl;
                return DWAMF_DATAIO_SUCCESS;
            }
        }

        virtual int trainingStep(const Array<Scalar, Dynamic, Dynamic> &batchV, Scalar learningRate = 0.1, bool monitorProgress = true)
        {
            const int batchSize = batchV.rows();
            Scalar learningRatePerSample = learningRate / batchSize;

            // cout << "batch size: " << batchSize << endl;
            // cout << "BATCH: " << batchV << endl;

            // cout << "WEIGHTS: " << weights << endl;
            // cout << "BIAS_V: " << biasV << endl;
            // cout << "BIAS_H: " << biasH << endl;

            // evaluate data averages
            Matrix<Scalar, Dynamic, 1> datAvgV = batchV.colwise().mean();

            Array<Scalar, Dynamic, Dynamic> sigmoidH = this->probHGivenV(batchV);

            Matrix<Scalar, Dynamic, 1> datAvgH = sigmoidH.colwise().mean();

            Matrix<Scalar, Dynamic, Dynamic> datAvgVH(this->getNumVisible(), this->getNumHidden());
            for (int j = 0; j < this->getNumHidden(); ++j)
            {
                for (int i = 0; i < this->getNumVisible(); ++i)
                {
                    datAvgVH(i, j) = (batchV.col(i) * sigmoidH.col(j)).mean();
                }
            }
            // cout << "datAvgV = " << datAvgV << endl;
            // cout << "datAvgH = " << datAvgH << endl;
            // cout << "datAvgVH = " << datAvgVH << endl;

            // evaluate contribution to loss from (Einstein)
            //     -a_i x_i
            Matrix<Scalar, Dynamic, 1> batchVA = batchV.matrix() * this->biasV;
            this->lastLoss = -batchVA.mean();
            // cout << "-<a_i x_i> = " << lastLoss << endl;

            // evaluate model averages
            partitionFunction = 0.0;
            Matrix<Scalar, Dynamic, Dynamic> numVH = Matrix<Scalar, Dynamic, Dynamic>::Zero(this->getNumVisible(), this->getNumHidden());
            Matrix<Scalar, 1, Dynamic> numV = Matrix<Scalar, 1, Dynamic>::Zero(this->getNumVisible());
            Matrix<Scalar, Dynamic, 1> numH = Matrix<Scalar, Dynamic, 1>::Zero(this->getNumHidden());

            const long int numStatesV = ((long int) 1) << this->getNumVisible();
            const long int numStatesH = ((long int) 1) << this->getNumHidden();

            #ifndef _OPENMP
            ProgressMonitor monitor;
            if (monitorProgress) monitor.start();
            long int progressUpdateSteps = numStatesV / 10000;
            long int progressUpdateCounter = 0;
            #endif

            #pragma omp parallel for reduction(+:partitionFunction, numVH, numV, numH) schedule(static)
            for (long int v = 0; v < numStatesV; ++v)
            {
                #ifndef _OPENMP
                if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
                {
                    monitor.update((real_t) v / numStatesV);
                    progressUpdateCounter = 0;
                }
                #endif

                Scalar expAV = EXP(bitwise_masked_sum(this->biasV, v));

                // contribution to V numerator
                Array<Scalar, 1, Dynamic> sVec = bitwise_product(v, this->weights) + this->biasH.transpose();
                sVec = sVec.exp();
                Array<Scalar, 1, Dynamic> rVec = sVec + Array<Scalar, 1, Dynamic>::Ones(this->getNumHidden());
                Scalar rVecProd = rVec.prod();
                bitwise_masked_add(numV, expAV * rVecProd, v);

                // contribution to VH numerator
                for (int l = 0; l < this->getNumHidden(); ++l)
                {
                    Matrix<Scalar, Dynamic, 1> colAdd = Matrix<Scalar, Dynamic, 1>::Zero(this->getNumVisible());
                    bitwise_masked_add(colAdd, expAV * sVec(l) * rVecProd / rVec(l), v);
                    numVH.col(l) += colAdd;
                }

                // contribution to partition function
                partitionFunction += expAV * rVecProd;
            }

            #pragma omp parallel for reduction(+:numH) schedule(static)
            for (long int h = 0; h < numStatesH; ++h)
            {
                Scalar expBH = EXP(bitwise_masked_sum(this->biasH, h));

                Matrix<Scalar, Dynamic, 1> prodWH = bitwise_product(this->weights, h);
                Array<Scalar, Dynamic, 1> cVec = prodWH + this->biasV;
                cVec = cVec.exp();
                cVec += Array<Scalar, Dynamic, 1>::Ones(this->getNumVisible());
                bitwise_masked_add(numH, expBH * cVec.prod(), h);
            }

            // cout << "numV = " << numV << endl;
            // cout << "numH = " << numH << endl;
            // cout << "numVH = " << numVH << endl;
            // cout << "Z = " << partitionFunction << endl;

            #ifndef _OPENMP
            if (monitorProgress) monitor.end();
            #endif

            // contribution to loss from term (Einstein)
            //     -log sum_h exp(w_ij x_i h_j + b_j h_j)
            // (data average)
            // cf. [210625]
            Array<Scalar, Dynamic, Dynamic> sumHExpData = batchV.matrix() * this->weights + this->biasH.transpose().replicate(batchV.rows(), 1);
            sumHExpData = sumHExpData.exp();
            sumHExpData += Array<Scalar, Dynamic, Dynamic>::Ones(batchV.rows(), this->getNumHidden());
            sumHExpData = sumHExpData.log();
            this->lastLoss -= sumHExpData.colwise().mean().sum();

            // contribution to loss from term
            //     log Z
            this->lastLoss += LOG(partitionFunction);

            // update weights
            this->weights += learningRatePerSample * (datAvgVH - numVH / partitionFunction);
            this->biasV += learningRatePerSample * (datAvgV - numV.transpose() / partitionFunction);
            this->biasH += learningRatePerSample * (datAvgH - numH / partitionFunction);

            return 0; // no errors
        }

        virtual int updatePartitionFunction()
        {
            partitionFunction = 0.0;

            if (this->getNumVisible() <= this->getNumHidden())
            {
                // cycle visible states
                cout  << "#visible <= #hidden" << endl;

                const long int numStatesV = ((long int) 1) << this->getNumVisible();
                // const long int numStatesH = ((long int) 1) << getNumHidden();

                #pragma omp parallel for reduction(+:partitionFunction) schedule(static)
                for (long int v = 0; v < numStatesV; ++v)
                {
                    Scalar expAV = EXP(bitwise_masked_sum(this->biasV, v));
                    Array<Scalar, 1, Dynamic> rVec =  bitwise_product(v, this->weights) + this->biasH.transpose();
                    rVec = rVec.exp();
                    rVec += Array<Scalar, 1, Dynamic>::Ones(this->getNumHidden());
                    partitionFunction += expAV * rVec.prod();
                }
            }
            else
            {
                // cycle hidden states
                cout  << "#visible > #hidden" << endl;

                const long int numStatesH = ((long int) 1) << this->getNumHidden();

                // #ifndef _OPENMP
                // ProgressMonitor monitor;
                // if (monitorProgress) monitor.start();
                // long int progressUpdateSteps = numStatesH / 10000;
                // long int progressUpdateCounter = 0;
                // #endif

                #pragma omp parallel for reduction(+:partitionFunction) schedule(static)
                for (long int h = 0; h < numStatesH; ++h)
                {
                    // #ifndef _OPENMP
                    // if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
                    // {
                    //     monitor.update((real_t) h / numStatesH);
                    //     progressUpdateCounter = 0;
                    // }
                    // #endif

                    Scalar expBH = EXP(bitwise_masked_sum(this->biasH, h));
                    Array<Scalar, Dynamic, 1> rVec = bitwise_product(this->weights, h) + this->biasV;
                    rVec = rVec.exp();
                    rVec += Array<Scalar, Dynamic, 1>::Ones(this->getNumVisible());
                    partitionFunction += expBH * rVec.prod();
                }

                // #ifndef _OPENMP
                // if (monitorProgress) monitor.end();
                // #endif
            }

            cout << "Z = " << partitionFunction << endl;

            return 0; // no errors
        }
};

#endif
