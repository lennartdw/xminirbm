/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Special Matrix Functions
 *
 * This file contains routines to compute and manipulate:
 *   - matrix eigensystems (eigenvalues and -vectors)
 *   - matrix exponentials
 *   - outer matrix product (Kronecker product)
 */

#ifndef _MATRICES_HPP
#define _MATRICES_HPP

#include <cstdlib>

#include <eigen3/Eigen/Dense>

#include "types.hpp"
#include "../util/ProgressMonitor.hpp"

using namespace std;

namespace dwamf
{
    typedef struct {
        real_t value;
        Eigen::Matrix<complex_t, Eigen::Dynamic, 1> vector;
    } eigencomp_t;

    template<typename DerivedMatrix, typename DerivedVector>
    DerivedMatrix array_transform_rows(const Eigen::DenseBase<DerivedMatrix> &input, const function<DerivedVector(const DerivedVector&)> &ftransform)
    {
        if (input.rows() <= 0)
        {
            return DerivedMatrix();
        }

        DerivedVector firstRowTransformed = ftransform(input.row(0));

        DerivedMatrix output(input.rows(), firstRowTransformed.size());
        output.row(0) = firstRowTransformed;
        for (long int k = 1; k < input.rows(); ++k)
        {
            output.row(k) = ftransform(input.row(k));
        }

        return output;
    }

    template<typename T>
    Eigen::Matrix<T, Eigen::Dynamic, 1> matrix_autocorrelation(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &matrix)
    {
        int dim = MIN(matrix.rows(), matrix.cols());
        Eigen::Matrix<T, Eigen::Dynamic, 1> autocorr(dim-1);

        ProgressMonitor monitor;
        monitor.start();

        int d;
        for (d = 1; d < dim; ++d)
        {
            monitor.update((real_t) d / dim);

            T acv = 0.0;
            int m, n;
            for (m = 0; m < dim - d; ++m)
            {
                for (n = 0; n < dim - d; ++n)
                {
                    T diff = matrix(m+d, n+d) - matrix(m, n);
                    acv += diff*diff;
                }
            }
            autocorr(d-1) = acv / (dim - d);
        }
        monitor.end();

        return autocorr;
    }

    /**
      * Collect eigencomponents of the provided eigensystem
      * into tuples of one eigenvalue and its associated
      * eigenvector.
      */
    void matrix_eigensystem_compart(eigencomp_t *components, const Eigen::Matrix<real_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors);

    /**
      * Sort an eigensystem by the magnitude of the eigenvalues.
      */
    void matrix_eigensystem_sort(eigencomp_t *components, int dim);

    /**
     * Split collection of eigencomponents into a vector of eigenvalues
     * and a matrix of eigenvectors.
     * This is the inverse operation of matrix_eigensystem_compart.
     */
    void matrix_eigensystem_unravel(Eigen::Matrix<real_t, Eigen::Dynamic, 1> &eigenvalues, Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors, const eigencomp_t *components, int dim);

    template<typename T, int ROWS, int COLS>
    void matrix_chop(Eigen::Matrix<T, ROWS, COLS> &matrix, real_t threshold = 1.0e-15)
    {
        for (int m = 0; m < matrix.rows(); ++m)
        {
            for (int n = 0; n < matrix.cols(); ++n)
            {
                if (FABS(matrix(m, n)) < threshold)
                {
                    matrix(m, n) = 0.0;
                }
            }
        }
    }

    /**
     * Compute the matrix exponential for a complex matrix
     * given its eigenvectors and -values.
     */
    void matrix_exp(Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &result, complex_t s, const Eigen::Matrix<complex_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors);

    Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> matrix_exp(complex_t s, const Eigen::Matrix<complex_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors);
    /**
     * ... for self-adjoint matrices
     */
    void matrix_exp(Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &result, complex_t s, const Eigen::Matrix<real_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors);

    Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> matrix_exp(complex_t s, const Eigen::Matrix<real_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors);

    /**
     * Take the Kronecker product (outer product) of two
     * matrices.
     * NOTE: There is an experimental eigen3 module to
     * perform this operation, which is probably faster
     * in general.
     */
    template<typename T>
    void matrix_kronecker_product(Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &result, const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &m1, const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &m2)
    {
        result.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());

        int i, j;
        for (i = 0; i < m1.rows(); ++i)
        {
            for (j = 0; j < m1.cols(); ++j)
            {
                result.block(i*m2.rows(), j*m2.cols(), m2.rows(), m2.cols()) = m1(i, j) * m2;
            }
        }
    }

    template<typename T>
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> matrix_kronecker_product(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &m1, const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &m2)
    {
        Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> result(m1.rows()*m2.rows(), m1.cols()*m2.cols());
        matrix_kronecker_product(result, m1, m2);
        return result;
    }

    template<typename T>
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> matrix_slice(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &mat, const int_interval_t rowRange, const int rowSkip, const int_interval_t colRange, const int colSkip)
    {
        int newRows = (rowRange.to - rowRange.from) / rowSkip;
        int newCols = (colRange.to - colRange.from) / colSkip;

        Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> newMat(newRows, newCols);
        for (int m = 0; m < newRows; ++m)
        {
            for (int n = 0; n < newCols; ++n)
            {
                newMat(m, n) = mat(rowRange.from + rowSkip * m, colRange.from + colSkip * n);
            }
        }

        return newMat;
    }

    template<typename T>
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> matrix_slice(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> &mat, const int rowSkip, const int colSkip)
    {
        return matrix_slice(mat, {0, (int) mat.rows()}, rowSkip, {0, (int) mat.cols()}, colSkip);
    }
}

#endif
