/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "matrices.hpp"
 */
#include "matrices.hpp"

using namespace Eigen;

int __dwamf_eigencomp_cmp(const void* p1, const void *p2)
{
    const dwamf::eigencomp_t *c1 = (dwamf::eigencomp_t*) p1;
    const dwamf::eigencomp_t *c2 = (dwamf::eigencomp_t*) p2;

    if (c1->value < c2->value)
        return -1;
    else if (c1->value > c2->value)
        return 1;
    else
        return 0;
}

void dwamf::matrix_eigensystem_compart(eigencomp_t *components, const Matrix<real_t, Dynamic, 1> &eigenvalues, const Matrix<complex_t, Dynamic, Dynamic> &eigenvectors)
{
    int n;
    for (n = 0; n < eigenvalues.size(); ++n)
    {
        components[n].value = eigenvalues(n);
        components[n].vector = eigenvectors.col(n);
    }
}

void dwamf::matrix_eigensystem_sort(eigencomp_t *components, int dim)
{
    qsort(components, dim, sizeof(eigencomp_t), &__dwamf_eigencomp_cmp);
}

void dwamf::matrix_eigensystem_unravel(Eigen::Matrix<real_t, Eigen::Dynamic, 1> &eigenvalues, Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors, const eigencomp_t *components, int dim)
{
    eigenvalues.resize(dim);
    eigenvectors.resize(dim, dim);

    int n;
    for (n = 0; n < dim; ++n)
    {
        eigenvalues(n) = components[n].value;
        eigenvectors.col(n) = components[n].vector;
    }
}

void dwamf::matrix_exp(Matrix<complex_t, Dynamic, Dynamic> &result, complex_t s, const Matrix<complex_t, Dynamic, 1> &eigenvalues, const Matrix<complex_t, Dynamic, Dynamic> &eigenvectors)
{
    const int dim = eigenvalues.size();
    result.resize(dim, dim);
    result.setZero();
    int n;
    for (n = 0; n < dim; ++n)
    {
        result(n, n) = exp(s * eigenvalues(n));
    }
    result = eigenvectors * result * eigenvectors.adjoint();
}

Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> dwamf::matrix_exp(complex_t s, const Eigen::Matrix<complex_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors)
{
    Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> result;
    matrix_exp(result, s, eigenvalues, eigenvectors);
    return result;
}

/**
 * ... for self-adjoint matrices
 */
void dwamf::matrix_exp(Matrix<complex_t, Dynamic, Dynamic> &result, complex_t s, const Matrix<real_t, Dynamic, 1> &eigenvalues, const Matrix<complex_t, Dynamic, Dynamic> &eigenvectors)
{
    const int dim = eigenvalues.size();
    result.resize(dim, dim);
    result.setZero();
    int n;
    for (n = 0; n < dim; ++n)
    {
        result(n, n) = exp(s * eigenvalues(n));
    }
    result = eigenvectors * result * eigenvectors.adjoint();
}

Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> dwamf::matrix_exp(complex_t s, const Eigen::Matrix<real_t, Eigen::Dynamic, 1> &eigenvalues, const Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> &eigenvectors)
{
    Eigen::Matrix<complex_t, Eigen::Dynamic, Eigen::Dynamic> result(eigenvectors.rows(), eigenvectors.cols());
    matrix_exp(result, s, eigenvalues, eigenvectors);
    return result;
}
