/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "sets.hpp"
 */
#include "sets.hpp"

int *dwamf::sets_contractions(int n)
{
    if (n <= 0 || n % 2 != 0)
        return NULL;

    int numPairs = sets_binomial(n, 2);
    int *pairs = sets_subsets(n, 2);

    int numContractions = sets_factorial(n-1, 2);
    int *contractions = new int[numContractions * n];
    int numPairsPerContraction = n/2;

    int contractionCheck = 0;
    int *pairOffsets = new int[numPairsPerContraction];

    // find first contraction
    int level = 0;
    pairOffsets[0] = 0;
    contractionCheck |= (1<<pairs[0]) | (1<<pairs[1]);
    while ((++level) < numPairsPerContraction)
    {
        pairOffsets[level] = pairOffsets[level-1] + 1;
        while ( ( contractionCheck & (1 << pairs[2*pairOffsets[level]  ]) )
             || ( contractionCheck & (1 << pairs[2*pairOffsets[level]+1]) ) )
        {
            pairOffsets[level] += 1;
        }

        // block pair indices
        contractionCheck |= (1 << pairs[2*pairOffsets[level]  ]);
        contractionCheck |= (1 << pairs[2*pairOffsets[level]+1]);
        // printf("[level %d]: block %d, %d\n", level, pairs[2*pairOffsets[level]  ], pairs[2*pairOffsets[level]+1]);

    }

    int contractionCounter = 0;
    while (contractionCounter < numContractions)
    {
        // add contraction
        int contractionOffset = contractionCounter*n;
        int i;
        for (i = 0; i < numPairsPerContraction; ++i)
        {
            contractions[contractionOffset + 2*i    ] = pairs[2*pairOffsets[i]  ];
            contractions[contractionOffset + 2*i + 1] = pairs[2*pairOffsets[i]+1];
        }
        ++contractionCounter;
        // printf("added contraction no. %d\n", contractionCounter);
        if (contractionCounter >= numContractions)
        {
            break;
        }

        // find next contraction
        int level = numPairsPerContraction - 1;
        while (level >= 0)
        {
            // release pair indices
            contractionCheck &= ~(1 << pairs[2*pairOffsets[level]  ]);
            contractionCheck &= ~(1 << pairs[2*pairOffsets[level]+1]);
            // printf("[level %d]: release %d, %d\n", level, pairs[2*pairOffsets[level]  ], pairs[2*pairOffsets[level]+1]);

            pairOffsets[level] += 1;
            while ( pairOffsets[level] < numPairs - numPairsPerContraction + level + 1
                && (
                    ( contractionCheck & (1 << pairs[2*pairOffsets[level]  ]) )
                 || ( contractionCheck & (1 << pairs[2*pairOffsets[level]+1]) ) ) )
            {
                pairOffsets[level] += 1;
            }
            if (pairOffsets[level] < numPairs - numPairsPerContraction + level + 1)
            {
                // block pair indices
                contractionCheck |= (1 << pairs[2*pairOffsets[level]  ]);
                contractionCheck |= (1 << pairs[2*pairOffsets[level]+1]);
                // printf("[level %d]: block %d, %d\n", level, pairs[2*pairOffsets[level]  ], pairs[2*pairOffsets[level]+1]);
                break;
            }
            --level;
        }
        if (level < 0)
        {
            break;
        }

        int resumeDirectly = 0; // FALSE
        while ((++level) < numPairsPerContraction)
        {
            if (resumeDirectly)
            {
                // release pair indices
                contractionCheck &= ~(1 << pairs[2*pairOffsets[level]  ]);
                contractionCheck &= ~(1 << pairs[2*pairOffsets[level]+1]);
                // printf("[level %d]: release %d, %d\n", level, pairs[2*pairOffsets[level]  ], pairs[2*pairOffsets[level]+1]);

                pairOffsets[level] += 1;
                resumeDirectly = 0; // FALSE
            }
            else
            {
                pairOffsets[level] = pairOffsets[level-1] + 1;
            }

            while ( pairOffsets[level] < numPairs - numPairsPerContraction + level + 1
                && (
                    ( contractionCheck & (1 << pairs[2*pairOffsets[level]  ]) )
                 || ( contractionCheck & (1 << pairs[2*pairOffsets[level]+1]) ) ) )
            {
                pairOffsets[level] += 1;
            }
            if (pairOffsets[level] >= numPairs - numPairsPerContraction + level + 1)
            {
                // no remaining pairs at this level
                level -= 2;
                resumeDirectly = 1; // TRUE
                continue;
            }

            // block pair indices
            contractionCheck |= (1 << pairs[2*pairOffsets[level]  ]);
            contractionCheck |= (1 << pairs[2*pairOffsets[level]+1]);
            // printf("[level %d]: block %d, %d\n", level, pairs[2*pairOffsets[level]  ], pairs[2*pairOffsets[level]+1]);
        }
    }

    delete []pairs;
    delete []pairOffsets;

    return contractions;
}

int dwamf::sets_factorial(int n, int r)
{
    int fac = 1;
    int k;
    for (k = n; k >= 2; k -= r)
    {
        fac *= k;
    }

    return fac;
}

unsigned long int dwamf::sets_factorial(unsigned long int n, unsigned long int r)
{
    unsigned long int fac = 1;
    unsigned long int k;
    for (k = n; k >= 2; k -= r)
    {
        fac *= k;
    }

    return fac;
}

int *dwamf::sets_subsets(int n, int k)
{
    if (k <= 0 || k > n)
        return NULL;

    int numSubsets = sets_binomial(n, k);
    int *subsets = new int[numSubsets * k];

    int *elementOffsets = new int[k];
    int i;
    for (i = 0; i < k; ++i)
    {
        elementOffsets[i] = i;
    }

    int subsetCounter = 0;
    while (subsetCounter < numSubsets)
    {
        for (i = 0; i < k; ++i)
        {
            subsets[subsetCounter*k + i] = elementOffsets[i];
        }
        ++subsetCounter;

        int level = k-1;
        while (level >= 0)
        {
            elementOffsets[level] += 1;
            if (elementOffsets[level] < n - k + level + 1)
            {
                break;
            }
            --level;
        }
        if (level < 0)
        {
            break;
        }

        while ((++level) < k)
        {
            elementOffsets[level] = elementOffsets[level-1] + 1;
        }
    }

    delete []elementOffsets;

    return subsets;
}
