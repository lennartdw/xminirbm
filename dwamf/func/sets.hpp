/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Set Property and Manipulation Functions
 *
 * This file contains routines to compute:
 *   - combinatorial factors (factorial, binomial, ...)
 *   - subsets
 *   - Isserlis-Wick contractions
 */

#ifndef DWAMF_FUNC_SETS_HPP
#define DWAMF_FUNC_SETS_HPP

#include <cstdlib>
#include "types.hpp"

using namespace std;

namespace dwamf
{
    /**
     * Compute the binomial coefficient
     *     C(n, k) = n! / k! (n-k)!
     */
    template<typename T>
    T sets_binomial(T n, T k)
    {
        if (n < k)
        {
            return 0;
        }

        T res = 1;

        // Since C(n, k) = C(n, n-k)
        if (k > n - k)
            k = n - k;

        // Calculate value of [n * (n-1) * ... * (n-k+1)] / [k * (k-1) * ... * 1]
        for (T i = 0; i < k; ++i)
        {
            res *= (n - i);
            res /= (i + 1);
        }

        return res;
    }

    /**
     * Generate all Isserlis-Wick contractions of the
     * set {0, 1, ..., n-1}, where n is an even number.
     * The result is an array of (n-1)!! contractions listed
     * one after the other, where for every contraction the
     * corresponding pairs are likewise listed subsequently.
     * Here n!! denotes the double factorial,
     *     n!! = sets_factorial(n, 2)
     */
    int *sets_contractions(int n);

    /**
     * Compute the factorial n!
     * If given the second argument r, only every r-th number
     * starting from n is included in the product such that
     *     factorial(n, r) = n (n-r) (n-2r) ... 1
     */
    int sets_factorial(int n, int r = 1);
    unsigned long int sets_factorial(unsigned long int n, unsigned long int r = 1);

    template<typename T>
    T sets_falling_factorial(T n, T k)
    {
        T ffac = 1;
        for (T j = 0; j < k; ++j)
        {
            ffac *= (n - k);
        }

        return ffac;
    }

    /**
     * Given an ordered set {c_1, ..., c_k} of k elements,
     * compute the corresponding rank in the combinatorial
     * number system (cf. Wikipedia), i.e., the index of
     * the set in a lexicographical ordering of all k-subsets
     * of the natural numbers (including 0).
     * Explicitly, the rank is given by
     *     C(c_k, k) + ... + C(c_1, 1),
     * where C(n, k) denotes the binomial coefficient
     * (cf. sets_binomial).
     */
    template<typename T>
    T sets_rank(const vector<T> &orderedSet)
    {
        T rank = 0;
        for (T k = 1; k <= (T) orderedSet.size(); ++k)
        {
            rank += sets_binomial(orderedSet[k-1], k);
        }

        return rank;
    }

    /**
     * Generate the k-subset of the natural numbers
     * with the specified rank in the combinatorial
     * number system.
     * This method is inverse to sets_rank.
     */
    template<typename T>
    vector<T> sets_subset_from_rank(const size_t k, T rank)
    {
        vector<T> set(k);

        for (T j = k; j > 0; --j)
        {
            T c = j;
            T prevBinomial = 0;
            T binomial = 1;
            while (binomial <= rank)
            {
                ++c;
                prevBinomial = binomial;
                binomial = sets_binomial(c, j);
            }

            set[j-1] = c - 1;

            rank -= prevBinomial;
        }

        return set;
    }

    /**
     * Given the set {0, 1, ..., n-1}, generate all subsets
     * of exactly k elements.
     * The result is an array of C(n, k) * k entries corresponding
     * to the C(n, k) subsets listed one after the other. Here
     * C(n, k) is the binomial coefficient, "n choose k".
     */
    int *sets_subsets(int n, int k);


};

#endif
