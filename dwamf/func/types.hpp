/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Basic data types and wrappers for standard library functions
 */

#ifndef _TYPES_HPP
#define _TYPES_HPP

#include <cstdlib>
#include <cmath>
#include <complex>
#include <limits>

#include <eigen3/Eigen/Dense>

#include "../util/strings.hpp"

using namespace std;

#define MIN(_a_, _b_) ((_a_) < (_b_) ? (_a_) : (_b_))
#define MAX(_a_, _b_) ((_a_) > (_b_) ? (_a_) : (_b_))

#ifndef DWAMF_FPPREC_DOUBLE
#ifndef DWAMF_FPPREC_LONG_DOUBLE
#ifndef DWAMF_FPPREC_FLOAT128
/* default precision */
#define DWAMF_FPPREC_DOUBLE
#endif
#endif
#endif

/* precision: DOUBLE (64-bit) */
#ifdef DWAMF_FPPREC_DOUBLE
typedef double real_t; // the standard floating point type
#define __numeric_limits_real_maxdigits10 std::numeric_limits<real_t>::max_digits10
#define __numeric_limits_real_epsilon std::numeric_limits<real_t>::epsilon()
#define CEIL std::ceil
#define COS std::cos
#define ERF std::erf
#define EXP std::exp
#define FABS std::fabs
#define FLOOR std::floor
#define ISINF std::isinf
#define ISNAN std::isnan
#define LOG std::log
#define MOD dwamf::math_mod
#define POW std::pow
#define ROUND std::round
#define SIN std::sin
#define SQRT std::sqrt
#endif

/* precision: LONG DOUBLE (80-bit) */
#ifdef DWAMF_FPPREC_LONG_DOUBLE
typedef long double real_t; // the standard floating point type
#define __numeric_limits_real_maxdigits10 std::numeric_limits<real_t>::max_digits10
#define __numeric_limits_real_epsilon std::numeric_limits<real_t>::epsilon()
#define CEIL ceill
#define COS cosl
#define ERF erfl
#define EXP expl
#define FABS fabsl
#define FLOOR floorl
#define ISINF isinfl
#define ISNAN isnanl
#define LOG logl
#define POW powl
#define ROUND roundl
#define SIN sinl
#define SQRT sqrtl
#endif

/* precision: FLOAT128 (128-bit) */
#ifdef DWAMF_FPPREC_FLOAT128
extern "C" {
    #include <quadmath.h>
}
typedef __float128 real_t; // the standard floating point type
#define __numeric_limits_real_maxdigits10 36
#define __numeric_limits_real_epsilon 1.93e-34
#define CEIL ceilq
#define COS cosq
#define ERF erfq
#define EXP expq
#define FABS fabsq
#define FLOOR floorq
#define ISINF isinfq
#define ISNAN isnanq
#define LOG logq
#define POW powq
#define ROUND roundq
#define SIN sinq
#define SQRT sqrtq

namespace std {
    inline real_t abs(real_t x)
    {
        return fabsq(x);
    }

    inline real_t ceil(real_t x)
    {
        return ceilq(x);
    }

    inline real_t cos(real_t x)
    {
        return cosq(x);
    }

    inline real_t exp(real_t x)
    {
        return expq(x);
    }

    inline real_t floor(real_t x)
    {
        return floorq(x);
    }

    inline real_t log(real_t x)
    {
        return logq(x);
    }

    inline real_t sin(real_t x)
    {
        return sinq(x);
    }

    inline real_t sqrt(real_t x)
    {
        return sqrtq(x);
    }

    std::string to_string(real_t value);
    std::ostream& operator<<(std::ostream &stream, real_t value);
}
#endif

typedef std::complex<real_t> complex_t;

namespace dwamf
{
    typedef struct _dwamf_point2d_type {
        real_t x;
        real_t y;
    } point2d_t;

    typedef struct _dwamf_interval_type {
        real_t from;
        real_t to;
    } interval_t;

    typedef struct _dwamf_int_interval_type {
        int from;
        int to;
    } int_interval_t;

    typedef struct _dwamf_cell2d_type {
        interval_t ivX;
        interval_t ivY;
    } cell2d_t;

    typedef struct _dwamf_type_range {
        real_t from;
        real_t to;
        size_t numPoints;
        real_t ds;
    } range_t;

    typedef real_t (*transformf_t)(real_t);

    inline real_t math_mod(real_t num, real_t denom)
    {
        real_t cppFMOD = fmod(num, denom);
        if (cppFMOD < 0)
            return cppFMOD + denom;
        else
            return cppFMOD;
    }

    template<typename T>
    T math_mod(T num, T denom)
    {
        T cppMOD = num % denom;
        if (cppMOD < 0)
            return cppMOD + denom;
        else
            return cppMOD;
    }

    void print_point(const real_t *point, size_t dimension);

    void range_init(range_t &range, real_t a, real_t b, size_t n);
    int range_equal(range_t &r1, range_t &r2);
    range_t range_from_interval_uniform(interval_t iv, size_t numPoints, int centered = 0);
    range_t range_refine(const range_t oldRange, real_t refinementFactor);

    template<typename T>
    T *range_generate(T *range, size_t numPoints, const T &min, const T &max)
    {
        if (range == NULL)
        {
            range = new T[numPoints];
        }

        T dT = (max - min) / (numPoints - 1);
        for (int k = 0; k < numPoints; ++k)
        {
            range[k] = min + k * dT;
        }

        return range;
    }

    template<typename T>
    vector<T> range_generate(size_t numPoints, const T &min, const T &max)
    {
        vector<T> range(numPoints);

        T dT = (max - min) / (numPoints - 1);
        for (size_t k = 0; k < numPoints; ++k)
        {
            range[k] = min + k * dT;
        }

        return range;
    }

    template<typename T>
    vector<T> range_generate(size_t numPoints, const T &max)
    {
        return range_generate(numPoints, (T) 0, max);
    }

    template<typename T>
    vector<T> range_generate(size_t numPoints)
    {
        return range_generate<T>(numPoints, numPoints - 1);
    }

    /**
     * Convert the string @source to an integer value
     * and write it to @value.
     * Returns TRUE if @source is a valid integer
     * description or FALSE otherwise.
     */
    int parse_integer(long int *value, const char *source, int base = 10);
    int parse_integer(long int *value, const string &source, int base = 10);
    int to_integer(long int *value, const string &source);

    /**
     * Obtain the string representation of an integer in the
     * specified basis and with the requested minimum number
     * of digits.
     */
    string to_base_string(long int value, long int base, size_t minLen = 0);

    /**
     * Obtain a list of digits of the given value
     * in the specified base.
     * The list will have at least minLength entries.
     * The digit order is such that the least significant
     * digit is first and the most significant last,
     * i.e., the k-th entry of the vector corresponds to
     * the coefficient of base^k.
     *
     * This method is inverse to integer_from_digits.
     */
    vector<unsigned long int> integer_digits(unsigned long int value, unsigned long int base, size_t minLength = 0);

    // template<typename T>
    // Eigen::Matrix<T, Eigen::Dynamic, 1> integer_digits(Eigen::Matrix<T, Eigen::Dynamic, 1> &digitVec, unsigned long int value, unsigned long int base)
    template<typename Derived>
    Eigen::DenseBase<Derived> &integer_digits(Eigen::DenseBase<Derived> &digitVec, unsigned long int value, unsigned long int base)
    {
        vector<unsigned long int> digits = integer_digits(value, base, digitVec.size());
        if ((int) digits.size() > digitVec.size())
        {
            digitVec.resize(digits.size());
        }

        for (int i = 0; i < digitVec.size(); ++i)
        {
            digitVec(i) = (typename Derived::Scalar) digits[i];
        }

        return digitVec;
    }

    /**
     * Create an integer from the specified list of digits
     * in the specified base.
     * The digit order is such that the least significant
     * digit is first and the most significant last,
     * i.e., the k-th entry of the list corresponds to
     * the coefficient of base^k.
     *
     * This method is inverse to integer_digits.
     */
    template<typename T>
    unsigned long int integer_from_digits(const vector<T> &digits, unsigned long int base)
    {
        unsigned long int value = 0;
        unsigned long int mul = 1;
        for (auto const& digit : digits)
        {
            value += mul * digit;
            mul *= base;
        }

        return value;
    }

    template<typename Derived>
    unsigned long int integer_from_digits(const Eigen::DenseBase<Derived> &digitVec, unsigned long int base)
    {
        unsigned long int value = 0;
        unsigned long int mul = 1;
        for (long int i = 0; i < digitVec.size(); ++i)
        {
            value += mul * digitVec(i);
            mul *= base;
        }

        return value;
    }

    template<typename T>
    int parse_integer_list(vector<T> &list, const vector<string>::const_iterator &iterFrom, const vector<string>::const_iterator &iterTo, T base = 10)
    {
        for (vector<string>::const_iterator iter = iterFrom; iter != iterTo; ++iter)
        {
            long int value;
            if (parse_integer(&value, *iter, base))
            {
                list.push_back((T) value);
            }
            else
            {
                return 0; // FALSE
            }
        }

        return 1; // TRUE
    }

    template<typename T>
    int parse_integer_list(vector<T> &list, const vector<string> &items, T base = 10)
    {
        return parse_integer_list(list, items.begin(), items.end(), base);
    }

    template<typename T>
    int parse_integer_list(vector<T> &list, const string &source, const string &delimiter = " ", T base = 10)
    {
        return parse_integer_list(list, string_split(source, delimiter, true));
    }



    /**
     * Convert the string @source to a real number
     * and write it to @value.
     * Returns TRUE if @source is a valid number
     * description or FALSE otherwise.
     */
    int parse_real(real_t *value, const char *source);
    int parse_real(real_t *value, const string &source);

    template<typename T>
    int parse_real_list(vector<T> &list, const vector<string> &sources)
    {
        list.clear();

        for (auto &item : sources)
        {
            real_t value;
            if (parse_real(&value, item))
            {
                list.push_back((T) value);
            }
            else
            {
                return 0; // FALSE
            }
        }

        return 1; // TRUE
    }

    template<typename T>
    int parse_real_list(vector<T> &list, const string &source, const string &delimiter = " ")
    {
        return parse_real_list(list, string_split(source, delimiter, true));
    }


    /**
     * Same as parse_real, but returns 0 if conversion
     * was successful and a nonzero value otherwise.
     */
    int to_real(real_t *value, const string &source);

    template<typename T>
    int to_number(T *value, const string &source)
    {
        if (is_same<T, real_t>::value)
        {
            return to_real((real_t*) value, source);
        }
        else if (is_same<T, long int>::value)
        {
            return to_integer((long int*) value, source);
        }
        else
        {
            real_t realValue;
            int r = to_real(&realValue, source);
            if (r) return r; // an error occurred
            *value = (T) realValue;
            return 0; // conversion successful
        }
    }

    string to_prec_string(real_t value);
    string to_real_string(real_t value, int digits = __numeric_limits_real_maxdigits10 - 1);

    constexpr real_t CONST_PI = 3.141592653589793238462;
    constexpr complex_t CONST_I = 1.0i;

    /**
     * Compute a^b for integer a and positive integer b.
     */
    inline long int powi(long int a, unsigned long int b)
    {
        // fast version
        if (a == 2)
        {
            return ((long int) 1) << b;
        }
        else
        {
            long int result = 1;
            while(1)
            {
                if (b & 1)
                    result *= a;
                b >>= 1;
                if (!b)
                    break;
                a *= a;
            }

            return result;
        }

        // slow version:
        // int p = 1;
        // unsigned int k;
        // for (k = 0; k < b; ++k)
        // {
        //     p *= a;
        // }
        //
        // return p;

        // pow2 version
        // return 1 << b;
    }

    /**
     * Compute a^b for integer a and positive integer b.
     */
    template<long int _a>
    inline long int powi(unsigned long int b)
    {
        long int a = _a;
        long int result = 1;
        while(1)
        {
            if (b & 1)
                result *= a;
            b >>= 1;
            if (!b)
                break;
            a *= a;
        }

        return result;
    }

    template<>
    inline long int powi<2>(unsigned long int b)
    {
        return ((long int) 1) << b;
    }
};

#endif
