/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "types.hpp"
 */
#include "types.hpp"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <limits>

using namespace dwamf;

void dwamf::print_point(const real_t *point, size_t dimension)
{
    std::cout << "(";
    if (dimension > 0)
    {
        std::cout << point[0];
        size_t k;
        for (k = 1; k < dimension; ++k)
        {
            std::cout << ", " << point[k];
        }
    }
    std::cout << ")";
}

void dwamf::range_init(range_t &range, real_t a, real_t b, size_t n)
{
    range.from = a;
    range.to = b;
    range.numPoints = n;
    range.ds = (b - a) / n;
}

int dwamf::range_equal(range_t &r1, range_t &r2)
{
    return (r1.from == r2.from && r1.to == r2.to && r1.numPoints == r2.numPoints);
}

range_t dwamf::range_from_interval_uniform(interval_t iv, size_t numPoints, int centered)
{
    range_t range;
    range_init(range, iv.from, iv.to, numPoints);
    if (centered)
    {
        // centralize range points
        range.from += 0.5 * range.ds;
        range.to += 0.5 * range.ds;
    }

    return range;
}

range_t dwamf::range_refine(const range_t oldRange, real_t refinementFactor)
{
    range_t newRange;
    range_init(newRange, oldRange.from, oldRange.to, (size_t) (oldRange.numPoints * refinementFactor));
//     printf("range refine: factor %f; old %ld, new %ld\n", refinementFactor, oldRange.numPoints, newRange.numPoints);
    return newRange;
}

int dwamf::parse_integer(long int *value, const char *source, int base)
{
    char *endptr;
    *value = strtol(source, &endptr, base);
    return (endptr != NULL && endptr != source && endptr[0] == '\0');
}

int dwamf::parse_integer(long int *value, const string &source, int base)
{
    return parse_integer(value, source.c_str(), base);
}

int dwamf::to_integer(long int *value, const string &source)
{
    return !parse_integer(value, source.c_str());
}

string dwamf::to_base_string(long int value, long int base, size_t minLength)
{
    string s = "";
    while (value != 0)
    {
        s = to_string(value % base) + s;
        value /= base;
    }

    if (s == "")
        s = "0";

    while (s.length() < minLength)
        s = "0" + s;

    return s;
}

vector<unsigned long int> dwamf::integer_digits(unsigned long int value, unsigned long int base, size_t minLength)
{
    vector<unsigned long int> digits;
    while (value > 0)
    {
        digits.push_back(value % base);
        value /= base;
    }

    while (digits.size() < minLength)
    {
        digits.push_back(0);
    }

    return digits;
}

#ifndef DWAMF_FPPREC_FLOAT128
int dwamf::parse_real(real_t *value, const char *source)
{
    char *endptr;
    *value = strtod(source, &endptr);
    return (endptr != NULL && endptr != source && endptr[0] == '\0');
}
#else
int dwamf::parse_real(real_t *value, const char *source)
{
    char *endptr;
    *value = strtoflt128(source, &endptr);
//     printf("parsed real: %Qe, '%s'\n", *value, endptr);
//     if ((endptr != NULL && endptr[0] == '\0'))
//         printf("success\n");
    return (endptr != NULL && endptr != source && endptr[0] == '\0');
}
#endif

int dwamf::parse_real(real_t *value, const string &source)
{
    return parse_real(value, source.c_str());
}

int dwamf::to_real(real_t *value, const string &source)
{
    return !parse_real(value, source.c_str());
}

#ifndef DWAMF_FPPREC_FLOAT128
string dwamf::to_prec_string(real_t value)
{
    stringstream sstr;
    sstr << std::setprecision(__numeric_limits_real_maxdigits10) << value;
    return sstr.str();
}

string dwamf::to_real_string(real_t value, int digits)
{
    stringstream sstr;
    sstr << std::setprecision(digits-1) << value;
    return sstr.str();
}
#endif

#ifdef DWAMF_FPPREC_FLOAT128

std::string std::to_string(real_t value)
{
    size_t numDigits = __numeric_limits_real_maxdigits10;
    string format = "%." + to_string(numDigits) + "Qe";
    char str[numDigits+20];
    quadmath_snprintf(str, numDigits+20, format.c_str(), value);
    string strValue = str;
    return strValue;
//     return to_prec_string(value);
}

string dwamf::to_prec_string(real_t value)
{
    return std::to_string(value);
}

std::ostream& std::operator<<(std::ostream &stream, real_t value)
{
    string strValue = to_prec_string(value);
    return (stream << strValue);
}

#endif

