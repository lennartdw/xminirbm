/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Global constants
 */
#ifndef _GLOBALS_HPP
#define _GLOBALS_HPP

#ifdef _LOG_MANAGER_HPP
    #define EXTERN
#else
    #define EXTERN extern
#endif


#define ERROR_UNSUPPORTED  -120

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define ANSI_STYLE_BOLD    "\x1b[1m"
#define ANSI_STYLE_RESET   "\x1b[0m"

#include "LogManager.hpp"

EXTERN dwamf::LogManager STDLOG;

#endif
