/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Base class representing a logging and status file stream
 */

#ifndef _MONITOR_HPP
#define _MONITOR_HPP

#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;

namespace dwamf
{
    class Monitor
    {
        private:
            string message = "";
            FILE *stream = stdout;
            
        public:
            void clear();
            void print(string msg);
            void printf(const char *format, ...);
    };
};

#endif
