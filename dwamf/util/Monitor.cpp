/**
 * implements "Monitor.cpp"
 */
#include "Monitor.hpp"

#define MONITOR_BUFFER_SIZE 1024

using namespace dwamf;

void dwamf::Monitor::clear()
{
#ifndef DWAMF_MONITOR_SILENT
    size_t k;
    for (k = 0; k < message.length(); ++k)
    {
        fprintf(stream, "\b");
    }
    for (k = 0; k < message.length(); ++k)
    {
        fprintf(stream, " ");
    }
    for (k = 0; k < message.length(); ++k)
    {
        fprintf(stream, "\b");
    }
    message = "";
#endif
}

void dwamf::Monitor::print(string msg)
{
#ifndef DWAMF_MONITOR_SILENT
    clear();
    fprintf(stream, "%s", msg.c_str());
    fflush(stream);
    message = msg;
#endif
}

void dwamf::Monitor::printf(const char *format, ...)
{
    char buffer[MONITOR_BUFFER_SIZE];
    if (sprintf(buffer, format) >= 0)
    {
        print(buffer);
    }
}
