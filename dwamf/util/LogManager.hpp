/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Wrapper class to manage logging and status reports
 */

#ifndef _LOG_MANAGER_HPP
#define _LOG_MANAGER_HPP

#include <cstdio>
#include <cstdarg>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;

namespace dwamf
{
    class LogManager
    {
    private:
        char *buffer;
        size_t bufferSize;

        bool printMessagesToStdOut = false;
        bool printWarningsToStdOut = true;
        bool printErrorsToStdOut = true;
        bool printInfosToStdOut = true;

        FILE *msg_stream = stdout;
        FILE *wrn_stream = stdout;
        FILE *err_stream = stdout;
        FILE *inf_stream = stdout;

        string colorWarning;
        string colorError;
        string colorInfo;

        bool printTimeStamp = true;

        int writeToBuffer(const char *format, va_list varargs);

    public:
        LogManager();
        virtual ~LogManager();

        void flush();

        void print(string msg);
        void print_wrn(string msg);
        void print_err(string msg);
        void print_inf(string msg);
        void print_timestamp(FILE *file);
        void print_timestamp();

        void printf(const char *format, ...);
        void printf_wrn(const char *format, ...);
        void printf_err(const char *format, ...);
        void printf_inf(const char *format, ...);

        void setPrintTimeStamp(bool printStamp)
        {
            this->printTimeStamp = printStamp;
        }
    };
};

#endif
