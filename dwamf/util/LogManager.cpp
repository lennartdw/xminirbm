/**
 * implements "LogManager.hpp"
 */
#include "LogManager.hpp"
#include "globals.hpp"

#include <ctime>

dwamf::LogManager::LogManager()
{
    this->bufferSize = 4096;
    this->buffer = new char[bufferSize];

    this->colorWarning = ANSI_COLOR_YELLOW;
    this->colorError = ANSI_COLOR_RED;
    this->colorInfo = ANSI_COLOR_BLUE;
}

dwamf::LogManager::~LogManager()
{
    delete []buffer;
}

void dwamf::LogManager::flush()
{
    fflush(msg_stream);
    fflush(wrn_stream);
    fflush(err_stream);
    fflush(inf_stream);
}

void dwamf::LogManager::print(string msg)
{
    if (printTimeStamp)
        print_timestamp(err_stream);

    fprintf(msg_stream, msg.c_str());

    if (printMessagesToStdOut && msg_stream != stdout)
    {
        std::printf(msg.c_str());
    }
}

void dwamf::LogManager::print_wrn(string msg)
{
    if (printTimeStamp)
        print_timestamp(err_stream);

    fprintf(wrn_stream, "%sWARNING%s %s", colorWarning.c_str(), ANSI_COLOR_RESET, msg.c_str());
    if (printWarningsToStdOut && wrn_stream != stdout)
    {
        std::printf("%sWARNING%s %s", colorWarning.c_str(), ANSI_COLOR_RESET, msg.c_str());
    }
}

void dwamf::LogManager::print_err(string msg)
{
    if (printTimeStamp)
        print_timestamp(err_stream);

    fprintf(err_stream, "%sERROR%s %s", colorError.c_str(), ANSI_COLOR_RESET, msg.c_str());
    if (printErrorsToStdOut && err_stream != stdout)
    {
        std::printf("%sERROR%s %s", colorError.c_str(), ANSI_COLOR_RESET, msg.c_str());
    }
}

void dwamf::LogManager::print_inf(string msg)
{
    if (printTimeStamp)
        print_timestamp(err_stream);

    fprintf(inf_stream, "%sNOTE%s %s", colorInfo.c_str(), ANSI_COLOR_RESET, msg.c_str());
    if (printInfosToStdOut && inf_stream != stdout)
    {
        std::printf("%sERROR%s %s", colorInfo.c_str(), ANSI_COLOR_RESET, msg.c_str());
    }
}

void dwamf::LogManager::print_timestamp(FILE *stream)
{
    time_t currentTime = time(NULL);
    char timestampBuffer[40];
    strftime(timestampBuffer, 40, "%Y-%m-%d %H:%M:%S", std::localtime(&currentTime));
    fprintf(stream, "[%s] ", timestampBuffer);
    // fprintf(stream, "[%s]", std::asctime(std::localtime(&currentTime)));
}

void dwamf::LogManager::print_timestamp()
{
    print_timestamp(msg_stream);
    fprintf(msg_stream, "\n");
}

void dwamf::LogManager::printf(const char *format, ...)
{
    va_list varargs;
    va_start(varargs, format);
    writeToBuffer(format, varargs);
    print(buffer);
    va_end(varargs);
}

void dwamf::LogManager::printf_wrn(const char *format, ...)
{
    va_list varargs;
    va_start(varargs, format);
    writeToBuffer(format, varargs);
    print_wrn(buffer);
    va_end(varargs);
}

void dwamf::LogManager::printf_err(const char *format, ...)
{
    va_list varargs;
    va_start(varargs, format);
    writeToBuffer(format, varargs);
    print_err(buffer);
    va_end(varargs);
}

void dwamf::LogManager::printf_inf(const char *format, ...)
{
    va_list varargs;
    va_start(varargs, format);
    writeToBuffer(format, varargs);
    print_inf(buffer);
    va_end(varargs);
}

int dwamf::LogManager::writeToBuffer(const char *format, va_list varargs)
{
    if (vsprintf(buffer, format, varargs) < 0)
    {
        print_err("[logm] Could not output provided message: buffer overflow (limit: " + to_string(bufferSize) + ").\n");
        return 0; // FALSE
    }
    return 1; // TRUE
}
