/**
 * implements "ProgressMonitor.cpp"
 */
#include "ProgressMonitor.hpp"

string dwamf::ProgressMonitor::getTimeSpanString(double cpTimeInSec)
{
    string timeString = "";

    if (cpTimeInSec > 3600.00)
    {
        int hours = (int) (cpTimeInSec / 3600);
        int mins = (((int) cpTimeInSec % 3600) / 60);
        timeString += to_string(hours) + ":";
        if (mins < 10) timeString += "0" + to_string(mins);
        else timeString += to_string(mins);
        timeString += " h";
    }
    else if (cpTimeInSec > 60.0)
    {
        int mins = (int) (cpTimeInSec / 60);
        int secs = (int) ((int) cpTimeInSec % 60);
        timeString += to_string(mins) + ":";
        if (secs < 10) timeString += "0" + to_string(secs);
        else timeString += to_string(secs);
        timeString += " min";
    }
    else
    {
        timeString += to_string((int) cpTimeInSec) + " sec";
    }

    return timeString;
}

void dwamf::ProgressMonitor::clear()
{
    monitor.clear();
}

void dwamf::ProgressMonitor::end()
{
    this->progress = 1.0;

    monitor.print("(finished in " + ProgressMonitor::getTimeSpanString(difftime(time(NULL), startTime) ) + ")\n");
}

double dwamf::ProgressMonitor::getElapsedTime()
{
    return difftime(time(NULL), startTime);
}

void dwamf::ProgressMonitor::start(size_t numSteps, size_t updateSteps)
{
    this->numSteps = numSteps;
    this->numUpdateSteps = updateSteps;
    this->step = 0;
    this->updateCount = 0;
    this->progress = 0.0;
    this->startTime = time(NULL);

    monitor.print("(0 %)");
}

void dwamf::ProgressMonitor::update(double progress)
{
    this->progress = progress;

    if (progress > 0.0)
    {
        monitor.print("(" + to_string((int) (progress*100)) + " %, " + ProgressMonitor::getTimeSpanString((1.0-progress) * getElapsedTime() / progress) + " remaining)");
    }
    else
    {
        monitor.print("(0 %)");
    }
}

void dwamf::ProgressMonitor::nextStep()
{
    this->step += 1;
    if (numSteps > 0 && (++updateCount) > numUpdateSteps)
    {
        update((double) step / numSteps);
        updateCount = 0;
    }
}
