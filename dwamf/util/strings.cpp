/**
 * implements "strings.hpp"
 */
#include "strings.hpp"
#include <iostream>
#include <algorithm>

bool dwamf::string_containsWhitespace(const string &source)
{
    int i;
    for (i = 0; i < (int) source.length(); ++i)
    {
        if (isspace(source[i]))
            return true;
    }

    return false;
}

string dwamf::string_replace(const string &source, const string &pattern, const string &repl)
{
    // cout << "in '" << source << "' replace '" << pattern << "' by '" << repl << "'" << endl;
    string str = "";

    size_t subStart = 0;
    size_t subEnd = source.find(pattern, subStart);
    while (subEnd != string::npos)
    {
        str += source.substr(subStart, subEnd - subStart) + repl;
        subStart = subEnd + pattern.length();
        subEnd = source.find(pattern, subStart);
    }
    str += source.substr(subStart);

    return str;
}

vector<string> dwamf::string_split(const string &source, const string &separator, bool omitEmptyValues, bool matchAnyCharOfSeparator)
{
    vector<string> parts;

    size_t offset = 0;
    size_t nextSeparator;
    if (matchAnyCharOfSeparator)
    {
        nextSeparator = source.find_first_of(separator, offset);
    }
    else
    {
        nextSeparator = source.find(separator, offset);
    }

    while (nextSeparator != string::npos)
    {
        if (! omitEmptyValues || nextSeparator - offset > 0 )
        {
            parts.push_back(source.substr(offset, nextSeparator-offset));
        }

        if (matchAnyCharOfSeparator)
        {
            offset = nextSeparator + 1;
            nextSeparator = source.find_first_of(separator, offset);
        }
        else
        {
            offset = nextSeparator + separator.length();
            nextSeparator = source.find(separator, offset);
        }
    }
    if (! omitEmptyValues || source.length() - offset > 0 )
    {
        parts.push_back(source.substr(offset));
    }

    return parts;
}

bool dwamf::string_startsWith(const string &source, const string &pattern)
{
    return (source.length() >= pattern.length() && source.substr(0, pattern.length()) == pattern);
}

bool dwamf::string_endsWith(const string &source, const string &pattern)
{
    return (source.length() >= pattern.length() && source.substr(source.length() - pattern.length(), pattern.length()) == pattern);
}

string dwamf::string_toLowerCase(const string &source)
{
    string lowerCaseString = source;
    transform( lowerCaseString.begin(),
        lowerCaseString.end(),
        lowerCaseString.begin(),
        [](unsigned char c){ return std::tolower(c); } );
    return lowerCaseString;
}

string dwamf::string_trim(const string &source)
{
    size_t start = 0;
    size_t end = source.length()-1;
    while (start < end && isspace(source[start]))
    {
        ++start;
    }
    while (end > start && isspace(source[end]))
    {
        --end;
    }
    if (isspace(source[start]))
        return "";
    else
        return source.substr(start, end-start+1);
}
