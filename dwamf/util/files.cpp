/**
 * implements "files.hpp"
 */
#include "files.hpp"
#include <algorithm>
#include <iostream>
#include <sys/stat.h>
#include <cstring>

#ifndef DWAMF_EXCLUDE_BOOST_FILESYSTEM
#include <boost/filesystem.hpp>
#endif

#define BUF_SIZE 1023

bool dwamf::file_exists(const string &filePath)
{
    struct stat buffer;
    return (stat(filePath.c_str(), &buffer) == 0);
}

string dwamf::file_extension(const string &fileName)
{
    int offset = fileName.length() - 1;
    while (offset > 0 && (fileName[offset] != '/' && fileName[offset] != '\\'))
    {
        if (fileName[offset] == '.')
        {
            return fileName.substr(offset + 1, fileName.length() - offset - 1);
        }
        --offset;
    }
    return "";
}

#ifndef DWAMF_EXCLUDE_BOOST_FILESYSTEM
bool dwamf::file_is_directory(const string &filePath)
{
    return boost::filesystem::is_directory(filePath);
}

vector<string> dwamf::file_list(const string &directory)
{
    vector<string> fileList;

    boost::filesystem::path dir(directory);
    boost::filesystem::directory_iterator dirIter(dir);
    for (auto &file : dirIter)
    {
        fileList.push_back(file.path().filename().string());
    }
    sort(fileList.begin(), fileList.end());

    return fileList;
}
#endif

string dwamf::file_name(const string &fileName)
{
    int offset = fileName.length() - 1;
    int endIndex = fileName.length();
    while (offset > 0 && (fileName[offset-1] != '/' && fileName[offset-1] != '\\'))
    {
        if (endIndex >= (int) fileName.length() && fileName[offset-1] == '.')
        {
            endIndex = offset - 1;
        }
        --offset;
    }

    return fileName.substr(offset, endIndex - offset);
}

void dwamf::file_path_decomposition(string &directory, string &fileName, const string &filePath)
{
    const size_t pathLength = filePath.length();
    int offset = pathLength;
    while (offset > 0 && filePath[offset-1] != '/' && filePath[offset-1] != '\\')
    {
        --offset;
    }

    directory = filePath.substr(0, offset);
    fileName = filePath.substr(offset);
}

size_t dwamf::file_read_rawd(const char *fileName, double **dest)
{
    FILE *file = fopen(fileName, "r");

    if (file != NULL)
    {
        size_t bufferSize = sizeof(double) * BUF_SIZE;
        char *buffer = (char*) malloc(bufferSize);
        if (buffer == NULL)
        {
            // TODO: ERROR Out of memory.
            return 0;
        }
        size_t counter = 0;
        int c = fgetc(file);
        while (c != EOF)
        {
            if (counter >= bufferSize)
            {
                // buffer exceeded, reallocate
                bufferSize *= 2;
                buffer = (char*) realloc(buffer, bufferSize);
                if (buffer == NULL)
                {
                    // TODO: ERROR Out of memory.
                    return 0;
                }
            }

            buffer[counter++] = c;
            c = fgetc(file);
        }

        // check for file consistency
        if (counter % sizeof(double) != 0)
        {
            // TODO: ERROR File corrupted.
            return 0;
        }

        *dest = (double*) malloc(counter);
        memcpy(*dest, buffer, counter);

        free(buffer);
        fclose(file);

        // return number of acquired data points
        return counter / sizeof(double);
    }
    else
    {
        // TODO: ERROR File not found.
        return 0;
    }
}

int dwamf::file_write_rawd(const char *fileName, const double *data, const size_t numPoints)
{
    FILE *file = fopen(fileName, "w");
    if (file != NULL)
    {
        fwrite(data, sizeof(double), numPoints, file);
        fclose(file);
        return 1; // TRUE
    }
    else
    {
        return 0; // FALSE
    }
}
