/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Monitor for track progress of computations
 */

#ifndef _PROGRESS_MONITOR_HPP
#define _PROGRESS_MONITOR_HPP

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>

#include "Monitor.hpp"

using namespace std;

namespace dwamf
{
    class ProgressMonitor
    {
    private:
        double progress;
        time_t startTime;
        size_t step = 0;
        size_t numSteps = 0;

        size_t updateCount = 0;
        size_t numUpdateSteps = 0;

        Monitor monitor;

    public:
        void clear();
        void end();
        void nextStep();
        void start(size_t numSteps = 0, size_t updateInterval = 1);
        void update(double progress);

        /**
         * Obtain the time (in seconds) that has passed
         * since the call to start().
         */
        double getElapsedTime();

        static string getTimeSpanString(double cpTimeInSec);
    };
};

#endif
