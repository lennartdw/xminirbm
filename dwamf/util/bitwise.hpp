/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Bitwise integer manipulation and analysis routines
 */

#ifndef _DWAMF_BITWISE_HPP
#define _DWAMF_BITWISE_HPP

#include <eigen3/Eigen/Dense>

using namespace std;

namespace dwamf
{
    /**
     * Count the number of bits "1" of the integer k
     * (in the specified range of digits).
     */
    int bitwise_count1(long int k);
    int bitwise_count1(long int k, int from, int to);

    template<typename Derived>
    Eigen::MatrixBase<Derived> &bitwise_masked_add(Eigen::MatrixBase<Derived> &v, const typename Eigen::internal::traits<Derived>::Scalar value, long int mask)
    {
        long int flag = 1;
        for (int k = 0; k < v.size(); ++k)
        {
            if (mask & flag)
            {
                v(k) += value;
            }
            flag <<= 1;
        }

        return v;
    }

    template<typename Derived>
    Eigen::MatrixBase<Derived> &bitwise_masked_add(Eigen::MatrixBase<Derived> &m, const typename Eigen::internal::traits<Derived>::Scalar value, long int rowMask, long int colMask)
    {
        long int colFlag = 1;
        for (int j = 0; j < m.cols(); ++j)
        {
            if (colMask & colFlag)
            {
                long int rowFlag = 1;
                for (int i = 0; i < m.rows(); ++i)
                {
                    if (rowMask & rowFlag)
                    {
                        m(i, j) += value;
                    }
                    rowFlag <<= 1;
                }
            }
            colFlag <<= 1;
        }

        return m;
    }

    template<typename Derived>
    typename Eigen::internal::traits<Derived>::Scalar bitwise_masked_sum(const Eigen::MatrixBase<Derived> &v, long int mask)
    {
        typename Eigen::internal::traits<Derived>::Scalar sum = 0.0;
        long int flag = 1;
        for (int k = 0; k < v.size(); ++k)
        {
            if (mask & flag)
            {
                sum += v(k);
            }
            flag <<= 1;
        }

        return sum;
    }

    template<typename Derived>
    Eigen::Matrix<typename Eigen::internal::traits<Derived>::Scalar, Eigen::Dynamic, 1> bitwise_product(const Eigen::MatrixBase<Derived> &m, long int bitv)
    {
        Eigen::Matrix<typename Eigen::internal::traits<Derived>::Scalar, Eigen::Dynamic, 1> prod(m.rows());
        for (int k = 0; k < m.rows(); ++k)
        {
            prod(k) = bitwise_masked_sum(m.row(k), bitv);
        }
        return prod;
    }

    template<typename Derived>
    Eigen::Matrix<typename Eigen::internal::traits<Derived>::Scalar, 1, Eigen::Dynamic> bitwise_product(long int bitv, const Eigen::MatrixBase<Derived> &m)
    {
        Eigen::Matrix<typename Eigen::internal::traits<Derived>::Scalar, 1, Eigen::Dynamic> prod(m.cols());
        for (int k = 0; k < m.cols(); ++k)
        {
            prod(k) = bitwise_masked_sum(m.col(k), bitv);
        }
        return prod;
    }

    template<typename Derived>
    typename Eigen::internal::traits<Derived>::Scalar bitwise_product(long int bitv1, const Eigen::MatrixBase<Derived> &m, long int bitv2)
    {
        return bitwise_masked_sum(bitwise_product(bitv1, m), bitv2);
    }

};

#endif
