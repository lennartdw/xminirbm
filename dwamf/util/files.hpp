/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * File management routines
 */

#ifndef _DWAMF_FILES_HPP
#define _DWAMF_FILES_HPP

#include <string>
#include <vector>

using namespace std;

namespace dwamf
{
    bool file_exists(const string &filePath);
    string file_extension(const string &fileName);

#ifndef DWAMF_EXCLUDE_BOOST_FILESYSTEM
    bool file_is_directory(const string &filePath);
    vector<string> file_list(const string &directory);
#endif

    string file_name(const string &fileName);
    void file_path_decomposition(string &directory, string &fileName, const string &filePath);


    /**
     * Read raw data (i.e. byte streamed) values of
     * type double from the file @fileName and write
     * values to a malloc'd array whose location
     * is stored to @dest.
     * Return the number of values read from the file.
     *
     * The complementary function for writing byte streams
     * of double values is file_write_rawd.
     */
    size_t file_read_rawd(const char *fileName, double **dest);

    /**
     * Write raw (i.e. byte streamed) data in @data
     * consisting of @numPoints entries to the file
     * @fileName.
     *
     * The complementary function for reading byte streams
     * of double values is file_read_rawd.
     *
     * Return TRUE if data file is written successfully,
     * FALSE otherwise.
     */
    int file_write_rawd(const char *fileName, const double *data, const size_t numPoints);

};

#endif
