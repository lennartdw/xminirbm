/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * String manipulation routines
 */

#ifndef _DWAMF_STRINGS_HPP
#define _DWAMF_STRINGS_HPP

#include <string>
#include <vector>

using namespace std;

namespace dwamf
{
    /**
     * Check if the source string contains whitespace characters,
     * i.e., any character for which the C stdlib function 'isspace'
     * evaluates to a non-zero value.
     */
    bool string_containsWhitespace(const string &source);

    /**
     * Replace all occurrences of the pattern in the source by
     * the replacement.
     */
    string string_replace(const string &source, const string &pattern, const string &replacement);

    /**
     * Split the source into substrings at all occurrences of
     * the separator. The separator itself will not become
     * part of any of the substrings.
     * If omitEmptyValues is true, empty substrings (where two
     * consecutive separators occurred) will be excluded from the
     * returned list.
     * An empty string ("") results in an empty vector
     * if omitEmptyValues is true, or in a vector with
     * a single "" entry if omitEmptyValues is false.
     * If matchAnyCharOfSeparator is true, then any character
     * of the separator string is taken as a splitting point.
     * Otherwise, splits occur only at full matches with the
     * separator string.
     */
    vector<string> string_split(const string &source, const string &separator = " ", bool omitEmptyValues = false, bool matchAnyCharOfSeparator = false);

    /**
     * Check if the source string starts or ends with the specified
     * pattern string.
     */
    bool string_startsWith(const string &source, const string &pattern);
    bool string_endsWith(const string &source, const string &pattern);

    /**
     * Convert all characters in the source string to
     * lower case.
     */
    string string_toLowerCase(const string &source);

    /**
     * Remove leading and trailing whitespace characters
     * from the source.
     */
    string string_trim(const string &source);
};

#endif
