/**
 * implements "bitwise.hpp"
 */
#include "bitwise.hpp"


int dwamf::bitwise_count1(long int k)
{
    int count = 0;
    while (k)
    {
        k &= (k-1);
        ++count;
    }

    return count;
}

int dwamf::bitwise_count1(long int k, int from, int to)
{
    int count = 0;
    long int flag = (1 << from);
    for (int digit = from; digit < to; ++digit)
    {
        if (k & flag)
        {
            ++count;
        }
        flag <<= 1;
    }
    return count;
}
