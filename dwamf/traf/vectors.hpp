/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Routines to generate vector transformations
 * from string specifications.
 */

#ifndef _TRAF_VECTORS_H
#define _TRAF_VECTORS_H

namespace dwamf
{
    template<typename DerivedVector>
    function<DerivedVector(const DerivedVector&)> vector_parse_transform(const vector<string> &trafSpecs)
    {
        vector< function<typename DerivedVector::Scalar(const DerivedVector&)> > outputTransforms;

        for (auto &trafSpec : trafSpecs)
        {
            vector<string> specs = string_split(trafSpec, " ", true);
            if (specs.size() <= 0)
            {
                // empty entry, omit
                continue;
            }

            if (specs[0] == "max")
            {
                // take maximum of all specified entries
                if (specs.size() <= 1)
                {
                    STDLOG.printf_err("Cannot generate vector transform: Expected at least one index specification for reduction 'max'.\n");
                    throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                }

                vector<long int> entryIndices;
                if (! parse_integer_list(entryIndices, specs.begin() + 1, specs.end()))
                {
                    STDLOG.printf_err("Cannot generate vector transform: Invalid index specification for reduction 'max'.\n");
                    throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                }

                outputTransforms.push_back([entryIndices](const DerivedVector &input) -> typename DerivedVector::Scalar {
                        typename DerivedVector::Scalar max = input(entryIndices[0]);
                        for (size_t k = 1; k < entryIndices.size(); ++k)
                        {
                            typename DerivedVector::Scalar comp = input(entryIndices[k]);
                            if (comp > max)
                            {
                                max = comp;
                            }
                        }

                        return max;
                    });
            }
            else if (specs[0] == "mean")
            {
                // take mean of all specified entries
                if (specs.size() <= 1)
                {
                    STDLOG.printf_err("Cannot generate vector transform: Expected at least one index specification for reduction 'mean'.\n");
                    throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                }

                vector<long int> entryIndices;
                if (! parse_integer_list(entryIndices, specs.begin() + 1, specs.end()))
                {
                    STDLOG.printf_err("Cannot generate vector transform: Invalid index specification for reduction 'max'.\n");
                    throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                }

                outputTransforms.push_back([entryIndices](const DerivedVector &input) -> typename DerivedVector::Scalar {
                        typename DerivedVector::Scalar sum = 0.0;
                        for (size_t k = 0; k < entryIndices.size(); ++k)
                        {
                            sum += input(entryIndices[k]);
                        }

                        return sum / entryIndices.size();
                    });
            }
            else if (specs[0] == "threshold")
            {
                // compare specified entries to threshold
                // and indicate whether their majority is above or below
                if (specs.size() <= 2)
                {
                    STDLOG.printf_err("Cannot generate vector transform: Expected a threshold specification and at least one index for reduction 'threshold'.\n");
                    throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                }

                vector<string> thresholdSpecs = string_split(specs[1], ",", false);
                // format: threshold=0, lower=-1, upper=1, even=0
                typename DerivedVector::Scalar threshold = 0;
                if (thresholdSpecs.size() >= 1 && thresholdSpecs[0] != "")
                {
                    real_t value;
                    if (parse_real(&value, thresholdSpecs[0]))
                    {
                        threshold = (typename DerivedVector::Scalar) value;
                    }
                    else
                    {
                        STDLOG.printf_err("Cannot generate vector transform: Invalid threshold value in threshold specification '%s' for reduction 'threshold'.\n", specs[1].c_str());
                        throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                    }
                }

                typename DerivedVector::Scalar lower = -1;
                if (thresholdSpecs.size() >= 2 && thresholdSpecs[1] != "")
                {
                    real_t value;
                    if (parse_real(&value, thresholdSpecs[1]))
                    {
                        lower = (typename DerivedVector::Scalar) value;
                    }
                    else
                    {
                        STDLOG.printf_err("Cannot generate vector transform: Invalid lower value in threshold specification '%s' for reduction 'threshold'.\n", specs[1].c_str());
                        throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                    }
                }

                typename DerivedVector::Scalar upper = 1;
                if (thresholdSpecs.size() >= 3 && thresholdSpecs[2] != "")
                {
                    real_t value;
                    if (parse_real(&value, thresholdSpecs[2]))
                    {
                        upper = (typename DerivedVector::Scalar) value;
                    }
                    else
                    {
                        STDLOG.printf_err("Cannot generate vector transform: Invalid upper value in threshold specification '%s' for reduction 'threshold'.\n", specs[1].c_str());
                        throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                    }
                }

                typename DerivedVector::Scalar even = 0;
                if (thresholdSpecs.size() >= 4 && thresholdSpecs[3] != "")
                {
                    real_t value;
                    if (parse_real(&value, thresholdSpecs[3]))
                    {
                        even = (typename DerivedVector::Scalar) value;
                    }
                    else
                    {
                        STDLOG.printf_err("Cannot generate vector transform: Invalid even value in threshold specification '%s' for reduction 'threshold'.\n", specs[1].c_str());
                        throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                    }
                }

                typename DerivedVector::Scalar fraction = 1;
                if (thresholdSpecs.size() >= 5 && thresholdSpecs[4] != "")
                {
                    real_t value;
                    if (parse_real(&value, thresholdSpecs[4]))
                    {
                        fraction = (typename DerivedVector::Scalar) value;
                    }
                    else
                    {
                        STDLOG.printf_err("Cannot generate vector transform: Invalid fraction value in threshold specification '%s' for reduction 'threshold'.\n", specs[1].c_str());
                        throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                    }
                }

                vector<long int> entryIndices;
                if (! parse_integer_list(entryIndices, specs.begin() + 2, specs.end()))
                {
                    STDLOG.printf_err("Cannot generate vector transform: Invalid index specification for reduction 'threshold'.\n");
                    throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
                }

                cout << "using transformation with l=" << lower << ", u=" << upper << ", e=" << even << ", f=" << fraction << endl;
                outputTransforms.push_back([threshold, lower, upper, even, fraction, entryIndices](const DerivedVector &input) -> typename DerivedVector::Scalar {
                        size_t countsBelow = 0;
                        size_t countsAbove = 0;
                        for (size_t k = 0; k < entryIndices.size(); ++k)
                        {
                            typename DerivedVector::Scalar entry = input(entryIndices[k]);
                            if (entry < threshold)
                            {
                                ++countsBelow;
                            }
                            else if (entry > threshold)
                            {
                                ++countsAbove;
                            }
                        }

                        if (countsBelow > fraction * countsAbove)
                        {
                            return lower;
                        }
                        else if (fraction * countsAbove > countsBelow)
                        {
                            return upper;
                        }
                        else
                        {
                            return even;
                        }
                    });
            }
            else
            {
                STDLOG.printf_err("Cannot generate vector transform: Unknown entry transformation '%s'.\n", specs[0].c_str());
                throw std::runtime_error("invalid entry transformation '" + trafSpec + "'");
            }
        }

        return [outputTransforms](const DerivedVector &input) -> DerivedVector {
                DerivedVector output(outputTransforms.size());
                for (size_t k = 0; k < outputTransforms.size(); ++k)
                {
                    output(k) = outputTransforms[k](input);
                }

                return output;
            };
    }
};

#endif
