/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Data series container, consisting of a header (cf. conf/Configuration)
 * and a table of data.
 */

#ifndef _DWAMF_DATASERIES_HPP
#define _DWAMF_DATASERIES_HPP

#include <fstream>
#include <iomanip>
#include <sstream>

#include <eigen3/Eigen/Dense>

#include "../conf/Configuration.hpp"
#include "DataIO.hpp"

namespace dwamf
{
    template<typename T>
    class DataSeries
    {
        private:
            string paramPrefix = "";
            string dataPrefix = "% ";

        public:
            Configuration params;
            Eigen::Matrix<T, Dynamic, Dynamic> data;

            DataSeries(Configuration parameters, const Eigen::Matrix<T, Dynamic, Dynamic> &dataTable)
            {
                this->params = parameters;
                this->data = dataTable;
            }

            DataSeries(const Eigen::Matrix<T, Dynamic, Dynamic> &dataTable)
            {
                this->data = dataTable;
            }

            DataSeries()
            {
            }

            string getDataPrefix() const
            {
                return this->dataPrefix;
            }

            string getParamPrefix() const
            {
                return this->paramPrefix;
            }

            DataSeries<T> getSubseries(int rowOffset, int colOffset, int numRows, int numCols)
            {
                return DataSeries<T>(params.copy(), data.block(rowOffset, colOffset, numRows, numCols));
            }

            static DataSeries loadFromFile(string fileName, int (*parse_entry)(T *value, const string &str), const string &paramIndicator = "", const string &dataIndicator = "%")
            {
                Eigen::Matrix<T, Dynamic, Dynamic> dataTable;
                DataIO::importMatrix<T, Dynamic, Dynamic>(fileName, dataTable, parse_entry, dataIndicator, paramIndicator);
                Configuration config(fileName.c_str(), fileName.c_str(), paramIndicator, dataIndicator);

                DataSeries series(config, dataTable);
                series.setParamPrefix(paramIndicator);
                series.setDataPrefix(dataIndicator);

                return series;
            }

            int saveToFile(string fileName, const string &paramIndicator, const string &dataIndicator) const
            {
                // save parameters (configuration)
                if (! params.saveToFile(fileName.c_str(), paramIndicator))
                // if (! params.saveToFile(fileName.c_str()))
                {
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                // append data to file
                ofstream output(fileName.c_str(), ofstream::app);
                if (output.good())
                {
                    // precision settings
                    int digits10 = std::numeric_limits<real_t>::max_digits10;
                    if (std::numeric_limits<T>::max_digits10 > 0)
                    {
                        digits10 = std::numeric_limits<T>::max_digits10;
                    }
                    output << std::scientific << std::setprecision(digits10);

                    return DataIO::exportMatrix(output, data, dataIndicator);
                }
                else
                {
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }
            }

            int saveToFile(string fileName) const
            {
                return saveToFile(fileName, paramPrefix, dataPrefix);
            }

            void setDataPrefix(string value)
            {
                this->dataPrefix = value;
            }

            void setParamPrefix(string value)
            {
                this->paramPrefix = value;
            }
    };

};

#endif
