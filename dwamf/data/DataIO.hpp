/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Routines to import and export numeric and text-based data
 */

#ifndef _DWAMF_DATAIO_HPP
#define _DWAMF_DATAIO_HPP

#include <fstream>
#include <iomanip>
#include <sstream>
#include <forward_list>
#include <unordered_map>

#include <eigen3/Eigen/Dense>

#include <boost/multi_array.hpp>

#include "../util/globals.hpp"
#include "../util/LogManager.hpp"

using namespace std;
using namespace Eigen;

const int DWAMF_DATAIO_SUCCESS = 0;
const int DWAMF_DATAIO_END_OF_FILE = 1;
const int DWAMF_DATAIO_FILE_NOT_ACCESSIBLE = -1;
const int DWAMF_DATAIO_INVALID_CONTENTS = -5;

#define NUMFEXACT(_a_) ((_a_) == (long int) (_a_) ? ((long int) (_a_)) : (_a_))

namespace dwamf
{
    class DataIO
    {
        private:
            template<typename T>
            static int readEntryRaw(FILE *file, T &entry, bool reverseByteOrder = false)
            {
                char rawEntry[sizeof(T)];
                for (size_t i = 0; i < sizeof(T); ++i)
                {
                    int c = fgetc(file);
                    if (c == EOF)
                    {
                        if (i == 0)
                        {
                            return DWAMF_DATAIO_END_OF_FILE;
                        }
                        else
                        {
                            STDLOG.printf_err("End of file reached while reading in a single entry, indicating a potential file corruption.\n");
                            return DWAMF_DATAIO_INVALID_CONTENTS;
                        }
                    }
                    if (reverseByteOrder)
                    {
                        rawEntry[sizeof(T) - i - 1] = c;
                    }
                    else
                    {
                        rawEntry[i] = c;
                    }
                }
                entry = *(T*) rawEntry;

                return DWAMF_DATAIO_SUCCESS;
            }

        public:
            /**
             * Save a list of elements of a fixed type to a text file.
             * The individual elements are converted to strings using
             * the specified entryToString function.
             * For every element, the string representation is extended by
             * prepending dataPrefix and appending dataPostfix.
             */
            template<typename T>
            static int exportList(ofstream &sout, const std::vector<T> &data, const function<string(const T&)> entryToString, const string &dataPrefix = "", const string &dataPostfix = "\n")
            {
                if (data.size() <= 0)
                {
                    STDLOG.printf_err("The provided list is empty.\n");
                    return DWAMF_DATAIO_INVALID_CONTENTS;
                }

                for (size_t n = 0; n < data.size(); ++n)
                {
                    sout << dataPrefix << entryToString( data[n] ) << dataPostfix;
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename T>
            static int exportList(const string &fileName, const std::vector<T> &data, const function<string(const T&)> entryToString, const string &dataPrefix = "", const string &dataPostfix = "\n")
            {
                ofstream sout(fileName.c_str(), ofstream::out);
                if (! sout.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                return exportList(sout, data, entryToString, dataPrefix, dataPostfix);
            }

            template<typename T>
            static int exportList(ofstream &sout, const std::vector<T> &data, const string &dataPrefix = "", const string &dataPostfix = "\n")
            {
                if (data.size() <= 0)
                {
                    STDLOG.printf_err("The provided list is empty.\n");
                    return DWAMF_DATAIO_INVALID_CONTENTS;
                }

                for (size_t n = 0; n < data.size(); ++n)
                {
                    sout << dataPrefix << NUMFEXACT( data[n] ) << dataPostfix;
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename T>
            static int exportList(const string &fileName, const std::vector<T> &data, const string &dataPrefix = "")
            {
                ofstream sout(fileName.c_str(), ofstream::out);
                if (! sout.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                int digits10 = std::numeric_limits<real_t>::max_digits10;
                if (std::numeric_limits<T>::max_digits10 > 0)
                {
                    digits10 = std::numeric_limits<T>::max_digits10;
                }
                sout << std::scientific << std::setprecision(digits10);

                return exportList(sout, data, dataPrefix);
            }

            template<typename T>
            static int exportListRaw(string fileName, const std::vector<T> &data)
            {
                FILE *file = fopen(fileName.c_str(), "w");
                if (file == NULL)
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }
                fwrite(data.data(), sizeof(T), data.size(), file);
                fclose(file);

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename Key, typename Value>
            static int exportMap(ofstream &sout, const std::unordered_map<Key, Value> &map, const function<string(const Key&)> &keyToString, const function<string(const Value&)> &valueToString, const string &keyValueSeparator = " ", const string &entryPrefix = "", const string &entryPostfix = "\n")
            {
                if (map.size() <= 0)
                {
                    STDLOG.printf_err("The provided map is empty.\n");
                    return DWAMF_DATAIO_INVALID_CONTENTS;
                }

                for (auto &entry : map)
                {
                    sout << entryPrefix << keyToString(entry.first) << keyValueSeparator << valueToString(entry.second) << entryPostfix;
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename Key, typename Value>
            static int exportMap(const string &fileName, const std::unordered_map<Key, Value> &map, const function<string(const Key&)> &keyToString, const function<string(const Value&)> &valueToString, const string &keyValueSeparator = " ", const string &entryPrefix = "", const string &entryPostfix = "\n")
            {
                ofstream sout(fileName.c_str(), ofstream::out);
                if (! sout.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                return exportMap(sout, map, keyToString, valueToString, keyValueSeparator, entryPrefix, entryPostfix);
            }

            //template<typename T, int ROWS, int COLS>
            //static int exportMatrix(ofstream &sout, const Eigen::Matrix<T, ROWS, COLS> &data, const string &dataPrefix = "")
            template<typename Derived>
            static int exportMatrix(ofstream &sout, const Eigen::DenseBase<Derived> &data, const string &dataPrefix = "")
            {
                // format: TSV
                if (data.rows() <= 0 || data.cols() <= 0)
                {
                    STDLOG.printf_wrn("The provided matrix is empty.\n");
                    // return DWAMF_DATAIO_INVALID_CONTENTS;
                    return DWAMF_DATAIO_SUCCESS;
                }

                int m, n;
                for (m = 0; m < data.rows(); ++m)
                {
                    // cout << "export row " << m << endl;
                    sout << dataPrefix << NUMFEXACT( data(m, 0) );
                    for (n = 1; n < data.cols(); ++n)
                    {
                        sout << '\t' << NUMFEXACT(data(m, n));
                    }
                    sout << std::endl;
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            // template<typename T, int ROWS, int COLS>
            // static int exportMatrix(string fileName, const Eigen::Matrix<T, ROWS, COLS> &data, const string &dataPrefix = "")
            template<typename Derived>
            static int exportMatrix(const string &fileName, const Eigen::DenseBase<Derived> &data, const string &dataPrefix = "")
            {
                ofstream sout(fileName.c_str(), ofstream::out);
                if (! sout.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                int digits10 = std::numeric_limits<real_t>::max_digits10;
                if (std::numeric_limits<typename Derived::Scalar>::max_digits10 > 0)
                {
                    digits10 = std::numeric_limits<typename Derived::Scalar>::max_digits10;
                }
                sout << std::scientific << std::setprecision(digits10);

                return exportMatrix(sout, data, dataPrefix);
            }

            template<typename T, int ROWS, int COLS>
            static int exportMatrixRaw(const string &fileName, const Eigen::Matrix<T, ROWS, COLS> &data)
            {
                FILE *file = fopen(fileName.c_str(), "w");
                if (file == NULL)
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                fwrite(data.data(), sizeof(T), data.size(), file);
                fclose(file);

                return DWAMF_DATAIO_SUCCESS;
            }

            static int exportText(ostream &sout, const string &data)
            {
                sout << data;
                return DWAMF_DATAIO_SUCCESS;
            }

            static int exportText(const string &fileName, const string &data)
            {
                ofstream sout(fileName.c_str(), ofstream::out);
                if (! sout.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                return exportText(sout, data);
            }

            template<typename OutputSource>
            static int exportText(OutputSource &outSource, const vector<string> &lines)
            {
                string fullText = "";
                for (auto &line : lines)
                {
                    fullText += line + "\n";
                }

                return exportText(outSource, fullText);;
            }

            /**
             * Load a list of elements of a fixed type from
             * an input stream.
             * The individual lines read from the stream are
             * interpreted as those elements and are converted
             * to the respective type using the parse_entry function.
             * This function should return 0 for successful conversions
             * or an error code otherwise.
             * If dataPrefix is nonempty, only lines starting with
             * this string are parsed.
             * If ignorePrefix is nonempty, lines starting with this
             * string are skipped.
             */
            template<typename T>
            // static int importList(istream &input, vector<T> &data, int (*parse_entry)(T *value, const string &str) = to_number<T>, const string &dataPrefix = "", const string &ignorePrefix = "#")
            static int importList(istream &input, vector<T> &data, const function<int(T*, const string&)> &parse_entry = to_number<T>, const string &dataPrefix = "", const string &ignorePrefix = "#")
            {
                string line;

                const size_t dataPrefixLength = dataPrefix.length();
                const size_t ignorePrefixLength = ignorePrefix.length();

                size_t lineCount = 0;
                while (! input.eof() && getline(input, line))
                {
                    ++lineCount;

                    const size_t length = line.length();
                    size_t offset = 0;

                    if (dataPrefixLength > 0 && (length <= dataPrefixLength || line.substr(0, dataPrefixLength) != dataPrefix))
                    {
                        // line does not carry data prefix
                        // std::cout << "    (no data)" << std::endl;
                        continue;
                    }
                    else if (ignorePrefixLength > 0 && length >= ignorePrefixLength && line.substr(0, ignorePrefixLength) == ignorePrefix)
                    {
                        // line carries ignore prefix
                        // std::cout << "    (ignore)" << std::endl;
                        continue;
                    }
                    else
                    {
                        offset += dataPrefixLength;
                    }

                    int status;
                    T entry;
                    if ( (status = parse_entry(&entry, line.substr(offset))) )
                    {
                        STDLOG.printf_err("Invalid entry '%s' at line %ld (error code: %d).\n", line.substr(offset).c_str(), lineCount, status);
                        return DWAMF_DATAIO_INVALID_CONTENTS;
                    }
                    data.push_back(entry);
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename T>
            // static int importList(const string &fileName, vector<T> &data, int (*parse_entry)(T *value, const string &str) = to_number<T>, const string &dataPrefix = "", const string &ignorePrefix = "#")
            static int importList(const string &fileName, vector<T> &data, const function<int(T*, const string&)> &parse_entry = to_number<T>, const string &dataPrefix = "", const string &ignorePrefix = "#")
            {
                ifstream input(fileName.c_str());
                if (! input.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                return importList(input, data, parse_entry, dataPrefix, ignorePrefix);
            }

            template<typename T>
            static int importListRaw(const string &fileName, std::vector<T> &data, bool reverseByteOrder = false)
            {
                FILE *file = fopen(fileName.c_str(), "r");
                if (file == NULL)
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                int r;
                while (1)
                {
                    T nextElement;
                    r = readEntryRaw(file, nextElement, reverseByteOrder);
                    if (r == DWAMF_DATAIO_SUCCESS)
                    {
                        data.push_back(nextElement);
                    }
                    else
                    {
                        // reached end of file
                        // or received error code
                        break;
                    }

                }

                if (r < 0) // an error occured
                {
                    STDLOG.printf_err("Aborting import of raw list after %ld entries.\n", data.size());
                }
                else
                {
                    r = DWAMF_DATAIO_SUCCESS;
                }

                // char nextElement[sizeof(T)];
                // int c = fgetc(file);
                // while (c != EOF)
                // {
                //     nextElement[0] = c;
                //     for (size_t i = 1; i < sizeof(T); ++i)
                //     {
                //         c = fgetc(file);
                //         if (c == EOF)
                //         {
                //             STDLOG.printf_err("End of file '%s' reached while reading in a single entry, indicating a potential file corruption.\n", fileName.c_str());
                //             return DWAMF_DATAIO_INVALID_CONTENTS;
                //         }
                //         nextElement[i] = c;
                //     }
                //     data.push_back(*(T*) nextElement);
                //
                //     c = fgetc(file);
                // }
                fclose(file);

                return r;
            }

            template<typename T>
            static int importListRaw(const string &fileName, std::forward_list<T> &list)
            {
                typename std::forward_list<T>::iterator iter = list.before_begin();

                FILE *file = fopen(fileName.c_str(), "r");
                if (file == NULL)
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                char nextElement[sizeof(T)];
                int c = fgetc(file);
                while (c != EOF)
                {
                    nextElement[0] = c;
                    for (size_t i = 1; i < sizeof(T); ++i)
                    {
                        c = fgetc(file);
                        if (c == EOF)
                        {
                            STDLOG.printf_err("End of file '%s' reached while reading in a single entry, indicating a potential file corruption.\n", fileName.c_str());
                            return DWAMF_DATAIO_INVALID_CONTENTS;
                        }
                        nextElement[i] = c;
                    }
                    iter = list.insert_after(iter, *(T*) nextElement);

                    c = fgetc(file);
                }
                fclose(file);

                return DWAMF_DATAIO_SUCCESS;
            }

            /**
             * Load a hashed map of key-value pairs from
             * an input stream.
             * This calls importList to load a list of key-value pairs
             * and subsequently inserts them into to target map.
             * Hence the parse_entry function receives an individual
             * entry (line of text from the file) and should extract
             * the key and value for that entry. It should return 0 for
             * successful conversions or an error code otherwise.
             * See also importList.
             */
            template<typename Key, typename Value>
            static int importMap(istream &input, unordered_map<Key, Value> &map, const function<int(std::pair<Key, Value>*, const string&)> &parse_entry, const string &entryPrefix = "", const string &ignorePrefix = "#")
            {
                vector< std::pair<Key, Value> > entryList;
                int r = DataIO::importList(input, entryList, parse_entry, entryPrefix, ignorePrefix);
                if (r == DWAMF_DATAIO_SUCCESS)
                {
                    for (auto &entry : entryList)
                    {
                        map[entry.first] = entry.second;
                    }

                    return DWAMF_DATAIO_SUCCESS;
                }
                else
                {
                    STDLOG.printf_err("Failed to import map: invalid list of entries.\n");
                    return r;
                }
            }

            template<typename Key, typename Value>
            static int importMap(const string &fileName, unordered_map<Key, Value> &map, const function<int(std::pair<Key, Value>*, const string&)> &parse_entry, const string &entryPrefix = "", const string &ignorePrefix = "#")
            {
                ifstream input(fileName.c_str());
                if (! input.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                return importMap(input, map, parse_entry, entryPrefix, ignorePrefix);
            }

            template<typename T, int ROWS, int COLS>
            static int importMatrix(istream &input, Eigen::Matrix<T, ROWS, COLS> &data, int (*parse_entry)(T *value, const string &str) = to_number<T>, const string &dataPrefix = "", const string &ignorePrefix = "#")
            {
                vector<vector<T>> rawData;

                size_t maxNumCols = 0;
                string line;

                const size_t dataPrefixLength = dataPrefix.length();
                const size_t ignorePrefixLength = ignorePrefix.length();

                while (! input.eof() && getline(input, line))
                {
                    // std::cout << "next line: '" << line << "'" << std::endl;
                    const size_t length = line.length();
                    size_t offset = 0;

                    if (dataPrefixLength > 0 && (length <= dataPrefixLength || line.substr(0, dataPrefixLength) != dataPrefix))
                    {
                        // line does not carry data prefix
                        // std::cout << "    (no data)" << std::endl;
                        continue;
                    }
                    else if (ignorePrefixLength > 0 && length >= ignorePrefixLength && line.substr(0, ignorePrefixLength) == ignorePrefix)
                    {
                        // line carries ignore prefix
                        // std::cout << "    (ignore)" << std::endl;
                        continue;
                    }
                    else
                    {
                        offset += dataPrefixLength;
                    }

                    vector<T> rawCol;
                    while (1)
                    {
                        // skip leading whitespaces
                        while (offset < length && isblank(line[offset]))
                        {
                            ++offset;
                        }
                        if (offset >= length || line[offset] == '#' || line[offset] == '!')
                        {
                            // end of line reached
                            if (rawCol.size() > 0)
                                rawData.push_back(rawCol);

                            if (rawCol.size() > maxNumCols)
                                maxNumCols = rawCol.size();

                            // std::cout << "    contains " << rawCol.size() << " columns" << std::endl;
                            break;
                        };

                        size_t valueFrom = offset;
                        ++offset;
                        while (offset < length && ! isblank(line[offset]))
                        {
                            ++offset;
                        }
                        size_t valueTo = offset;

                        int status;
                        T cellValue;
                        if ( (status = parse_entry(&cellValue, line.substr(valueFrom, valueTo-valueFrom))) )
                        {
                            STDLOG.printf_err("Invalid entry '%s' at cell %ld,%ld (error code: %d).\n", line.substr(valueFrom, valueTo-valueFrom).c_str(), rawData.size(), rawCol.size(), status);
                            return DWAMF_DATAIO_INVALID_CONTENTS;
                        }
                        rawCol.push_back(cellValue);
                    }
                }
                // std::cout << "TOTAL rows=" << rawData.size() << ", MAX cols=" << maxNumCols << std::endl;
                data.resize(rawData.size(), maxNumCols);

                size_t row, col;
                for (row = 0; row < rawData.size(); ++row)
                {
                    for (col = 0; col < rawData[row].size(); ++col)
                    {
                        data(row, col) = rawData[row][col];
                    }
                    for (; col < maxNumCols; ++col)
                    {
                        data(row, col) = NAN;
                    }
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename T, int ROWS, int COLS>
            static int importMatrix(const string &fileName, Eigen::Matrix<T, ROWS, COLS> &data, int (*parse_entry)(T *value, const string &str) = to_number<T>, const string &dataPrefix = "", const string &ignorePrefix = "#")
            {
                ifstream input(fileName.c_str());
                if (! input.good())
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                return importMatrix(input, data, parse_entry, dataPrefix, ignorePrefix);
            }

            template<typename OutType, int ROWS, int COLS, typename InType = OutType>
            static int importMatrixIDX(const string &fileName, Eigen::Matrix<OutType, ROWS, COLS> &data, const function<OutType(InType)> &parse_entry = [](InType x) -> OutType {
                return (OutType) x;
            })
            {
                if (ROWS == 1 || COLS == 1)
                {
                    boost::multi_array<OutType, 1> arrData;
                    int r = importMultiArrayIDX(fileName, arrData, parse_entry);
                    if (r == DWAMF_DATAIO_SUCCESS)
                    {
                        if (ROWS == 1)
                        {
                            data.resize(1, arrData.size());
                        }
                        else
                        {
                            data.resize(arrData.size(), 1);
                        }
                        memcpy(data.data(), arrData.data(), sizeof(OutType) * arrData.size());
                        return DWAMF_DATAIO_SUCCESS;
                    }
                    else
                    {
                        STDLOG.printf_err("Failed to load matrix from IDX file '%s'.\n", fileName.c_str());
                        return r;
                    }
                }
                else
                {
                    boost::multi_array<OutType, 2> arrData;
                    int r = importMultiArrayIDX(fileName, arrData, parse_entry);
                    if (r == DWAMF_DATAIO_SUCCESS)
                    {
                        data.resize(arrData.shape()[0], arrData.shape()[1]);
                        for (long int j = 0; j < data.cols(); ++j)
                        {
                            for (long int i = 0; i < data.rows(); ++i)
                            {
                                data(i, j) = arrData[i][j];
                            }
                        }
                        return DWAMF_DATAIO_SUCCESS;
                    }
                    else
                    {
                        STDLOG.printf_err("Failed to load matrix from IDX file '%s'.\n", fileName.c_str());
                        return r;
                    }
                }
            }

            template<typename T, int ROWS, int COLS>
            static int importMatrixRaw(const string &fileName, Eigen::Matrix<T, ROWS, COLS> &data)
            {
                FILE *file = fopen(fileName.c_str(), "r");
                if (file == NULL)
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                size_t inputSize = sizeof(T) * data.size();
                char *buffer = (char*) data.data();
                size_t i;
                for (i = 0; i < inputSize; ++i)
                {
                    int c = fgetc(file);
                    if (c == EOF)
                    {
                        STDLOG.printf_err("End of file reached before all matrix entries could be filled, indicating a potential file corruption.\n", fileName.c_str());
                        return DWAMF_DATAIO_INVALID_CONTENTS;
                    }
                    buffer[i] = c;
                }
                if (fgetc(file) != EOF)
                {
                    STDLOG.printf_wrn("All matrix entries have been filled without reaching the end of the file '%s', indicating a potential file corruption.\n", fileName.c_str());
                }
                fclose(file);

                return DWAMF_DATAIO_SUCCESS;
            }

            template<typename OutType, size_t N, typename InType = OutType>
            static int importMultiArrayIDX(const string &fileName, boost::multi_array<OutType, N> &data, const function<OutType(InType)> &parse_entry = [](InType x) -> OutType {
                return (OutType) x;
            })
            {
                FILE *file = fopen(fileName.c_str(), "r");
                if (file == NULL)
                {
                    STDLOG.printf_err("The provided file '%s' is not accessible.\n", fileName.c_str());
                    fclose(file);
                    return DWAMF_DATAIO_FILE_NOT_ACCESSIBLE;
                }

                int c1 = fgetc(file);
                int c2 = fgetc(file);
                if (c1 != 0 || c2 != 0)
                {
                    STDLOG.printf_err("The provided file '%s' does not seem to be in the IDX format: Invalid magic number.\n", fileName.c_str());
                    fclose(file);
                    return DWAMF_DATAIO_INVALID_CONTENTS;
                }

                int type = fgetc(file);
                size_t entryLength;
                switch (type)
                {
                    case 0x08:
                    case 0x09:
                        entryLength = 1;
                        break;
                    case 0x0B:
                        entryLength = 2;
                        break;
                    case 0x0C:
                    case 0x0D:
                        entryLength = 4;
                        break;
                    case 0x0E:
                        entryLength = 8;
                        break;
                    default:
                        STDLOG.printf_err("The provided file '%s' does not seem to be in the IDX format: Invalid data type code '%d'.\n", fileName.c_str(), type);
                        fclose(file);
                        return DWAMF_DATAIO_INVALID_CONTENTS;
                }
                if (entryLength != sizeof(InType))
                {
                    STDLOG.printf_err("The size of the input data type (%ld) does not match the size of the entries in the IDX file (%ld).\n", sizeof(InType), entryLength);
                    fclose(file);
                    return DWAMF_DATAIO_INVALID_CONTENTS;
                }

                int numDims = fgetc(file);
                if (numDims < 0 || (size_t) numDims < N)
                {
                    STDLOG.printf_err("The data dimension %d of the provided file '%s' does not match the expected dimension %ld.\n", numDims, fileName.c_str(), N);
                }
                else if ((size_t) numDims > N)
                {
                    STDLOG.printf_inf("The data of the provided file '%s' has %d dimensions, but the target is expected to have only %ld dimensions. Higher dimensions will be flattened.\n", fileName.c_str(), numDims, N);
                }

                // read in dimensions
                boost::array<typename boost::multi_array<OutType, N>::index, N> dims;
                size_t numEntries = 1;
                for (size_t k = 0; k < (size_t) numDims; ++k)
                {
                    int nextDim;
                    if (readEntryRaw(file, nextDim, true) == DWAMF_DATAIO_SUCCESS)
                    {
                        if (k < N)
                        {
                            dims[k] = nextDim;
                        }
                        else
                        {
                            dims[N-1] *= nextDim;
                        }
                        numEntries *= nextDim;
                        // cout << "dim " << k << ": " << nextDim << endl;
                    }
                    else
                    {
                        STDLOG.printf_err("Error while reading in dimension %d of the IDX file '%s'.\n", k, fileName.c_str());
                        fclose(file);
                        return DWAMF_DATAIO_INVALID_CONTENTS;
                    }
                }

                data.resize(dims);

                // read in entries
                for (size_t i = 0; i < numEntries; ++i)
                {
                    InType entry;
                    if (readEntryRaw(file, entry, true) == DWAMF_DATAIO_SUCCESS)
                    {
                        *(data.data() + i) = parse_entry(entry);
                    }
                    else
                    {
                        STDLOG.printf_err("Error while reading entry %ld of the IDX file '%s'.\n", i, fileName.c_str());
                        fclose(file);
                        return DWAMF_DATAIO_INVALID_CONTENTS;
                    }
                }
                if (fgetc(file) != EOF)
                {
                    STDLOG.printf_wrn("All entries have been filled without reaching the end of the file '%s', indicating a potential file corruption.\n", fileName.c_str());
                }
                fclose(file);

                return DWAMF_DATAIO_SUCCESS;
            }

            static int importText(istream &input, vector<string> &lines)
            {
                if (! input.good())
                {
                    STDLOG.printf_err("The input stream is not good.\n");
                    return DWAMF_DATAIO_INVALID_CONTENTS;
                }

                string line;
                while (! input.eof() && getline(input, line))
                {
                    lines.push_back(line);
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            static int importText(const string &fileName, vector<string> &lines)
            {
                ifstream input(fileName.c_str());
                return importText(input, lines);
            }

            static int importText(istream &input, string &data)
            {
                vector<string> lines;
                int r = importText(input, lines);
                if (r != DWAMF_DATAIO_SUCCESS)
                    return r;

                for (auto &line : lines)
                {
                    data += line + "\n";
                }

                return DWAMF_DATAIO_SUCCESS;
            }

            static int importText(const string &fileName, string &data)
            {
                ifstream input(fileName.c_str());
                return importText(input, data);
            }
    };
};

#endif
