/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Tool Kit: Statistics
 */

#ifndef _DWAMF_STATISTICS_H
#define _DWAMF_STATISTICS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

// #ifdef __cplusplus
// extern "C" {
// #endif

namespace dwamf
{
    const int STAT_HIST_PDF = 0;
    const int STAT_HIST_RELFREQ = 1;
    const int STAT_HIST_ABSFREQ = 2;
    const int STAT_HIST_MODE_LOG = 8;

    /**
     * Extract minimum and maximum values of the
     * data set @data consisting of @numData points,
     * and write results to @*min and @*max, respectively.
     */
    void stat_minmax(double *min, double *max, const double *data, const size_t numData);

    /**
     * Extract the minimum or maximum value from the
     * data set @data consisting of @numData points.
     */
    double stat_max(const double *data, const size_t numData);
    double stat_min(const double *data, const size_t numData);

    /**
     * Determine the median of the data set @data
     * consisting of @numData points.
     */
    double stat_median(const double *data, const size_t numData);

    /**
     * Compute the statistical mean value of the data set
     * @data consisting of @numData points.
     */
    double stat_mean(const double *data, const size_t numData);

    /**
     * Compute the statistical empirical variance of the data set
     * @data consisting of @numData points.
     */
    double stat_variance_wm(const double *data, const size_t numData, const double mean);
    double stat_variance(const double *data, const size_t numData);

    /**
     * Compute the statistical empirical covariance of two data sets
     * @data1 and @data2 consisting of @numData points each.
     */
    double stat_covariance_wm(const double *data1, const double *data2, const size_t numData, const double mean1, const double mean2);
    double stat_covariance(const double *data1, const double *data2, const size_t numData);

    /**
     * Compute the empirical covariance matrix of @numVars random variables
     * from the @numData samples @data and store it to @covMat (row-wise).
     * If @covMat is NULL, memory to store the covariance matrix elements
     * will be malloc'd.
     * Return the pointer to the covariance matrix.
     */
    double *stat_covariance_matrix(double *covMat, const double **data, const size_t numVars, const size_t numData);

    /**
     * Compute the statistical empirical centralized moment
     * of order @order of the data set
     * @data consisting of @numData points.
     */
    double stat_cmoment_wm(const int order, const double *data, const size_t numData, const double mean);
    double stat_cmoment(const int order, const double *data, const size_t numData);

    /**
     * Compute the statistical empirical moment of order @order
     * of the data set @data consisting of @numData points.
     */
    double stat_moment(const int order, const double *data, const size_t numData);

    /**
     * Compute an empirical estimate of the normalized
     * autocorrelation at time @time from the data
     * set @data consisting of @numData points.
     */
    double stat_autocorr_wmv(const size_t time, const double *data, const size_t numData, const double mean, const double variance);
    double stat_autocorr(const size_t time, const double *data, const size_t numData);

    /**
     * Compute the normalized autocorrelation function
     * for a given data set @data consisting of
     * @numData points up to a maximum time @maxTime.
     * The result is stored in @acfunc.
     */
    void stat_autocorr_function(double *acfunc, const size_t maxTime, const double *data, const size_t numData);

    /**
     * Estimate the integrated autocorrelation time
     * of the data set @data consisting of @numData
     * points by means of the standard estimator
     *   IAT = 0.5 + sum autocorr(t) from t=1 to n ,
     * where n is the minimum of @tMax and the time t
     * for which autocorr(t) first lies below the
     * @threshold.
     */
    double stat_iat(const double *data, const size_t numData, size_t tMax, const double threshold);

    /**
     * Estimate the integrated autocorrelation time
     * of the data set @data consisting of @numData
     * points by a binning analysis.
     * The algorithm collects data points in bins of
     * increasing size up to the maximum size @maxBinSize
     * and looks for a plateau where the change from
     * one size step to the next is below the @threshold.
     * The ratio of variances of the mean of the binned and
     * unbinned data converges to the integrated autocorrelation
     * time in the limit of infinite bin size.
     */
    double stat_iat_binning(const double *data, const size_t numData, const size_t maxBinSize, const double threshold);

    /**
     * Perform a binning analysis on the provided @data
     * consisting of @numData points by creating @numBins
     * sequential subsets, averaging within these and
     * storing the results in @bins.
     */
    void stat_binning(double *bins, const size_t numBins, const double *data, const size_t numData);

    /**
     * Compute jackknife estimators for an observable @fobs
     * from a sample @data of @numData points.
     * Both the jackknife mean and jackknife variances
     * are computed and stored to @jkMean and @jkVar,
     * respectively.
     * The observable is a function taking a data point
     * and returning the corresponding observable' value.
     */
    void stat_jackknife(double *jkMean, double *jkVar, double (*fobs)(const double), const double *data, const size_t numData);

    /**
     * Create a histogram from a given data set @data
     * of length @numData using @numBins bins.
     * The bin mean is stored to @binMean, the relative
     * number of entries in each bin in @relCount.
     * The analysis may be restricted to data points in the
     * interval from @minValue to @maxValue.
     */
    void stat_hist(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData);
    void stat_hist_wmode(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData, const int mode);
    void stat_hist_wrange(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData, const double minValue, const double maxValue);
    void stat_hist_wrange_wmode(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData, const double minValue, const double maxValue, const int mode);

    /**
     * Create a histogram from a given data set @data
     * of length @numData using @numClasses classes (bins),
     * and save it to the file @fileName.
     *
     * See also documentation of stat_hist(...).
     */
    void stat_hist_to_file(const char *fileName, const double *data, const size_t numData, const size_t numClasses);
    void stat_hist_to_file_wmode(const char *fileName, const double *data, const size_t numData, const size_t numClasses, const int mode);
    void stat_hist_to_file_wrange(const char *fileName, const double *data, const size_t numData, const size_t numBins, const double minValue, const double maxValue);
    void stat_hist_to_file_wrange_wmode(const char *fileName, const double *data, const size_t numData, const size_t numBins, const double minValue, const double maxValue, const int mode);

    /**
     * Create a histogram from a given data set @data
     * of length @numData using a resolution @resolution.
     * Save binned data to the file @fileName.
     */
    void stat_hist_to_file_wres(const char *fileName, const double *data, const size_t numData, const double resolution);



    /**
     * Create a 2D histogram from a given set of x-
     * and y-data, @dataX and @dataY, respectively,
     * consisting of @numData points. The data will
     * be distributed over @numBins square bins
     * in the xy-plane.
     * The central bin coordinates are written to
     * the arrays @binX and @binY, respectively.
     * The relative number of data points per bin
     * is stored to @rbc.
     */
    void stat_hist2d(double *binX, double *binY, double *rbc, const size_t numBins, const double *dataX, const double *dataY, const size_t numData);

    /**
     * Create a 2D histogram from the points in the
     * given range [@minX, @maxX] x [@minY, @maxY].
     * See also "stat_hist2d".
     */
    void stat_hist2d_wrange(double *binX, double *binY, double *rbc, const size_t numBins, const double *dataX, const double *dataY, const size_t numData, const double minX, const double maxX, const double minY, const double maxY);

    /**
     * Create a 2D histogram from a given set of x-
     * and y-data, @dataX and @dataY, respectively,
     * consisting of @numData points. The data will
     * be distributed over @numBins square bins
     * in the xy-plane.
     * Save the histogram to the file @fileName.
     */
    void stat_hist2d_to_file(const char *fileName, const double *dataX, const double *dataY, const size_t numData, const size_t numBins);

    /**
     * Create a 2D histogram from the points in the
     * given range [@minX, @maxX] x [@minY, @maxY]
     * and write it to the file @fileName.
     * See also "stat_hist2d_to_file".
     */
    void stat_hist2d_to_file_wrange(const char *fileName, const double *dataX, const double *dataY, const size_t numData, const size_t numBins, const double minX, const double maxX, const double minY, const double maxY);
}


// #ifdef __cplusplus
// }
// #endif

#endif /* _DWAMF_STATISTICS_H */
