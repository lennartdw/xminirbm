/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "statistics.hpp"
 */
#include "statistics.hpp"

#define util_cmp(_type_, _a_, _b_) (*(_type_*) (_a_) - *(_type_*) (_b_))

int util_cmpc(const void *a, const void *b)
{
    return util_cmp(char, a, b);
}

int util_cmpd(const void *a, const void *b)
{
    return util_cmp(double, a, b);
}

int util_cmpf(const void *a, const void *b)
{
    return util_cmp(float, a, b);
}

int util_cmpi(const void *a, const void *b)
{
    return util_cmp(int, a, b);
}

int util_cmps(const void *a, const void *b)
{
    return strcmp((char*) a, (char*) b);
}

void util_qsortd(double *data, const size_t numData)
{
    if (numData <= 1) return;

    double pivot = data[0];
    double *left = data+1;
    double *right = data+numData-1;

    while (left < right)
    {
        while (left < right && *left <= pivot) ++left;
        while (left < right && *right >= pivot) --right;

        if (left < right)
        {
            double tmp = *left;
            *left = *right;
            *right = tmp;
            ++left;
            --right;
        }
    }

    if (*left > pivot)
    {
        --left;
        --right;
    }

    data[0] = *left;
    *left = pivot;

    util_qsortd(data, left - data);
    util_qsortd(right+1, numData - (right-data) - 1);
}

void util_qsortd_wcompd(double *data, double *comp, const size_t numData)
{
    if (numData <= 1) return;
    //printf("quicksort on %ld data\n", numData);
    double pivot = data[0];
    double *left = data+1;
    double *right = data+numData-1;

    while (left < right)
    {
        while (left < right && *left <= pivot) ++left;
        while (left < right && *right >= pivot) --right;

        if (left < right)
        {
            // swap elements
            double tmp = *left;
            *left = *right;
            *right = tmp;

            // swap elements in companion
            tmp = comp[left-data];
            comp[left-data] = comp[right-data];
            comp[right-data] = tmp;

            ++left;
            --right;
        }
    }

    if (*left > pivot)
    {
        --left;
        --right;
    }

    // swap pivot to border
    data[0] = *left;
    *left = pivot;

    double tmp = comp[0];
    comp[0] = comp[left-data];
    comp[left-data] = tmp;

    util_qsortd_wcompd(data, comp, left - data);
    util_qsortd_wcompd(right+1, comp + (right-data) + 1, numData - (right-data) - 1);
}

void dwamf::stat_minmax(double *min, double *max, const double *data, const size_t numData)
{
    double maxValue = -INFINITY;
    double minValue = INFINITY;
    size_t i;
    for (i = 0; i < numData; ++i)
    {
        if (data[i] > maxValue) maxValue = data[i];
        if (data[i] < minValue) minValue = data[i];
    }

    *min = minValue;
    *max = maxValue;
}

double dwamf::stat_max(const double *data, const size_t numData)
{
    double minValue, maxValue;
    dwamf::stat_minmax(&minValue, &maxValue, data, numData);
    return maxValue;
}

double dwamf::stat_min(const double *data, const size_t numData)
{
    double minValue, maxValue;
    dwamf::stat_minmax(&minValue, &maxValue, data, numData);
    return minValue;
}

double dwamf::stat_median(const double *data, const size_t numData)
{
    if (numData <= 0) return NAN;
    if (numData == 1) return data[0];

    double *sortedData = (double*) malloc(sizeof(double) * numData);
    memcpy(sortedData, data, sizeof(double) * numData);

    qsort(sortedData, numData, sizeof(double), &util_cmpd);

    double median;
    if (numData % 2 == 0)
    {
        median = 0.5 * (sortedData[numData/2 - 1] + sortedData[numData/2]);
    }
    else
    {
        median = sortedData[numData/2];
    }

    free(sortedData);
    return median;
}

double dwamf::stat_mean(const double *data, const size_t numData)
{
    double sum = 0.0;
    size_t i;
    for (i = 0; i < numData; ++i) sum += data[i];
    return sum / numData;
}

double dwamf::stat_variance_wm(const double *data, const size_t numData, const double mean)
{
    double sum = 0.0;
    size_t i;
    for (i = 0; i < numData; ++i)
    {
        double dx = data[i] - mean;
        sum += dx*dx;
    }
    return sum / (numData - 1);
}

double dwamf::stat_variance(const double *data, const size_t numData)
{
    return dwamf::stat_variance_wm(data, numData, stat_mean(data, numData));
}

double dwamf::stat_covariance_wm(const double *data1, const double *data2, const size_t numData, const double mean1, const double mean2)
{
    double sum = 0.0;
    size_t dIndex;
    for (dIndex = 0; dIndex < numData; ++dIndex)
    {
        sum += (data1[dIndex] - mean1) * (data2[dIndex] - mean2);
    }
    return sum / (numData-1);
}

double dwamf::stat_covariance(const double *data1, const double *data2, const size_t numData)
{
    return dwamf::stat_covariance_wm(data1, data2, numData, dwamf::stat_mean(data1, numData), dwamf::stat_mean(data2, numData));
}

double *dwamf::stat_covariance_matrix(double *covMat, const double **data, const size_t numVars, const size_t numData)
{
    if (covMat == NULL)
    {
        covMat = (double*) malloc(sizeof(double) * numVars*numVars);
    }
    double *means = (double*) malloc(sizeof(double) * numVars);

    size_t i, j;
    for (i = 0; i < numVars; ++i)
    {
        means[i] = dwamf::stat_mean(data[i], numData);
    }
    for (i = 0; i < numVars; ++i)
    {
        for (j = i; j < numVars; ++j)
        {
            double sum = 0.0;
            size_t dIndex;
            for (dIndex = 0; dIndex < numData; ++dIndex)
            {
                sum += (data[i][dIndex] - means[i]) * (data[j][dIndex] - means[j]);
            }
            covMat[i*numVars + j] = sum / (numData-1);
            if (i != j) covMat[j*numVars + i] = covMat[i*numVars + j];
        }
    }
    return covMat;
}

double dwamf::stat_cmoment_wm(const int order, const double *data, const size_t numData, const double mean)
{
    double sum = 0.0;
    size_t i;
    for (i = 0; i < numData; ++i)
    {
        double dx = data[i] - mean;
        sum += pow(dx, order);
    }
    double biasCorrect = pow(numData, order-2);
    for (i = 1; i < (size_t) order; ++i)
    {
        biasCorrect /= (double) (numData - i);
    }
    return sum * biasCorrect;
}

double dwamf::stat_cmoment(const int order, const double *data, const size_t numData)
{
    return dwamf::stat_cmoment_wm(order, data, numData, dwamf::stat_mean(data, numData));
}

double dwamf::stat_moment(const int order, const double *data, const size_t numData)
{
    double sum = 0.0;
    size_t i;
    for (i = 0; i < numData; ++i)
    {
        sum += pow(data[i], order);
    }
    return sum / numData;
}

double dwamf::stat_autocorr_wmv(const size_t time, const double *data, const size_t numData, const double mean, const double variance)
{
    if (time >= numData || variance == 0.0) return 0.0;

    double sum = 0.0;
    size_t i;
    for (i = 0; i < numData - time; ++i)
    {
        sum += (data[i] - mean) * (data[i+time] - mean);
    }
    return sum / variance / (numData - time);
}

double dwamf::stat_autocorr(const size_t time, const double *data, const size_t numData)
{
    double mean = dwamf::stat_mean(data, numData);
    double variance = dwamf::stat_variance_wm(data, numData, mean);
    return dwamf::stat_autocorr_wmv(time, data, numData, mean, variance);
}

void dwamf::stat_autocorr_function(double *acfunc, const size_t maxTime, const double *data, const size_t numData)
{
    double mean = dwamf::stat_mean(data, numData);
    double var = dwamf::stat_variance_wm(data, numData, mean);
    printf("mean: %e\n", mean);
    printf("var: %e\n", var);

    size_t time;
    for (time = 0; time < maxTime; ++time)
    {
        acfunc[time] = dwamf::stat_autocorr_wmv(time, data, numData, mean, var);
        printf("acf %ld: %e\n", time, acfunc[time]);
    }
}

double dwamf::stat_iat(const double *data, const size_t numData, size_t tMax, const double threshold)
{
    if (tMax > numData) tMax = numData;

    double mean = dwamf::stat_mean(data, numData);
    double var = dwamf::stat_variance_wm(data, numData, mean);

    double sum = 0.0;
    size_t i;
    for (i = 1; i < tMax; ++i)
    {
        double ac = dwamf::stat_autocorr_wmv(i, data, numData, mean, var);
        if (ac < threshold) break;

        sum += ac;
    }
    return sum + 0.5;
}

double dwamf::stat_iat_binning(const double *data, const size_t numData, const size_t maxBinSize, const double threshold)
{
    double var = dwamf::stat_variance(data, numData);
    if (var == 0.0) return 0.0;

//    double iatResults[(int) (log(maxBinSize) / log(2))];
//    int index = 0;
    size_t binSize = 1;
    double lastIAT = 0.0;
    while (binSize <= maxBinSize)
    {
        size_t numBins = numData / binSize;
        if (numBins * binSize < numData) numBins += 1;

        // create bins
        double *bins = (double*) malloc(sizeof(double) * numBins);
        dwamf::stat_binning(bins, numBins, data, numData);

        // estimate integrated autocorrelation time
        // by the ratio of variances of the mean
        // of the binned and unbinned data
        double iat = dwamf::stat_variance(bins, numBins) / var * binSize;
//        iatResults[index++] = iat;

        if (iat - lastIAT < threshold)
        {
            // plateau has been reached
            // printf("plateau %f, %f, %f reached after %d steps\n", lastIAT, iat, threshold, index);
//            plot_y(iatResults, index);
            return lastIAT;
        }
        lastIAT = iat;

        // free memory and increase bin size
        free(bins);
        binSize *= 2;
    }

//    plot_y(iatResults, index);
    return lastIAT;
}

void dwamf::stat_binning(double *bins, const size_t numBins, const double *data, const size_t numData)
{
    if (numBins > numData)
    {
        // copy data to bins and fill up with zeros
        memcpy(bins, data, sizeof(double) * numData);
        memset(bins, 0, sizeof(double) * (numBins - numData));
        return;
    }

    size_t binSize = numData / numBins;
    size_t i;
    for (i = 0; i < numBins - 1; ++i)
    {
        bins[i] = dwamf::stat_mean(data + i*binSize, binSize);
    }
    if (numBins * binSize < numData) // last bin is smaller
    { // rescale bin size
        binSize = numData - numBins * binSize;
    }
    bins[numBins-1] = dwamf::stat_mean(data + numData - binSize, binSize);
}

void dwamf::stat_jackknife(double *jkMean, double *jkVar, double (*fobs)(const double), const double *data, const size_t numData)
{
    /** TODO: CAUTION Untested algorithm. */

    double mean = dwamf::stat_mean(data, numData);
    const size_t numDataM1 = numData - 1;

    // compute jacknife data
    double *jkData = (double*) malloc(sizeof(double) * numData);
    size_t i;
    for (i = 0; i < numData; ++i)
    {
        jkData[i] = fobs((mean*numData - data[i]) / numDataM1);
    }

    // calculate estimators
    *jkMean = dwamf::stat_mean(jkData, numData);
    *jkVar = dwamf::stat_variance_wm(jkData, numData, *jkMean) * numDataM1 * numDataM1 / numData;

    // free memory
    free(jkData);
}

void dwamf::stat_hist_wrange_wmode(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData, const double minValue, const double maxValue, const int mode)
{
    // count values per bin
    double valueSpan = maxValue - minValue;
    if (valueSpan == 0.0) valueSpan = 1.0;

    // init bins
    size_t i;
    for (i = 0; i < numBins; ++i)
    {
        relCount[i] = 0.0;
        binMean[i] = minValue + (i+0.5) * valueSpan / numBins;
    }

    double pointWeight;
    if (mode & STAT_HIST_RELFREQ)
    {
        pointWeight = 1.0 / numData; // relative frequencies
    }
    else if (mode & STAT_HIST_ABSFREQ)
    {
        pointWeight = 1.0; // absolute frequencies
    }
    else
    {
        pointWeight = numBins / (numData * valueSpan); // empirical probability density function
    }

    for (i = 0; i < numData; ++i)
    {
        size_t bin = (size_t) ((data[i] - minValue) * numBins / valueSpan);
        if (0 <= bin && bin < numBins)
        {
            relCount[bin] += pointWeight;
        }
        else if (bin == numBins && data[i] == maxValue) // value is maximum (right boundary), put in right-most bin
        {
            relCount[numBins-1] += pointWeight;
        }

    }
}

void dwamf::stat_hist_wrange(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData, const double minValue, const double maxValue)
{
    dwamf::stat_hist_wrange_wmode(binMean, relCount, numBins, data, numData, minValue, maxValue, 0);
}

void dwamf::stat_hist_wmode(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData, const int mode)
{
    // determine minimum and maximum values
    double minValue, maxValue;
    dwamf::stat_minmax(&minValue, &maxValue, data, numData);

    dwamf::stat_hist_wrange_wmode(binMean, relCount, numBins, data, numData, minValue, maxValue, mode);
}

void dwamf::stat_hist(double *binMean, double *relCount, const size_t numBins, const double *data, const size_t numData)
{
    dwamf::stat_hist_wmode(binMean, relCount, numBins, data, numData, 0);
}

void __stat_hist_to_file(const char *fileName, const double *binMean, const double *relCount, const size_t numBins, int mode)
{
    FILE *file = fopen(fileName, "w");
    if (file == NULL)
    {
        // TODO: ERROR File not accessible.
        return;
    }
    fprintf(file, "# bin\tmean\trel_count\n");
    size_t i;
    for (i = 0; i < numBins; ++i)
    {
        fprintf(file, "%ld\t%e\t%e", i, binMean[i], relCount[i]);
        if (mode & dwamf::STAT_HIST_MODE_LOG)
        {
            fprintf(file, "\t%e", log(relCount[i]));
        }
        fprintf(file, "\n");
    }

    fclose(file);
}

void dwamf::stat_hist_to_file_wmode(const char *fileName, const double *data, const size_t numData, const size_t numClasses, const int mode)
{
    double *hist = (double*) malloc(sizeof(double) * numClasses);
    double *classMean = (double*) malloc(sizeof(double) * numClasses);

    dwamf::stat_hist_wmode(classMean, hist, numClasses, data, numData, mode);

    // save to file
    __stat_hist_to_file(fileName, classMean, hist, numClasses, mode);

    free(hist);
    free(classMean);
}

void dwamf::stat_hist_to_file(const char *fileName, const double *data, const size_t numData, const size_t numClasses)
{
    dwamf::stat_hist_to_file_wmode(fileName, data, numData, numClasses, 0);
}

void dwamf::stat_hist_to_file_wrange_wmode(const char *fileName, const double *data, const size_t numData, const size_t numBins, const double minValue, const double maxValue, const int mode)
{
    double *hist = (double*) malloc(sizeof(double) * numBins);
    double *binMean = (double*) malloc(sizeof(double) * numBins);

    dwamf::stat_hist_wrange_wmode(binMean, hist, numBins, data, numData, minValue, maxValue, mode);

    // save to file
    __stat_hist_to_file(fileName, binMean, hist, numBins, mode);

    free(hist);
    free(binMean);
}

void dwamf::stat_hist_to_file_wrange(const char *fileName, const double *data, const size_t numData, const size_t numBins, const double minValue, const double maxValue)
{
    dwamf::stat_hist_to_file_wrange_wmode(fileName, data, numData, numBins, minValue, maxValue, 0);
}

void dwamf::stat_hist_to_file_wres(const char *fileName, const double *data, const size_t numData, const double resolution)
{
    // determine minimum and maximum values
    double minValue, maxValue;
    dwamf::stat_minmax(&minValue, &maxValue, data, numData);

    size_t numBins;
    if (maxValue == minValue)
    {
        numBins = 1;
    }
    else
    {
        numBins = (size_t) ((maxValue - minValue) / resolution) + 1;
    }

    double *binMean = (double*) malloc(sizeof(double) * numBins);
    double *relCount = (double*) malloc(sizeof(double) * numBins);

    dwamf::stat_hist_wrange(binMean, relCount, numBins, data, numData, minValue, maxValue);

    __stat_hist_to_file(fileName, binMean, relCount, numBins, 0);

    free(binMean);
    free(relCount);
}

void dwamf::stat_hist2d_wrange(double *binX, double *binY, double *rbc, const size_t numBins, const double *dataX, const double *dataY, const size_t numData, const double minX, const double maxX, const double minY, const double maxY)
{
    // zero counters
    size_t i;
    for (i = 0; i < numBins; ++i)
    {
        rbc[i] = 0.0;
    }

    double spanX = maxX - minX;
    if (spanX == 0.0)
    {
        spanX = 1.0;
        //minX = minX - spanX/2;
        //maxX = maxX + spanX/2;
    }
    double spanY = maxY - minY;
    if (spanY == 0.0)
    {
        spanY = 1.0;
        //minY = minY - spanY/2;
        //maxY = maxY + spanY/2;
    }

    // compute square mesh of spanned area
    // of numBins tiles
    const double binSize = sqrt(spanX * spanY / numBins);
    const size_t numBinsX = (size_t) (spanX / binSize);
    const size_t numBinsY = (size_t) (spanY / binSize);

    // count values per bin
    size_t pointsInRange = 0;
    for (i = 0; i < numData; ++i)
    {
        size_t xIndex = (size_t) ((dataX[i] - minX) * numBinsX / spanX);
        if (xIndex < 0 || xIndex > numBinsX)
            continue; // skip data point out of range
        if (xIndex == numBinsX)
            xIndex = numBinsX-1; // max value

        size_t yIndex = (size_t) ((dataY[i] - minY) * numBinsY / spanY);

        if (yIndex < 0 || yIndex > numBinsY)
            continue; // skip data point out of range
        if (yIndex == numBinsY)
            yIndex = numBinsY-1; // max value

        rbc[xIndex * numBinsY + yIndex] += 1.0;
        ++pointsInRange;
    }

    if (pointsInRange < numData)
    {
        printf("[stat] [hist2d] Warning: %ld data points outside plotting range.\n", numData - pointsInRange);
    }

    // normalization
    double integral = 0.0;
    double normalization = numData * spanX * spanY / (numBinsX * numBinsY);
    size_t xIndex, yIndex;
    for (xIndex = 0; xIndex < numBinsX; ++xIndex)
    {
        for (yIndex = 0; yIndex < numBinsY; ++yIndex)
        {
            size_t binIndex = xIndex * numBinsY + yIndex;
            binX[binIndex] = minX + (xIndex+0.5) * spanX / numBinsX;
            binY[binIndex] = minY + (yIndex+0.5) * spanY / numBinsY;
            rbc[binIndex] = rbc[binIndex] / normalization;

            integral += rbc[binIndex];
        }
    }
    integral *= (spanX / numBinsX) * (spanY / numBinsY);
    if (pointsInRange < numData)
    {
        printf("[stat] [hist2d] integral (mass in range): %f\n", integral);
    }

    // clear unused bins
    for (i = numBinsX*numBinsY; i < numBins; ++i)
    {
        binX[i] = NAN;
        binY[i] = NAN;
        rbc[i] = NAN;
    }
}

void dwamf::stat_hist2d(double *binX, double *binY, double *rbc, const size_t numBins, const double *dataX, const double *dataY, const size_t numData)
{
    // determine minimum and maximum values
    double minX, maxX;
    dwamf::stat_minmax(&minX, &maxX, dataX, numData);
    double minY, maxY;
    dwamf::stat_minmax(&minY, &maxY, dataY, numData);

    dwamf::stat_hist2d_wrange(binX, binY, rbc, numBins, dataX, dataY, numData, minX, maxX, minY, maxY);
}

void __stat_hist2d_to_file(const char *fileName, const double *binX, const double *binY, const double *rbc, const size_t numBins)
{
    FILE *file = fopen(fileName, "w");
    if (file == NULL)
    {
        // TODO: ERROR File not accessible.
        return;
    }
    fprintf(file, "# bin\tmean_x\tmean_y\trel_count\n");
    size_t i;
    for (i = 0; i < numBins; ++i)
    {
        if (! isnan(rbc[i]))
        {
            fprintf(file, "%ld\t%e\t%e\t%e\n", i, binX[i], binY[i], rbc[i]);
        }
    }

    fclose(file);
}

void dwamf::stat_hist2d_to_file_wrange(const char *fileName, const double *dataX, const double *dataY, const size_t numData, const size_t numBins, const double minX, const double maxX, const double minY, const double maxY)
{
    double *binX = (double*) malloc(sizeof(double) * numBins);
    double *binY = (double*) malloc(sizeof(double) * numBins);
    double *rbc = (double*) malloc(sizeof(double) * numBins);

    dwamf::stat_hist2d_wrange(binX, binY, rbc, numBins, dataX, dataY, numData, minX, maxX, minY, maxY);

    // save to file
    __stat_hist2d_to_file(fileName, binX, binY, rbc, numBins);

    free(binX);
    free(binY);
    free(rbc);
}

void dwamf::stat_hist2d_to_file(const char *fileName, const double *dataX, const double *dataY, const size_t numData, const size_t numBins)
{
    double *binX = (double*) malloc(sizeof(double) * numBins);
    double *binY = (double*) malloc(sizeof(double) * numBins);
    double *rbc = (double*) malloc(sizeof(double) * numBins);

    dwamf::stat_hist2d(binX, binY, rbc, numBins, dataX, dataY, numData);

    // save to file
    __stat_hist2d_to_file(fileName, binX, binY, rbc, numBins);

    free(binX);
    free(binY);
    free(rbc);
}

