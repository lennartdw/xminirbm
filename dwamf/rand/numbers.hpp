/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Tool Kit: Random numbers
 *
 * Wrapper functions for generating pseudo random numbers.
 * Different random number generators may be used with
 * the same calls by redirecting via these wrappers.
 *
 * If you would like to use the GSL random number generator,
 * consider #define-ing _RANDOM_GSL_MACROS BEFORE including
 * this header to improve performance.
 */

#ifndef _DWAMF_RANDOM_NUMBERS_H
#define _DWAMF_RANDOM_NUMBERS_H

#include <string>
// #ifdef __cplusplus
// extern "C" {
// #endif

/*****************************************************
 * SECTION Random Number Generator Dependent Routines
 *****************************************************/

namespace dwamf
{
    /**
     * Initialize the random number generator and seed it
     * with @seed.
     */
    void rand_init(unsigned long int seed, const std::string &type = "ranlxs0");

    /**
     * Terminate the random number generator.
     */
    void rand_term(void);
};

#ifdef _RANDOM_GSL_MACROS
#include "numbers-gsl-macros.hpp"
#endif

#ifndef _RANDOM_GSL_MACROS

namespace dwamf
{
    /**
     * Generates a uniformly distributed pseudo-random number
     * in the interval [0,1).
     */
    double randd();

    /**
     * Generates a uniformly distributed pseudo-random integer
     * between 0 and nMax-1 (inclusive).
     */
    unsigned long int randuli(const unsigned long int nMax);
};

#endif

/*****************************************************
 * SECTION Independent Routines
 *****************************************************/

namespace dwamf
{
    /**
     * Generate a pseudo-random standard Gaussian distributed
     * random number using the Box-Muller method.
     */
    double rand_gauss();

    /**
     * Generate an exponentially distributed random number
     * with rate parameter 1.
     */
    double rand_exp();
};

// #ifdef __cplusplus
// }
// #endif

#endif /* _DWAMF_RANDOM_NUMBERS_H */
