/**
 * Tool Kit: Random numbers
 * (c) 2016-22 Lennart Dabelow
 * 
 * This file contains all routines that are
 * independent of the specific random number generator.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "numbers.hpp"

using namespace dwamf;

double dwamf::rand_gauss()
{
    // storage for second generated number (Box-Muller)
    static double tmpGauss = NAN;

    if (isnan(tmpGauss))
    {
        double r = sqrt(-2.0 * log(1.0 - randd()));
        double phi = 2 * M_PI * randd();

        tmpGauss = r * cos(phi);
        return r * sin(phi);
    }
    else
    {
        double x = tmpGauss;
        tmpGauss = NAN;
        return x;
    }
}

double dwamf::rand_exp()
{
    return -log(randd());
}
