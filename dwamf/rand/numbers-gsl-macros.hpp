/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Tool Kit: Random numbers
 *
 * Wrapper functions for generating pseudo random numbers.
 * Different random number generators may be used with
 * the same calls by redirecting via these wrappers.
 *
 * If you would like to use the GSL random number generator,
 * consider #define-ing _RANDOM_GSL_MACROS BEFORE including
 * this header to improve performance.
 */

#ifndef _DWAMF_RAND_NUMBERS_GSL_MACROS_H
#define _DWAMF_RAND_NUMBERS_GSL_MACROS_H

#include <gsl/gsl_rng.h>

extern gsl_rng *__DWAMF_RAND_GSL_RNG;

#define randd() gsl_rng_uniform(__DWAMF_RAND_GSL_RNG)
#define rand_uli(_nMax_) gsl_rng_uniform_int(__DWAMF_RAND_GSL_RNG, (_nMax_))

#define rand_gsl_generator_ptr __DWAMF_RAND_GSL_RNG

#endif /* _DWAMF_RAND_NUMBERS_GSL_MACROS_H */
