/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "RealProbabilityDistribution.hpp"
 */
#include "RealProbabilityDistribution.hpp"

#include "numbers.hpp"
#include "numbers-gsl-macros.hpp"

#include <iostream>

using namespace Eigen;

dwamf::RealProbabilityDistribution::RealProbabilityDistribution()
{
    cdfRootSolver = gsl_root_fsolver_alloc(gsl_root_fsolver_brent);
    inverseCDFParams.dist = this;

    inverseCDFRootFunction.function = RealProbabilityDistribution::inverseCDFRoot;
    inverseCDFRootFunction.params = &inverseCDFParams;
}

dwamf::RealProbabilityDistribution::~RealProbabilityDistribution()
{
    gsl_root_fsolver_free(cdfRootSolver);
}

real_t dwamf::RealProbabilityDistribution::getVariate()
{
    inverseCDFParams.prob = randd();
    gsl_root_fsolver_set(cdfRootSolver, &inverseCDFRootFunction, getNumericalLowerBound(), getNumericalUpperBound());

    int iterationCounter = 0;
    int status;
    do
    {
        ++iterationCounter;
        status = gsl_root_fsolver_iterate(cdfRootSolver);

        real_t sLower = gsl_root_fsolver_x_lower(cdfRootSolver);
        real_t sUpper = gsl_root_fsolver_x_upper(cdfRootSolver);
        if (sUpper - sLower <= getNumericalTolerance())
        {
            return gsl_root_fsolver_root(cdfRootSolver);
        }
    }
    while (status == GSL_SUCCESS && iterationCounter < getMaxIterations());

    cout << "[rand] WARNING Inversion of the cumulative distribution function failed to converge within " << iterationCounter << " steps." << endl;

    return gsl_root_fsolver_root(cdfRootSolver);
}

Matrix<real_t, Dynamic, 1> dwamf::RealProbabilityDistribution::getVariate(long int sampleSize)
{
    Matrix<real_t, Dynamic, 1> randVec(sampleSize);
    int i;
    for (i = 0; i < sampleSize; ++i)
    {
        randVec(i) = getVariate();
    }
    return randVec;
}
