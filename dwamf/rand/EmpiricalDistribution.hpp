/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Representation of empirical distributions of datasets
 */
 
#ifndef _DWAMF_EMPIRICALDISTRIBUTION_HPP
#define _DWAMF_EMPIRICALISTRIBUTION_HPP

#include "DiscreteDistribution.hpp"

namespace dwamf
{
    template<typename T>
    class EmpiricalDistribution : public DiscreteDistribution<T>
    {
        private:
            unordered_map<T, real_t> stateProbsMap;

        public:
            EmpiricalDistribution(const unordered_map<T, real_t> &stateProbsMapping)
            {
                this->stateProbsMap = stateProbsMapping;
            }

            virtual ~EmpiricalDistribution()
            {
            }

            virtual real_t getProbability(const T &x) const override
            {
                auto entryIter = stateProbsMap.find(x);
                if (entryIter == stateProbsMap.end())
                {
                    return 0.0;
                }
                else
                {
                    return entryIter->second;
                }
            }
    };
};

#endif
