/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "sets.hpp"
 */
#include "numbers.hpp"
#include "sets.hpp"
#include "../func/sets.hpp"

using namespace dwamf;

vector<size_t> dwamf::rand_subset(const size_t n, const size_t k)
{
    size_t numSubsets = sets_binomial(n, k);
    size_t subsetRank = (size_t) (numSubsets * randd());
    return sets_subset_from_rank(k, subsetRank);
}
