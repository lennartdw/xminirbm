/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Tool Kit: Random numbers
 * GNU Scientific Library
 *
 * Wrappers for the GNU Scientific Library functions
 * to the interface provided by "random.h".
 */

#include <stdlib.h>
#include <time.h>
#include <gsl/gsl_rng.h>

#include "numbers.hpp"
#include "../util/globals.hpp"

gsl_rng *__DWAMF_RAND_GSL_RNG = NULL;

using namespace std;

void dwamf::rand_init(unsigned long int seed, const string &type)
{
    // create and seed a new random number generator
    // of the ranlux type
    if (type == "ranlxs0")
    {
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_ranlxs0);
    }
    else if (type == "ranlxs1")
    {
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_ranlxs1);
    }
    else if (type == "ranlxs2")
    {
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_ranlxs2);
    }
    else if (type == "ranlxd1")
    {
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_ranlxd1);
    }
    else if (type == "ranlxd2")
    {
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_ranlxd2);
    }
    else if (type == "mt19937")
    {
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_mt19937);
    }
    else
    {
        STDLOG.printf_wrn("Unknown random number generator type '%s'. Will use default 'ranlxs0'.\n", type);
        __DWAMF_RAND_GSL_RNG = gsl_rng_alloc(gsl_rng_ranlxs0);
    }
    gsl_rng_set(__DWAMF_RAND_GSL_RNG, seed);

    STDLOG.printf("[rand] initialized GSL random-number generator '%s' with seed %ld\n", type.c_str(), seed);
}

void dwamf::rand_term(void)
{
    if (__DWAMF_RAND_GSL_RNG != NULL)
    {
        gsl_rng_free(__DWAMF_RAND_GSL_RNG);
        __DWAMF_RAND_GSL_RNG = NULL;
    }
}

#ifndef _RANDOM_GSL_MACROS

double dwamf::randd()
{
    return gsl_rng_uniform(__DWAMF_RAND_GSL_RNG);
}

unsigned long int dwamf::randuli(const unsigned long int nMax)
{
    return gsl_rng_uniform_int(__DWAMF_RAND_GSL_RNG, nMax);
}

#endif
