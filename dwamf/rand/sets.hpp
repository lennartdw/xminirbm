/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Tool kit: Random numbers
 *
 * Routines to draw random (sub)sets
 */
 
#ifndef _DWAMF_RANDOM_SETS_H
#define _DWAMF_RANDOM_SETS_H

#include <cstdlib>
#include <vector>

namespace dwamf
{
    /**
     * Pick a subset of {0, 1, ..., n-1} with k elements
     * uniformly at random.
     */
    std::vector<size_t> rand_subset(const size_t n, const size_t k);
};

#endif
