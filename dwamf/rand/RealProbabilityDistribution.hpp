/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Abstract real probability distribution representation
 */

#ifndef _DWAMF_REALPROBABILITYDISTRIBUTION_HPP
#define _DWAMF_REALPROBABILITYDISTRIBUTION_HPP

#include <gsl/gsl_roots.h>
#include <eigen3/Eigen/Dense>

#include "../func/types.hpp"

#include <iostream>

namespace dwamf
{
    class RealProbabilityDistribution
    {
        private:
            gsl_function inverseCDFRootFunction;
            gsl_root_fsolver *cdfRootSolver;

            struct _inverseCDFParams {
                RealProbabilityDistribution *dist;
                real_t prob;
            } inverseCDFParams;

            static real_t inverseCDFRoot(real_t s, void *params)
            {
                struct _inverseCDFParams icp = *((struct _inverseCDFParams*) params);
                // std::cout << "s: " << s << ", cdf: " << icp.dist->CDF(s) << ", prob: " << icp.prob << std::endl;
                return icp.dist->CDF(s) - icp.prob;
            }

        public:
            RealProbabilityDistribution();
            virtual ~RealProbabilityDistribution();

            virtual real_t CDF(real_t s) const = 0; // cumulative distribution function
            // moments:
            virtual real_t getMean() const = 0;
            virtual real_t getStandardDeviation() const = 0;

            // Maximal number of iterations for the
            // numerical CDF inversion
            virtual int getMaxIterations() const
            {
                return 20;
            }

            // Numerical lower bound for the CDF inversion
            virtual real_t getNumericalLowerBound() const
            {
                return getMean() - 10.0 * getStandardDeviation();
            }
            // Numerical upper bound for the CDF inversion
            virtual real_t getNumericalUpperBound() const
            {
                return getMean() + 10.0 * getStandardDeviation();
            }
            // Numerical tolerance (relative precision) for the
            // CDF inversion
            virtual real_t getNumericalTolerance() const
            {
                return getMean() * 1.0e-10;
            }

            /**
             * Generate a random variate or a vector of variates
             * from the distribution.
             */
            virtual real_t getVariate();
            virtual Eigen::Matrix<real_t, Eigen::Dynamic, 1> getVariate(long int sampleSize);
    };
};

#endif
