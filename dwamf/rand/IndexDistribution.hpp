/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Probability distribution on the index set {0, ..., N-1}
 */
 
#include "RealProbabilityDistribution.hpp"

namespace dwamf
{
    class IndexDistribution : RealProbabilityDistribution
    {
        private:
            // vector<real_t> probabilities;
            vector<real_t> cumProbs;
            real_t mean;
            real_t stdDev;

            bool momentsAvailable = false;
            void calculateMoments(bool forceUpdate = false);

            size_t inverse_CDF(real_t r);
            // size_t inverse_CDF_old(real_t r);

        public:
            /**
             * Generate index distribution from provided list
             * of probabilities.
             */
            IndexDistribution(const vector<real_t> &probs);

            IndexDistribution()
            {
            }

            /**
             * Load index distribution from the provided file
             * containing a raw list of probabilities.
             * This is more memory efficient than the list/vector
             * constructor because the information is directly loaded
             * into the distribution container.
             */
            static int loadFromFile(IndexDistribution &dist, const string &fileName);

            virtual ~IndexDistribution() {}

            virtual real_t CDF(real_t s) const override;

            virtual real_t getMean() const override
            {
                return momentsAvailable ? mean : NAN;
            }

            virtual real_t getProbability(size_t index) const
            {
                return cumProbs[index+1] - cumProbs[index];
            }

            virtual real_t getStandardDeviation() const override
            {
                return momentsAvailable ? stdDev : NAN;
            }

            virtual real_t getNumericalLowerBound() const override
            {
                return 0.0;
            }

            virtual real_t getNumericalUpperBound() const override
            {
                return cumProbs.size() - 1;
            }

            virtual size_t getIntegerVariate();

            virtual real_t getVariate() override
            {
                return (real_t) getIntegerVariate();
            }
    };
};
