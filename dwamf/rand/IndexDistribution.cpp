/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "IndexDistribution.hpp"
 */
#include "IndexDistribution.hpp"

#include "numbers.hpp"
#include "numbers-gsl-macros.hpp"

#include "../data/DataIO.hpp"
#include "../util/globals.hpp"

using namespace std;
using namespace Eigen;

dwamf::IndexDistribution::IndexDistribution(const vector<real_t> &probabilities)
{
    cumProbs.push_back(0.0);
    real_t cumProb = 0.0;
    for (size_t k = 0; k < (size_t) probabilities.size(); ++k)
    {
        cumProb += probabilities[k];
        cumProbs.push_back(cumProb);
    }

    if (cumProb < 1.0)
    {
        STDLOG.printf_wrn("The sum of the probabilities (%f) provided to the index distribution is less than 1. The missing weight will be assigned to the last element.\n", cumProb);
    }
    else if (cumProb > 1.0)
    {
        STDLOG.printf_wrn("The sum of the probabilities (%f) provided to the index distribution exceeds 1. Elements for which the cumulated weight is larger than 1 will be ignored.\n", cumProb);
    }
}

int dwamf::IndexDistribution::loadFromFile(dwamf::IndexDistribution &dist, const string &fileName)
{
    // load probabilities from file
    int r = DataIO::importListRaw(fileName, dist.cumProbs);
    if (r != DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf_err("Could not load index distribution from file '%s'.\n", fileName.c_str());
        return r;
    }

    // accumulate in place
    if (dist.cumProbs.size() > 0)
    {
        real_t nextProb = dist.cumProbs[0];
        dist.cumProbs[0] = 0.0;
        real_t cumProb = nextProb;
        for (size_t k = 1; k < (size_t) dist.cumProbs.size(); ++k)
        {
            nextProb = dist.cumProbs[k];
            dist.cumProbs[k] = cumProb;
            cumProb += nextProb;
        }
        dist.cumProbs.push_back(cumProb);

        if (cumProb < 1.0)
        {
            STDLOG.printf_wrn("The sum of the probabilities (%f) provided to the index distribution is less than 1. The missing weight will be assigned to the last element.\n", cumProb);
        }
        else if (cumProb > 1.0)
        {
            STDLOG.printf_wrn("The sum of the probabilities (%f) provided to the index distribution exceeds 1. Elements for which the cumulated weight is larger than 1 will be ignored.\n", cumProb);
        }

        return DWAMF_DATAIO_SUCCESS;
    }
    else
    {
        STDLOG.printf_err("The probability list loaded from the provided file '%s' is empty.\n", fileName.c_str());
        return DWAMF_DATAIO_INVALID_CONTENTS;
    }
}

void dwamf::IndexDistribution::calculateMoments(bool forceUpdate)
{
    if (forceUpdate || ! momentsAvailable)
    {
        mean = 0.0;
        for (size_t k = 1; k < (size_t) cumProbs.size() - 1; ++k)
        {
            mean += k * getProbability(k);
        }

        // TODO: implement std dev

        momentsAvailable = true;
    }
}

real_t dwamf::IndexDistribution::CDF(real_t s) const
{
    // cadlag convention

    if (s < 0.0)
    {
        return 0.0;
    }
    else if ((size_t) s >= cumProbs.size())
    {
        return 1.0;
    }
    else
    {
        return cumProbs[(size_t) s + 1];
    }
}

size_t dwamf::IndexDistribution::inverse_CDF(real_t r)
{
    size_t left = 0;
    size_t right = cumProbs.size() - 1;
    size_t pivot = (left + right) / 2;
    // cout << "l = " << left << ", r = " << right << endl;
    while (left + 1 < right)
    {
        // cout << "pivot = " << pivot << " with cdf = " << cumProbs[pivot] << endl;
        if (cumProbs[pivot] <= r)
        {
            left = pivot;
        }
        else
        {
            right = pivot;
        }
        // cout << "l = " << left << ", r = " << right << endl;

        pivot = (left + right) / 2;
    }

    return left;
}

size_t dwamf::IndexDistribution::getIntegerVariate()
{
    real_t r = randd();
    return inverse_CDF(r);
}
