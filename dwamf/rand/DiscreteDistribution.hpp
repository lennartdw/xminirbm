/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Abstract discrete probability distribution representation
 */

#ifndef _DWAMF_DISCRETEDISTRIBUTION_HPP
#define _DWAMF_DISCRETEDISTRIBUTION_HPP

#include "../func/types.hpp"

namespace dwamf
{
    template<typename T>
    class DiscreteDistribution
    {
        public:
            DiscreteDistribution()
            {
            }

            virtual ~DiscreteDistribution()
            {
            }

            virtual real_t getProbability(const T &x) const = 0;
    };
};

#endif
