/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Abstract representation of a random data generator
 * using Markov-chain Monte-Carlo methods (Gibbs sampling).
 */

#ifndef _DWAMF_MONTECARLOSAMPLER_HPP
#define _DWAMF_MONTECARLOSAMPLER_HPP

#include "../func/types.hpp"

namespace dwamf
{
    template<typename T>
    class MonteCarloSampler
    {
        public:
            template<typename ReductionType>
            static ReductionType estimateAutocorrelationFunctionFromCentralizedSamples(const vector<ReductionType> &samples, size_t s)
            {
                ReductionType corr = samples[0] * samples[s];
                for (size_t t = 1; t < samples.size() - s; ++t)
                {
                    corr += (samples[t] * samples[t+s] - corr) / (t+1);
                }

                return corr;
            }

            template<typename ReductionType>
            static ReductionType estimateAutocorrelationFunctionFromSamplesAndSampleMean(const vector<ReductionType> &samples, const ReductionType &sampleMean, size_t s)
            {
                ReductionType corr = (samples[0] - sampleMean) * (samples[s] - sampleMean);
                for (size_t t = 1; t < samples.size() - s; ++t)
                {
                    corr += ((samples[t] - sampleMean) * (samples[t+s] - sampleMean) - corr) / (t+1);
                }

                return corr;
            }

            /**
             * Calculate an estimate of the autocorrelation function
             *     g(s) := <f(X(t)) f(X(t+s))> - <f(X(t))> <f(X(t+s))> ,
             * assuming the sampler is in the stationary regime
             * after the burn-in steps.
             * The estimate is based on empirical averages
             * from the specified number of samples using the
             * provided step size.
             * The maximal delay time s corresponds to the given
             * number of steps.
             */
            template<typename ReductionType>
            vector<ReductionType> estimateAutocorrelationFunction(
                const function<ReductionType(const T&)> &f,
                const size_t numSamples,
                const size_t stepSize,
                size_t numSteps,
                const size_t burnInSteps = 0,
                const function<ReductionType(const ReductionType&)> &ftransform_sample_mean = [](const ReductionType &mean) -> ReductionType {
                    return mean;
                },
                const function<bool(const vector<ReductionType>&, const ReductionType&)> &fcheck_sample_consistency = [](const vector<ReductionType>&, const ReductionType&) -> bool {
                    return true;
                },
                bool monitorProgress = false)
            {
                if (numSteps > numSamples)
                {
                    STDLOG.printf_wrn("The maximal number of delay steps cannot exceed the number of samples. Setting numSteps to %ld.\n", numSamples);
                    numSteps = numSamples;
                }

                // generate samples
                ReductionType sampleMean;
                vector<ReductionType> samples;
                generateTransformedVariates(samples, sampleMean, numSamples, f, stepSize, burnInSteps);

                if (! fcheck_sample_consistency(samples, sampleMean))
                {
                    STDLOG.printf_err("The sample consistency check indicated that the samples are inconsistent or inadequate for estimating the autocorrelation function. Returning an empty vector.\n");
                    return vector<ReductionType>();
                }

                // centralize samples
                sampleMean = ftransform_sample_mean(sampleMean);
                for (size_t t = 0; t < numSamples; ++t)
                {
                    samples[t] -= sampleMean;
                }

                ProgressMonitor monitor;
                if (monitorProgress)
                {
                    printf("\rCalculating autocorrelation ... ");
                    monitor.start();
                }

                // calculate autocorrelation for requested delays
                vector<ReductionType> acf;
                for (size_t s = 0; s < numSteps; ++s)
                {
                    if (monitorProgress)
                    {
                        monitor.update((real_t) s / numSteps);
                    }

                    acf.push_back(estimateAutocorrelationFunctionFromCentralizedSamples(samples, s));
                }

                if (monitorProgress)
                {
                    monitor.end();
                }

                return acf;
            }

            template<typename ReductionType>
            vector<ReductionType> estimateAutocorrelationFunction(const function<ReductionType(const T&)> &f, const size_t numSamples, const size_t stepSize = 1, const size_t burnInSteps = 0, bool monitorProgress = false)
            {
                return estimateAutocorrelationFunction(f, numSamples, stepSize, numSamples, burnInSteps, monitorProgress);
            }

            virtual vector<T> estimateAutocorrelationFunction(const size_t numSamples, const size_t stepSize, size_t numSteps, const size_t burnInSteps = 0, bool monitorProgress = false)
            {
                return estimateAutocorrelationFunction<T>([](const T &sample) -> T {
                        return sample;
                    },
                    numSamples, stepSize, numSteps, burnInSteps, [](const T &mean) -> T {
                        return mean;
                    }, [](const vector<T> &samples, const T &sampleMean)
                    {
                        return true;
                    },
                    monitorProgress);
            }

            virtual vector<T> estimateAutocorrelationFunction(const size_t numSamples, const size_t stepSize = 1, const size_t burnInSteps = 0, bool monitorProgress = false)
            {
                return estimateAutocorrelationFunction(numSamples, stepSize, numSamples, burnInSteps, monitorProgress);
            }

            /**
             * Estimate the integrated autocorrelation time
             * from the provided samples.
             * NOTE: For memory efficiency, this method works in-place
             *     on "samples" and "sampleMean" and modifies them
             *     during the estimation process. The samples cannot
             *     be used afterwards for further calculations.
             */
            template<typename ValueReductionType>
            static ValueReductionType estimateIntegratedAutocorrelationTime(
                vector<T> &samples,
                T &sampleMean,
                const function<ValueReductionType(const T&)> &freduce_acf, // reduction of autocorrelation function
                const size_t maxNumSteps,
                const ValueReductionType &cThreshold = 5.0,
                const function<T(const T&)> &ftransform_sample_mean = [](const T &mean) -> T {
                    return mean;
                },
                const function<bool(const vector<T>&, const T&)> &fcheck_sample_consistency = [](const vector<T>&, const T&) -> bool {
                    return true;
                },
                ValueReductionType *varPtr = NULL)
            {
                if (! fcheck_sample_consistency(samples, sampleMean))
                {
                    STDLOG.printf_err("The sample consistency check indicated that the samples are inconsistent or inadequate for estimating the autocorrelation time. Returning NAN.\n");
                    return NAN;
                }

                // centralize samples
                sampleMean = ftransform_sample_mean(sampleMean);
                for (size_t t = 0; t < samples.size(); ++t)
                {
                    samples[t] -= sampleMean;
                }

                // estimate variance
                const ValueReductionType corr0 = freduce_acf(estimateAutocorrelationFunctionFromCentralizedSamples(samples, 0));
                if (varPtr != NULL)
                {
                    *varPtr = corr0;
                }

                ValueReductionType act = 1.0;
                size_t s = 1;
                while (s < cThreshold * act)
                {
                    if (s >= maxNumSteps)
                    {
                        STDLOG.printf_wrn("Reached the maximum number of steps (%ld) without passing the target threshold C = %f. Returning NAN.\n", maxNumSteps, cThreshold);
                        return NAN;
                    }

                    // calculate autocorrelation-function estimate
                    const ValueReductionType corr = freduce_acf(estimateAutocorrelationFunctionFromCentralizedSamples(samples, s));

                    // update autocorrelation-time estimate
                    act += 2.0 * corr / corr0;

                    ++s;
                }

                return act;
            }

            template<typename SampleReductionType, typename ValueReductionType>
            ValueReductionType estimateIntegratedAutocorrelationTime(
                const function<SampleReductionType(const T&)> &freduce_sample, // "observable"
                const function<ValueReductionType(const SampleReductionType&)> &freduce_acf, // reduction of autocorrelation function
                const size_t numSamples,
                const size_t stepSize,
                const size_t maxNumSteps,
                const size_t burnInSteps = 0,
                const ValueReductionType &cThreshold = 5.0,
                const function<SampleReductionType(const SampleReductionType&)> &ftransform_sample_mean = [](const SampleReductionType &mean) -> SampleReductionType {
                    return mean;
                },
                const function<bool(const vector<SampleReductionType>&, const SampleReductionType&)> &fcheck_sample_consistency = [](const vector<SampleReductionType>&, const SampleReductionType&) -> bool {
                    return true;
                },
                ValueReductionType *varPtr = NULL)
            {
                // generate all samples;
                // includes thermalization steps
                SampleReductionType sampleMean;
                vector<SampleReductionType> samples;
                generateTransformedVariates(samples, sampleMean, numSamples, freduce_sample, stepSize, burnInSteps);

                return MonteCarloSampler<SampleReductionType>::estimateIntegratedAutocorrelationTime(samples, sampleMean, freduce_acf, maxNumSteps, cThreshold, ftransform_sample_mean, fcheck_sample_consistency, varPtr);
            }

            /**
             * Estimate the integrated autocorrelation time
             *     t_ac = 1 + 2 * sum_{s=1}^inf g(s) / g(0)
             * of the given autocorrelation function (ACF).
             * The infinite sum is truncated at the smallest
             * time M such that
             *     M >= C * t_ac ,
             * where C is the specified threshold and t_ac
             * is the current guess of the autocorrelation time
             * (up to time M - 1).
             * Returns t_ac or NAN if the provided ACF data
             * are insufficient to obtain a reliable estimate
             * at the requested threshold.
             */
            static T estimateIntegratedAutocorrelationTime(const function<const T*(size_t)> &fACF, const T &cThreshold = 5.0)
            {
                const T *normValuePtr = fACF(0);
                if (normValuePtr == NULL)
                {
                    STDLOG.printf_wrn("No autocorrelation-function estimate available at t = 0, cannot normalize output. Returning NAN.\n");
                    return NAN;
                }

                T act = 1.0;
                size_t s = 1;
                while (s < cThreshold * act)
                {
                    const T *acfValuePtr = fACF(s);
                    if (acfValuePtr == NULL)
                    {
                        STDLOG.printf_wrn("No autocorrelation-function estimate available at t = %ld, but the desired target threshold of C = %f has not yet been reached. Returning NAN.\n", s, cThreshold);
                        return NAN;
                    }

                    act += 2.0 * (*acfValuePtr) / (*normValuePtr);
                    // cout << "acf(" << s << ") = " << acf[s] << "; acf(0) = " << acf[0] << endl;
                    ++s;
                }

                return act;
            }

            static T estimateIntegratedAutocorrelationTime(const vector<T> &acf, const T &cThreshold = 5.0)
            {
                T act = 1.0;
                size_t s = 1;
                while (s < cThreshold * act)
                {
                    if (s >= acf.size())
                    {
                        STDLOG.printf_wrn("The length t = %ld of the provided correlation-function time series is too short to obtain a reliable estimate of the integrated autocorrelation time at the target threshold of C = %f. Returning NAN.\n", acf.size(), cThreshold);
                        return NAN;
                    }

                    act += 2.0 * acf[s] / acf[0];
                    // cout << "acf(" << s << ") = " << acf[s] << "; acf(0) = " << acf[0] << endl;
                    ++s;
                }

                return act;
            }

            /**
             * Initialize the sampler such that the sampling method
             * becomes available.
             */
            virtual void initialize() = 0;

            /**
             * Initialize the sampler by calling initialize()
             * and subsequently run the specified number of
             * burn-in steps to thermalize it.
             */
            virtual void initialize(const size_t burnInSteps)
            {
                initialize();
                next(burnInSteps);
            }

            /**
             * Generate the next state by advancing the Markov
             * chain by one step.
             */
            virtual void next() = 0;

            /**
             * Advance the chain for the specifed number of
             * steps using next().
             */
            virtual void next(const size_t steps)
            {
                for (size_t s = 0; s < steps; ++s)
                {
                    next();
                }
            }

            /**
             * Return the sample corresponding to the current
             * state of the sampler.
             */
            virtual T getCurrentSample() = 0;

            /**
             * Generate a new sample by advancing the sampler's
             * state for the specified number of steps using next().
             * Returns the sample provided by getCurrentSample()
             * in the final state.
             */
            virtual T getVariate(const size_t steps = 1)
            {
                next(steps);
                return getCurrentSample();
            }

            template<typename ReductionType>
            void generateTransformedVariates(vector<ReductionType> &samples, ReductionType &sampleMean, const size_t numSamples, const function<ReductionType(const T&)> &f, const size_t stepSize = 1, const size_t burnInSteps = 0, bool monitorProgress = false)
            {
                if (numSamples <= 0)
                {
                    return; // empty vector
                }

                // carry out burn-in steps
                next(burnInSteps);

                // ProgressMonitor monitor;
                if (monitorProgress)
                {
                    printf("Sampling ... ");
                    fflush(stdout);
                }

                // generate first sample
                sampleMean = f(getVariate(stepSize));
                samples.push_back(sampleMean);

                // generate remaining samples
                // and calculate mean on-line
                for (size_t t = 1; t < numSamples; ++t)
                {
                    ReductionType nextSample = f(getVariate(stepSize));
                    sampleMean += (nextSample - sampleMean) / (t+1);
                    samples.push_back(nextSample);
                }
            }
    };
};

#endif
