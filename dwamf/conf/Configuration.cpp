/**
 * (c) 2016-22 Lennart Dabelow
 * 
 * implements "Configuration.hpp"
 */

#include "Configuration.hpp"

#include <fstream>
#include <cctype>
#include <cstring>

#include "../util/strings.hpp"
#include "../util/LogManager.hpp"

using namespace Eigen;
using namespace dwamf;

dwamf::Configuration::Configuration(void *runtime, const char *name, string file, string entryPrefix, string ignorePrefix, string dynamicValuePrefix)
{
    this->name = name;
    this->fileName = file;
    this->entryPrefix = entryPrefix;
    this->ignorePrefix = ignorePrefix;
    this->dynamicValuePrefix = dynamicValuePrefix;

    reload();
    // cout << "done loading config:" << endl;
    // printf("name: %p\n", &name);
    // printf("configTable: %p\n", &configTable);
    // printf("fileName: %p\n", &fileName);
    // printf("entryPrefix: %p\n", &entryPrefix);
    // printf("RE: %p to %p\n", &RE, RE);
}

dwamf::Configuration::Configuration(void *runtime, const char *name)
{
    this->name = name;
}

Configuration dwamf::Configuration::copy() const
{
    Configuration copyConfig(this->name.c_str());
    copyConfig.entryPrefix = this->entryPrefix;
    for (auto &entry : this->configTable)
    {
        copyConfig.setValue(entry.first, entry.second);
    }

    return copyConfig;
}

int dwamf::Configuration::load(istream &input)
{
    string line;
    size_t numLines = 0;

    while (! input.eof() && getline(input, line))
    {
        // cout << "config parser: next line `" << line << "`" << endl;
        ++numLines;
        const size_t length = line.length();
        size_t offset = 0;

        // skip leading whitespaces
        while (offset < length && isblank(line[offset]))
        {
            ++offset;
        }
        if (offset >= length) continue; // end of line reached

        const size_t entryPrefixLength = entryPrefix.length();
        const size_t ignorePrefixLength = ignorePrefix.length();
        if (entryPrefix != "" && ( length - offset <= entryPrefixLength || line.substr(offset, entryPrefixLength) != entryPrefix))
        {
            // line does not carry entry prefix
            continue;
        }
        else if (ignorePrefix != "" && ( length - offset <= ignorePrefixLength || line.substr(offset, ignorePrefixLength) == ignorePrefix))
        {
            // line carries ignore prefix
            continue;
        }
        else
        {
            offset += entryPrefixLength;
            // skip leading whitespaces
            while (offset < length && isblank(line[offset]))
            {
                ++offset;
            }
            if (offset >= length) continue; // end of line reached
        }

        if (line[offset] == '#') continue; // comment

        // scan key
        int startIndex = offset++;
        while (offset < length && line[offset] != ':')
        {
            ++offset;
        }
        string key = line.substr(startIndex, offset - startIndex);
        ++offset;

        // skip whitespaces
        while (offset < length && isblank(line[offset]))
        {
            ++offset;
        }

        // scan value
        string value;
        if (offset >= length)
        {
            // empty value
            value = "";
        }
        else
        {
            startIndex = offset++;
            while (offset < length && line[offset] != '#')
            {
                ++offset;
            }
            // trim trailing whitespaces
            while (isblank(line[offset-1]))
            {
                --offset;
            }
            value = line.substr(startIndex, offset - startIndex);
        }

        // store key-value pair
        this->configTable[key] = value;

        // cout << "config: add `" << key << "`=`" << value << "`" << endl;
    }
    // cout << "Read " << numLines << " lines from config." << endl;
    return 1; // TRUE
}

int dwamf::Configuration::load(const string &str)
{
    istringstream iss(str);
    return load(iss);
}

int dwamf::Configuration::load(const vector<string> &strList)
{
    for (auto &str : strList)
    {
        if (! load(str))
        {
            return 0; // FALSE
        }
    }

    return 1; // TRUE
}

void dwamf::Configuration::reload()
{
    configTable.clear();

    if (fileName != "")
    {
        // cout << "Load config from file: '" << fileName << "'" << endl;
        ifstream input(fileName.c_str());
        if (! input.good())
        {
            // ERROR Invalid file
            STDLOG.printf_err("[conf] Configuration file not accessible: '%s'. Returning an empty configuration.\n", fileName.c_str());
            // TODO: throw exception
            return;
        }

        load(input);
    }
}

vector<string> dwamf::Configuration::getKeys() const
{
    vector<string> keys;
    for (auto &entry : configTable)
    {
        keys.push_back(entry.first);
    }

    return keys;
}

string dwamf::Configuration::getName() const
{
    return this->name;
}

string dwamf::Configuration::getValue(const string &key) const
{
    string value = this->configTable.at(key);
    if (dynamicValuePrefix != "" && string_startsWith(value, dynamicValuePrefix + "[") && string_endsWith(value, "]"))
    {
        return replaceValues(value.substr(dynamicValuePrefix.length() + 1, value.length() - dynamicValuePrefix.length() - 2));
    }
    else
    {
        return value;
    }
}

string dwamf::Configuration::getValue(const string &key, const string &defaultValue) const
{
    try
    {
        return getValue(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

double dwamf::Configuration::getValueDouble(const string &key) const
{
    return stod(getValue(key));
}

double dwamf::Configuration::getValueDouble(const string &key, double defaultValue) const
{
    try
    {
        return getValueDouble(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

real_t dwamf::Configuration::getValueReal(const string &key) const
{
    real_t r;
    if (! parse_real(&r, getValue(key).c_str()))
        throw (-1);
    return r;
}

real_t dwamf::Configuration::getValueReal(const string &key, real_t defaultValue) const
{
    try
    {
        return getValueReal(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

complex_t dwamf::Configuration::getValueComplex(const string &key) const
{
    vector<string> parts = string_split(getValue(key), ",", false);
    if (parts.size() == 1)
    {
        // only real part
        real_t r;
        if (! parse_real(&r, string_trim(parts[0]).c_str()))
            throw (-1);
        return r;
    }
    else if (parts.size() == 2)
    {
        // real and imaginary parts
        real_t real, imag;
        if (! ( parse_real(&real, string_trim(parts[0]).c_str()) && parse_real(&imag, string_trim(parts[1]).c_str()) ) )
            throw (-1);
        return real + 1.0i * imag;
    }
    throw(-2);
}

complex_t dwamf::Configuration::getValueComplex(const string &key, complex_t defaultValue) const
{
    try
    {
        return getValueComplex(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

int dwamf::Configuration::getValueInt(const string &key) const
{
    return stoi(getValue(key));
}

int dwamf::Configuration::getValueInt(const string &key, int defaultValue) const
{
    try
    {
        return getValueInt(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

size_t dwamf::Configuration::getValueSize(const string &key) const
{
    return stoul(getValue(key));
}

size_t dwamf::Configuration::getValueSize(const string &key, size_t defaultValue) const
{
    try
    {
        return getValueSize(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

bool dwamf::Configuration::getValueBool(const string &key) const
{
    string rawValue = string_toLowerCase(string_trim(getValue(key)));
    if (rawValue == "true" || rawValue == "yes" || rawValue == "1")
    {
        return true;
    }
    else if (rawValue == "false" || rawValue == "no" || rawValue == "0")
    {
        return false;
    }

    throw("Cannot parse configuration value to Boolean type.");
}

bool dwamf::Configuration::getValueBool(const string &key, bool defaultValue) const
{
    try
    {
        return getValueBool(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

Matrix<real_t, Dynamic, 1> dwamf::Configuration::getVectorReal(const string &key) const
{
    vector<string> components = string_split(getValue(key), " ", true);
    Matrix<real_t, Dynamic, 1> vec(components.size());

    int n;
    for (n = 0; n < vec.size(); ++ n)
    {
        real_t r;
        if (! parse_real(&r, components[n].c_str()))
            throw (-1);

        vec(n) = r;
    }

    return vec;
}

Matrix<real_t, Dynamic, 1> dwamf::Configuration::getVectorReal(const string &key, const Matrix<real_t, Dynamic, 1> &defaultValue) const
{
    try
    {
        return getVectorReal(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

vector<real_t> dwamf::Configuration::getCPPVectorReal(const string &key) const
{
    vector<real_t> list;
    if (! parse_real_list(list, string_split(getValue(key), " ", true)))
    {
        throw(-1);
    }
    return list;
}

Matrix<int, Dynamic, 1> dwamf::Configuration::getVectorInt(const string &key) const
{
    vector<string> components = string_split(getValue(key), " ", true);
    Matrix<int, Dynamic, 1> vec(components.size());

    int n;
    for (n = 0; n < vec.size(); ++n)
    {
        long int value;
        if (! parse_integer(&value, components[n].c_str()))
            throw (-1);

        vec(n) = (int) value;
    }

    return vec;
}

Matrix<int, Dynamic, 1> dwamf::Configuration::getVectorInt(const string &key, const Matrix<int, Dynamic, 1> &defaultValue) const
{
    try
    {
        return getVectorInt(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

vector<int> dwamf::Configuration::getCPPVectorInt(const string &key) const
{
    vector<int> list;
    if (! parse_integer_list(list, string_split(getValue(key), " ", true)))
    {
        throw(-1);
    }
    return list;
}

Matrix<real_t, Dynamic, Dynamic> dwamf::Configuration::getMatrixReal(const string &key, const string &rowSeparator) const
{
    vector<string> rows = string_split(getValue(key), rowSeparator, false);
    int maxNumCols = 0;
    vector<vector<real_t>> entries(rows.size());

    int rowIndex;
    for (rowIndex = 0; rowIndex < (int) rows.size(); ++rowIndex)
    {
        vector<string> components = string_split(rows[rowIndex], " ", true);

        int n;
        for (n = 0; n < (int) components.size(); ++ n)
        {
            real_t r;
            if (! parse_real(&r, components[n].c_str()))
                throw (-1);

            entries[rowIndex].push_back(r);
        }

        if ((int) components.size() > maxNumCols)
            maxNumCols = components.size();
    }

    Matrix<real_t, Dynamic, Dynamic> mat(rows.size(), maxNumCols);
    int colIndex;
    for (rowIndex = 0; rowIndex < mat.rows(); ++rowIndex)
    {
        for (colIndex = 0; colIndex < (int) entries[rowIndex].size(); ++colIndex)
        {
            mat(rowIndex, colIndex) = entries[rowIndex][colIndex];
        }
        for (colIndex = entries[rowIndex].size(); colIndex < mat.cols(); ++colIndex)
        {
            mat(rowIndex, colIndex) = 0.0;
        }
    }

    return mat;
}

Matrix<real_t, Dynamic, Dynamic> dwamf::Configuration::getMatrixReal(const string &key, const string &rowSeparator, const Matrix<real_t, Dynamic, Dynamic> &defaultValue) const
{
    try
    {
        return getMatrixReal(key);
    }
    catch (...)
    {
        return defaultValue;
    }
}

Matrix<real_t, Dynamic, Dynamic> dwamf::Configuration::getMatrixReal(const string &key, const Matrix<real_t, Dynamic, Dynamic> &defaultValue) const
{
    return getMatrixReal(key, ";", defaultValue);
}

void dwamf::Configuration::insert(const Configuration &config, const string &prefix)
{
    for (const auto &pair : config.configTable)
    {
        this->configTable[prefix + pair.first] = pair.second;
    }
}

string dwamf::Configuration::replaceValues(const string &source, const string idLeft, const string idRight) const
{
    string str = source;

    for (const auto& pair : this->configTable)
    {
        // cout << "replace " << idLeft + pair.first + idRight << " by " << pair.second << endl;
        str = string_replace(str, idLeft + pair.first + idRight, pair.second);
    }

    return str;
}

bool dwamf::Configuration::isKey(const string &value) const
{
    return (configTable.find(value) != configTable.end());
}

void dwamf::Configuration::print(const string indent) const
{
    for (const auto& pair : this->configTable)
    {
        cout << indent << pair.first << ": '" << pair.second << "'" << endl;
    }
}

int dwamf::Configuration::saveToFile(const string &fileName, const string indent) const
{
    int r = saveToFile(fopen(fileName.c_str(), "w"), indent);
    if (! r)
    {
        STDLOG.printf_err("       Could not save configuration to file '%s'.\n", fileName.c_str());
    }
    return r;
}

int dwamf::Configuration::saveToFile(FILE *file, const string indent) const
{
    if (file == NULL)
    {
        STDLOG.printf_err("[conf] File not accessible.\n");
        return 0; // FALSE
    }

    for (const auto& pair : this->configTable)
    {
        fprintf(file, "%s%s: %s\n", indent.c_str(), pair.first.c_str(), pair.second.c_str());
    }
    fclose(file);

    return 1; // TRUE
}

void dwamf::Configuration::setEntryPrefix(string prefix)
{
    this->entryPrefix = prefix;
}

void dwamf::Configuration::setValue(const string &key, const string &value)
{
    this->configTable[key] = value;
}

void dwamf::Configuration::setValue(const string &key, double value)
{
    stringstream sstr;
    sstr << std::setprecision(std::numeric_limits<double>::max_digits10) << value;
    setValue(key, sstr.str());
}

void dwamf::Configuration::setValue(const string &key, long double value)
{
    stringstream sstr;
    sstr << std::setprecision(std::numeric_limits<long double>::max_digits10) << value;

    setValue(key, sstr.str());
}

void dwamf::Configuration::setValue(const string &key, size_t value)
{
    setValue(key, to_string(value));
}

void dwamf::Configuration::setValue(const string &key, int value)
{
    setValue(key, to_string(value));
}

#ifdef DWAMF_FPPREC_FLOAT128
void dwamf::Configuration::setValue(const string &key, __float128 value)
{
    setValue(key, to_string(value));
}
#endif

void dwamf::Configuration::setValue(const string &key, const Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> &value, const string &rowSeparator)
{
    stringstream sstr;
    sstr << std::setprecision(std::numeric_limits<long double>::max_digits10);
    int i, j;
    for (i = 0; i < value.rows(); ++i)
    {
        if (i > 0)
            sstr << rowSeparator;

        for (j = 0; j < value.cols(); ++j)
        {
            if (j > 0)
                sstr << " ";
            sstr << value(i, j);
        }
    }

    setValue(key, sstr.str());
}
