/**
 * DWAMF Auxiliary Library
 * (c) 2016-22 Lennart Dabelow
 *
 * Configuration file import/export.
 */

#ifndef _CONFIGURATION_HPP
#define _CONFIGURATION_HPP

#include <cstdlib>
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <unordered_map>
#include <map>
#include <limits>

#include <eigen3/Eigen/Dense>

#include <eigen3/Eigen/Dense>

#include "../func/types.hpp"
#include "../util/globals.hpp"

using namespace std;

namespace dwamf
{
    /**
     * Configuration manager with value parsing, default values,
     * and I/O support.
     */
    class Configuration
    {
        private:
            string name;
            map<string, string> configTable;

            string fileName = "";
            string entryPrefix = "";
            string ignorePrefix = "";
            string dynamicValuePrefix = "@";

        public:
            Configuration(void *runtime, const char *name, string fileName, string entryPrefix, string ignorePrefix, string dynamicValuePrefix);
            Configuration(void *runtime, const char *name, string fileName, string entryPrefix, string ignorePrefix) : Configuration(runtime, name, fileName, entryPrefix, ignorePrefix, "@") {};
            Configuration(void *runtime, const char *name, string fileName, string entryPrefix) : Configuration(runtime, name, fileName, entryPrefix, "") {};
            Configuration(const char *name, string fileName, string entryPrefix, string ignorePrefix) : Configuration(NULL, name, fileName, entryPrefix, ignorePrefix) {};
            Configuration(void *runtime, const char *name, string fileName) : Configuration(runtime, name, fileName, "") {};
            Configuration(const char *name, string fileName, string entryPrefix) : Configuration(name, fileName, entryPrefix, "") {};
            Configuration(const char *name, string fileName) : Configuration(name, fileName, "") {};
            Configuration(void *runtime, const char *name);
            Configuration(const char *name) : Configuration(NULL, name) {};
            Configuration() : Configuration("") {};

            Configuration copy() const;

            vector<string> getKeys() const;

            const map<string, string> &getKeyValuePairs() const
            {
                return configTable;
            }

            string getName() const;

            string getValue(const string &key) const;
            string getValue(const string &key, const string &defaultValue) const;
            double getValueDouble(const string &key) const;
            double getValueDouble(const string &key, double defaultValue) const;
            real_t getValueReal(const string &key) const;
            real_t getValueReal(const string &key, real_t defaultValue) const;
            complex_t getValueComplex(const string &key) const;
            complex_t getValueComplex(const string &key, complex_t defaultValue) const;
            int getValueInt(const string &key) const;
            int getValueInt(const string &key, int defaultValue) const;
            size_t getValueSize(const string &key) const;
            size_t getValueSize(const string &key, size_t defaultValue) const;
            bool getValueBool(const string &key) const;
            bool getValueBool(const string &key, bool defaultValue) const;

            Eigen::Matrix<real_t, Eigen::Dynamic, 1> getVectorReal(const string &key) const;
            Eigen::Matrix<real_t, Eigen::Dynamic, 1> getVectorReal(const string &key, const Eigen::Matrix<real_t, Eigen::Dynamic, 1> &defaultValue) const;
            vector<real_t> getCPPVectorReal(const string &key) const;

            Eigen::Matrix<int, Eigen::Dynamic, 1> getVectorInt(const string &key) const;
            Eigen::Matrix<int, Eigen::Dynamic, 1> getVectorInt(const string &key, const Eigen::Matrix<int, Eigen::Dynamic, 1> &defaultValue) const;
            vector<int> getCPPVectorInt(const string &key) const;

            Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> getMatrixReal(const string &key, const string &rowSeparator = ";") const;
            Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> getMatrixReal(const string &key, const string &rowSeparator, const Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> &defaultValue) const;
            Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> getMatrixReal(const string &key, const Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> &defaultValue) const;

            /**
             * Merge another configuration into this one.
             */
            void insert(const Configuration &config, const string &prefix = "");

            bool isKey(const string &value) const;

            int load(istream &input);
            int load(const string &str);
            int load(const vector<string> &strList);
            void reload();

            string replaceValues(const string &source, const string idLeft = "<", const string idRight = ">") const;

            void print(const string indent = "") const;
            int saveToFile(const string &fileName, const string indent = "") const;
            int saveToFile(FILE *file, const string indent = "") const;

            void setEntryPrefix(string prefix);

            void setValue(const string &key, const string &value);
            void setValue(const string &key, double value);
            void setValue(const string &key, complex_t value);
            void setValue(const string &key, long double value);
            void setValue(const string &key, size_t value);
            void setValue(const string &key, int value);
#ifdef DWAMF_FPPREC_FLOAT128
            void setValue(const string &key, __float128 value);
#endif
            void setValue(const string &key, const Eigen::Matrix<real_t, Eigen::Dynamic, Eigen::Dynamic> &value, const string &rowSeparator = ";");

            template<typename T>
            void setValue(const string &key, const vector<T> &valueList)
            {
                stringstream sstr;
                sstr << std::setprecision(std::numeric_limits<T>::max_digits10);
                for (size_t n = 0; n < valueList.size(); ++n)
                {
                    if (n > 0)
                        sstr << " ";

                    sstr << valueList[n];
                }
                setValue(key, sstr.str());
            }

            size_t size()
            {
                return configTable.size();
            }
    };
};

#endif /* _CONFIGURATION_HPP */
