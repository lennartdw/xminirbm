========================
 Exact MiniRBM Learning
========================

Train and analyze small-scale restricted Boltzmann machines (RBM) numerically exactly.

Accompanying publication:
    L. Dabelow and M. Ueda, "Three Learning Stages and Accuracy-Efficiency Tradeoff of Restricted Boltzmann Machines" (2022)

-------------------
 Compile and build
-------------------

The program can be compiled and linked using GNU make from the root directory.
We used the GNU C++ Compiler (g++), version 11.1.0.
Object files are compiled in the folder "build" by default (see Makefile). Make sure that this folder and the following subfolders exist:
    build/
    build/datasets/
    build/dwamf/
    build/dwamf/conf
    build/dwamf/func
    build/dwamf/rand
    build/dwamf/util
    build/util/

Required libraries:
  - Boost (auxiliary library for C++), version >=1.78,
    see https://www.boost.org/
  - Eigen3 (matrix algebra), version >=3.4,
    see https://eigen.tuxfamily.org/
  - GNU Scientific Library, version >=2.7,
    see https://www.gnu.org/software/gsl/

Older versions may work, but have not been tested.

Parallelization of matrix operations via OpenMP (in the Eigen3 library) can be activated by compiling without the "EIGEN_DONT_PARALLELIZE" macro. Simply remove '-D"EIGEN_DONT_PARALLELIZE"' from 'PREFIX' in the Makefile.
Since we generally evaluated a large number of different (hyper)parameter configurations simultaneously, we found it overall advantageous to disable Eigen3 parallelization and use the different threads for different configurations instead.
Furthermore note that some of the algorithms in networks/ExactBinaryRBM.hpp introduce additional OpenMP parallelization of sums over states. When those methods are used, parallelization in Eigen3 should be disabled since the two will compete for the same resources otherwise.
As a general remark, the code is not optimized for training performance as it was intended to be run for small example systems for which the loss can be evaluated exactly.


-----
 Run
-----

General usage:
    ./xminirbm [configuration file]? [options/instructions]*

Call
    ./xminirbm -h
for a brief overview of options and instructions.

Exemplary configuration files for various experiments can be found in the folder "config". These include comments on the meaning of each of the configuration entries. These files also contain exemplary calls for specific tasks (e.g., training the machine on the respective dataset, evaluating loss measures, estimating autocorrelation times, etc.). Note, however, that not every configuration file contains all calls for all analysis steps performed in each example.


----------
 Datasets
----------

For the examples of ground-state tomography of the transverse-field Ising chain (Fig. 2), the hook pattern (Fig. 3) and the digit patterns (Fig. 4a-c), we provide examplary training datasets as used in our experiments in the folder "test_data". The MNIST dataset can be downloaded from http://yann.lecun.com/exdb/mnist/.
The target distributions needed for calculating the exact loss are built into the program for the hook- and digit-pattern examples. For the Ising model, they can be downloaded from https://gitlab.com/lennartdw/xminirbm/, see file "TFIM-ground-state.zip".

