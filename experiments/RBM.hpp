/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * RBM training and analysis experiments.
 */
 
#ifndef _XMINIRBM_EXPERIMENTS_RBM
#define _XMINIRBM_EXPERIMENTS_RBM

#include "data/DataSeries.hpp"
#include "util/files.hpp"

#include "../analysis/TotalCorrelationAnalyzer.hpp"
#include "../analysis/ProbabilityAnalyzer.hpp"
#include "../datasets/ImageBW.hpp"
#include "../datasets/prototypes.hpp"
#include "../networks/ExactBinaryRBM.hpp"
#include "../platforms/UnsupervisedPlatform.hpp"
#include "../util/headers.hpp"

#include "Utilities.hpp"


template<typename Scalar>
BinaryRBM<Scalar> *xminirbm_load_rbm(const Configuration &config, const string &machineId)
{
    BinaryRBM<Scalar> *machine;
    if (string_startsWith(machineId, "x2rbm"))
    {
        machine = new ExactBinaryRBM<Scalar>();
        if (ExactBinaryRBM<Scalar>::loadFromFile(static_cast<ExactBinaryRBM<Scalar>&>(*machine), config.getValue("data_directory", "./data"), machineId))
        {
            // an error occurred
            delete machine;
            return NULL;
        }
    }
    else
    {
        machine = new BinaryRBM<Scalar>();
        if (BinaryRBM<Scalar>::loadFromFile(*machine, config.getValue("data_directory", "./data"), machineId, config.getValueInt("cd_order", 1), config.getValueBool("cd_persistent", false), config.getValueBool("cd_order_adaptive", false)))
        {
            // an error occurred
            delete machine;
            return NULL;
        }
    }

    return machine;
}

template<typename Scalar>
bool xminirbm_load_distribution_analyzers(Matrix<Scalar, Dynamic, 1> **stateDistPtr, unordered_map<unsigned long int, Scalar> **stateProbsPtr, int numVariables, vector< DistributionAnalyzer<Scalar>* > &analyzers, const Configuration &config)
{
    cout << "[x2rbm] collecting analyzers" << endl;

    // for image analyzers, check dimensions
    int imageWidth = config.getValueInt("image_width", 5);
    int imageHeight = config.getValueInt("image_height", 5);

    vector<string> analyzerIds = string_split(config.getValue("dist_analyzers", ""), " ", true);
    for (auto &analyzerId : analyzerIds)
    {
        DistributionAnalyzer<Scalar> *analyzer = NULL;
        if (analyzerId == "S")
        {
            // full state distribution
            *stateDistPtr = new Matrix<Scalar, Dynamic, 1>();
        }
        else if (analyzerId == "CE")
        {
            // default cross-entropy analyzer (index distribution)
            if (*stateProbsPtr == NULL)
            {
                // state probabilities have not yet been loaded
                *stateProbsPtr = new unordered_map<unsigned long int, Scalar>();
                string distFile = config.getValue("sampling_distribution_file", "./dist.raw");
                vector<real_t> distProbs;
                if (DataIO::importListRaw(distFile, distProbs) != DWAMF_DATAIO_SUCCESS)
                {
                    STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
                    return false;
                }

                for (size_t s = 0; s < distProbs.size(); ++s)
                {
                    if (distProbs[s] > 0.0)
                    {
                        (**stateProbsPtr)[s] = distProbs[s];
                    }
                }

                // test if probabilities sum to unity:
                double probSum = 0.0;
                double effDimSum = 0.0;
                double entropy = 0.0;
                for (auto &ip : **stateProbsPtr)
                {
                    probSum += ip.second;
                    effDimSum += ip.second * ip.second;
                    entropy -= ip.second * LOG(ip.second);
                }
                if (FABS(1.0 - probSum) > 1.0e-6)
                {
                    STDLOG.printf_wrn("State probabilities do not sum to 1: found %e instead.\n", probSum);
                }
                cout << "prb sum: " << probSum << endl;
                cout << "eff dim: " << 1.0 / effDimSum << endl;
                cout << "entropy: " << entropy << endl;
            }

            unordered_map<unsigned long int, Scalar> *stateProbs = *stateProbsPtr;
            analyzer = new CrossEntropyAnalyzer<Scalar>(analyzerId,
                [stateProbs](const Array<Scalar, 1, Dynamic> &sample) -> Scalar {
                    // calculate state index
                    unsigned long int stateIndex = integer_from_digits(sample, 2);
                    // check probability
                    auto stateInfo = stateProbs->find(stateIndex);
                    if (stateInfo == stateProbs->end())
                    {
                        // cout << "state " << stateIndex << " has prob. 0" << endl;
                        return 0.0;
                    }
                    else
                    {
                        // cout << "state " << stateIndex << " has prob. " << stateInfo->second << endl;
                        return stateInfo->second;
                    }
                });
        }
        else if (analyzerId == "emp-CE")
        {
            // cross-entropy analyzer for an empirical distribution
            // (collection of samples)
            if (*stateProbsPtr != NULL)
            {
                STDLOG.printf_wrn("Empirical cross-entropy analyzer will overwrite a previously loaded target probability distribution. Selected analyzers may not be compatible.\n");
                delete *stateProbsPtr;
            }

            // import empirical distribution
            *stateProbsPtr = new unordered_map<unsigned long int, Scalar>();
            string dataFile = config.getValue("sampling_distribution_file", config.getValue("sampling_data_file", "./set.raw"));
            const long int numSamples = config.getValueInt("sampling_num_samples", 5000);
            Array<Scalar, Dynamic, Dynamic> samples(numSamples, config.getValueInt("num_visible", 25));
            if (! xminirbm_load_dataset(samples, dataFile))
            {
                STDLOG.printf_err("Could not load samples for empirical distribution from file '%s'.\n", dataFile.c_str());
                return false;
            }

            for (long int sampleIndex = 0; sampleIndex < numSamples; ++sampleIndex)
            {
                unsigned long int stateIndex = integer_from_digits(samples.row(sampleIndex), 2);
                auto stateInfo = (*stateProbsPtr)->find(stateIndex);
                if (stateInfo == (*stateProbsPtr)->end())
                {
                    (**stateProbsPtr)[stateIndex] = 1.0 / numSamples;
                }
                else
                {
                    (**stateProbsPtr)[stateIndex] += 1.0 / numSamples;
                }
            }

            unordered_map<unsigned long int, Scalar> *stateProbs = *stateProbsPtr;
            analyzer = new CrossEntropyAnalyzer<Scalar>(analyzerId, [stateProbs](const Array<Scalar, 1, Dynamic> &sample) -> Scalar {
                    // calculate image index
                    unsigned long int stateIndex = integer_from_digits(sample, 2);
                    // check probability
                    auto stateInfo = stateProbs->find(stateIndex);
                    if (stateInfo == stateProbs->end())
                    {
                        // cout << "state " << stateIndex << " has prob. 0" << endl;
                        return 0.0;
                    }
                    else
                    {
                        // cout << "state " << stateIndex << " has prob. " << stateInfo->second << endl;
                        return stateInfo->second;
                    }
                });

        }
        else if (analyzerId == "TC")
        {
            analyzer = new TotalCorrelationAnalyzer<Scalar>(analyzerId, numVariables, 2);
        }
        else if (analyzerId == "PH")
        {
            // pattern "hook"
            Scalar dummy;
            typename ImageBW<Scalar>::Pattern pattern = datasets_pattern_hook<Scalar>(dummy);
            analyzer = ImageBW<Scalar>::createPatternDistributionAnalyzer(analyzerId, imageWidth, imageHeight, pattern);
        }
        else if (analyzerId == "PH-CE")
        {
            // pattern "hook", cross entropy
            Scalar dummy;
            typename ImageBW<Scalar>::Pattern pattern = datasets_pattern_hook<Scalar>(dummy);
            analyzer = ImageBW<Scalar>::createPatternCrossEntropyAnalyzer(analyzerId, imageWidth, imageHeight, pattern, (Scalar) config.getValueReal("data_opx", 0.5));
        }
        else if (analyzerId == "PC")
        {
            // pattern "circle"
            Scalar dummy;
            typename ImageBW<Scalar>::Pattern pattern = datasets_pattern_circle<Scalar>(dummy);
            analyzer = ImageBW<Scalar>::createPatternDistributionAnalyzer(analyzerId, imageWidth, imageHeight, pattern);
        }
        else if (analyzerId == "PN-CE")
        {
            const Scalar catProb = 0.1; // category probability
            // load image probabilities from files
            // as generated by experiment_classify
            // from experiments/Classification
            if (*stateProbsPtr == NULL)
            {
                // state probabilities have not yet been loaded
                *stateProbsPtr = new unordered_map<unsigned long int, Scalar>();
                for (int k = 0; k < 10; ++k)
                {
                    string fileName = config.getValue("data_distribution_directory", config.getValue("data_directory", "./data")) + "/PN-" + to_string(imageWidth) + "x" + to_string(imageHeight) + "-cat" + to_string(k) + ".dat";
                    vector< IndexProbabilityInfo<Scalar> > catImgInfoList;
                    if (DataIO::importList(fileName, catImgInfoList, classify_parse_index_probability_info<Scalar>) == DWAMF_DATAIO_SUCCESS)
                    {
                        for (const auto &info : catImgInfoList)
                        {
                            auto imgProbEntry = (*stateProbsPtr)->find(info.index);
                            if (imgProbEntry == (*stateProbsPtr)->end())
                            {
                                (**stateProbsPtr)[info.index] = catProb * info.probability;
                            }
                            else
                            {
                                (**stateProbsPtr)[info.index] += catProb * info.probability;
                            }
                        }
                    }
                    else
                    {
                        STDLOG.printf_err("Could not load image probabilities of category '%d'.\n", k);
                        return false;
                    }
                }

                // test if probabilities sum to unity:
                double probSum = 0.0;
                double entropy = 0.0;
                for (auto &ip : **stateProbsPtr)
                {
                    probSum += ip.second;
                    entropy -= ip.second * LOG(ip.second);
                }
                if (FABS(1.0 - probSum) > 1.0e-6)
                {
                    STDLOG.printf_wrn("Image probabilities do not sum to 1: found %e instead.\n", probSum);
                }
                cout << "prb sum: " << probSum << endl;
                cout << "entropy: " << entropy << endl;
            }

            analyzer = ImageBW<Scalar>::createCrossEntropyAnalyzer(analyzerId, imageWidth, imageHeight, **stateProbsPtr);
        }
        else
        {
            STDLOG.printf_wrn("Unknown analyzer '%s', will be skipped.\n", analyzerId.c_str());
        }

        if (analyzer != NULL)
            analyzers.push_back(analyzer);

    }

    return true;
}

template<typename Scalar>
bool xminirbm_load_distribution_analyzers(vector<DistributionAnalyzer<Scalar>*> &analyzers, const unordered_map<string, real_t> &refStateDist, const Configuration &config)
{
    cout << "[x2rbm] collecting analyzers (with given reference distribution)" << endl;

    vector<string> analyzerIds = string_split(config.getValue("dist_analyzers", ""), " ", true);
    for (auto &analyzerId : analyzerIds)
    {
        DistributionAnalyzer<Scalar> *analyzer = NULL;

        if (analyzerId == "CE" || analyzerId == "emp-CE")
        {
            // standard cross-entropy analyzer (string-key distribution)
            analyzer = new CrossEntropyAnalyzer<Scalar>(analyzerId, [refStateDist](const Array<Scalar, 1, Dynamic> &sample) -> Scalar {
                    // calculate state key
                    string stateKey = BinaryRBM<Scalar>::statesToString(sample);
                    // check probability
                    auto stateInfo = refStateDist.find(stateKey);
                    if (stateInfo == refStateDist.end())
                    {
                        return 0.0;
                    }
                    else
                    {
                        return stateInfo->second;
                    }
                });
        }
        else if (analyzerId == "P")
        {
            // probability list of selected states
            analyzer = new ProbabilityAnalyzer<Scalar>(analyzerId);
        }
        else
        {
            STDLOG.printf_wrn("Unknown analyzer '%s', will be skipped.\n", analyzerId.c_str());
        }

        if (analyzer != NULL)
        {
            analyzers.push_back(analyzer);
        }
    }

    return true;
}

template<typename Scalar>
void experiment_rbm_sample_VGivenH(const Configuration &config)
{
    cout << "[rbm] Starting binary RBM " << ANSI_STYLE_BOLD << "sampling of visible given hidden" << ANSI_STYLE_RESET << " configurations" << endl;

    string dataId = config.getValue("data_id", "");
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        int imageWidth = config.getValueInt("image_width", 5);
        int imageHeight = config.getValueInt("image_height", 5);

        numVisibleVariables = imageHeight * imageWidth;
        cout << "        image dimensions: " << imageWidth << "x" << imageHeight << endl;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
        cout << "        # visible units: " << numVisibleVariables << endl;
    }
    int numHiddenVariables = config.getValueInt("num_hidden", 16);
    cout << "        # hidden units: " << numHiddenVariables << endl;

    size_t numSamples = config.getValueSize("sampling_num_samples", 1);
    string inFile = config.getValue("sampling_input_file", "./sample.raw");
    Array<Scalar, Dynamic, Dynamic, RowMajor> hiddenSamples(numSamples, numHiddenVariables);
    if (! xminirbm_load_dataset(hiddenSamples, inFile))
    {
        STDLOG.printf_err("Failed to import hidden state configurations.\n");
        return;
    }

    vector<string> machineIds = string_split(config.getValue("sampling_machines", ""), " ", true);
    for (auto &machineId : machineIds)
    {
        cout << "[rbm] Machine ID: " << machineId << endl;
        BinaryRBM<Scalar> machine;
        if (BinaryRBM<Scalar>::loadFromFile(machine, config.getValue("data_directory", "./data"), machineId))
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        if (machine.getNumVisible() != numVisibleVariables || machine.getNumHidden() != numHiddenVariables)
        {
            STDLOG.printf_err("Number of visible (%ld) and/or hidden units (%ld) does not match the specified configuration.\n", machine.getNumVisible(), machine.getNumHidden());
            continue;
        }

        // sample visible configurations
        Array<Scalar, Dynamic, Dynamic, RowMajor> visibleSamples = machine.randomVGivenH(hiddenSamples);

        // store results
        string outFile = config.getValue("sampling_output_file", "./sample.raw");
        if (xminirbm_save_dataset(visibleSamples, outFile))
        {
            STDLOG.printf("Saved %ld samples of visible state configurations from hidden ones as '%s'.\n", numSamples, outFile.c_str());
        }
        else
        {
            STDLOG.printf_err("Failed to save samples of visible state configurations to file '%s'.\n", outFile.c_str());
        }
    }
}

template<typename Scalar>
void experiment_rbm_sample_mc(const Configuration &config)
{
    cout << "[rbm] Starting binary RBM " << ANSI_STYLE_BOLD << "sampling via Markov chain" << ANSI_STYLE_RESET << "" << endl;

    string dataId = config.getValue("data_id", "");
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        int imageWidth = config.getValueInt("image_width", 5);
        int imageHeight = config.getValueInt("image_height", 5);

        numVisibleVariables = imageHeight * imageWidth;
        cout << "        image dimensions: " << imageWidth << "x" << imageHeight << endl;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
        cout << "        # visible units: " << numVisibleVariables << endl;
    }
    int numHiddenVariables = config.getValueInt("num_hidden", 16);
    cout << "        # hidden units: " << numHiddenVariables << endl;

    size_t numSamples = config.getValueSize("sampling_num_samples", 1);
    size_t numChains = config.getValueSize("sampling_num_chains", 1);
    size_t chainOffset = config.getValueSize("sampling_chain_offset", 0);
    size_t stepSize = config.getValueSize("sampling_step_size", 1);
    size_t thermalizationSteps = config.getValueSize("sampling_thermalization_steps", 0);

    bool storeVisible = config.getValueBool("sampling_output_visible", true);
    bool storeHidden = config.getValueBool("sampling_output_hidden", false);

    bool monitorProgress = config.getValueBool("progress_monitor", false);

    vector<string> machineIds = string_split(config.getValue("sampling_machines", ""), " ", true);
    for (auto &machineId : machineIds)
    {
        cout << "[rbm] Machine ID: " << machineId << endl;
        BinaryRBM<Scalar> machine;
        if (BinaryRBM<Scalar>::loadFromFile(machine, config.getValue("data_directory", "./data"), machineId))
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        if (machine.getNumVisible() != numVisibleVariables || machine.getNumHidden() != numHiddenVariables)
        {
            STDLOG.printf_err("Number of visible (%ld) and/or hidden units (%ld) does not match the specified configuration.\n", machine.getNumVisible(), machine.getNumHidden());
            continue;
        }

        ProgressMonitor monitor;
        if (monitorProgress) monitor.start();

        // initialize Monte Carlo sampler
        machine.initializeMonteCarloSampler(numChains, thermalizationSteps);
        string initialStateFileName = config.getValue("sampling_initial_state_file", "");
        if (initialStateFileName != "")
        {
            Array<Scalar, Dynamic, Dynamic> initialState;
            if (xminirbm_load_dataset(initialState, initialStateFileName, "", false))
            {
                STDLOG.printf("Loaded initial state for Monte-Carlo sampler from file '%s'.\n", initialStateFileName.c_str());
                machine.setMonteCarloSamplerState(initialState);
            }
            else
            {
                STDLOG.printf_err("Failed to load initial state for Monte-Carlo sampler from file '%s'.\n", initialStateFileName.c_str());
                return;
            }
        }

        // initialize storage for samples
        vector< Array<Scalar, Dynamic, Dynamic, RowMajor> > samplesV;
        if (storeVisible)
        {
            for (size_t chain = 0; chain < numChains; ++chain)
            {
                Array<Scalar, Dynamic, Dynamic, RowMajor> v(numSamples, numVisibleVariables);
                v.row(0) = machine.getCurrentSample().row(chain);
                samplesV.push_back(v);
            }
        }

        vector< Array<Scalar, Dynamic, Dynamic, RowMajor> > samplesH;
        if (storeHidden)
        {
            for (size_t chain = 0; chain < numChains; ++chain)
            {
                Array<Scalar, Dynamic, Dynamic, RowMajor> h(numSamples, numHiddenVariables);
                h.row(0) = machine.getCurrentSampleHidden().row(chain);
                samplesH.push_back(h);
            }
        }


        // sample
        for (size_t sampleIndex = 1; sampleIndex < numSamples; ++sampleIndex)
        {
            if (monitorProgress)
            {
                monitor.update((real_t) sampleIndex / numSamples);
            }

            // advance chain
            machine.next(stepSize);

            if (storeVisible)
            {
                for (size_t chain = 0; chain < numChains; ++chain)
                {
                    samplesV[chain].row(sampleIndex) = machine.getCurrentSample().row(chain);
                }
            }

            if (storeHidden)
            {
                for (size_t chain = 0; chain < numChains; ++chain)
                {
                    samplesH[chain].row(sampleIndex) = machine.getCurrentSampleHidden().row(chain);
                }
            }
        }

        if (monitorProgress) monitor.end();

        // store results
        string fileNameBase = config.getValue("data_directory", "./data") + "/" + machineId + "-mcmc-samples" + config.getValue("sampling_output_postfix", "");
        string outFileType = config.getValue("sampling_output_file_type", "txt");

        for (size_t chain = 0; chain < numChains; ++chain)
        {
            if (storeVisible)
            {
                string fileName = fileNameBase + "-v" + to_string(chainOffset + chain) + "." + outFileType;
                if (! xminirbm_save_dataset(samplesV[chain], fileName, outFileType))
                {
                    STDLOG.printf_err("Failed to export visible-unit samples of chain %ld.\n", chainOffset + chain);
                }
            }

            if (storeHidden)
            {
                string fileName = fileNameBase + "-h" + to_string(chainOffset + chain) + "." + outFileType;
                if (! xminirbm_save_dataset(samplesH[chain], fileName, outFileType))
                {
                    STDLOG.printf_err("Failed to export hidden-unit samples of chain %ld.\n", chainOffset + chain);
                }
            }
        }
    }

}

template<typename Scalar>
void experiment_rbm_train(const Configuration &config)
{
    cout << "[rbm] Starting " << ANSI_STYLE_BOLD << "training" << ANSI_STYLE_RESET << " of binary RBMs" << endl;

    string modelId = config.getValue("model_id", "x2rbm");
    if (modelId == "x2rbm")
    {
        cout << "Machine type: exact-loss binary RBM" << endl;
    }
    else if (modelId == "rbm" || modelId == "cdrbm")
    {
        cout << "Machine type: contrastive divergence binary RBM" << endl;
    }
    else
    {
        STDLOG.printf_err("Invalid machine type (model id) '%s'.\n", modelId.c_str());
        return;
    }

    // load setup and hyperparameters
    string dataId = config.getValue("data_id", "");

    int imageWidth = config.getValueInt("image_width", 5);
    int imageHeight = config.getValueInt("image_height", 5);
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        numVisibleVariables = imageHeight * imageWidth;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
    }

    string featuresId = config.getValue("data_features", "PH1");

    vector< typename ImageBW<Scalar>::Pattern > patterns;
    vector<Scalar> patternProbabilities;
    bool periodicBoundaryConditions;
    if (! xminirbm_load_pattern_distribution(patterns, patternProbabilities, periodicBoundaryConditions, featuresId))
    {
        return;
    }

    int numSamplesTrain = config.getValueInt("training_num_samples", 5000);
    int numSamplesTest = config.getValueInt("test_num_samples", 0);

    int numHiddenVariables = config.getValueInt("num_hidden", 4);

    int machineSample = config.getValueInt("sample_offset", 0);

    string machineId = xminirbm_generate_machine_id<Scalar>(config);
    cout << "[rbm] Machine ID: " << machineId << endl;

    int startingEpoch = config.getValueInt("training_epoch_start", -1);
    int endingEpoch = config.getValueInt("training_epoch_end", 0);

    // generate or load training data
    // cout << "load training data" << endl;
    Array<Scalar, Dynamic, Dynamic> trainingData(numSamplesTrain, numVisibleVariables);
    string trainingDataFileName = config.getValue("training_data_file", "");
    if (trainingDataFileName != "" || machineSample > 0 || startingEpoch > 0)
    {
        // load training data from disk
        if (trainingDataFileName == "")
        {
            trainingDataFileName = config.getValue("data_directory", "./data") + "/" + machineId + "-traindat.dat";
        }

        if (! xminirbm_load_dataset(trainingData, trainingDataFileName))
        {
            STDLOG.printf_err("Failed to import training data.\n");
            return;
        }
    }
    else
    {
        // generate new random training data
        vector< ImageBW<Scalar> > trainingImages;

        if (dataId == "imgbw")
        {
            // TODO: implement
        }
        else if (dataId == "imgbwpat")
        {
            trainingImages = ImageBW<Scalar>::random(numSamplesTrain, imageWidth, imageHeight, patterns, patternProbabilities, config.getValueReal("data_opx", 0.5), periodicBoundaryConditions);
        }
        else
        {
            STDLOG.printf_err("Cannot generate training data: Unsupported image type '%s'.\n", dataId.c_str());

            return;
        }
        trainingData = ImageBW<Scalar>::flatten(trainingImages);

        // store training data
        string trainingDataFileName = config.getValue("data_directory", "./data") + "/" + machineId + "-traindat.dat";
        Matrix<Scalar, Dynamic, Dynamic> trainingDataMat = trainingData.matrix();
        if (DataIO::exportMatrix(trainingDataFileName, trainingDataMat) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("        Saved training data as '%s'.\n", trainingDataFileName.c_str());
        }
        else
        {
            STDLOG.printf_err("Failed to save training data as '%s'.\n", trainingDataFileName.c_str());
        }
    }

    // load or generate machine and training platform
    BinaryRBM<Scalar> *machine;
    if (startingEpoch >= 0)
    {
        // load machine snapshot
        machine = xminirbm_load_rbm<Scalar>(config, machineId + "-t" + to_string(startingEpoch));

        if (machine == NULL)
        {
            STDLOG.printf_err("Failed to load previous machine snapshot from t = %d.\n", startingEpoch);
            return;
        }
    }
    else
    {
        startingEpoch = 0;

        string initialMachineId = config.getValue("training_initial_machine_id", "");
        if (initialMachineId == "")
        {
            // newly initialized machine
            if (modelId == "x2rbm")
            {
                machine = new ExactBinaryRBM<Scalar>(numVisibleVariables, numHiddenVariables);
            }
            else
            {
                Scalar stddevWeights = config.getValueReal("initial_weights_stddev", 1.0e-2);
                Scalar stddevBiasV = config.getValueReal("initial_vbias_stddev", 1.0e-1);
                Scalar stddevBiasH = config.getValueReal("initial_hbias_stddev", 1.0e-1);

                machine = new BinaryRBM<Scalar>(
                        numVisibleVariables,
                        numHiddenVariables,
                        config.getValueInt("cd_order", 1),
                        config.getValueBool("cd_persistent", false),
                        stddevWeights,
                        stddevBiasV,
                        stddevBiasH
                    );

                if (config.getValueBool("initial_vbias_adapt", false))
                {
                    // adapt bias of visible units to training data
                    // to circumvent correlation-learning regime
                    Array<Scalar, 1, Dynamic> initBiasV = trainingData.colwise().mean();
                    initBiasV = initBiasV / (Array<Scalar, 1, Dynamic>::Ones(numVisibleVariables) - initBiasV);
                    initBiasV = initBiasV.log();
                    initBiasV = initBiasV.max(-stddevBiasV).min(stddevBiasV);
                    machine->setBiasVisible(initBiasV.matrix());
                }
            }
        }
        else
        {
            // load pre-trained machine
            machine = xminirbm_load_rbm<Scalar>(config, initialMachineId);
            if (machine == NULL)
            {
                STDLOG.printf_err("Failed to load pre-trained machine with ID '%s'.\n", initialMachineId.c_str());
                return;
            }
            else if (machine->getNumVisible() != numVisibleVariables || machine->getNumHidden() != numHiddenVariables)
            {
                STDLOG.printf_err("Number of units of the provided initial machine with ID '%s' (visible: %d, hidden: %d) does not match dimensions of the present model (visible: %d, hidden: %d).\n", initialMachineId.c_str(), machine->getNumVisible(), machine->getNumHidden(), numVisibleVariables, numHiddenVariables);
                return;
            }
        }


        if (config.getValueBool("cd_order_adaptive", false))
        {
            machine->activateAdaptiveCDOrder(config.getValueSize("cd_order_adaptive_num_samples", 10000), config.getValueReal("cd_order_adaptive_multiplier", 1.0), config.getValueInt("cd_order_adaptive_min_epoch_step", 1) * trainingData.rows()  / config.getValueSize("training_batch_size", 1000), config.getValueReal("cd_order_adaptive_iact_c", 5.0), config.getValueSize("cd_order_adaptive_min_num_samples", 1000));
        }

        // save initial configuration
        machine->saveToFile(config.getValue("data_directory", "./data"), machineId + "-t0");
    }

    UnsupervisedPlatform<Scalar> platform = UnsupervisedPlatform<Scalar>::fromConfiguration(machine, config);
    platform.setNumEpochs(startingEpoch);


    Array<Scalar, Dynamic, Dynamic> testData(numSamplesTest, numVisibleVariables);
    if (numSamplesTest > 0)
    {
        // generate test images
        vector< ImageBW<Scalar> > testImages;
        if (dataId == "imgbw")
        {
            // TODO: implement
        }
        else if (dataId == "imgbwpat")
        {
            testImages = ImageBW<Scalar>::random(numSamplesTest, imageWidth, imageHeight, patterns, patternProbabilities, config.getValueReal("data_opx", 0.5), periodicBoundaryConditions);
        }
        testData = ImageBW<Scalar>::flatten(testImages);
    }

    // perform training steps
    for (int epoch = startingEpoch; epoch < endingEpoch; ++epoch)
    {
        // cout << "epoch: " << epoch << endl;
        int r = platform.train(trainingData, config.getValueBool("progress_monitor", true));
        if (r)
        {
            STDLOG.printf_err("Aborting training in epoch #%d due to internal error.\n", epoch + 1);
            return;
        }

        if (platform.isSnapshotEpoch(epoch+1))
        {
            // save current machine configuration
            string machineFileId = machineId + "-t" + to_string(epoch+1);
            if (machine->saveToFile(config.getValue("data_directory", "./data"), machineFileId) == DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf("Saved machine with ID '%s'.\n", machineFileId.c_str());
            }
            else
            {
                STDLOG.printf_err("Failed to save machine with ID '%s'.\n", machineFileId.c_str());
                return;
            }

            // record meta data
            Configuration params;
            params.setValue("training_loss", platform.trainingLoss());

            if (numSamplesTest > 0)
            {
                // evaluate test performance
                r = platform.test(testData);
                if (r)
                {
                    STDLOG.printf_wrn("Error during test routine in epoch #%d. Meta data will be incomplete.\n", epoch + 1);
                }
                else
                {
                    params.setValue("test_loss", platform.testLoss());
                    params.setValue("reconstruction_error", platform.reconstructionError());
                }
            }

            // save meta data
            string paramsFileName = config.getValue("data_directory", "./data") + "/" + machineId + "-t" + to_string(epoch+1) + ".params";
            if (params.saveToFile(paramsFileName))
            {
                STDLOG.printf("        Saved training and test meta data of epoch #%d as '%s'.\n", epoch+1, paramsFileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Failed to save training meta data as '%s'.\n", paramsFileName.c_str());
                return;
            }
        }
    }
    cout << "[rbm] Trained machine for " << (endingEpoch - startingEpoch) << " epochs." << endl;

    // clean up
    delete machine;

    cout << "[rbm] Finished experiment." << endl;
}

template<typename Scalar>
void experiment_rbm_test(const Configuration &config)
{
    cout << "[rbm] Starting test of 'Binary RBM'" << endl;

    string dataId = config.getValue("data_id", "");

    int imageWidth = config.getValueInt("image_width", 5);
    int imageHeight = config.getValueInt("image_height", 5);
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        numVisibleVariables = imageHeight * imageWidth;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
    }

    string featuresId = config.getValue("data_features", "PH1");

    vector< typename ImageBW<Scalar>::Pattern > patterns;
    vector<Scalar> patternProbabilities;
    bool periodicBoundaryConditions;
    if (! xminirbm_load_pattern_distribution(patterns, patternProbabilities, periodicBoundaryConditions, featuresId))
    {
        return;
    }

    vector<string> machineIds = string_split(config.getValue("test_machines", ""), " ", true);

    int numSamplesTest = config.getValueInt("test_num_samples", 0);

    Array<Scalar, Dynamic, Dynamic> testData(numSamplesTest, numVisibleVariables);
    if (numSamplesTest > 0)
    {
        // generate test images
        vector< ImageBW<Scalar> > testImages;
        if (dataId == "imgbw")
        {
            // TODO: implement
        }
        else if (dataId == "imgbwpat")
        {
            testImages = ImageBW<Scalar>::random(numSamplesTest, imageWidth, imageHeight, patterns, patternProbabilities, config.getValueReal("data_opx", 0.5), periodicBoundaryConditions);
        }
        testData = ImageBW<Scalar>::flatten(testImages);
    }

    for (auto &machineId : machineIds)
    {
        cout << "[rbm] Machine ID: " << machineId << endl;

        BinaryRBM<Scalar> *machine = xminirbm_load_rbm<Scalar>(config, machineId);
        if (machine == NULL)
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        UnsupervisedPlatform<Scalar> platform = UnsupervisedPlatform<Scalar>::fromConfiguration(machine, config);

        int r = platform.test(testData);
        if (r)
        {
            STDLOG.printf_wrn("Error during test routine. Meta data will be incomplete.\n");
        }
        else
        {
            Configuration params;
            params.setValue("test_loss", platform.testLoss());
            params.setValue("reconstruction_error", platform.reconstructionError());

            string paramsFileName = config.getValue("data_directory", "./data") + "/" + machineId + "-test.params";
            if (params.saveToFile(paramsFileName))
            {
                STDLOG.printf("        Saved test results as '%s'.\n", paramsFileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Failed to save test results as '%s'.\n", paramsFileName.c_str());
            }
        }

        delete machine;
    }
}

/**
 * Analyze the autocorrelation function of RBM Markov-chain
 * Monte-Carlo samplers.
 */
template<typename Scalar>
void experiment_rbm_acf(const Configuration &config)
{
    cout << "[rbm] Starting estimation of the " << ANSI_STYLE_BOLD << "autocorrelation function" << ANSI_STYLE_RESET << " for binary RBMs" << endl;

    vector<string> machineIds = string_split(config.getValue("acf_machines", ""), " ", true);

    vector<string> outputReductions = string_split(config.getValue("acf_output", "acf"), " ", true);

    for (const auto &machineId : machineIds)
    {
        cout << "[rbm] Machine ID: " << machineId << endl;

        BinaryRBM<Scalar> *machine = xminirbm_load_rbm<Scalar>(config, machineId);
        if (machine == NULL)
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        // initialize sampler
        machine->initializeMonteCarloSampler(config.getValueSize("mcmc_num_chains", 1), config.getValueSize("mcmc_thermalization_steps", 0));
        string initialStateFileName = config.getValue("acf_initial_state_file", "");
        if (initialStateFileName != "")
        {
            Array<Scalar, Dynamic, Dynamic> initialState;
            if (xminirbm_load_dataset(initialState, initialStateFileName, "", false))
            {
                STDLOG.printf("Loaded initial state for Monte-Carlo sampler from file '%s'.\n", initialStateFileName.c_str());
                machine->setMonteCarloSamplerState(initialState);
            }
            else
            {
                STDLOG.printf_err("Failed to load initial state for Monte-Carlo sampler from file '%s'.\n", initialStateFileName.c_str());
                return;
            }
        }

        // load correlation function specifications
        size_t numSamples = config.getValueSize("acf_num_samples", 1000);
        size_t stepSize = config.getValueSize("acf_step_size", 1);
        size_t numSteps = config.getValueSize("acf_num_steps", config.getValueSize("acf_num_samples", 1000));
        size_t thermalizationSteps = config.getValueSize("acf_thermalization_steps", 0);

        string functionId = config.getValue("acf_function", "id");
        function< Array<Scalar, Dynamic, Dynamic>(const Array<Scalar, Dynamic, Dynamic> &)> f;
        if (functionId == "square" || functionId == "^2" || functionId == "pow2")
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) -> Array<Scalar, Dynamic, Dynamic> {
                    return x*x;
                };
        }
        else if (functionId == "mean")
        {
            // take mean over all visible units
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) -> Array<Scalar, Dynamic, Dynamic> {
                    return x.rowwise().mean();
                };
        }
        else if (functionId == "corr2")
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) ->
                Array<Scalar, Dynamic, Dynamic> {
                    const int numUnitPairs = (x.cols() * (x.cols() - 1)) / 2;
                    Array<Scalar, Dynamic, Dynamic> corr2(x.rows(), numUnitPairs);
                    for (int chain = 0; chain < x.rows(); ++chain)
                    {
                        int pair = 0;
                        for (int i = 0; i < x.cols() - 1; ++i)
                        {
                            for (int j = i + 1; j < x.cols(); ++j)
                            {
                                corr2(chain, pair) = x(chain, i) * x(chain, j);
                                ++pair;
                            }
                        }
                    }

                    return corr2;
                };
        }
        else if (functionId == "corr2NN")
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) ->
                Array<Scalar, Dynamic, Dynamic> {
                    const int numUnitPairs = x.cols();
                    Array<Scalar, Dynamic, Dynamic> corrNN(x.rows(), numUnitPairs);
                    for (int chain = 0; chain < x.rows(); ++chain)
                    {
                        int pair = 0;
                        for (int i = 0; i < x.cols(); ++i)
                        {
                            corrNN(chain, pair) = x(chain, i) * x(chain, (i+1) % x.cols());
                            ++pair;
                        }
                    }

                    return corrNN;
                };
        }
        else if (functionId == "corr2NNN")
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) ->
                Array<Scalar, Dynamic, Dynamic> {
                    const int numUnitPairs = x.cols();
                    Array<Scalar, Dynamic, Dynamic> corrNNN(x.rows(), numUnitPairs);
                    for (int chain = 0; chain < x.rows(); ++chain)
                    {
                        int pair = 0;
                        for (int i = 0; i < x.cols(); ++i)
                        {
                            corrNNN(chain, pair) = x(chain, i) * x(chain, (i+2) % x.cols());
                            ++pair;
                        }
                    }

                    return corrNNN;
                };
        }
        else if (functionId == "corr2-mean")
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) ->
                Array<Scalar, Dynamic, Dynamic> {
                    const int numUnitsSquared = x.cols() * x.cols();
                    Array<Scalar, Dynamic, Dynamic> corr2Mean(x.rows(), 1);
                    for (int chain = 0; chain < x.rows(); ++chain)
                    {
                        corr2Mean(chain) = (x.row(chain).matrix().transpose() * x.row(chain).matrix()).mean() / numUnitsSquared;
                    }

                    return corr2Mean;
                };
        }
        else if (functionId == "corr3NN")
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) ->
                Array<Scalar, Dynamic, Dynamic> {
                    const int numUnitPairs = x.cols();
                    Array<Scalar, Dynamic, Dynamic> corrNN(x.rows(), numUnitPairs);
                    for (int chain = 0; chain < x.rows(); ++chain)
                    {
                        int pair = 0;
                        for (int i = 0; i < x.cols(); ++i)
                        {
                            corrNN(chain, pair) = x(chain, i) * x(chain, (i+1) % x.cols()) * x(chain, (i+2) % x.cols());
                            ++pair;
                        }
                    }

                    return corrNN;
                };
        }
        else // identity function
        {
            f = [](const Array<Scalar, Dynamic, Dynamic> &x) -> Array<Scalar, Dynamic, Dynamic> {
                    return x;
                };
        }

        string acfMode = config.getValue("acf_mode", "full");

        bool recordSampleMean = config.getValueBool("acf_record_sample_mean", false);
        Array<Scalar, Dynamic, Dynamic> sampleMean;

        function< Array<Scalar, Dynamic, Dynamic>(const Array<Scalar, Dynamic, Dynamic>&) > acfMeanTransform = [recordSampleMean, &sampleMean](const Array<Scalar, Dynamic, Dynamic> &mean) -> Array<Scalar, Dynamic, Dynamic> {
            if (recordSampleMean)
            {
                sampleMean = mean;
            }
            return mean;
        };
        if (config.getValueBool("acf_mean_over_chains", false))
        {
            acfMeanTransform = [recordSampleMean, &sampleMean](const Array<Scalar, Dynamic, Dynamic> &mean) -> Array<Scalar, Dynamic, Dynamic> {
                if (recordSampleMean)
                {
                    sampleMean = mean;
                }

                Array<Scalar, Dynamic, Dynamic> chainwiseMean(mean.rows(), mean.cols());
                for (long int unitIndex = 0; unitIndex < mean.cols(); ++unitIndex)
                {
                    chainwiseMean.col(unitIndex) = Array<Scalar, Dynamic, 1>::Constant(mean.rows(), mean.col(unitIndex).mean());
                }

                return chainwiseMean;
            };
        }


        function< bool(const vector< Array<Scalar, Dynamic, Dynamic> >&, const Array<Scalar, Dynamic, Dynamic>&) > acfCheckSampleConsistency;
        Scalar interchainStdDevThreshold = (Scalar) config.getValueReal("acf_interchain_stddev_threshold", NAN);
        Scalar interchainStdDev = NAN;
        if (ISNAN(interchainStdDevThreshold))
        {
            acfCheckSampleConsistency = [](const vector< Array<Scalar, Dynamic, Dynamic> > &samples, const Array<Scalar, Dynamic, Dynamic> &sampleMean) -> bool {
                return true;
            };
        }
        else
        {
            acfCheckSampleConsistency = [interchainStdDevThreshold, &interchainStdDev, recordSampleMean, &sampleMean](const vector< Array<Scalar, Dynamic, Dynamic> > &samples, const Array<Scalar, Dynamic, Dynamic> &mean) -> bool {
                if (recordSampleMean)
                {
                    sampleMean = mean;
                }

                interchainStdDev = 0.0;
                for (int unitIndex = 0; unitIndex < mean.cols(); ++unitIndex)
                {
                    interchainStdDev += SQRT((mean.col(unitIndex) - mean.col(unitIndex).mean()).square().sum());
                }
                interchainStdDev /= (mean.cols() * SQRT(mean.rows() - 1));
                cout << "[rbm] inter-chain standard deviation: " << interchainStdDev << endl;
                return (interchainStdDev <= interchainStdDevThreshold);
            };
        }


        DataSeries<Scalar> series;

        series.setDataPrefix("");
        series.setParamPrefix("# ");
        series.params.setValue("num-samples", numSamples);
        series.params.setValue("step-size", stepSize);
        series.params.setValue("thermalization-steps", thermalizationSteps);
        series.params.setValue("num-steps", numSteps);
        if (acfMode == "full")
        {
            // evaluate full autocorrelation function (ACF)
            // and estimate reduction from pre-calculated ACF values
            vector< Array<Scalar, Dynamic, Dynamic> > acf = machine->estimateAutocorrelationFunction(f, numSamples, stepSize, numSteps, thermalizationSteps, acfMeanTransform, acfCheckSampleConsistency, config.getValueBool("progress_monitor", false));

            if (acf.size() <= 0)
            {
                STDLOG.printf_wrn("Empty time interval, autocorrelation estimate did not produce any output.\n");
                return;
            }
            else
            {
                for (auto &reduction : outputReductions)
                {
                    if (reduction == "acf" || reduction == "autocorrelation-function")
                    {
                        int acfDim = acf[0].rows() * acf[0].cols();
                        Matrix<Scalar, Dynamic, Dynamic, RowMajor> fullACF(acf.size(), acfDim);
                        for (size_t s = 0; s < acf.size(); ++s)
                        {
                            fullACF.row(s) = Map< Array<Scalar, 1, Dynamic> >(acf[s].data(), acfDim);
                        }

                        series.data = fullACF;
                    }
                    else if (reduction == "umacf" || reduction == "unit-mean-autocorrelation-function")
                    {
                        // average autocorrelation functions over
                        // visible units (cols), but not over chains (rows)
                        Matrix<Scalar, Dynamic, Dynamic, RowMajor> unitMeanACF(acf.size(), acf[0].rows());
                        for (size_t s = 0; s < acf.size(); ++s)
                        {
                            unitMeanACF.row(s) = acf[s].rowwise().mean();
                        }

                        series.data = unitMeanACF;
                    }
                    else if (reduction == "miact" || reduction == "mean-integrated-autocorrelation-time")
                    {
                        // calculate mean over sampler chains (rows)
                        // and visible units/output dimensions (columns)
                        vector<Scalar> meanACF;
                        for (const auto &acfValue : acf)
                        {
                            meanACF.push_back(acfValue.mean());
                        }

                        // calculate integrated autocorrelation time
                        Scalar cThreshold = config.getValueReal("acf_iact_c", 5.0);
                        series.params.setValue("integrated-autocorrelation-time-threshold-c", cThreshold);

                        Scalar act = MonteCarloSampler<Scalar>::estimateIntegratedAutocorrelationTime(meanACF, cThreshold);
                        series.params.setValue("mean-integrated-autocorrelation-time", act);
                    }
                    else if (reduction == "iact" || reduction == "integrated-autocorrelation-time")
                    {
                        // calculate mean over sampler chains (rows)
                        // but NOT over output dimensions (columns)

                        // cycle output dimensions
                        const int numOutputDimensions = acf[0].cols();
                        Scalar sum = 0.0;
                        for (int i = 0; i < numOutputDimensions; ++i)
                        {
                            vector< Scalar > meanACF;
                            for (const auto &acfValue : acf)
                            {
                                meanACF.push_back(acfValue.col(i).mean());
                            }

                            // calculate integrated autocorrelation time
                            Scalar cThreshold = config.getValueReal("acf_iact_c", 5.0);
                            series.params.setValue("integrated-autocorrelation-time-threshold-c", cThreshold);

                            Scalar act = MonteCarloSampler<Scalar>::estimateIntegratedAutocorrelationTime(meanACF, cThreshold);
                            sum += act;
                            series.params.setValue("integrated-autocorrelation-time-" + to_string(i), act);
                        }
                        series.params.setValue("integrated-autocorrelation-time-mean", sum / numOutputDimensions);
                    }
                    else
                    {
                        STDLOG.printf_wrn("Unknown reduction '%s' of autocorrelation function, will be ignored.\n", reduction.c_str());
                    }
                }
            }
        }
        else if (acfMode == "time")
        {
            // calculate only integrated autocorrelation time
            // for one specific reduction
            if (outputReductions.size() != 1)
            {
                STDLOG.printf_err("Invalid combination of arguments for autocorrelation-function estimator: Can only evaluate exactly one reduction in mode 'time', but %ld were requested.\n", outputReductions.size());
                return;
            }

            Scalar cThreshold = config.getValueReal("acf_iact_c", 5.0);
            series.params.setValue("integrated-autocorrelation-time-threshold-c", cThreshold);

            Scalar unitVar = NAN;
            string reduction = outputReductions[0];
            if (reduction == "miact" || reduction == "mean-integrated-autocorrelation-time")
            {
                // calculate mean over sampler chains (rows)
                // and visible units/output dimensions (columns)
                function<Scalar(const Array<Scalar, Dynamic, Dynamic>&)> freduce_acf = [](const Array<Scalar, Dynamic, Dynamic> &acfValue) -> Scalar {
                        return acfValue.mean();
                    };
                Scalar act = machine->estimateIntegratedAutocorrelationTime(f, freduce_acf, numSamples, stepSize, numSteps, thermalizationSteps, cThreshold, acfMeanTransform, acfCheckSampleConsistency, &unitVar);
                series.params.setValue("mean-integrated-autocorrelation-time", act);
                series.params.setValue("unit-variance", unitVar);
                // cout << "finished estimate" << endl;
            }
            else if (reduction == "iact" || reduction == "integrated-autocorrelation-time")
            {
                // calculate mean over sampler chains (rows)
                // but NOT over output dimensions (columns)

                // cycle output dimensions
                const int numOutputDimensions = config.getValueInt("num_units", config.getValueInt("num_visible", 25));
                Scalar sum = 0.0;
                for (int i = 0; i < numOutputDimensions; ++i)
                {
                    function< Scalar(const Array<Scalar, Dynamic, Dynamic>&) > freduce_acf = [i](const Array<Scalar, Dynamic, Dynamic> &sample) -> Scalar {
                            return sample.col(i).mean();
                        };

                    Scalar act = machine->estimateIntegratedAutocorrelationTime(f, freduce_acf, numSamples, stepSize, numSteps, thermalizationSteps, cThreshold, acfMeanTransform, acfCheckSampleConsistency, &unitVar);
                    sum += act;
                    series.params.setValue("integrated-autocorrelation-time-" + to_string(i), act);
                    series.params.setValue("unit-variance-" + to_string(i), unitVar);
                }
                series.params.setValue("integrated-autocorrelation-time-mean", sum / numOutputDimensions);
            }
            else
            {
                STDLOG.printf_err("Unknown reduction '%s' for autocorrelation estimates in mode 'time'. No output will be produced.\n", reduction.c_str());
                return;
            }
        }

        if (! ISNAN(interchainStdDevThreshold))
        {
            series.params.setValue("interchain-stddev", interchainStdDev);
        }

        string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-acf";
        if (functionId == "square" || functionId == "^2" || functionId == "pow2")
        {
            fileName += "-pow2";
        }
        // else if (functionId == "mean")
        // {
        //     fileName += "-mean";
        // }
        // else if (functionId == "corr2-mean")
        // {
        //     fileName += "-corr2-mean";
        // }
        else if (functionId != "id")
        {
            fileName += "-" + functionId;
        }

        string postfix = config.getValue("acf_postfix", "");
        fileName += postfix;

        if (series.saveToFile(fileName + ".dat") == DWAMF_DATAIO_SUCCESS)
        {
            cout << "      Saved autocorrelation function estimate as '" << fileName << ".dat'." << endl;
        }
        else
        {
            STDLOG.printf_err("Could not save autocorrelation function estimate as '%s.dat'.\n", fileName.c_str());
        }

        if (recordSampleMean)
        {
            if (DataIO::exportMatrix(fileName + "-sample_mean.dat", sampleMean) == DWAMF_DATAIO_SUCCESS)
            {
                cout << "      Saved sample mean as '" << fileName << "-sample_mean.dat'." << endl;
            }
            else
            {
                STDLOG.printf_err("Could not save autocorrelation function estimate as '%s-sample_mean.dat'.\n", fileName.c_str());
            }
        }

        delete machine;
    }
}

template<typename Scalar>
void experiment_rbm_visflip(const Configuration &config)
{
    cout << "[rbm] Starting transformation 'visible-unit flip' of 'Binary RBM'" << endl;

    string machineId = config.getValue("visflip_machine", "");

    cout << "[rbm] Machine ID: " << machineId << endl;

    BinaryRBM<Scalar> *machine = xminirbm_load_rbm<Scalar>(config, machineId);
    if (machine == NULL)
    {
        STDLOG.printf_err("Failed to import machine.\n");
        return;
    }

    machine->flipVisibleUnits();

    string fileId = config.getValue("visflip_output_id", machineId + "-visflip");
    if (machine->saveToFile(config.getValue("data_directory", "./data"), fileId) == DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf("Saved machine with flipped visible units using ID '%s'.\n", fileId.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save machine with flipped visible units using ID '%s'.\n", fileId.c_str());
    }

    cout << "[rbm] Finished experiment." << endl;
}

template<typename Scalar>
void experiment_x2rbm_hdist(const Configuration &config)
{
    cout << "[x2rbm] Starting " << ANSI_STYLE_BOLD << "hidden distribution analysis" << ANSI_STYLE_RESET << " for exact binary RBMs" << endl;

    vector<string> machineIds = string_split(config.getValue("hdist_machines", ""), " ", true);

    int numHiddenVariables = config.getValueInt("num_hidden", 16);
    cout << "        # hidden units: " << numHiddenVariables << endl;

    Matrix<Scalar, Dynamic, 1> *stateDist = NULL;
    unordered_map<unsigned long int, Scalar> *stateProbs = NULL;
    vector<DistributionAnalyzer<Scalar>*> analyzers;
    if (! xminirbm_load_distribution_analyzers(&stateDist, &stateProbs, numHiddenVariables, analyzers, config))
    {
        STDLOG.printf_err("Failed to load analyzers.\n");
        return;
    }

    for (auto &machineId : machineIds)
    {
        cout << "[x2rbm] Machine ID: " << machineId << endl;
        // load original machine
        ExactBinaryRBM<Scalar> machine;
        string dataDir = config.getValue("data_directory", "./data");
        if (ExactBinaryRBM<Scalar>::loadFromFile(machine, dataDir, machineId))
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }
        // swap visible and hidden units
        // to adopt distribution analysis tools for visible variables below
        machine = machine.getTransposedMachine();

        // carry out analysis
        if (stateProbs == NULL)
        {
            // cycle all states
            machine.analyzeDistributionVisible(analyzers, stateDist, config.getValueBool("progress_monitor", true));
        }
        else
        {
            // cycle only states in *stateProbs map
            // NOTE: this branch will probably not be used
            //     because there will usually be no reference
            //     distribution for the hidden variables
            machine.analyzeDistributionVisible(analyzers, *stateProbs, stateDist, config.getValueBool("progress_monitor", true));
        }

        // store results
        if (stateDist != NULL)
        {
            string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-hdist-S.raw";
            if (DataIO::exportMatrixRaw(fileName, *stateDist) == DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf("Saved full state distribution as '%s'.\n", fileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Could not save full state distribution: File '%s' not accessible.\n", fileName.c_str());
            }
        }

        for (auto analyzer : analyzers)
        {
            string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-hdist-" + analyzer->getName() + ".dst";
            if (analyzer->saveToFile(fileName))
            {
                STDLOG.printf("Saved analysis of '%s' as '%s'.\n", analyzer->getName().c_str(), fileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Could not save analysis of '%s': File '%s' not accessible.\n", analyzer->getName().c_str(), fileName.c_str());
            }

            // reset analyzer
            analyzer->reset();
        }

        machine.saveMetaDataToFile(dataDir, machineId);
    }

    // free analyzer memory
    if (stateDist != NULL)
    {
        delete stateDist;
    }

    if (stateProbs != NULL)
    {
        delete stateProbs;
    }

    for (auto analyzer : analyzers)
    {
        delete analyzer;
    }
}

template<typename Scalar>
void experiment_x2rbm_vdist(const Configuration &config)
{
    cout << "[x2rbm] Starting " << ANSI_STYLE_BOLD << "visible distribution analysis" << ANSI_STYLE_RESET << " for exact binary RBMs" << endl;

    string dataId = config.getValue("data_id", "");
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        int imageWidth = config.getValueInt("image_width", 5);
        int imageHeight = config.getValueInt("image_height", 5);

        numVisibleVariables = imageHeight * imageWidth;
        cout << "        image dimensions: " << imageWidth << "x" << imageHeight << endl;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
        cout << "        # visible units: " << numVisibleVariables << endl;
    }

    vector<string> machineIds = string_split(config.getValue("vdist_machines", ""), " ", true);

    Matrix<Scalar, Dynamic, 1> *stateDist = NULL;
    unordered_map<unsigned long int, Scalar> *stateProbs = NULL;
    vector<DistributionAnalyzer<Scalar>*> analyzers;
    if (! xminirbm_load_distribution_analyzers(&stateDist, &stateProbs, numVisibleVariables, analyzers, config))
    {
        STDLOG.printf_err("Failed to load analyzers.\n");
        return;
    }

    for (auto &machineId : machineIds)
    {
        cout << "[x2rbm] Machine ID: " << machineId << endl;
        ExactBinaryRBM<Scalar> machine;
        string dataDir = config.getValue("data_directory", "./data");
        if (ExactBinaryRBM<Scalar>::loadFromFile(machine, dataDir, machineId))
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        // carry out analysis
        if (stateProbs == NULL)
        {
            // cycle all states
            machine.analyzeDistributionVisible(analyzers, stateDist, config.getValueBool("progress_monitor", true));
        }
        else
        {
            // cycle only states in *stateProbs map
            machine.analyzeDistributionVisible(analyzers, *stateProbs, stateDist, config.getValueBool("progress_monitor", true));
        }

        // store results
        if (stateDist != NULL)
        {
            string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-vdist-S" + config.getValue("output_postfix", "") + ".raw";
            if (DataIO::exportMatrixRaw(fileName, *stateDist) == DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf("Saved full state distribution as '%s'.\n", fileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Could not save full state distribution: File '%s' not accessible.\n", fileName.c_str());
            }
        }

        for (auto analyzer : analyzers)
        {
            string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-vdist-" + analyzer->getName() + config.getValue("output_postfix", "") + ".dst";
            if (analyzer->saveToFile(fileName))
            {
                STDLOG.printf("Saved analysis of '%s' as '%s'.\n", analyzer->getName().c_str(), fileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Could not save analysis of '%s': File '%s' not accessible.\n", analyzer->getName().c_str(), fileName.c_str());
            }

            // reset analyzer
            analyzer->reset();
        }

        machine.saveMetaDataToFile(dataDir, machineId);
    }

    // free analyzer memory
    if (stateDist != NULL)
    {
        delete stateDist;
    }

    if (stateProbs != NULL)
    {
        delete stateProbs;
    }

    for (auto analyzer : analyzers)
    {
        delete analyzer;
    }
}

template<typename Scalar>
void experiment_x2rbm_vdist_given_hdist(const Configuration &config)
{
    cout << "[x2rbm] Starting " << ANSI_STYLE_BOLD << "visible distribution analysis given hidden distribution" << ANSI_STYLE_RESET << " for exact binary RBMs" << endl;

    string dataId = config.getValue("data_id", "");
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        int imageWidth = config.getValueInt("image_width", 5);
        int imageHeight = config.getValueInt("image_height", 5);

        numVisibleVariables = imageHeight * imageWidth;
        cout << "        image dimensions: " << imageWidth << "x" << imageHeight << endl;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
        cout << "        # visible units: " << numVisibleVariables << endl;
    }

    vector<string> machineIds = string_split(config.getValue("vdist_machines", ""), " ", true);

    unordered_map<string, real_t> refStateDist;
    string refDistFile = config.getValue("sampling_distribution_file", "./dist.raw");
    if (! xminirbm_load_distribution(refStateDist, refDistFile, numVisibleVariables, config.getValue("sampling_distribution_type", "")))
    {
        STDLOG.printf_err("Failed to import reference distribution.\n");
        return;
    }

    vector<DistributionAnalyzer<Scalar>*> analyzers;
    if (! xminirbm_load_distribution_analyzers(analyzers, refStateDist, config))
    {
        STDLOG.printf_err("Failed to load analyzers.\n");
        return;
    }

    for (auto &machineId : machineIds)
    {
        cout << "[x2rbm] Machine ID: " << machineId << endl;
        // load machine
        ExactBinaryRBM<Scalar> machine;
        string dataDir = config.getValue("data_directory", "./data");
        if (ExactBinaryRBM<Scalar>::loadFromFile(machine, dataDir, machineId))
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        // load hidden distribution
        vector<real_t> hiddenStateDist;
        string hiddenDistFile = config.getValue("hidden_distribution_file",
            config.getValue("distribution_directory", config.getValue("data_directory", "./data")) + "/" + machineId + "-" + config.getValue("hidden_distribution_id", "hdist-S") + ".raw");
        if (DataIO::importListRaw(hiddenDistFile, hiddenStateDist) != DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf_err("Failed to import hidden state distribution.\n");
            continue;
        }

        // carry out analysis
        if (! machine.analyzeDistributionVisibleFromHidden(analyzers, refStateDist, hiddenStateDist, config.getValueBool("progress_monitor", true)))
        {
            STDLOG.printf_err("An error occurred during the analysis. No ouput will be generated.\n");
            continue;
        }
        
        // store results
        for (auto analyzer : analyzers)
        {
            string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-vdist_given_hdist-" + analyzer->getName() + config.getValue("output_postfix", "") + ".dst";
            if (analyzer->saveToFile(fileName))
            {
                STDLOG.printf("Saved analysis of '%s' as '%s'.\n", analyzer->getName().c_str(), fileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Could not save analysis of '%s': File '%s' not accessible.\n", analyzer->getName().c_str(), fileName.c_str());
            }

            // reset analyzer
            analyzer->reset();
        }

        machine.saveMetaDataToFile(dataDir, machineId);
    }

    // free analyzer memory
    for (auto analyzer : analyzers)
    {
        delete analyzer;
    }
}

void experiment_x2rbm_vdist_partial(const Configuration &config)
{
    cout << "[x2rbm] Starting " << ANSI_STYLE_BOLD << "partial visible distribution analysis" << ANSI_STYLE_RESET << " for exact binary RBMs" << endl;

    string dataId = config.getValue("data_id", "");
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        int imageWidth = config.getValueInt("image_width", 5);
        int imageHeight = config.getValueInt("image_height", 5);

        numVisibleVariables = imageHeight * imageWidth;
        cout << "        image dimensions: " << imageWidth << "x" << imageHeight << endl;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
        cout << "        # visible units: " << numVisibleVariables << endl;
    }

    vector<string> machineIds = string_split(config.getValue("vdist_machines", ""), " ", true);

    unordered_map<string, real_t> refStateDist;
    string refDistFile = config.getValue("sampling_distribution_file", "./dist.raw");

    if (! xminirbm_load_distribution(refStateDist, refDistFile, numVisibleVariables, config.getValue("sampling_distribution_type", "")))
    {
        STDLOG.printf_err("Failed to import reference distribution.\n");
        return;
    }

    vector<DistributionAnalyzer<real_t>*> analyzers;
    if (! xminirbm_load_distribution_analyzers(analyzers, refStateDist, config))
    {
        STDLOG.printf_err("Failed to load analyzers.\n");
        return;
    }

    for (auto &machineId : machineIds)
    {
        cout << "[x2rbm] Machine ID: " << machineId << endl;
        // load machine
        ExactBinaryRBM<real_t> machine;
        string dataDir = config.getValue("data_directory", "./data");
        if (ExactBinaryRBM<real_t>::loadFromFile(machine, dataDir, machineId))
        {
            STDLOG.printf_err("Failed to import machine.\n");
            continue;
        }

        // carry out analysis
        if (! machine.analyzeDistributionVisible(analyzers, refStateDist, config.getValueBool("progress_monitor", true)))
        {
            STDLOG.printf_err("An error occured during the analysis. No output will be generated.\n");
            continue;
        }

        // store results
        for (auto analyzer : analyzers)
        {
            string fileName = config.getValue("data_directory", "./data") + "/" + machineId + "-vdist_partial-" + analyzer->getName() + config.getValue("output_postfix", "") + ".dst";
            if (analyzer->saveToFile(fileName))
            {
                STDLOG.printf("Saved analysis of '%s' as '%s'.\n", analyzer->getName().c_str(), fileName.c_str());
            }
            else
            {
                STDLOG.printf_err("Could not save analysis of '%s': File '%s' not accessible.\n", analyzer->getName().c_str(), fileName.c_str());
            }

            // reset analyzer
            analyzer->reset();
        }

        machine.saveMetaDataToFile(dataDir, machineId);
    }
}

#endif
