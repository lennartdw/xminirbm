/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Auxiliary routines for experiments.
 */
 
#ifndef _XMINIRBM_EXPERIMENTS_UTILITIES
#define _XMINIRBM_EXPERIMENTS_UTILITIES

#include "rand/DiscreteDistribution.hpp"
#include "rand/EmpiricalDistribution.hpp"

#include "../datasets/BinaryGaussianMixtureDistribution.hpp"
#include "../networks/BinaryRBM.hpp"


template<typename Scalar>
string xminirbm_generate_machine_id(const Configuration &config)
{
    string machineId = config.getValue("machine_id", "");
    if (machineId == "")
    {
        // machine id not defined in configuration,
        // generate one according to experiment specifications
        string modelId = config.getValue("model_id", "x2rbm");
        machineId += modelId;
        if (modelId == "rbm")
        {
            real_t stddevWeights = config.getValueReal("initial_weights_stddev", 1.0e-2);
            real_t stddevBiasV = config.getValueReal("initial_vbias_stddev", 1.0e-1);
            real_t stddevBiasH = config.getValueReal("initial_hbias_stddev", 1.0e-1);
            bool adaptBiasV = config.getValueBool("initial_vbias_adapt", false);
            if (stddevWeights != 1.0e-2 || stddevBiasV != 1.0e-1 || stddevBiasH != 1.0e-1 || adaptBiasV)
            {
                machineId += "-i" + to_real_string(stddevWeights) + "w" + (adaptBiasV ? "S" : to_real_string(stddevBiasV)) + "a" + to_real_string(stddevBiasH) + "b";
            }
        }

        string dataId = config.getValue("data_id", "");
        if (dataId != "")
            machineId += "-" + dataId;
        if (is_same<Scalar, double>::value)
        {
            // do nothing
        }
        else if (is_same<Scalar, float>::value)
        {
            machineId += "-f32";
        }
        else
        {
            STDLOG.printf_wrn("Unsupported scalar type.\n");
        }

        string featuresId = config.getValue("data_features", "");
        if (featuresId != "")
            machineId += "-" + featuresId;

        if (dataId == "imgbw")
        {
            machineId += "-opx" + to_real_string(config.getValueReal("data_opx", 0.5));
        }
        else if (dataId == "imgbwpat")
        {
            machineId += "-opx" + to_real_string(config.getValueReal("data_opx", 0.5));
        }
        else if (dataId != "")
        {
            STDLOG.printf_wrn("Unsupported image type '%s'.\n", dataId.c_str());
        }

        int numSamplesTrain = config.getValueInt("training_num_samples", 5000);
        machineId += "-S" + to_string(numSamplesTrain);
        bool shuffleTrainingData = config.getValueBool("training_shuffle", true);
        if (shuffleTrainingData)
        {
            machineId += "x";
        }

        int numHiddenVariables = config.getValueInt("num_hidden", 4);
        machineId += "-NH" + to_string(numHiddenVariables);
        real_t learningRate = config.getValueReal("training_learning_rate", 1.0);
        machineId += "-_h" + to_real_string(learningRate);
        int batchSize = config.getValueInt("training_batch_size", 1000);
        machineId += "-B" + to_string(batchSize);
        real_t weightDecay = config.getValueReal("training_weight_decay", 0.0);
        if (weightDecay > 0.0)
        {
            machineId += "-_l" + to_real_string(weightDecay);
        }

        if (modelId == "rbm" || modelId == "cdrbm")
        {
            machineId += "-";
            if (config.getValueBool("cd_persistent", false))
            {
                machineId += "P";
            }
            machineId += "CD";
            if (config.getValueBool("cd_order_adaptive", false))
            {
                machineId += "A+S" + to_string(config.getValueSize("cd_order_adaptive_num_samples", 10000));
                real_t multiplierACDE = config.getValueReal("cd_order_adaptive_multiplier", 1.0);
                if (multiplierACDE != 1.0)
                {
                    machineId += "+m" + to_real_string(multiplierACDE);
                }
            }
            else
            {
                machineId += to_string(config.getValueInt("cd_order", 1));
            }

        }

        int machineSample = config.getValueInt("sample_offset", 0);
        if (machineSample > 0)
        {
            machineId += "-no" + to_string(machineSample);
        }
    }

    return machineId;
}

template<typename Derived>
string xminirbm_dataset_to_string(const DenseBase<Derived> &dataset, const function<string(const typename Derived::Scalar&)> &fentry_to_string = [](const typename Derived::Scalar &entry) -> string {
        return to_string(entry);
    }, const string &rowSeparator = "\n", const string &colSeparator = "\t")
{
    if (dataset.rows() <= 0 || dataset.cols() <= 0)
    {
        return "";
    }

    string str = "";
    for (long int row = 0; row < dataset.rows(); ++row)
    {
        string rowStr = fentry_to_string(dataset(row, 0));
        for (long int col = 1; col < dataset.cols(); ++col)
        {
            rowStr += colSeparator + fentry_to_string(dataset(row, col));
        }

        if (row > 0)
        {
            str += rowSeparator;
        }
        str += rowStr;
    }

    return str;
}

template<typename Derived>
string xminirbm_binary_dataset_to_string(const DenseBase<Derived> &dataset, const string &rowSeparator = "\n", const string &colSeparator = "", typename Derived::Scalar threshold = 0.5)
{
    return xminirbm_dataset_to_string(dataset, [threshold](const typename Derived::Scalar &entry) -> string {
            if (entry < threshold)
            {
                return "0";
            }
            else
            {
                return "1";
            }
        }, rowSeparator, colSeparator);
}

/**
 * Load a dataset from a file.
 * If checkDatasetBounds is true,
 * the shape (number of rows and columns) of "trainingData" must be nonzero,
 * and the rows (number of training samples) and columns (number of dimensions/
 * visible units) of the file's data must match the specified shape.
 * If the file type is unspecified, it is inferred from the file's extension.
 */
template<typename EigenMatrixOrArray>
bool xminirbm_load_dataset(EigenMatrixOrArray &trainingData, const string &trainingDataFileName, string fileType = "", bool checkDatasetBounds = true, long int numVisibleVariables = -1)
{
    typedef typename EigenMatrixOrArray::Scalar Scalar;

    long int numSamplesTrain = trainingData.rows();
    if (checkDatasetBounds)
    {
        numVisibleVariables = trainingData.cols();
    }
    else if (numVisibleVariables <= 0)
    {
        numVisibleVariables = trainingData.cols();
    }

    if (fileType == "")
    {
        fileType = file_extension(trainingDataFileName);
    }

    if (fileType == "raw")
    {
        vector<unsigned long int> rawTrainingData;
        if (DataIO::importListRaw(trainingDataFileName, rawTrainingData) != DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf_err("Failed to load dataset from file '%s'.\n", trainingDataFileName.c_str());

            return false;
        }
        else if (checkDatasetBounds && rawTrainingData.size() != (size_t) numSamplesTrain)
        {
            STDLOG.printf_err("Loaded raw dataset from file '%s', but number of entries (%ld) does not match with specified number of samples (%d).\n", trainingDataFileName.c_str(), rawTrainingData.size(), numSamplesTrain );

            return false;
        }
        else
        {
            STDLOG.printf("Loaded dataset from raw data file '%s'.\n", trainingDataFileName.c_str());
        }

        if (numSamplesTrain <= 0)
        {
            numSamplesTrain = (long int) rawTrainingData.size();
        }

        trainingData.resize(numSamplesTrain, numVisibleVariables);

        Matrix<Scalar, Dynamic, 1> nextSample(numVisibleVariables);
        for (int s = 0; s < numSamplesTrain; ++s)
        {
            trainingData.row(s) = integer_digits(nextSample, rawTrainingData[s], 2);
        }
    }
    else if (fileType == "idx")
    {
        Matrix<Scalar, Dynamic, Dynamic> trainingDataMat;
        if (DataIO::importMatrixIDX<Scalar, Dynamic, Dynamic, unsigned char>(trainingDataFileName, trainingDataMat, [](unsigned char px) -> Scalar {
                if (px < 128)
                {
                    return 0.0;
                }
                else
                {
                    return 1.0;
                }
            }) != DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf_err("Failed to load dataset from IDX file '%s'.\n", trainingDataFileName.c_str());

            return false;
        }

        if (checkDatasetBounds && trainingDataMat.rows() != numSamplesTrain)
        {
            STDLOG.printf_err("The provided IDX file '%s' contains %ld samples, but %ld were expected.\n", trainingDataFileName.c_str(), trainingDataMat.rows(), numSamplesTrain);

            return false;
        }

        if (checkDatasetBounds && trainingDataMat.cols() != numVisibleVariables)
        {
            STDLOG.printf_err("The provided IDX file '%s' contains samples for %ld visible units, but %ld were expected.\n", trainingDataFileName.c_str(), trainingDataMat.cols(), numVisibleVariables);

            return false;
        }

        trainingData = trainingDataMat.array();
    }
    else if (fileType == "txt")
    {
        // load text source
        string source;
        if (DataIO::importText(trainingDataFileName, source) == DWAMF_DATAIO_SUCCESS)
        {
            // convert to array
            if (BinaryRBM<Scalar>::constructStates(trainingData, source))
            {
                if (checkDatasetBounds && trainingData.rows() != numSamplesTrain)
                {
                    STDLOG.printf_err("Loaded dataset from file '%s', but number of entries/rows (%ld) does not match with specified number of samples (%ld).\n", trainingDataFileName.c_str(), trainingData.rows(), numSamplesTrain );

                    return false;
                }
                else if (checkDatasetBounds && trainingData.cols() != numVisibleVariables)
                {
                    STDLOG.printf_err("Loaded dataset from file '%s', but number of columns (%ld) does not match with specified number of visible units (%ld).\n", trainingDataFileName.c_str(), trainingData.cols(), numVisibleVariables );

                    return false;
                }
                else
                {
                    STDLOG.printf("Loaded dataset from text sources in file '%s'.\n", trainingDataFileName.c_str());
                }
            }
            else
            {
                STDLOG.printf_err("Loaded text sources for dataset from file '%s', but failed to convert them to a valid dataset.\n", trainingDataFileName.c_str());
            }
        }
        else
        {
            STDLOG.printf_err("Failed to load dataset from file '%s'.\n", trainingDataFileName.c_str());

            return false;
        }
    }
    else
    {
        // direct matrix import
        Matrix<Scalar, Dynamic, Dynamic> trainingDataMat;
        if (DataIO::importMatrix(trainingDataFileName, trainingDataMat) != DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf_err("Failed to load dataset from file '%s'.\n", trainingDataFileName.c_str());

            return false;
        }
        else if (checkDatasetBounds && trainingDataMat.rows() != numSamplesTrain)
        {
            STDLOG.printf_err("Loaded dataset from file '%s', but number of entries/rows (%ld) does not match with specified number of samples (%ld).\n", trainingDataFileName.c_str(), trainingDataMat.rows(), numSamplesTrain );

            return false;
        }
        else if (checkDatasetBounds && trainingDataMat.cols() != numVisibleVariables)
        {
            STDLOG.printf_err("Loaded dataset from file '%s', but number of columns (%ld) does not match with specified number of visible units (%ld).\n", trainingDataFileName.c_str(), trainingDataMat.cols(), numVisibleVariables );

            return false;
        }
        else
        {
            STDLOG.printf("Loaded dataset from file '%s'.\n", trainingDataFileName.c_str());
        }
        trainingData = trainingDataMat.array();
    }

    return true;
}

template<typename Key, typename Scalar>
bool xminirbm_load_empirical_distribution(unordered_map<Key, real_t> &stateProbsMap, const string &distFile, const long int numUnits, const function<Key(const Array<Scalar, Dynamic, Dynamic>&)> &fStateToKey = [](const Array<Scalar, Dynamic, Dynamic> &state) -> Key {
        return (Key) xminirbm_binary_dataset_to_string(state, "", "", 0.5);
    })
{
    cout << "        distribution type: empirical" << endl;
    Array<Scalar, Dynamic, Dynamic, RowMajor> data;
    if (! xminirbm_load_dataset(data, distFile, "", false, numUnits))
    {
        STDLOG.printf_err("Cannot generate empirical distribution from dataset.\n");
        return false;
    }

    real_t singleSampleProb = 1.0 / data.rows();
    cout << "        num. samples: " << data.rows() << endl;
    cout << "        num. visible units: " << data.cols() << endl;

    for (long int row = 0; row < data.rows(); ++row)
    {
        Key key = fStateToKey(data.row(row));
        stateProbsMap[key] += singleSampleProb;
    }

    cout << "        num. distinct samples: " << stateProbsMap.size() << endl;

    return true;
}

bool xminirbm_load_distribution(unordered_map<string, real_t> &stateProbsMap, const string &distFile, const long int numUnits, string distType = "", bool normalize = true)
{
    real_t probSum = 0.0;

    if (distType == "")
    {
        distType = file_extension(distFile);
    }

    if (distType == "empirical" || distType == "dat")
    {
        return xminirbm_load_empirical_distribution<string, real_t>(stateProbsMap, distFile, numUnits);
    }
    else if (distType == "full" || distType == "raw")
    {
        cout << "       distribution type: full probability list" << endl;
        vector<real_t> stateProbs;
        if (DataIO::importListRaw(distFile, stateProbs) == DWAMF_DATAIO_SUCCESS)
        {
            for (size_t stateIndex = 0; stateIndex < stateProbs.size(); ++stateIndex)
            {
                real_t prob = stateProbs[stateIndex];
                if (prob > 0.0)
                {
                    string key = to_base_string(stateIndex, 2, numUnits);
                    stateProbsMap[key] = prob;
                    probSum += prob;
                }
            }
        }
        else
        {
            STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
            return false;
        }
    }
    else //if (distType == "list")
    {
        cout << "        distribution type: key-value list" << endl;
        vector< ProbabilityInfo<string, real_t> > stateProbInfoList;
        function<int(ProbabilityInfo<string, real_t>*, const string&)> fParseProbInfo = [](ProbabilityInfo<string, real_t> *info, const string &source) -> int {
                return classify_parse_probability_info(info, source);
            };
        if (DataIO::importList(distFile, stateProbInfoList, fParseProbInfo) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("Loaded probability list from file '%s'.\n", distFile.c_str());

            for (auto &info : stateProbInfoList)
            {
                stateProbsMap[info.key] += info.probability;
                probSum += info.probability;
            }
        }
        else
        {
            STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
            return false;
        }

    }

    if (normalize && probSum != 1.0)
    {
        STDLOG.printf_inf("Probability distribution was not normalized (probability sum: %f). Will correct normalization before proceeding.\n", probSum);
        for (auto &stateProb : stateProbsMap)
        {
            stateProbsMap[stateProb.first] = stateProb.second / probSum;
        }
    }

    return true;
}

bool xminirbm_load_distribution(DiscreteDistribution<string> **distPtr, const string &distFile, const long int numUnits, string distType, const Configuration &config)
{
    unordered_map<string, real_t> stateProbsMap;
    if (distType == "empirical-gaussian")
    {
        if (xminirbm_load_empirical_distribution<string, real_t>(stateProbsMap, distFile, numUnits))
        {
            real_t stdDev = config.getValueReal("distribution_mixture_stddev", 1.0);
            STDLOG.printf("Generating Gaussian mixture with standard deviation %e from empirical distribution.\n", stdDev);
            *distPtr = new BinaryGaussianMixtureDistribution(stateProbsMap, numUnits, stdDev);
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        if (xminirbm_load_distribution(stateProbsMap, distFile, numUnits, distType))
        {
            *distPtr = new EmpiricalDistribution<string>(stateProbsMap);
            return true;
        }
        else
        {
            return false;
        }
    }
}

template<typename Scalar>
bool xminirbm_load_pattern_distribution(vector< typename ImageBW<Scalar>::Pattern > &patterns, vector<Scalar> &patternProbabilities, bool &periodicBoundaryConditions, const string featuresId)
{
    if (featuresId == "PH1")
    {
        Scalar dummy;
        patterns.push_back(datasets_pattern_hook<Scalar>(dummy));
        patternProbabilities.push_back(1.0);
        periodicBoundaryConditions = true;
    }
    // else if (featuresId == "PL")
    // {
    //     Scalar dummy;
    //     patterns.push_back(datasets_pattern_lineH<Scalar>(dummy));
    //     patternProbabilities.push_back(0.5);
    //     patterns.push_back(datasets_pattern_lineV<Scalar>(dummy));
    //     patternProbabilities.push_back(0.5);
    //     periodicBoundaryConditions = false;
    // }
    else if (featuresId == "PN")
    {
        patterns.push_back(datasets_patterns_0<Scalar>()[0]);
        patterns.push_back(datasets_patterns_1<Scalar>()[0]);
        patterns.push_back(datasets_patterns_2<Scalar>()[0]);
        patterns.push_back(datasets_patterns_3<Scalar>()[0]);
        patterns.push_back(datasets_patterns_4<Scalar>()[0]);
        patterns.push_back(datasets_patterns_5<Scalar>()[0]);
        patterns.push_back(datasets_patterns_6<Scalar>()[0]);
        patterns.push_back(datasets_patterns_7<Scalar>()[0]);
        patterns.push_back(datasets_patterns_8<Scalar>()[0]);
        patterns.push_back(datasets_patterns_9<Scalar>()[0]);

        for (int i = 0; i < 10; ++i)
        {
            patternProbabilities.push_back(0.1);
        }

        periodicBoundaryConditions = false;
    }
    else
    {
        STDLOG.printf_err("Invalid features id '%s'.\n", featuresId.c_str());
        return false;
    }

    return true;
}

template<typename Scalar>
unordered_map<unsigned long int, Scalar> xminirbm_load_state_distribution_PN(const Configuration &config, const Scalar catProb = 1.0)
{
    // load image probabilities from files
    // as generated by experiment_classify
    // from experiments/Classification
    unordered_map<unsigned long int, Scalar> stateProbs = new unordered_map<unsigned long int, Scalar>();
    for (int k = 0; k < 10; ++k)
    {
        string fileName = config.getValue("data_distribution_directory", config.getValue("data_directory", "./data")) + "/PN-" + to_string(config.getValueInt("image-width", 5)) + "x" + to_string(config.getValueInt("image-height", 5)) + "-cat" + to_string(k) + ".dat";
        vector< IndexProbabilityInfo<Scalar> > catImgInfoList;
        if (DataIO::importList(fileName, catImgInfoList, classify_parse_index_probability_info<Scalar>) == DWAMF_DATAIO_SUCCESS)
        {
            for (const auto &info : catImgInfoList)
            {
                auto imgProbEntry = stateProbs->find(info.index);
                if (imgProbEntry == stateProbs->end())
                {
                    (*stateProbs)[info.index] = catProb * info.probability;
                }
                else
                {
                    (*stateProbs)[info.index] += catProb * info.probability;
                }
            }
        }
        else
        {
            STDLOG.printf_err("Could not load image probabilities of category '%d'.\n", k);
            return NULL;
        }
    }

    // test if probabilities sum to unity:
    double probSum = 0.0;
    double entropy = 0.0;
    for (auto &ip : *stateProbs)
    {
        probSum += ip.second;
        entropy -= ip.second * LOG(ip.second);
    }
    if (FABS(1.0 - probSum) > 1.0e-6)
    {
        STDLOG.printf_wrn("Image probabilities do not sum to 1: found %e instead.\n", probSum);
    }
    cout << "prb sum: " << probSum << endl;
    cout << "entropy: " << entropy << endl;

    return stateProbs;
}

bool xminirbm_load_transformation_specifications(vector<string> &transformSpecs, const string &transform)
{
    if (string_startsWith(transform, "file:"))
    {
        string transformFileName = string_trim(transform.substr(5));
        if (DataIO::importText(transformFileName, transformSpecs) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("[traf] loaded transformation specifications from file '%s'\n", transformFileName.c_str());
        }
        else
        {
            STDLOG.printf_err("Failed to import transformation specifications from file '%s'.\n", transformFileName.c_str());
            return false;
        }
    }
    else
    {
        transformSpecs = string_split(transform, "\n", true);
    }

    return true;
}

/**
 * Export the provided dataset to the specified file.
 * If the file type is "raw", every scalar data point is converted to
 * a single bit indicating whether its value is above the threshold
 * (bit 1) or below (bit 0).
 * If the file type is unspecified, it is inferred from the file extension.
 */
template<typename Derived>
bool xminirbm_save_dataset(const ArrayBase<Derived> &dataset, const string &outFile, string fileType = "", const typename Derived::Scalar &threshold = 0.5)
{
    if (fileType == "")
    {
        fileType = file_extension(outFile);
    }

    if (fileType == "raw")
    {
        // raw binary states
        vector<unsigned long int> rawDataset;
        for (int s = 0; s < dataset.rows(); ++s)
        {
            unsigned long int entry = 0;
            unsigned long int bitFlag = 1;
            for (int i = 0; i < dataset.cols(); ++i)
            {
                if (dataset(s, i) >= threshold)
                {
                    entry |= bitFlag;
                }
                bitFlag <<= 1;
            }
            rawDataset.push_back(entry);
        }

        if (DataIO::exportListRaw(outFile, rawDataset) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("Saved raw dataset as '%s'.\n", outFile.c_str());
        }
        else
        {
            STDLOG.printf_err("Could not save raw dataset as '%s'.\n", outFile.c_str());
            return false;
        }
    }
    else if (fileType == "txt" || fileType == "str" || fileType == "string")
    {
        if (DataIO::exportText(outFile, BinaryRBM<typename Derived::Scalar>::statesToString(dataset)) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("Saved text dataset as '%s'.\n", outFile.c_str());
        }
        else
        {
            STDLOG.printf_err("Could not save text dataset as '%s'.\n", outFile.c_str());
            return false;
        }
    }
    else
    {
        // default: text based matrix
        if (DataIO::exportMatrix(outFile, dataset) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("Saved dataset as '%s'.\n", outFile.c_str());
        }
        else
        {
            STDLOG.printf_err("Could not save dataset as '%s'.\n", outFile.c_str());
            return false;
        }
    }

    return true;
}

template<typename Key, typename Scalar = real_t>
bool xminirbm_save_distribution(const unordered_map<Key, Scalar> &stateProbsMap, const string &distFile, const function<string(const Key&)> &fKeyToString = [](const Key &key) -> string {
        return (string) key;
    })
{
    function<string(const Scalar&)> valueToString = [](const Scalar &value) -> string {
            return to_prec_string(value);
        };
    if (DataIO::exportMap(distFile, stateProbsMap, fKeyToString, valueToString) == DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf("Saved distribution as '%s'.\n", distFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Could not save distribution as '%s'.\n", distFile.c_str());
        return false;
    }

    return true;
}

#endif
