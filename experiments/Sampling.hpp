/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Experiments for sampling from fixed distribution.
 */
 
#ifndef _XMINIRBM_EXPERIMENTS_SAMPLING
#define _XMINIRBM_EXPERIMENTS_SAMPLING

#include "rand/IndexDistribution.hpp"

void experiment_sample_indices(const Configuration &config)
{
    cout << "[sam] Starting " << ANSI_STYLE_BOLD << "experiment 'sample indices'" << ANSI_STYLE_RESET << endl;

    // load input distribution
    string distFile = config.getValue("sampling_distribution_file", "./dist.raw");
    IndexDistribution dist;
    if (IndexDistribution::loadFromFile(dist, distFile) != DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
        return;
    }

    // sample
    size_t numSamples = config.getValueSize("sampling_num_samples", 1);
    vector<unsigned long int> samples(numSamples);
    for (size_t s = 0; s < numSamples; ++s)
    {
        samples[s] = dist.getIntegerVariate();
    }

    // store samples
    string outFile = config.getValue("sampling_output_file", "./sample.raw");
    if (DataIO::exportListRaw(outFile, samples) != DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf_err("Failed to save samples to file '%s'.\n", outFile.c_str());
        return;
    }
    else
    {
        STDLOG.printf("Saved %ld samples as '%s'.\n", numSamples, outFile.c_str());
    }
}

#endif
