/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Distribution characterization and classification experiments.
 */

#ifndef _XMINIRBM_EXPERIMENTS_CLASSIFICATION
#define _XMINIRBM_EXPERIMENTS_CLASSIFICATION

#include "util/files.hpp"
#include "rand/DiscreteDistribution.hpp"

#include "../datasets/Classification.hpp"

#include "Utilities.hpp"

template<typename Scalar>
void experiment_classify(const Configuration &config)
{
    cout << "[class] Starting " << ANSI_STYLE_BOLD << "classification experiment" << ANSI_STYLE_RESET << endl;

    // load setup and hyperparameters
    string dataId = config.getValue("data_id", "imgbwpat");

    int imageWidth = config.getValueInt("image_width", 5);
    int imageHeight = config.getValueInt("image_height", 5);

    string featuresId = config.getValue("data_features", "PH1");

    bool periodicBoundaries = true;
    vector< vector< typename ImageBW<Scalar>::Pattern > > categories;
    if (featuresId == "PN")
    {
        categories.push_back(datasets_patterns_0<Scalar>());
        categories.push_back(datasets_patterns_1<Scalar>());
        categories.push_back(datasets_patterns_2<Scalar>());
        categories.push_back(datasets_patterns_3<Scalar>());
        categories.push_back(datasets_patterns_4<Scalar>());
        categories.push_back(datasets_patterns_5<Scalar>());
        categories.push_back(datasets_patterns_6<Scalar>());
        categories.push_back(datasets_patterns_7<Scalar>());
        categories.push_back(datasets_patterns_8<Scalar>());
        categories.push_back(datasets_patterns_9<Scalar>());

        periodicBoundaries = false;
    }

    Scalar dataOPX = config.getValueReal("data_opx", 0.5);

    vector< vector< IndexProbabilityInfo<Scalar> > > imgIndicesByCat = classify_imagebw<Scalar>(categories, imageWidth, imageHeight, dataOPX, periodicBoundaries);

    // save classifications
    for (size_t catIndex = 0; catIndex < categories.size(); ++catIndex)
    {
        string fileName = config.getValue("data_directory", "./data") + "/" + featuresId + "-" + to_string(imageWidth) + "x" + to_string(imageHeight) + "-cat" + to_string(catIndex) + ".dat";
        if (DataIO::exportList< IndexProbabilityInfo<Scalar> >(fileName,
                imgIndicesByCat[catIndex],
                [](const IndexProbabilityInfo<Scalar> &info) -> string {
                    return to_string(info.index) + "\t" + to_prec_string(info.probability);
                }) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("        saved %ld images of category %ld as '%s'\n", imgIndicesByCat[catIndex].size(), catIndex, fileName.c_str());
        }
        else
        {
            STDLOG.printf_err("Failed to save images of category %ld as '%s'.\n", catIndex, fileName.c_str());
        }
    }
}

template<typename Scalar>
void experiment_dist(const Configuration &config)
{
    cout << "[x2rbm] Starting " << ANSI_STYLE_BOLD << "distribution analysis" << ANSI_STYLE_RESET << endl;

    string dataId = config.getValue("data_id", "imgbwpat");

    int imageWidth = config.getValueInt("image_width", 5);
    int imageHeight = config.getValueInt("image_height", 5);
    int numVisibleVariables;
    if (string_startsWith(dataId, "img"))
    {
        numVisibleVariables = imageHeight * imageWidth;
        cout << "        image dimensions: " << imageWidth << "x" << imageHeight << endl;
    }
    else
    {
        numVisibleVariables = config.getValueInt("num_visible", 25);
        cout << "        # visible units: " << numVisibleVariables << endl;
    }

    string featuresId = config.getValue("data_features", "PH1");
    Scalar dataOPX = config.getValueReal("data_opx", 0.5);
    vector< typename ImageBW<Scalar>::Pattern > patterns;
    vector<Scalar> patternProbabilities;
    bool periodicBoundaryConditions;
    if (! xminirbm_load_pattern_distribution(patterns, patternProbabilities, periodicBoundaryConditions, featuresId))
    {
        return;
    }
    cout << "       distribution features id: " << featuresId << " with opx = " << dataOPX << endl;

    Matrix<Scalar, Dynamic, 1> distProbs(((unsigned long int) 1) << numVisibleVariables);

    ProgressMonitor monitor;
    long int updateSteps = 100000;
    long int updateCounter = 0;
    monitor.start();

    // cycle all states and calculate probabilities
    for (unsigned long int v = 0; v < (unsigned long int) distProbs.size(); ++v)
    {
        if ((++updateCounter) >= updateSteps)
        {
            monitor.update((real_t) v / distProbs.size());
            updateCounter = 0;
        }

        ImageBW<Scalar> img = ImageBW<Scalar>::createBinaryImage(imageWidth, imageHeight, v, periodicBoundaryConditions);
        distProbs(v) = img.getProbability(patterns, patternProbabilities, dataOPX);
    }

    monitor.end();

    string outFile = config.getValue("output_file", dataId + "-" + featuresId + "-opx" + to_real_string(dataOPX) + "-probs.raw");
    if (DataIO::exportMatrixRaw(outFile, distProbs) == DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf("Saved distribution as '%s'.\n", outFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save distribution as '%s'.\n", outFile.c_str());
    }
}

void experiment_crossent(const Configuration &config)
{
    cout << "[class] Starting " << ANSI_STYLE_BOLD << "cross entropy analysis" << ANSI_STYLE_RESET << endl;

    const int numUnits = config.getValueInt("num_units", config.getValueInt("num_visible", 25));

    // import distributions
    string targetDistFile = config.getValue("sampling_distribution_file", "");
    if (targetDistFile == "")
    {
        STDLOG.printf_err("No target/sampling distribution specified.\n");
        return;
    }
    unordered_map<string, real_t> targetDistProbsMap;
    if (! xminirbm_load_distribution(targetDistProbsMap, targetDistFile, numUnits, config.getValue("sampling_distribution_type", ""), config.getValueBool("sampling_distribution_normalize", config.getValueBool("distribution_normalize", true))))
    {
        STDLOG.printf_err("Failed to import target distribution.\n");
        return;
    }

    string modelDistFile = config.getValue("model_distribution_file", "");
    if (modelDistFile == "")
    {
        STDLOG.printf_err("No model distribution specified.\n");
        return;
    }

    DiscreteDistribution<string> *modelDistPtr = NULL;
    if (! xminirbm_load_distribution(&modelDistPtr, modelDistFile, numUnits, config.getValue("model_distribution_type", ""), config))
    {
        STDLOG.printf_err("Failed to import model distribution.\n");
        return;
    }

    real_t minModelProb = config.getValueReal("model_probability_min", 0.0);
    size_t numUnmatchedTargetProbs = 0;

    real_t targetEntropy = 0.0;
    real_t crossEntropy = 0.0;

    bool monitorProgress = config.getValueBool("progress_monitor", true);
    ProgressMonitor monitor;
    if (monitorProgress) monitor.start();
    long int progressUpdateSteps = MIN(targetDistProbsMap.size() / 10000, 50000);
    long int progressUpdateCounter = 0;
    long int totalStateCounter = 0;

    // real_t modelProbSum = 0.0;
    for (auto &targetEntry : targetDistProbsMap)
    {
        if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
        {
            monitor.update((real_t) totalStateCounter / targetDistProbsMap.size());
            progressUpdateCounter = 0;
        }

        if (targetEntry.second > 0.0)
        {
            targetEntropy -= targetEntry.second * LOG(targetEntry.second);

            real_t modelProb = modelDistPtr->getProbability(targetEntry.first);
            // modelProbSum += modelProb;
            if (modelProb <= 0.0)
            {
                if (minModelProb > 0.0)
                {
                    modelProb = minModelProb;
                    ++numUnmatchedTargetProbs;
                }
                else
                {
                    // no entry for target in model
                    targetEntropy = NAN;
                    crossEntropy = INFINITY;
                    break;
                }
            }

            crossEntropy -= targetEntry.second * LOG(modelProb);
        }

        ++totalStateCounter;
    }

    if (monitorProgress) monitor.end();

    Configuration output;
    if (minModelProb > 0.0)
    {
        output.setValue("num-unmatched-target-probabilities", numUnmatchedTargetProbs);
        real_t renormalizationConst = LOG(1.0 + numUnmatchedTargetProbs * minModelProb);
        output.setValue("renormalization-constant", renormalizationConst);
        crossEntropy += renormalizationConst;
    }

    output.setValue("target-entropy", targetEntropy);
    cout << "          target entropy: " << targetEntropy << endl;
    output.setValue("cross-entropy", crossEntropy);
    cout << "          cross entropy: " << crossEntropy << endl;

    string outFile = config.getValue("output_file", "crossent.dst");
    if (output.saveToFile(outFile))
    {
        STDLOG.printf("Saved cross entropy analysis as '%s'.\n", outFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save cross entropy analysis as '%s'.\n", outFile.c_str());
    }

    // free memory
    if (modelDistPtr != NULL)
    {
        delete modelDistPtr;
    }
}

void experiment_lpdist(const Configuration &config)
{
    cout << "[class] Starting " << ANSI_STYLE_BOLD << "Lp distance analysis" << ANSI_STYLE_RESET << endl;

    const int numUnits = config.getValueInt("num_units", config.getValueInt("num_visible", 25));

    // import distributions
    string targetDistFile = config.getValue("sampling_distribution_file", "");
    if (targetDistFile == "")
    {
        STDLOG.printf_err("No target/sampling distribution specified.\n");
        return;
    }
    unordered_map<string, real_t> targetDistProbsMap;
    if (! xminirbm_load_distribution(targetDistProbsMap, targetDistFile, numUnits, config.getValue("sampling_distribution_type", ""), config.getValueBool("sampling_distribution_normalize", config.getValueBool("distribution_normalize", true))))
    {
        STDLOG.printf_err("Failed to import target distribution.\n");
        return;
    }

    string modelDistFile = config.getValue("model_distribution_file", "");
    if (modelDistFile == "")
    {
        STDLOG.printf_err("No model distribution specified.\n");
        return;
    }
    unordered_map<string, real_t> modelDistProbsMap;
    if (! xminirbm_load_distribution(modelDistProbsMap, modelDistFile, numUnits, config.getValue("model_distribution_type", ""), config.getValueBool("sampling_distribution_normalize", config.getValueBool("distribution_normalize", true))))
    {
        STDLOG.printf_err("Failed to import model distribution.\n");
        return;
    }

    vector<int> pValues = config.getCPPVectorInt("distribution_Lp");
    vector<real_t> pDistances(pValues.size(), 0.0);

    bool monitorProgress = config.getValueBool("progress_monitor", true);
    ProgressMonitor monitor;
    if (monitorProgress) monitor.start();
    long int progressUpdateSteps = MIN(targetDistProbsMap.size() / 10000, 50000);
    long int progressUpdateCounter = 0;
    long int totalStateCounter = 0;

    // cycle target entries and compute contribution
    // from all registered states
    for (auto &targetEntry : targetDistProbsMap)
    {
        if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
        {
            monitor.update((real_t) totalStateCounter / (targetDistProbsMap.size() + modelDistProbsMap.size()));
            progressUpdateCounter = 0;
        }

        real_t targetProb = targetEntry.second;
        real_t modelProb;
        auto modelEntryIter = modelDistProbsMap.find(targetEntry.first);
        if (modelEntryIter == modelDistProbsMap.end())
        {
            modelProb = 0.0;
        }
        else
        {
            modelProb = modelEntryIter->second;
        }

        real_t probDiff = FABS(targetProb - modelProb);
        for (size_t pIndex = 0; pIndex < pValues.size(); ++pIndex)
        {
            if (pValues[pIndex] > 1)
            {
                pDistances[pIndex] += POW(probDiff, pValues[pIndex]);
            }
            else if (pValues[pIndex] < 0)
            {
                // max distance
                if (probDiff > pDistances[pIndex])
                {
                    pDistances[pIndex] = probDiff;
                }
            }
            else
            {
                pDistances[pIndex] += probDiff;
            }
        }

        ++totalStateCounter;
    }

    // cycle model entries and compute contribution
    // from states registered in the model map,
    // but not in the target map
    for (auto &modelEntry : modelDistProbsMap)
    {
        if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
        {
            monitor.update((real_t) totalStateCounter / (targetDistProbsMap.size() + modelDistProbsMap.size()));
            progressUpdateCounter = 0;
        }

        real_t modelProb = modelEntry.second;
        if (modelProb > 0.0 && targetDistProbsMap.find(modelEntry.first) == targetDistProbsMap.end())
        {
            for (size_t pIndex = 0; pIndex < pValues.size(); ++pIndex)
            {
                if (pValues[pIndex] > 1)
                {
                    pDistances[pIndex] += POW(modelProb, pValues[pIndex]);
                }
                else if (pValues[pIndex] < 0)
                {
                    // max distance
                    if (modelProb > pDistances[pIndex])
                    {
                        pDistances[pIndex] = modelProb;
                    }
                }
                else
                {
                    pDistances[pIndex] += modelProb;
                }
            }
        }

        ++totalStateCounter;
    }

    if (monitorProgress) monitor.end();

    Configuration output;
    for (size_t pIndex = 0; pIndex < pValues.size(); ++pIndex)
    {
        real_t dist;
        if (pValues[pIndex] > 1)
        {
            dist = POW(pDistances[pIndex], 1.0 / pValues[pIndex]);
        }
        else
        {
            dist = pDistances[pIndex];
        }

        string key = "L" + (pValues[pIndex] < 0 ? "inf" : to_string(pValues[pIndex])) + "-distance";
        output.setValue(key, dist);
        cout << "        " + key + ": " << dist << endl;
    }

    string outFile = config.getValue("output_file", "Lp-distance.dst");
    if (output.saveToFile(outFile))
    {
        STDLOG.printf("Saved Lp-distance analysis as '%s'.\n", outFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save Lp-distance analysis as '%s'.\n", outFile.c_str());
    }
}

void experiment_modes(const Configuration &config)
{
    // cf. [211104] for the definition of modes and strong modes

    cout << "[class] Starting " << ANSI_STYLE_BOLD << "mode analysis" << ANSI_STYLE_RESET << endl;

    string distFile = config.getValue("distribution_file", "");
    if (distFile == "")
    {
        STDLOG.printf_err("No distribution specified.\n");
        return;
    }

    int numVisible = config.getValueInt("num_visible", 25);
    int unitDim = config.getValueInt("dim_visible", 2);
    if (unitDim != 2)
    {
        STDLOG.printf_err("Mode calculation currently only available for visible units of dimension 2, but present distribution is supposed to have unit dimension %d.\n", unitDim);
        return;
    }

    vector<real_t> distProbs;
    if (DataIO::importListRaw(distFile, distProbs) != DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
        return;
    }
    else if (distProbs.size() != ((size_t) 1) << numVisible)
    {
        STDLOG.printf_err("Invalid dimension of distribution file: Expected %ld state probabilities for a system of %d units of dimension %d, but found %ld.\n", ((size_t) 1) << numVisible, numVisible, unitDim, distProbs.size());
        return;
    }

    vector<bool> isModeCandidate(distProbs.size(), true);
    vector<unsigned long int> modes;
    vector<unsigned long int> strongModes;

    // cycle all states and check if they are (strong) modes
    for (unsigned long int v = 0; v < distProbs.size(); ++v)
    {
        if (isModeCandidate[v])
        {
            real_t neighborProbSum = 0.0;
            // cycle all neighbors (all states with Hamming distance 1)
            for (unsigned long int k = 0; k < (unsigned long int) numVisible; ++k)
            {
                unsigned long int cunit = v ^ (((unsigned long int) 1) << k);
                real_t cprob = distProbs[cunit];
                if (cprob < distProbs[v])
                {
                    isModeCandidate[cunit] = false;
                    neighborProbSum += cprob;
                }
                else
                {
                    // at least one neighbor with higher probability
                    isModeCandidate[v] = false;
                    break;
                }
            }

            if (isModeCandidate[v])
            {
                modes.push_back(v);
                if (distProbs[v] > neighborProbSum)
                {
                    strongModes.push_back(v);
                }
            }
        }
    }

    string outFile = config.getValue("output_file", "totcorr.dst");
    Configuration output;
    output.setValue("num_modes", modes.size());
    cout << "       number of modes: " << modes.size() << endl;
    output.setValue("modes", modes);
    output.setValue("num_strong_modes", strongModes.size());
    cout << "       number of strong modes: " << strongModes.size() << endl;
    output.setValue("strong_modes", strongModes);

    if (output.saveToFile(outFile))
    {
        STDLOG.printf("Saved mode analysis as '%s'.\n", outFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save mode analysis as '%s'.\n", outFile.c_str());
    }
}

void experiment_totcorr(const Configuration &config)
{
    cout << "[class] Starting " << ANSI_STYLE_BOLD << "total correlation analysis" << ANSI_STYLE_RESET << endl;

    string distFile = config.getValue("distribution_file", "");
    if (distFile == "")
    {
        STDLOG.printf_err("No distribution specified.\n");
        return;
    }

    string distType = config.getValue("distribution_type", "*");
    string distFileType = file_extension(distFile);

    int numVisible = config.getValueInt("num_visible", 25);
    int unitDim = config.getValueInt("dim_visible", 2);


    vector<real_t> fullDistProbs;
    unordered_map<unsigned long int, real_t> stateProbsMap;
    bool useFullDist;

    if (distType == "empirical")
    {
        useFullDist = false;
        cout << "        distribution type: empirical" << endl;
        // import empirical distribution
        vector<unsigned long int> samples;
        if (DataIO::importListRaw(distFile, samples) != DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf_err("Failed to load samples of empirical distribution from file '%s'.\n", distFile.c_str());
            return;
        }
        real_t singleSampleProb = 1.0 / samples.size();
        cout << "        num. samples: " << samples.size() << endl;

        for (auto stateIndex : samples)
        {
            auto stateInfo = stateProbsMap.find(stateIndex);
            if (stateInfo == stateProbsMap.end())
            {
                stateProbsMap[stateIndex] = singleSampleProb;
            }
            else
            {
                stateProbsMap[stateIndex] += singleSampleProb;
            }
        }

        cout << "        num. distinct samples: " << stateProbsMap.size() << endl;
    }
    else
    {
        cout << "        distribution type: default" << endl;
        if (distFileType == "raw")
        {
            useFullDist = true;

            // load full distribution
            if (DataIO::importListRaw(distFile, fullDistProbs) != DWAMF_DATAIO_SUCCESS)
            {
                STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
                return;
            }
            else if (fullDistProbs.size() != ((size_t) 1) << numVisible)
            {
                STDLOG.printf_err("Invalid dimension of distribution file: Expected %ld state probabilities for a system of %d units of dimension %d, but found %ld.\n", ((size_t) 1) << numVisible, numVisible, unitDim, fullDistProbs.size());
                return;
            }
        }
        else
        {
            useFullDist = false;

            // load mapping from states to probabilities
            real_t probSum = 0.0;
            vector< IndexProbabilityInfo<real_t> > stateProbInfoList;
            if (DataIO::importList(distFile, stateProbInfoList, classify_parse_index_probability_info<real_t>) == DWAMF_DATAIO_SUCCESS)
            {
                for (const auto &info : stateProbInfoList)
                {
                    auto stateProbEntry = stateProbsMap.find(info.index);
                    if (stateProbEntry == stateProbsMap.end())
                    {
                        stateProbsMap[info.index] = info.probability;
                    }
                    else
                    {
                        stateProbsMap[info.index] += info.probability;
                    }
                    probSum += info.probability;
                }

                if (probSum != 1.0)
                {
                    STDLOG.printf_inf("Probability distribution was not normalized (probability sum: %f). Will correct normalization before proceeding.\n", probSum);
                    for (auto &stateProb : stateProbsMap)
                    {
                        stateProbsMap[stateProb.first] = stateProb.second / probSum;
                    }
                }
            }
            else
            {
                STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
                return;
            }
        }
    }


    real_t jointEntropy = 0.0;

    Array<real_t, Dynamic, Dynamic> margProbs = Array<real_t, Dynamic, Dynamic>::Zero(numVisible, unitDim);

    if (useFullDist)
    {
        for (unsigned long int v = 0; v < fullDistProbs.size(); ++v)
        {
            if (fullDistProbs[v] > 0.0)
            {
                jointEntropy -= fullDistProbs[v] * LOG(fullDistProbs[v]);

                vector<unsigned long int> unitValues = integer_digits(v, unitDim, numVisible);

                // cycle units and adjust marginal distributions
                for (int k = 0; k < numVisible; ++k)
                {
                    margProbs(k, unitValues[k]) += fullDistProbs[v];
                }
            }
        }
    }
    else
    {
        for (auto &stateProb : stateProbsMap)
        {
            jointEntropy -= stateProb.second * LOG(stateProb.second);
            vector<unsigned long int> unitValues = integer_digits(stateProb.first, unitDim, numVisible);
            // cycle units and adjust marginal distributions
            for (int k = 0; k < numVisible; ++k)
            {
                margProbs(k, unitValues[k]) += stateProb.second;
            }
        }
    }
    cout << "        joint entropy: " << jointEntropy << endl;

    string outFile = config.getValue("output_file", "totcorr.dst");
    Configuration output;
    output.setValue("joint-entropy", jointEntropy);

    real_t margEntropySum = 0.0;
    for (int k = 0; k < numVisible; ++k)
    {
        real_t margEntropy = 0.0;
        for (int n = 0; n < unitDim; ++n)
        {
            const real_t margProb = margProbs(k, n);
            if (margProb > 0.0)
            {
                margEntropy -= margProb * LOG(margProb);
            }
        }

        output.setValue("marginal-entropy-" + to_string(k), margEntropy);
        margEntropySum += margEntropy;
    }
    cout << "        sum of marginal entropies: " << margEntropySum << endl;

    real_t totCorr = margEntropySum - jointEntropy;
    cout << "        total correlation: " << totCorr << endl;
    output.setValue("total-correlation", totCorr);

    if (output.saveToFile(outFile))
    {
        STDLOG.printf("Saved total correlation analysis as '%s'.\n", outFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save total correlation analysis as '%s'.\n", outFile.c_str());
    }
}

void experiment_totcorr_empirical(const Configuration &config)
{
    cout << "[class] Starting " << ANSI_STYLE_BOLD << "empirical total correlation analysis" << ANSI_STYLE_RESET << endl;

    string distFile = config.getValue("distribution_file", "");
    if (distFile == "")
    {
        STDLOG.printf_err("No distribution specified.\n");
        return;
    }

    int numVisible = config.getValueInt("num_visible", 25);
    int unitDim = config.getValueInt("dim_visible", 2);

    unordered_map<string, real_t> stateProbsMap;
    if (! xminirbm_load_empirical_distribution<string, real_t>(stateProbsMap, distFile, numVisible))
    {
        STDLOG.printf_err("Failed to import empirical distribution for dataset from file '%s'.\n", distFile.c_str());
        return;
    }

    real_t jointEntropy = 0.0;
    Array<real_t, Dynamic, Dynamic> margProbs = Array<real_t, Dynamic, Dynamic>::Zero(numVisible, unitDim);
    Array<int, 1, Dynamic> unitValues(numVisible);

    for (auto &stateProb : stateProbsMap)
    {
        jointEntropy -= stateProb.second * LOG(stateProb.second);
        if (BinaryRBM<real_t>::constructStates(unitValues, stateProb.first))
        {
            // cycle units and adjust marginal distributions
            for (int k = 0; k < numVisible; ++k)
            {
                margProbs(k, unitValues(k)) += stateProb.second;
            }
        }
        else
        {
            STDLOG.printf_err("Key '%s' is not a valid state representation.\n", stateProb.first.c_str());
            return;
        }
    }

    cout << "        joint entropy: " << jointEntropy << endl;
    string outFile = config.getValue("output_file", "totcorr.dst");
    Configuration output;
    output.setValue("joint-entropy", jointEntropy);

    real_t margEntropySum = 0.0;
    for (int k = 0; k < numVisible; ++k)
    {
        real_t margEntropy = 0.0;
        for (int n = 0; n < unitDim; ++n)
        {
            const real_t margProb = margProbs(k, n);
            if (margProb > 0.0)
            {
                margEntropy -= margProb * LOG(margProb);
            }
        }

        output.setValue("marginal-entropy-" + to_string(k), margEntropy);
        margEntropySum += margEntropy;
    }
    cout << "        sum of marginal entropies: " << margEntropySum << endl;

    real_t totCorr = margEntropySum - jointEntropy;
    cout << "        total correlation: " << totCorr << endl;
    output.setValue("total-correlation", totCorr);

    if (output.saveToFile(outFile))
    {
        STDLOG.printf("Saved total correlation analysis as '%s'.\n", outFile.c_str());
    }
    else
    {
        STDLOG.printf_err("Failed to save total correlation analysis as '%s'.\n", outFile.c_str());
    }
}

void experiment_margprobs(const Configuration &config)
{
    cout << "[class] Starting " << ANSI_STYLE_BOLD << "marginal distributions analysis" << ANSI_STYLE_RESET << endl;

    string distFile = config.getValue("distribution_file", "");
    if (distFile == "")
    {
        STDLOG.printf_err("No distribution specified.\n");
        return;
    }

    int numVisible = config.getValueInt("num_visible", 25);
    int unitDim = config.getValueInt("dim_visible", 2);

    vector<int> margOrders;
    try
    {
        margOrders = config.getCPPVectorInt("marginal_distribution_orders");
    }
    catch (int e)
    {
        margOrders.push_back(1);
    }

    vector<real_t> distProbs;
    if (DataIO::importListRaw(distFile, distProbs) != DWAMF_DATAIO_SUCCESS)
    {
        STDLOG.printf_err("Could not load input distribution from file '%s'.\n", distFile.c_str());
        return;
    }
    else if (distProbs.size() != ((size_t) 1) << numVisible)
    {
        STDLOG.printf_err("Invalid dimension of distribution file: Expected %ld state probabilities for a system of %d units of dimension %d, but found %ld.\n", ((size_t) 1) << numVisible, numVisible, unitDim, distProbs.size());
        return;
    }

    // prepare tables for marginal probabilities
    // of all requested orders,
    // cf. [211029]
    vector< Array<real_t, Dynamic, Dynamic> > margProbs;
    for (auto &margOrder : margOrders)
    {
        margProbs.push_back(Array<real_t, Dynamic, Dynamic>::Zero(sets_binomial(numVisible, margOrder), powi(unitDim, margOrder)));
    }

    ProgressMonitor monitor;
    bool monitorProgress = config.getValueBool("progress_monitor", "true");
    if (monitorProgress)
        monitor.start();

    // cycle all states
    for (unsigned long int v = 0; v < distProbs.size(); ++v)
    {
        if (monitorProgress)
            monitor.update((real_t) v / distProbs.size());

        if (distProbs[v] > 0.0)
        {
            // cycle marginal distributions of different orders
            for (int margIndex = 0; margIndex < (int) margOrders.size(); ++margIndex)
            {
                // cycle all marginal configurations
                for (int confIndex = 0; confIndex < margProbs[margIndex].rows(); ++confIndex)
                {
                    // transform confIndex to unit specification
                    // and select appropriate probability column
                    // according to v
                    vector<int> units = sets_subset_from_rank(margOrders[margIndex], confIndex);
                    int probCol = 0;

                    for (int unitIndex = 0; unitIndex < (int) units.size(); ++unitIndex)
                    {
                        if (v & ((unsigned long int) 1) << units[unitIndex])
                        {
                            probCol |= (1 << unitIndex);
                        }
                    }
                    
                    margProbs[margIndex](confIndex, probCol) += distProbs[v];
                }
            }
        }
    }

    if (monitorProgress)
        monitor.end();

    // save results
    string outFileRoot = config.getValue("output_file", "margprobs");
    for (int margIndex = 0; margIndex < (int) margOrders.size(); ++margIndex)
    {
        string outFile = outFileRoot + "-" + to_string(margOrders[margIndex]) + ".dst";
        if (DataIO::exportMatrix(outFile, margProbs[margIndex]) == DWAMF_DATAIO_SUCCESS)
        {
            STDLOG.printf("Saved %d-marginal probabilities as '%s'.\n", margOrders[margIndex], outFile.c_str());
        }
        else
        {
            STDLOG.printf_err("Failed to save %d-marginal probabilities as '%s'.\n", margOrders[margIndex], outFile.c_str());
        }
    }
}

#endif
