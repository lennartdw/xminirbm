/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Experiments for dataset transformations.
 */

#ifndef _XMINIRBM_EXPERIMENTS_TRANSFORMATION
#define _XMINIRBM_EXPERIMENTS_TRANSFORMATION

#include "func/matrices.hpp"
#include "traf/vectors.hpp"

template<typename Scalar>
void experiment_transform_dataset(const Configuration &config)
{
    cout << "[traf] Starting " << ANSI_STYLE_BOLD << "dataset transformation experiment" << ANSI_STYLE_RESET << endl;

    // load transformation specifications
    vector<string> transformSpecs;
    if (! xminirbm_load_transformation_specifications(transformSpecs, config.getValue("data_transform", "")))
    {
        STDLOG.printf_err("Invalid transformation specification.\n");
        return;
    }

    // import data
    string dataFileName = config.getValue("data_file", "");
    if (dataFileName == "")
    {
        STDLOG.printf_err("No data file specified.\n");
        return;
    }

    size_t numSamples = config.getValueSize("data_num_samples", config.getValueSize("training_num_samples", 5000));
    size_t numUnits = config.getValueSize("data_dimension", config.getValueSize("num_visible", config.getValueSize("image_width", 5) * config.getValueSize("image_height", 5)));
    Array<Scalar, Dynamic, Dynamic, RowMajor> dataset(numSamples, numUnits);
    if (! xminirbm_load_dataset(dataset, dataFileName))
    {
        STDLOG.printf_err("Failed to load dataset to be transformed.\n");
        return;
    }

    // carry out transformation
    Array<Scalar, Dynamic, Dynamic, RowMajor> transformedDataset;
    if (transformSpecs.size() <= 0)
    {
        STDLOG.printf_wrn("The provided transformation specifications are empty. No transformation will be applied.\n");
        transformedDataset = dataset;
    }
    else
    {
        transformedDataset = array_transform_rows(dataset, vector_parse_transform< Array<Scalar, 1, Dynamic> >(transformSpecs));
        STDLOG.printf("[traf] applied transformation\n");
    }
    
    // store result
    string defaultDir, defaultName;
    file_path_decomposition(defaultDir, defaultName, dataFileName);
    string outFile = config.getValue("output_file", defaultDir + "/transformed-" + defaultName);
    if (! xminirbm_save_dataset(transformedDataset, outFile))
    {
        STDLOG.printf_err("Could not save transformed dataset.\n");
        return;
    }

}

template<typename Scalar>
void experiment_transform_dist(const Configuration &config)
{
    cout << "[traf] Starting " << ANSI_STYLE_BOLD << "distribution transformation experiment" << ANSI_STYLE_RESET << endl;

    // load transformation specifications
    vector<string> transformSpecs;
    if (! xminirbm_load_transformation_specifications(transformSpecs, config.getValue("distribution_transform", "")))
    {
        STDLOG.printf_err("Invalid transformation specification.\n");
        return;
    }
    auto ftransform = vector_parse_transform< Array<Scalar, 1, Dynamic> >(transformSpecs);

    // import distribution
    string distFile = config.getValue("distribution_file", "");
    if (distFile == "")
    {
        STDLOG.printf_err("No distribution specified.\n");
        return;
    }

    int numUnits = config.getValueInt("num_units", config.getValueInt("num_visible", 25));

    unordered_map<string, real_t> stateProbsMap;
    string distType = config.getValue("distribution_type", "");
    if (! xminirbm_load_distribution(stateProbsMap, distFile, numUnits, distType, config.getValueBool("distribution_normalize", true)))
    {
        STDLOG.printf_err("Failed to import distribution from file '%s'.\n", distFile.c_str());
        return;
    }

    unordered_map<string, real_t> transformedStateProbsMap;
    for (auto &sourceProbEntry : stateProbsMap)
    {
        Array<Scalar, Dynamic, Dynamic> inputState;
        if (BinaryRBM<Scalar>::constructStates(inputState, sourceProbEntry.first))
        {
            Array<Scalar, Dynamic, Dynamic> transformedState = array_transform_rows(inputState, ftransform);
            string transformedKey = xminirbm_binary_dataset_to_string(transformedState);
            transformedStateProbsMap[transformedKey] += sourceProbEntry.second;
            // Note: If transformedKey does not exist in the map yet,
            //     the '+=' operation will create a new entry and
            //     initialize it to 0 before performing the increment.
        }
        else
        {
            STDLOG.printf_err("Failed to transform distribution: Corrupted input state '%s.\n'", sourceProbEntry.first.c_str());
            return;
        }
    }

    // store result
    string defaultDir, defaultName;
    file_path_decomposition(defaultDir, defaultName, distFile);
    string outFile = config.getValue("output_file", defaultDir + "/transformed-" + defaultName);
    if (! xminirbm_save_distribution(transformedStateProbsMap, outFile))
    {
        STDLOG.printf_err("Could not save transformed distribution.\n");
        return;
    }
}

#endif
