/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Platform for training and testing unsupervised networks.
 */

#ifndef _XMINIRBM_PLATFORMS_UNSUPERVISEDPLATFORM
#define _XMINIRBM_PLATFORMS_UNSUPERVISEDPLATFORM

#include "../networks/UnsupervisedMachine.hpp"

template<typename Scalar>
class UnsupervisedPlatform
{
    private:
        UnsupervisedMachine<Scalar> *machine = NULL;
        int batchSize;
        Scalar learningRate; // TODO: replace by learning schedule
        Scalar weightDecay;
        bool shuffleTrainingSamples = true;
        int numEpochs = 0;

        Scalar lastTrainingLoss;
        Scalar lastTestLoss;
        Scalar lastReconstructionError;

        int verbosity = 1;

    public:
        UnsupervisedPlatform(UnsupervisedMachine<Scalar> *machine, int batchSize, Scalar learningRate, Scalar weightDecay = 0.0, bool shuffleTrainingSamples = true, int startingEpoch = 0)
        {
            this->machine = machine;
            this->batchSize = batchSize;
            this->learningRate = learningRate;
            this->weightDecay = weightDecay;
            this->shuffleTrainingSamples = shuffleTrainingSamples;
            this->numEpochs = startingEpoch;
        }

        /**
         * Obtain the network managed by this platform.
         */
        virtual const UnsupervisedMachine<Scalar> *getMachine()
        {
            return machine;
        }

        static UnsupervisedPlatform fromConfiguration(UnsupervisedMachine<Scalar> *machine, const Configuration &config)
        {
            int startingEpoch = config.getValueInt("training_epoch_start", -1);
            Scalar learningRate = config.getValueReal("training_learning_rate", 1.0);
            Scalar weightDecay = config.getValueReal("training_weight_decay", 0.0);
            int batchSize = config.getValueInt("training_batch_size", 1000);
            bool shuffleTrainingData = config.getValueBool("training_shuffle", true);
            return UnsupervisedPlatform(machine, batchSize, learningRate, weightDecay, shuffleTrainingData, startingEpoch);
        }

        virtual int getNumEpochs() const
        {
            return numEpochs;
        }

        virtual bool isSnapshotEpoch(int t) const
        {
            return (t <= 20
                    || (t <= 60 && t % 5 == 0)
                    || (t <= 200 && t % 10 == 0)
                    || (t <= 400 && t % 20 == 0)
                    || (t <= 1000 && t % 50 == 0)
                    || (t <= 2500 && t % 100 == 0)
                    || (t <= 10000 && t % 250 == 0)
                    || (t <= 25000 && t % 500 == 0)
                    || (t <= 100000 && t % 1000 == 0)
                    || (t <= 400000 && t % 2000 == 0)
                    || (t <= 1000000 && t % 5000 == 0)
                    || (t <= 10000000 && t % 10000 == 0));
        }

        virtual Scalar reconstructionError() const
        {
            return lastReconstructionError;
        }

        virtual void setNumEpochs(const int value)
        {
            numEpochs = value;
        }

        /**
         * Perform a training epoch using the provided samples.
         */
        virtual int train(const Array<Scalar, Dynamic, Dynamic> &samples, bool monitorProgress = true)
        {
            numEpochs += 1;

            time_t startTime = time(NULL);

            // cout << "First sample:" << endl;
            // cout << samples.row(0) << endl;

            vector<int> indices = range_generate<int>(samples.rows());
            if (shuffleTrainingSamples)
            {
                // Mersenne twister random number generator
                random_device rd;
                mt19937 rng(rd());
                shuffle(indices.begin(), indices.end(), rng);
            }
            // cout << "batch size = " << batchSize << endl;
            // cout << "sample size = " << samples.rows() << endl;
            // cout << "num visible = " << machine->getNumVisible() << endl;
            STDLOG.printf("Training epoch #%d ... ", numEpochs);
            lastTrainingLoss = 0.0;

            ProgressMonitor monitor;
            if (monitorProgress) monitor.start();
            
            Array<Scalar, Dynamic, Dynamic> batch(batchSize, machine->getNumVisible());
            for (int offset = 0; offset < samples.rows(); offset += batchSize)
            {
                if (monitorProgress) monitor.update((real_t) offset / samples.rows());

                // load batch data
                int batchEnd = offset + batchSize;
                if (batchEnd > samples.rows())
                {
                    batchEnd = samples.rows();
                    batch.resize(batchEnd - offset, machine->getNumVisible());
                }

                for (int k = 0; k < batchEnd - offset; ++k)
                {
                    batch.row(k) = samples.row(indices[offset + k]);
                }

                // perform training step
                int r = machine->trainingStep(batch, learningRate, weightDecay);
                if (r) // an error occurred
                {
                    STDLOG.printf_err("Epoch #%d: Training failed on batch for indices %d through %d.\n", numEpochs, offset, batchEnd);
                    return r;
                }

                Scalar batchLoss = machine->loss();
                lastTrainingLoss += batchLoss * (batchEnd - offset) / samples.rows();
                if (verbosity >= 2)
                {
                    cout << "    batch loss (#" << (offset/batchSize + 1) << "): " << batchLoss << endl;
                }

            }
            if (monitorProgress) monitor.end();

            time_t endTime = time(NULL);

            if (verbosity >= 1)
            {
                STDLOG.printf("Finished epoch #%d in %s.\n", numEpochs, ProgressMonitor::getTimeSpanString(difftime(endTime, startTime)).c_str());

                STDLOG.printf("    total loss: ");
                cout << lastTrainingLoss << endl;
            }

            return 0; // no errors
        }

        virtual Scalar trainingLoss() const
        {
            return lastTrainingLoss;
        }

        /**
         * Test the machine quality using the provided samples.
         */
        virtual int test(const Array<Scalar, Dynamic, Dynamic> &samples)
        {
            time_t startTime = time(NULL);

            lastTestLoss = machine->loss(samples);

            Array<Scalar, Dynamic, Dynamic> recDiff = machine->reconstruct(samples) - samples;
            recDiff = recDiff.abs2();

            lastReconstructionError = recDiff.colwise().mean().sum();

            time_t endTime = time(NULL);
            STDLOG.printf("Finished test in %s.\n", ProgressMonitor::getTimeSpanString(difftime(endTime, startTime)).c_str());

            STDLOG.printf("    loss: ");
            cout << lastTestLoss << endl;

            STDLOG.printf("    reconstruction error: ");
            cout << lastReconstructionError << endl;

            return 0; // no errors
        }

        virtual Scalar testLoss() const
        {
            return lastTestLoss;
        }
};

#endif
