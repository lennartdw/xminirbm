/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Abstract framework for the analysis of network distributions.
 */

#ifndef _XMINIRBM_ANALYSIS_DISTRIBUTIONANALYZER
#define _XMINIRBM_ANALYSIS_DISTRIBUTIONANALYZER

template<typename Scalar>
class DistributionAnalyzer
{
    private:
        string name;

    public:
        DistributionAnalyzer(const string &name)
        {
            this->name = name;
        }

        virtual ~DistributionAnalyzer() {}

        virtual const string &getName() const
        {
            return this->name;
        }

        virtual void normalize(Scalar normConst = NAN) = 0;

        virtual void processSample(const Array<Scalar, 1, Dynamic> &sample, Scalar pseudoProbability) = 0;

        virtual void reset() = 0;

        virtual bool saveToFile(const string &fileName) = 0;
};

#endif
