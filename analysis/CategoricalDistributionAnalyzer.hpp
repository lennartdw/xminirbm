/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Distribution of feature categories based on
 * classifier function applied to samples.
 */

#ifndef _XMINIRBM_ANALYSIS_CATEGORICALDISTRIBUTIONANALYZER
#define _XMINIRBM_ANALYSIS_CATEGORICALDISTRIBUTIONANALYZER

#include <unordered_map>

template<typename Key, typename Scalar>
class CategoricalDistributionAnalyzer : public DistributionAnalyzer<Scalar>
{
    private:
        bool normalized = false;
        unordered_map<Key, Scalar> categoryProbabilities;
        function<Key(const Array<Scalar, 1, Dynamic>&, void*)> classifier;
        void *metaData = NULL;

    public:
        CategoricalDistributionAnalyzer(const string &name, function<Key(const Array<Scalar, 1, Dynamic>&, void*)> classifierFunction) : DistributionAnalyzer<Scalar>(name)
        {
            this->classifier = classifierFunction;

            reset();
        }

        virtual ~CategoricalDistributionAnalyzer() {}

        virtual void normalize(Scalar normConst = NAN) override
        {
            if (! normalized)
            {
                // calculate normalization constant
                Scalar probSum = 0.0;
                for (auto &entry : categoryProbabilities)
                {
                    probSum += entry.second;
                }

                // normalize
                for (auto &entry : categoryProbabilities)
                {
                    entry.second /= probSum;
                }

                normalized = true;
            }
        }

        virtual void processSample(const Array<Scalar, 1, Dynamic> &sample, Scalar pseudoProbability) override
        {
            Key cat = classifier(sample, metaData);

            auto catProbEntry = categoryProbabilities.find(cat);
            if (catProbEntry == categoryProbabilities.end())
            {
                // new category
                categoryProbabilities[cat] = pseudoProbability;
            }
            else
            {
                // existing category, new sample
                catProbEntry->second += pseudoProbability;
            }

            normalized = false;
        }

        virtual void reset() override
        {
            categoryProbabilities.clear();
            normalized = false;
        }

        virtual bool saveToFile(const string &fileName) override
        {
            try
            {
                ofstream fout(fileName);
                for (auto &cat : categoryProbabilities)
                {
                    fout << cat.first << "\t" << cat.second << endl;
                }
                fout.close();

                return true;
            }
            catch (...)
            {
                return false;
            }
        }
};

#endif
