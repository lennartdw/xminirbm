/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Analyzer for discrete probability distributions
 * with restricted support.
 */

#ifndef _XMINIRBM_ANALYSIS_PROBABILITYANALYZER_HPP
#define _XMINIRBM_ANALYSIS_PROBABILITYANALYZER_HPP

#include "../networks/BinaryRBM.hpp"

template<typename Scalar>
class ProbabilityAnalyzer : public DistributionAnalyzer<Scalar>
{
    private:
        unordered_map<string, Scalar> probabilityList;
        Scalar normalizationConstant;

    public:
        ProbabilityAnalyzer(const string &name) : DistributionAnalyzer<Scalar>(name)
        {
            reset();
        }

        virtual ~ProbabilityAnalyzer() {}

        virtual void normalize(Scalar normConst = NAN) override
        {
            if (ISNAN(normConst))
            {
                // do nothing;
                // normalization is provided
                // from accumulated pseudo-probabilities
            }
            else
            {
                normalizationConstant = normConst;
            }

            for (auto &probEntry : probabilityList)
            {
                probEntry.second /= normalizationConstant;
            }
            normalizationConstant = 1.0;
        }

        virtual void processSample(const Array<Scalar, 1, Dynamic> &sample, Scalar pseudoProbability) override
        {
            string key = BinaryRBM<Scalar>::statesToString(sample);
            probabilityList[key] += pseudoProbability;
            normalizationConstant += pseudoProbability;
        }

        virtual void reset() override
        {
            probabilityList.clear();
            normalizationConstant = 0.0;
        }

        virtual bool saveToFile(const string &fileName) override
        {
            return xminirbm_save_distribution(probabilityList, fileName);
        }
};

#endif
