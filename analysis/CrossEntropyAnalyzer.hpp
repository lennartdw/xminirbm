/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Analyzer for the full state distribution
 * of a neural network.
 */

#ifndef _XMINIRBM_ANALYSIS_CROSSENTROPYANALYZER
#define _XMINIRBM_ANALYSIS_CROSSENTROPYANALYZER

#include "conf/Configuration.hpp"

template <typename Scalar>
class CrossEntropyAnalyzer : public DistributionAnalyzer<Scalar>
{
    private:
        function<Scalar(const Array<Scalar, 1, Dynamic>&)> refProbDist;
        Scalar unnormalizedCrossEntropy;
        Scalar normalizationConstant;

    public:
        CrossEntropyAnalyzer(const string &name, function<Scalar(const Array<Scalar, 1, Dynamic>&)> referenceProbabilityDistribution) : DistributionAnalyzer<Scalar>(name)
        {
            this->refProbDist = referenceProbabilityDistribution;
            reset();
        }

        virtual ~CrossEntropyAnalyzer() {}

        virtual void normalize(Scalar normConst = NAN) override
        {
            if (ISNAN(normConst))
            {
                // do nothing
                // normalization is provided upon output
            }
            else
            {
                normalizationConstant = normConst;
            }
        }

        virtual void processSample(const Array<Scalar, 1, Dynamic> &sample, Scalar pseudoProbability) override
        {
            Scalar refProb = refProbDist(sample);
            if (refProb > 0.0)
            {
                unnormalizedCrossEntropy -= refProb * LOG(pseudoProbability);
            }
            normalizationConstant += pseudoProbability;
        }

        virtual void reset() override
        {
            unnormalizedCrossEntropy = 0.0;
            normalizationConstant = 0.0;
        }

        virtual bool saveToFile(const string &fileName) override
        {
            Configuration config;
            config.setValue("unnormalized-cross-entropy", unnormalizedCrossEntropy);
            config.setValue("normalization", normalizationConstant);
            config.setValue("cross-entropy", LOG(normalizationConstant) + unnormalizedCrossEntropy);

            return (bool) config.saveToFile(fileName);
        }
};

#endif
