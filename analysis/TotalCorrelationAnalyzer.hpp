/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Analyzer for the total correlation of a discrete
 * probability distribution.
 */
 
#ifndef _XMINIRBM_ANALYSIS_TOTALCORRELATIONANALYZER_HPP
#define _XMINIRBM_ANALYSIS_TOTALCORRELATIONANALYZER_HPP

template<typename Scalar>
class TotalCorrelationAnalyzer : public DistributionAnalyzer<Scalar>
{
    private:
        int numVariables;
        int dimension;

        Scalar unnormalizedJointEntropy;
        Scalar jointNormalization;
        Array<Scalar, Dynamic, Dynamic> marginalDistributions;

    public:
        TotalCorrelationAnalyzer(const string &name, int numVariables, int dimension) : DistributionAnalyzer<Scalar>(name)
        {
            this->numVariables = numVariables;
            this->dimension = dimension;

            reset();
        }

        virtual ~TotalCorrelationAnalyzer() {}

        virtual void normalize(Scalar normConst = NAN) override
        {
            // do nothing, normalization is computed internally
        }

        virtual void processSample(const Array<Scalar, 1, Dynamic> &sample, Scalar pseudoProbability) override
        {
            if (pseudoProbability > 0.0)
            {
                unnormalizedJointEntropy -= pseudoProbability * LOG(pseudoProbability);
                jointNormalization += pseudoProbability;

                // cycle variables and adjust marginal distributions
                for (int k = 0; k < marginalDistributions.rows(); ++k)
                {
                    int sampleDigit = ROUND(sample(k));
                    marginalDistributions(k, sampleDigit) += pseudoProbability;
                }
            }
        }

        virtual void reset() override
        {
            unnormalizedJointEntropy = 0.0;
            jointNormalization = 0.0;
            marginalDistributions = Array<Scalar, Dynamic, Dynamic>::Zero(numVariables, dimension);
        }

        virtual bool saveToFile(const string &fileName) override
        {
            Configuration config;
            config.setValue("unnormalized-joint-entropy", unnormalizedJointEntropy);
            config.setValue("joint-normalization", jointNormalization);

            Scalar jointEntropy = LOG(jointNormalization) + unnormalizedJointEntropy / jointNormalization;
            config.setValue("joint-entropy", jointEntropy);

            Scalar marginalEntropySum = 0.0;
            for (int k = 0; k < marginalDistributions.rows(); ++k)
            {
                // calculate marginal entropies:
                Scalar marginalNormalization = 0.0;
                Scalar unnormalizedMarginalEntropy = 0.0;

                for (int n = 0; n < marginalDistributions.cols(); ++n)
                {
                    const Scalar marginalPseudoProb = marginalDistributions(k, n);
                    if (marginalPseudoProb > 0.0)
                    {
                        unnormalizedMarginalEntropy -= marginalPseudoProb * LOG(marginalPseudoProb);
                        marginalNormalization += marginalPseudoProb;
                    }
                }

                Scalar marginalEntropy = LOG(marginalNormalization) + unnormalizedMarginalEntropy / marginalNormalization;
                config.setValue("marginal-entropy-" + to_string(k), marginalEntropy);
                marginalEntropySum += marginalEntropy;
            }

            config.setValue("total-correlation", marginalEntropySum - jointEntropy);

            return (bool) config.saveToFile(fileName);
        }
};

#endif
