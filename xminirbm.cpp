/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Main file: entry point and task selection
 */

#include "./experiments/Classification.hpp"
#include "./experiments/RBM.hpp"
#include "./experiments/Sampling.hpp"
#include "./experiments/Transformation.hpp"
#include "./util/headers.hpp"
#include "./util/test.hpp"

using namespace std;
using namespace dwamf;

int constexpr XMINIRBM_TASK_COMPLETE = 0;
int constexpr XMINIRBM_TASK_CONTINUE = 1;

int constexpr XMINIRBM_FLAG_DOUBLE_PRECISION = 1;
int FLAGS = 0; //XMINIRBM_FLAG_DOUBLE_PRECISION;

Configuration CONFIG;
bool CONFIG_SPECIFIED = false;

void clean()
{

}

int process_tasks(const vector<string> &args, bool interactive = false)
{
    // process command-line args
    for (size_t k = 0; k < args.size(); ++k)
    {
        if (args[k][0] == '-')
        {
            if (args[k] == "-h" || args[k] == "--help")
            {
                cout << "USAGE: xminirbm [configuration file] [options/instructions]" << endl;
                cout << endl;
                cout << "OPTIONS:" << endl;
                cout << "  -c <key> <value>, --configure <key> <value>" << endl;
                cout << "        set the configuration entry 'key' to 'value'" << endl;
                cout << "  -c! <key> <value>, --configure-with-substitution <key> <value>" << endl;
                cout << "        set the configuration entry 'key' to 'value', replacing any other configuration keys occurring in 'value' between angular brackets by their corresponding values" << endl;
                cout << "  --get-configuration-value <key>" << endl;
                cout << "        print the configuration value associated with the given key" << endl;
                cout << "  -h, --help" << endl;
                cout << "        display this help message" << endl;
                cout << "  --list-configuration" << endl;
                cout << "        display all configuration entries" << endl;
                cout << "  -p32, --precision=single" << endl;
                cout << "        use 32-bit floating point precision (default)" << endl;
                cout << "  -p64, --precision=double" << endl;
                cout << "        use 64-bit (aka double) floating point precision" << endl;
                cout << "  -t <name>, --test <name>" << endl;
                cout << "        run the test suite with the specified name" << endl;
                cout << "  -x <name>, --experiment <name>" << endl;
                cout << "        run the experiment with the specified name;" << endl;
                cout << "        see below for a list of available experiments" << endl;
                cout << endl;
                cout << "EXPERIMENTS:" << endl;
                cout << "  classify" << endl;
                cout << "        sort images into categories and evaluate associated probability distributions" << endl;
                cout << "  data-transform" << endl;
                cout << "        apply a transformation to every entry of a dataset" << endl;
                cout << "  dist" << endl;
                cout << "        calculate the distribution (probability of all configurations) of a specific feature scheme" << endl;
                cout << "  dist-crossent" << endl;
                cout << "        calculate the cross entropy between two (discrete) probability distributions" << endl;
                cout << "  dist-Lpdist" << endl;
                cout << "        calculate the L^p distance between two (discrete) probability distributions" << endl;
                cout << "  dist-margprobs" << endl;
                cout << "        calculate the marginal probabilities of the individual units (states) of a multivariate (discrete) probability distribution" << endl;
                cout << "  dist-modes" << endl;
                cout << "        calculate the modes of a multivariate (discrete) probability distribution" << endl;
                cout << "  dist-sample-indices" << endl;
                cout << "        sample from a multivariate discrete probability distribution;" << endl;
                cout << "        output file is a (binary) list of indices of the sampled states" << endl;
                cout << "  dist-totcorr" << endl;
                cout << "        calculate the total correlation across the individual units of a multivariate (discrete) probability distribution" << endl;
                cout << "  dist-totcorr-empirical" << endl;
                cout << "         calculate the total correlation of a dataset (empirical probability distribution)" << endl;
                cout << "  dist-transform" << endl;
                cout << "        given a probability distribution on a state space, calculate the distribution upon applying a transformation to the state space" << endl;
                cout << "  rbm-acf (Binary RBM)" << endl;
                cout << "        calculate the autocorrelation function (or reductions thereof, such as the integrated autocorrelation time) of the Gibbs sampler of an RBM;" << endl;
                cout << "  rbm-sample-mc" << endl;
                cout << "        generate samples of an RBM's visible and/or hidden units via alternating Gibbs sampling using Markov chains;" << endl;
                cout << "        IDs of machines for inspection should be configured via 'acf_machines'" << endl;
                cout << "  rbm-sample-v|h" << endl;
                cout << "        generate samples of an RBM's visible units given a configuration of the hidden units" << endl;
                cout << "        IDs of machines for inspection should be configured via 'acf_machines'" << endl;
                cout << "  rbm-test (Test of RBM)" << endl;
                cout << "        evaluation of performance characteristics on a set of test data" << endl;
                cout << "  rbm-train (Binary RBM)" << endl;
                cout << "        (stochastic) gradient descent training and testing for RBM models with binary units and cross-entropy loss, using the training algorithm and hyperparameters as specified in the configuration file" << endl;
                cout << "  rbm-visflip (Binary RBM)" << endl;
                cout << "        transform an RBM model by swapping the '0' and '1' states of all units;" << endl;
                cout << "        ID of the machine should be configured via 'visflip_machine'" << endl;
                cout << "  x2rbm-hdist (Hidden distribution of RBM)" << endl;
                cout << "        analysis of the exact distribution of an RBM's hidden units;" << endl;
                cout << "        analyzers can be specified by configuring 'dist_analyzers';" << endl;
                cout << "        IDs of machines for inspection should be configured via 'hdist_machines'" << endl;
                cout << "  x2rbm-vdist (Visible distribution of RBM)" << endl;
                cout << "        analysis of the exact distribution of an RBM's visible units;" << endl;
                cout << "        analyzers can be specified by configuring 'dist_analyzers';" << endl;
                cout << "        IDs of machines for inspection should be configured via 'vdist_machines'" << endl;
                cout << "  x2rbm-vdist|hdist (Visible distribution of RBM given its hidden distribution)" << endl;
                cout << "        analysis of the exact distribution of an RBM's visible units using the provided (previously calculated) distribution of its hidden units;" << endl;
                cout << "        analyzers can be specified by configuring 'dist_analyzers';" << endl;
                cout << "        IDs of machines for inspection should be configured via 'vdist_machines'" << endl;
                cout << "  x2rbm-vdist_partial (Partial visible distribution of RBM)" << endl;
                cout << "        analysis of the exact distribution of an RBM's visible units on a subset of all possible states provided by the support of a reference distribution;" << endl;
                cout << "        analyzers can be specified by configuring 'dist_analyzers';" << endl;
                cout << "        IDs of machines for inspection should be configured via 'vdist_machines'" << endl;

            }
            else if (args[k] == "-c" || args[k] == "--configure")
            {
                // set config value
                if (k + 2 < args.size())
                {
                    CONFIG.setValue(args[k+1], args[k+2]);
                    k += 2;
                }
                else
                {
                    STDLOG.printf_err("Expected two arguments after option '-c': -c <key> <value>\n");
                }
            }
            else if (args[k] == "-c!" || args[k] == "--configure-with-substitution")
            {
                // set config value
                if (k + 2 < args.size())
                {
                    CONFIG.setValue(args[k+1], CONFIG.replaceValues(args[k+2]));
                    k += 2;
                }
                else
                {
                    STDLOG.printf_err("Expected two arguments after option '-c!': -c! <key> <value>\n");
                }
            }
            else if (args[k] == "--get-configuration-value")
            {
                if (CONFIG_SPECIFIED)
                {
                    if (k + 1 < args.size())
                    {
                        try
                        {
                            cout << CONFIG.getValue(args[k+1]) << endl;
                        }
                        catch (...)
                        {
                            STDLOG.printf_wrn("No configuration entry found for key '%s'.\n", args[k+1].c_str());
                        }
                        k += 1;
                    }
                    else
                    {
                        STDLOG.printf_err("Expected two arguments after option '-c': -c <key> <value>\n");
                    }
                }
                else
                {
                    STDLOG.printf_err("No configuration specified.\n");
                }
            }
            else if (args[k] == "-i" || args[k] == "--interactive")
            {
                interactive = true;
            }
            else if (args[k] == "--list-configuration")
            {
                if (CONFIG_SPECIFIED)
                {
                    CONFIG.print();
                }
                else
                {
                    STDLOG.printf_err("No configuration specified.\n");
                }
            }
            else if (args[k] == "-p32" || args[k] == "--precision=32" || args[k] == "--precision=single")
            {
                FLAGS &= ~XMINIRBM_FLAG_DOUBLE_PRECISION;
                cout << "       using single precision" << endl;
                // relaunch random number generator
                // with single precision
                rand_term();
                rand_init(time(NULL) + getpid(), "ranlxs2");
            }
            else if (args[k] == "-p64" || args[k] == "--precision=64" || args[k] == "--precision=double")
            {
                FLAGS |= XMINIRBM_FLAG_DOUBLE_PRECISION;
                cout << "       using double precision" << endl;
                // relaunch random number generator
                // with double precision
                rand_term();
                rand_init(time(NULL) + getpid(), "ranlxd2");
            }
            else if (args[k] == "-q" || args[k] == "--quit")
            {
                return XMINIRBM_TASK_COMPLETE;
            }
            else if (args[k] == "-t" || args[k] == "--test")
            {
                // Test routine
                ++k;
                if (k < args.size())
                {
                    string routine = args[k];
                    test(routine);
                }
            }
            else if (args[k] == "-x" || args[k] == "--experiment")
            {
                if (! CONFIG_SPECIFIED)
                {
                    STDLOG.printf_wrn("No configuration file specified, will proceed experiment with built-in values.\n");
                }

                // run experiment
                ++k;
                if (k < args.size())
                {
                    string experimentId = args[k];
                    if (experimentId == "classify")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_classify<double>(CONFIG);
                        }
                        else
                        {
                            experiment_classify<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "data-transform")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_transform_dataset<double>(CONFIG);
                        }
                        else
                        {
                            experiment_transform_dataset<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "dist")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_dist<double>(CONFIG);
                        }
                        else
                        {
                            experiment_dist<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "dist-crossent")
                    {
                        experiment_crossent(CONFIG);
                    }
                    else if (experimentId == "dist-Lpdist")
                    {
                        experiment_lpdist(CONFIG);
                    }
                    else if (experimentId == "dist-margprobs")
                    {
                        experiment_margprobs(CONFIG);
                    }
                    else if (experimentId == "dist-modes")
                    {
                        experiment_modes(CONFIG);
                    }
                    else if (experimentId == "dist-sample-indices")
                    {
                        experiment_sample_indices(CONFIG);
                    }
                    else if (experimentId == "dist-totcorr")
                    {
                        experiment_totcorr(CONFIG);
                    }
                    else if (experimentId == "dist-totcorr-empirical")
                    {
                        experiment_totcorr_empirical(CONFIG);
                    }
                    else if (experimentId == "dist-transform")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_transform_dist<double>(CONFIG);
                        }
                        else
                        {
                            experiment_transform_dist<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "rbm-acf")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_rbm_acf<double>(CONFIG);
                        }
                        else
                        {
                            experiment_rbm_acf<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "rbm-sample-mc")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_rbm_sample_mc<double>(CONFIG);
                        }
                        else
                        {
                            experiment_rbm_sample_mc<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "rbm-sample-v|h")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_rbm_sample_VGivenH<double>(CONFIG);
                        }
                        else
                        {
                            experiment_rbm_sample_VGivenH<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "rbm-test")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_rbm_test<double>(CONFIG);
                        }
                        else
                        {
                            experiment_rbm_test<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "rbm-train")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_rbm_train<double>(CONFIG);
                        }
                        else
                        {
                            experiment_rbm_train<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "rbm-visflip")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_rbm_visflip<double>(CONFIG);
                        }
                        else
                        {
                            experiment_rbm_visflip<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "x2rbm-hdist")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_x2rbm_hdist<double>(CONFIG);
                        }
                        else
                        {
                            experiment_x2rbm_hdist<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "x2rbm-vdist")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_x2rbm_vdist<double>(CONFIG);
                        }
                        else
                        {
                            experiment_x2rbm_vdist<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "x2rbm-vdist_given_hdist" || experimentId == "x2rbm-vdist|hdist")
                    {
                        if (FLAGS & XMINIRBM_FLAG_DOUBLE_PRECISION)
                        {
                            experiment_x2rbm_vdist_given_hdist<double>(CONFIG);
                        }
                        else
                        {
                            experiment_x2rbm_vdist_given_hdist<float>(CONFIG);
                        }
                    }
                    else if (experimentId == "x2rbm-vdist_partial")
                    {
                        experiment_x2rbm_vdist_partial(CONFIG);
                    }
                    else
                    {
                        STDLOG.printf_err("Unknown experiment '%s'.\n", experimentId.c_str());
                    }
                }
            }

        }
        else if (file_exists(args[k].c_str()))
        {
            CONFIG = Configuration(NULL, args[k].c_str(), args[k]);
            CONFIG_SPECIFIED = true;
            STDLOG.printf("Loaded configuration file '%s'.\n", args[k].c_str());
        }
        else
        {
            STDLOG.printf_wrn("Argument '%s' at position %d is not a valid option or configuration file. Will be ignored.\n", args[k].c_str(), k);
        }
    }

    if (interactive)
    {
        return XMINIRBM_TASK_CONTINUE;
    }
    else
    {
        return XMINIRBM_TASK_COMPLETE;
    }
}

int read_tasks(vector<string> &targetArgList)
{
    string prompt;
    do
    {
        STDLOG.printf("> ");
        getline(cin, prompt);
        prompt = string_trim(prompt);
    }
    while (prompt == "");

    // TODO: parse prompt
    targetArgList.push_back(prompt);

    return XMINIRBM_TASK_COMPLETE;
}

int main(int argc, char **argv)
{
    cout << "Exact MiniRBM Learning" << endl;

    time_t launchTime = time(NULL);
    cout << "[mrbm] launched: " << std::asctime(std::localtime(&launchTime));
    rand_init(launchTime + getpid(), "ranlxs2"); // default: single-precision RANLUX level 2

    vector<string> args;
    // process command-line arguments to task list
    for (int k = 1; k < argc; ++k)
    {
        args.push_back(argv[k]);
    }

    // process tasks
    int r = process_tasks(args);
    while (r == XMINIRBM_TASK_CONTINUE)
    {
        // prompt for new tasks
        STDLOG.printf("All tasks complete. Waiting for instructions ...\n");
        args.clear();
        r = read_tasks(args);
        if (r)
        {
            // an error occurred
            break;
        }

        r = process_tasks(args, true);
    }

    rand_term();
    clean();

    time_t exitTime = time(NULL);
    cout << "[mrbm] exiting: " << std::asctime(std::localtime(&exitTime));
    cout << "       total run time: " << ProgressMonitor::getTimeSpanString(difftime(exitTime, launchTime)) << endl;

    return r;
}
