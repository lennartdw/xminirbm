
/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Global constants and prototype datasets.
 */

#ifndef _XMINIRBM_DATASETS_PROTOTYPES
#define _XMINIRBM_DATASETS_PROTOTYPES

#include "../util/headers.hpp"

#include "ImageBW.hpp"

extern bool __XMINIRBM_DATASETS_PATTERNS_INITIALIZED;
void __xminirbm_datasets_patterns_initialize();

extern typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK;
extern typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK;

template<typename Scalar>
typename ImageBW<Scalar>::Pattern datasets_pattern_hook(const Scalar &dummy)
{
    STDLOG.printf_err("Invalid data type for image pattern.\n");
    return typename ImageBW<Scalar>::Pattern();
}

template<>
typename ImageBW<float>::Pattern datasets_pattern_hook<float>(const float &dummy);

template<>
typename ImageBW<double>::Pattern datasets_pattern_hook<double>(const double &dummy);

//
// extern typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_LINEH;
// extern typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_LINEH;
//
// template<typename Scalar>
// typename ImageBW<Scalar>::Pattern datasets_pattern_lineH(const Scalar &dummy)
// {
//     STDLOG.printf_err("Invalid data type for image pattern.\n");
//     return typename ImageBW<Scalar>::Pattern();
// }
//
// template<>
// typename ImageBW<float>::Pattern datasets_pattern_lineH<float>(const float &dummy);
//
// template<>
// typename ImageBW<double>::Pattern datasets_pattern_lineH<double>(const double &dummy);
//
// extern typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_LINEV;
// extern typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_LINEV;

// template<typename Scalar>
// typename ImageBW<Scalar>::Pattern datasets_pattern_lineV(const Scalar &dummy)
// {
//     STDLOG.printf_err("Invalid data type for image pattern.\n");
//     return typename ImageBW<Scalar>::Pattern();
// }
//
// template<>
// typename ImageBW<float>::Pattern datasets_pattern_lineV<float>(const float &dummy);
//
// template<>
// typename ImageBW<double>::Pattern datasets_pattern_lineV<double>(const double &dummy);

extern typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE;
extern typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE;

template<typename Scalar>
typename ImageBW<Scalar>::Pattern datasets_pattern_circle(const Scalar &dummy)
{
    STDLOG.printf_err("Invalid data type for image pattern.\n");
    return typename ImageBW<Scalar>::Pattern();
}

template<>
typename ImageBW<float>::Pattern datasets_pattern_circle<float>(const float &dummy);

template<>
typename ImageBW<double>::Pattern datasets_pattern_circle<double>(const double &dummy);

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_1()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  3,  0, 1.0 });
    p1.pixels.push_back({  4,  0, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({  -1, 0, 0.0, false, true });
    p1.pixels.push_back({   5, 0, 0.0, false, true });

    p1.pixels.push_back({  -1, 1, 0.0, false, true });
    p1.pixels.push_back({   0, 1, 0.0, false, true });
    p1.pixels.push_back({   1, 1, 0.0, false, true });
    p1.pixels.push_back({   2, 1, 0.0, false, true });
    p1.pixels.push_back({   3, 1, 0.0, false, true });
    p1.pixels.push_back({   4, 1, 0.0, false, true });
    p1.pixels.push_back({   5, 1, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;

}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_2()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0, 0,  1.0 });
    p1.pixels.push_back({  0, 1,  1.0 });
    p1.pixels.push_back({  1, 1,  1.0 });
    p1.pixels.push_back({  2, 1,  1.0 });
    p1.pixels.push_back({  2, 0,  1.0 });
    p1.pixels.push_back({  3, 0,  1.0 });
    p1.pixels.push_back({  4, 0,  1.0 });
    p1.pixels.push_back({  4, 1,  1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({  -1, 0, 0.0, false, true });
    p1.pixels.push_back({   1, 0, 0.0, false, false });
    p1.pixels.push_back({   5, 0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  3,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  0,  2, 0.0, false, true });
    p1.pixels.push_back({  1,  2, 0.0, false, true });
    p1.pixels.push_back({  2,  2, 0.0, false, true });
    p1.pixels.push_back({  3,  2, 0.0, false, true });
    p1.pixels.push_back({  4,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_3()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  0,  1, 1.0 });
    p1.pixels.push_back({  1,  1, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  3,  1, 1.0 });
    p1.pixels.push_back({  4,  1, 1.0 });
    p1.pixels.push_back({  4,  0, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  1,  0, 0.0, false, false });
    p1.pixels.push_back({  3,  0, 0.0, false, false });
    p1.pixels.push_back({  5,  0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  0,  2, 0.0, false, true });
    p1.pixels.push_back({  1,  2, 0.0, false, true });
    p1.pixels.push_back({  2,  2, 0.0, false, true });
    p1.pixels.push_back({  3,  2, 0.0, false, true });
    p1.pixels.push_back({  4,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_4()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });
    p1.pixels.push_back({  0,  2, 1.0 });
    p1.pixels.push_back({  1,  2, 1.0 });
    p1.pixels.push_back({  2,  2, 1.0 });
    p1.pixels.push_back({  3,  2, 1.0 });
    p1.pixels.push_back({  4,  2, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  3,  0, 0.0, false, false });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  0,  1, 0.0, false, false });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  3,  1, 0.0, false, false });
    p1.pixels.push_back({  4,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    p1.pixels.push_back({ -1,  3, 0.0, false, true });
    p1.pixels.push_back({  0,  3, 0.0, false, true });
    p1.pixels.push_back({  1,  3, 0.0, false, true });
    p1.pixels.push_back({  2,  3, 0.0, false, true });
    p1.pixels.push_back({  3,  3, 0.0, false, true });
    p1.pixels.push_back({  4,  3, 0.0, false, true });
    p1.pixels.push_back({  5,  3, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_5()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  1, 1.0 });
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });
    p1.pixels.push_back({  3,  1, 1.0 });
    p1.pixels.push_back({  4,  1, 1.0 });
    p1.pixels.push_back({  4,  0, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  3,  0, 0.0, false, false });
    p1.pixels.push_back({  5,  0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  0,  2, 0.0, false, true });
    p1.pixels.push_back({  1,  2, 0.0, false, true });
    p1.pixels.push_back({  2,  2, 0.0, false, true });
    p1.pixels.push_back({  3,  2, 0.0, false, true });
    p1.pixels.push_back({  4,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_6()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  3,  0, 1.0 });
    p1.pixels.push_back({  4,  0, 1.0 });
    p1.pixels.push_back({  4,  1, 1.0 });
    p1.pixels.push_back({  4,  2, 1.0 });
    p1.pixels.push_back({  3,  2, 1.0 });
    p1.pixels.push_back({  2,  2, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });

    p1.pixels.push_back({ 3,  1, 0.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  5,  0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  0,  1, 0.0, false, false });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({  1,  2, 0.0, false, false });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    p1.pixels.push_back({  1,  3, 0.0, false, true });
    p1.pixels.push_back({  2,  3, 0.0, false, true });
    p1.pixels.push_back({  3,  3, 0.0, false, true });
    p1.pixels.push_back({  4,  3, 0.0, false, true });
    p1.pixels.push_back({  5,  3, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_7()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  0,  1, 1.0 });
    p1.pixels.push_back({  0,  2, 1.0 });
    p1.pixels.push_back({  1,  2, 1.0 });
    p1.pixels.push_back({  2,  2, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });
    p1.pixels.push_back({  3,  1, 1.0 });
    p1.pixels.push_back({  4,  1, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  1,  0, 0.0, false, false });
    p1.pixels.push_back({  2,  0, 0.0, false, false });
    p1.pixels.push_back({  3,  0, 0.0, false, false });
    p1.pixels.push_back({  4,  0, 0.0, false, false });
    p1.pixels.push_back({  5,  0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  3,  2, 0.0, false, false });
    p1.pixels.push_back({  4,  2, 0.0, false, false });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    p1.pixels.push_back({ -1,  3, 0.0, false, true });
    p1.pixels.push_back({  0,  3, 0.0, false, true });
    p1.pixels.push_back({  1,  3, 0.0, false, true });
    p1.pixels.push_back({  2,  3, 0.0, false, true });
    p1.pixels.push_back({  3,  3, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_8()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  3,  0, 1.0 });
    p1.pixels.push_back({  4,  0, 1.0 });
    p1.pixels.push_back({  0,  1, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });
    p1.pixels.push_back({  4,  1, 1.0 });
    p1.pixels.push_back({  0,  2, 1.0 });
    p1.pixels.push_back({  1,  2, 1.0 });
    p1.pixels.push_back({  2,  2, 1.0 });
    p1.pixels.push_back({  3,  2, 1.0 });
    p1.pixels.push_back({  4,  2, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  5,  0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  3,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    p1.pixels.push_back({ -1,  3, 0.0, false, true });
    p1.pixels.push_back({  0,  3, 0.0, false, true });
    p1.pixels.push_back({  1,  3, 0.0, false, true });
    p1.pixels.push_back({  2,  3, 0.0, false, true });
    p1.pixels.push_back({  3,  3, 0.0, false, true });
    p1.pixels.push_back({  4,  3, 0.0, false, true });
    p1.pixels.push_back({  5,  3, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_9()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  0,  1, 1.0 });
    p1.pixels.push_back({  2,  1, 1.0 });
    p1.pixels.push_back({  0,  2, 1.0 });
    p1.pixels.push_back({  1,  2, 1.0 });
    p1.pixels.push_back({  2,  2, 1.0 });
    p1.pixels.push_back({  3,  2, 1.0 });
    p1.pixels.push_back({  4,  2, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  3,  0, 0.0, false, false });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  3,  1, 0.0, false, false });
    p1.pixels.push_back({  4,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    p1.pixels.push_back({ -1,  3, 0.0, false, true });
    p1.pixels.push_back({  0,  3, 0.0, false, true });
    p1.pixels.push_back({  1,  3, 0.0, false, true });
    p1.pixels.push_back({  2,  3, 0.0, false, true });
    p1.pixels.push_back({  3,  3, 0.0, false, true });
    p1.pixels.push_back({  4,  3, 0.0, false, true });
    p1.pixels.push_back({  5,  3, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

template<typename Scalar>
vector< typename ImageBW<Scalar>::Pattern > datasets_patterns_0()
{
    vector< typename ImageBW<Scalar>::Pattern > patterns;

    typename ImageBW<Scalar>::Pattern p1;
    p1.pixels.push_back({  0,  0, 1.0 });
    p1.pixels.push_back({  1,  0, 1.0 });
    p1.pixels.push_back({  2,  0, 1.0 });
    p1.pixels.push_back({  3,  0, 1.0 });
    p1.pixels.push_back({  4,  0, 1.0 });
    p1.pixels.push_back({  0,  1, 1.0 });
    p1.pixels.push_back({  4,  1, 1.0 });
    p1.pixels.push_back({  0,  2, 1.0 });
    p1.pixels.push_back({  1,  2, 1.0 });
    p1.pixels.push_back({  2,  2, 1.0 });
    p1.pixels.push_back({  3,  2, 1.0 });
    p1.pixels.push_back({  4,  2, 1.0 });

    p1.pixels.push_back({ -1, -1, 0.0, false, true });
    p1.pixels.push_back({  0, -1, 0.0, false, true });
    p1.pixels.push_back({  1, -1, 0.0, false, true });
    p1.pixels.push_back({  2, -1, 0.0, false, true });
    p1.pixels.push_back({  3, -1, 0.0, false, true });
    p1.pixels.push_back({  4, -1, 0.0, false, true });
    p1.pixels.push_back({  5, -1, 0.0, false, true });

    p1.pixels.push_back({ -1,  0, 0.0, false, true });
    p1.pixels.push_back({  5,  0, 0.0, false, true });

    p1.pixels.push_back({ -1,  1, 0.0, false, true });
    p1.pixels.push_back({  1,  1, 0.0, false, false });
    p1.pixels.push_back({  2,  1, 0.0, false, false });
    p1.pixels.push_back({  3,  1, 0.0, false, false });
    p1.pixels.push_back({  5,  1, 0.0, false, true });

    p1.pixels.push_back({ -1,  2, 0.0, false, true });
    p1.pixels.push_back({  5,  2, 0.0, false, true });

    p1.pixels.push_back({ -1,  3, 0.0, false, true });
    p1.pixels.push_back({  0,  3, 0.0, false, true });
    p1.pixels.push_back({  1,  3, 0.0, false, true });
    p1.pixels.push_back({  2,  3, 0.0, false, true });
    p1.pixels.push_back({  3,  3, 0.0, false, true });
    p1.pixels.push_back({  4,  3, 0.0, false, true });
    p1.pixels.push_back({  5,  3, 0.0, false, true });

    patterns.push_back(p1);

    return patterns;
}

#endif
