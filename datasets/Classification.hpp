/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Classification of built-in image distributions.
 */

#ifndef _CLEVER_DATASETS_CLASSIFICATION
#define _CLEVER_DATASETS_CLASSIFICATION

#include "func/types.hpp"

#include "../datasets/ImageBW.hpp"
#include "../datasets/prototypes.hpp"

template<typename Key, typename Scalar>
struct ProbabilityInfo {
    Key key;
    Scalar probability;
};

template<typename Key, typename Scalar>
int classify_parse_probability_info(ProbabilityInfo<Key, Scalar> *info, const string &src, const function<int(Key*, const string&)> &fparse_index = [](Key *value, const string &source) -> int {
        *value = (Key) source;
        return 1; // TRUE
    })
{
    vector<string> parts = string_split(src, " \t\n", true, true);
    if (parts.size() == 2)
    {
        real_t prob;
        if (fparse_index(&info->key, parts[0]) && parse_real(&prob, parts[1]))
        {
            info->probability = (Scalar) prob;
            return 0; // conversion successful
        }
    }

    return -1; // conversion failed
}

template<typename Scalar>
struct IndexProbabilityInfo {
    unsigned long int index;
    Scalar probability;
};

template<typename Scalar>
int classify_parse_index_probability_info(IndexProbabilityInfo<Scalar> *info, const string &src)
{
    vector<string> parts = string_split(src, " \t\n", true, true);
    if (parts.size() == 2)
    {
        long int index;
        real_t prob;
        if (parse_integer(&index, parts[0]) && parse_real(&prob, parts[1]))
        {
            info->index = (unsigned long int) index;
            info->probability = (Scalar) prob;
            return 0; // conversion successful
        }
    }

    return -1; // conversion failed
}

/**
 * Cycle all images of the given width and height
 * and classify them according to the specified categories.
 * Returns, for each category, a list of image indices
 * of its members.
 */
template<typename Scalar>
vector< vector< IndexProbabilityInfo<Scalar> > > classify_imagebw(const vector< vector< typename ImageBW<Scalar>::Pattern > > &categories, int imageWidth, int imageHeight, Scalar opx, bool periodicBoundaryConditions = true, bool monitorProgress = true)
{
    int numPixels = imageWidth * imageHeight;
    unsigned long int numImages = ((unsigned long int) 1) << numPixels;

    vector< vector< IndexProbabilityInfo<Scalar> > > imgIndicesByCat(categories.size());
    vector<Scalar> cumCatProb(categories.size(), 0.0);

    ProgressMonitor monitor;
    if (monitorProgress) monitor.start();
    long int progressUpdateSteps = numImages / 10000;
    long int progressUpdateCounter = 0;

    Matrix<Scalar, Dynamic, 1> flattenedPixels(numPixels);
    // cycle all possible images
    for (unsigned long int imgIndex = 0; imgIndex < numImages; ++imgIndex)
    {
        if (monitorProgress && (++progressUpdateCounter) >= progressUpdateSteps)
        {
            monitor.update((real_t) imgIndex / numImages);
            progressUpdateCounter = 0;
        }

        // generate image
        integer_digits(flattenedPixels, imgIndex, 2);
        ImageBW<Scalar> img(flattenedPixels, imageWidth, imageHeight, periodicBoundaryConditions);

        // cycle categories
        for (size_t catIndex = 0; catIndex < categories.size(); ++catIndex)
        {
            bool match = false;
            IndexProbabilityInfo<Scalar> info;

            // cycle patterns in category
            for (auto &pattern : categories[catIndex])
            {
                typename ImageBW<Scalar>::PatternPosition pos;
                if (img.findPattern(pos, pattern))
                {
                    match = true;
                    info.index = imgIndex;
                    Array<Scalar, Dynamic, 1> nonPatternPixels = img.getNonPatternPixels(pos, pattern);
                    info.probability = 1.0;
                    for (int pxIndex = 0; pxIndex < nonPatternPixels.size(); ++pxIndex)
                    {
                        if (nonPatternPixels[pxIndex] > 0.0)
                        {
                            info.probability *= opx;
                        }
                        else
                        {
                            info.probability *= (1.0 - opx);
                        }
                    }

                    break;
                }
            }

            if (match)
            {
                imgIndicesByCat[catIndex].push_back(info);
                cumCatProb[catIndex] += info.probability;
                // break;
                // no break: one image can belong to multiple categories
            }
        }
    }

    if (monitorProgress) monitor.end();

    // normalize probabilities within category
    for (size_t catIndex = 0; catIndex < categories.size(); ++catIndex)
    {
        for (auto &info : imgIndicesByCat[catIndex])
        {
            info.probability /= cumCatProb[catIndex];
        }
    }

    return imgIndicesByCat;
}

#endif
