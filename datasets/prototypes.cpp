/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Source/implementation of prototypes.hpp
 */
 
#include "prototypes.hpp"

typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK;
typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK;

// typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_LINEH;
// typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_LINEH;
//
// typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_LINEV;
// typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_LINEV;

typename ImageBW<float>::Pattern __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE;
typename ImageBW<double>::Pattern __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE;

bool __XMINIRBM_DATASETS_PATTERNS_INITIALIZED = false;

void __xminirbm_datasets_patterns_initialize()
{
    if (! __XMINIRBM_DATASETS_PATTERNS_INITIALIZED)
    {
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({ -1, -1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({ -1,  0, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({ -1,  1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({ -1,  2, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  0, -1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  0,  0, 1.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  0,  1, 1.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  0,  2, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  1, -1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  1,  0, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  1,  1, 1.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  1,  2, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  2,  0, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  2,  1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK.pixels.push_back({  2,  2, 0.0 });

        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({ -1, -1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({ -1,  0, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({ -1,  1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({ -1,  2, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  0, -1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  0,  0, 1.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  0,  1, 1.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  0,  2, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  1, -1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  1,  0, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  1,  1, 1.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  1,  2, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  2,  0, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  2,  1, 0.0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK.pixels.push_back({  2,  2, 0.0 });

        // center
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  0,  0,  0 });
        // ring
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  1,  0,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  1,  1,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  0,  1,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -1,  1,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -1,  0,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -1, -1,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  0, -1,  1 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  1, -1,  1 });
        // exterior
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  2,  0,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  2,  1,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  2,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  1,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  0,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -1,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -2,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -2,  1,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -2,  0,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -2, -1,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -2, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({ -1, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  0, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  1, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  2, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE.pixels.push_back({  2, -1,  0 });

        // center
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  0,  0,  0 });
        // ring
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  1,  0,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  1,  1,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  0,  1,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -1,  1,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -1,  0,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -1, -1,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  0, -1,  1 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  1, -1,  1 });
        // exterior
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  2,  0,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  2,  1,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  2,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  1,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  0,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -1,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -2,  2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -2,  1,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -2,  0,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -2, -1,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -2, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({ -1, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  0, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  1, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  2, -2,  0 });
        __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE.pixels.push_back({  2, -1,  0 });

        __XMINIRBM_DATASETS_PATTERNS_INITIALIZED = true;
    }
}

template<>
typename ImageBW<float>::Pattern datasets_pattern_hook<float>(const float &dummy)
{
    if (! __XMINIRBM_DATASETS_PATTERNS_INITIALIZED)
    {
        __xminirbm_datasets_patterns_initialize();
    }

    return __XMINIRBM_DATASETS_PATTERN_FLOAT_HOOK;
}

template<>
typename ImageBW<double>::Pattern datasets_pattern_hook<double>(const double &dummy)
{
    if (! __XMINIRBM_DATASETS_PATTERNS_INITIALIZED)
    {
        __xminirbm_datasets_patterns_initialize();
    }

    return __XMINIRBM_DATASETS_PATTERN_DOUBLE_HOOK;
}

template<>
typename ImageBW<float>::Pattern datasets_pattern_circle<float>(const float &dummy)
{
    if (! __XMINIRBM_DATASETS_PATTERNS_INITIALIZED)
    {
        __xminirbm_datasets_patterns_initialize();
    }

    return __XMINIRBM_DATASETS_PATTERN_FLOAT_CIRCLE;
}

template<>
typename ImageBW<double>::Pattern datasets_pattern_circle<double>(const double &dummy)
{
    if (! __XMINIRBM_DATASETS_PATTERNS_INITIALIZED)
    {
        __xminirbm_datasets_patterns_initialize();
    }

    return __XMINIRBM_DATASETS_PATTERN_DOUBLE_CIRCLE;
}
