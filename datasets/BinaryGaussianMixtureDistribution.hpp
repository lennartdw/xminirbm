/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Empirical multivariate binary probability distribution
 * with Gaussian smoothening.
 */

#ifndef _XMINIRBM_DATASETS_BINARYGAUSSIANMIXTUREDISTRIBUTION
#define _XMINIRBM_DATASETS_BINARYGAUSSIANMIXTUREDISTRIBUTION

#include "rand/DiscreteDistribution.hpp"
#include "func/sets.hpp"

class BinaryGaussianMixtureDistribution : public DiscreteDistribution<string>
{
    private:
        unordered_map<string, real_t> rawEmpiricalDist;
        size_t dimension;
        real_t normalization;
        real_t stdDev;
        real_t sqrt2PiVar;
        real_t negInvVar2;

        static real_t getSquaredDistance(const string &x1, const string &x2)
        {
            if (x1.length() == x2.length())
            {
                real_t d = 0.0;
                for (size_t k = 0; k < x1.length(); ++k)
                {
                    if (x1[k] != x2[k])
                    {
                        d += 1.0;
                    }
                }

                return d;
            }
            else
            {
                return NAN;
            }
        }

    public:
        BinaryGaussianMixtureDistribution(unordered_map<string, real_t> &stateProbsMap, size_t dim, real_t standardDeviation)
        {
            this->rawEmpiricalDist = stateProbsMap;
            this->dimension = dim;
            this->stdDev = standardDeviation;
            this->sqrt2PiVar = SQRT(2.0 * M_PI) * standardDeviation;
            this->negInvVar2 = -0.5 / (standardDeviation * standardDeviation);

            normalization = 0.0;
            for (size_t d = 0; d <= dimension; ++d)
            {
                real_t binom = sets_binomial((real_t) dimension, (real_t) d);
                real_t expon = EXP(negInvVar2 * d);
                if (ISINF(binom))
                {
                    cout << "binom = inf at d = " << d << endl;
                    if (expon > 1.0e-20)
                    {
                        cout << "exponent > 10^(-20)" << endl;
                    }
                }
                else
                {
                    normalization += binom * expon;
                }
            }
            cout << "normalization: " << normalization << endl;
        }

        virtual ~BinaryGaussianMixtureDistribution()
        {
        }

        virtual real_t getProbability(const string &x) const override
        {
            real_t prob = 0.0;
            for (auto &entry : rawEmpiricalDist)
            {
                real_t squaredDist = getSquaredDistance(x, entry.first);
                prob += entry.second * EXP(negInvVar2 * squaredDist);
            }
            
            return prob / normalization;
        }
};

#endif
