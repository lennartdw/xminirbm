/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Dataset of black-and-white mini images.
 */

#ifndef _XMINIRBM_DATASETS_IMAGEBW
#define _XMINIRBM_DATASETS_IMAGEBW

#include "../util/headers.hpp"

#include "../analysis/DistributionAnalyzer.hpp"
#include "../analysis/CategoricalDistributionAnalyzer.hpp"
#include "../analysis/CrossEntropyAnalyzer.hpp"

template<typename Scalar>
class ImageBW
{
    public:
        class Pattern; // defined below

    private:
        Array<Scalar, Dynamic, Dynamic> pixels;
        bool periodicBoundaryConditions = true;

    public:
        ImageBW(const Array<Scalar, Dynamic, Dynamic> &pixels, bool periodicBoundaries = true)
        {
            this->pixels = pixels;
            this->periodicBoundaryConditions = periodicBoundaries;
        }

        ImageBW(const Array<Scalar, 1, Dynamic> &flattenedPixels, int imageWidth, int imageHeight, bool periodicBoundaries = true, bool columnMajor = true)
        {
            this->pixels.resize(imageHeight, imageWidth);
            for (int j = 0; j < imageWidth; ++j)
            {
                for (int i = 0; i < imageHeight; ++i)
                {
                    this->pixels(i, j) = flattenedPixels(j * imageHeight + i);
                }
            }

            if (! columnMajor)
            {
                this->pixels = this->pixels.transpose();
            }

            this->periodicBoundaryConditions = periodicBoundaries;
        }

        virtual ~ImageBW() {}

        struct PatternPixel {
            int row;
            int col;
            Scalar color;
            bool errorAllowed = false;
            bool outOfBoundsAllowed = false;
        };

        struct PatternPosition {
            int row;
            int col;
        };

        class Pattern
        {
            public:
                vector<PatternPixel> pixels;

                size_t getNumPixels() const
                {
                    return pixels.size();
                }

                void getPatternBounds(int &rowMin, int &colMin, int &minHeight, int &minWidth) const
                {
                    rowMin = std::numeric_limits<int>::max();
                    colMin = std::numeric_limits<int>::max();

                    for (auto &px : pixels)
                    {
                        if (! px.outOfBoundsAllowed)
                        {
                            if (px.row < rowMin)
                                rowMin = px.row;
                            if (px.col < colMin)
                                colMin = px.col;
                        }
                    }

                    int rowMax = rowMin;
                    int colMax = colMin;
                    for (auto &px : pixels)
                    {
                        if (! px.outOfBoundsAllowed)
                        {
                            if (px.row > rowMax)
                                rowMax = px.row;
                            if (px.col > colMax)
                                colMax = px.col;
                        }
                    }

                    minHeight = rowMax - rowMin + 1;
                    minWidth = colMax - colMin + 1;
                }
        };

        static ImageBW<Scalar> createBinaryImage(int width, int height, unsigned long int code, bool periodicBoundaryConditions = true)
        {
            Array<Scalar, Dynamic, Dynamic> pixels(height, width);
            for (int j = 0; j < width; ++j)
            {
                for (int i = 0; i < height; ++i)
                {
                    if (code & (((unsigned long int) 1) << (j * height + i)))
                    {
                        pixels(i, j) = 1;
                    }
                    else
                    {
                        pixels(i, j) = 0;
                    }
                }
            }

            return ImageBW<Scalar>(pixels, periodicBoundaryConditions);
        }

        /**
         * Provide a cross-entropy analyzer for a reference distribution
         * whose images consist of uniform distributions of the
         * provided pattern with the given activation probability
         * of other (non-pattern) pixels, i.e., the distribution
         * underlying the ImageBW::random(...) function.
         */
        static CrossEntropyAnalyzer<Scalar> *createPatternCrossEntropyAnalyzer(const string &name, int imageWidth, int imageHeight, const Pattern &pattern, Scalar otherActProb)
        {
            return new CrossEntropyAnalyzer<Scalar>(name,
                    [imageWidth, imageHeight, pattern, otherActProb](const Array<Scalar, 1, Dynamic> &sample) -> Scalar {

                        ImageBW<Scalar> img(sample, imageWidth, imageHeight);
                        return img.getProbability(pattern, otherActProb);
                    }
                );
        }

        /**
         * Provide a cross-entropy analyzer for a reference distribution
         * whose images are distributed as specified in the
         * provided map of image indices onto probabilities.
         */
        static CrossEntropyAnalyzer<Scalar> *createCrossEntropyAnalyzer(const string &name, int imageWidth, int imageHeight, const unordered_map<unsigned long int, Scalar> &imageProbabilities)
        {
            const int numPixels = imageWidth * imageHeight;

            return new CrossEntropyAnalyzer<Scalar>(name,
                    [numPixels, imageProbabilities](const Array<Scalar, 1, Dynamic> &sample) -> Scalar {
                        // calculate image index
                        unsigned long int imageIndex = integer_from_digits(sample.matrix(), 2);

                        // check probability
                        auto imageInfo = imageProbabilities.find(imageIndex);
                        if (imageInfo == imageProbabilities.end())
                        {
                            return 0.0;
                        }
                        else
                        {
                            return imageInfo->second;
                        }
                    }
                );
        }

        static CategoricalDistributionAnalyzer<string, Scalar> *createPatternDistributionAnalyzer(const string &name, int imageWidth, int imageHeight, const Pattern &pattern)
        {
            return new CategoricalDistributionAnalyzer<string, Scalar>(name,
                    [imageWidth, imageHeight, pattern](const Array<Scalar, 1, Dynamic> &sample, void *metaData) -> string {
                        ImageBW<Scalar> img(sample, imageWidth, imageHeight);
                        PatternPosition pos;
                        if (img.findPattern(pos, pattern))
                        {
                            return to_string(pos.row) + "," + to_string(pos.col);
                        }
                        else
                        {
                            // pattern not found
                            return "-";
                        }
                    }
                );
        }

        bool findPattern(PatternPosition &pos, const Pattern &pattern, int errorTolerance = 0) const
        {
            int errorCount = 0;

            // cycle all pixels as potential pattern positions
            for (int j = 0; j < pixels.cols(); ++j)
            {
                for (int i = 0; i < pixels.rows(); ++i)
                {
                    // check all pattern entries for match
                    bool match = true;
                    for (auto &pp : pattern.pixels)
                    {
                        int row = i + pp.row;
                        if (row < 0 || row >= pixels.rows())
                        {
                            if (periodicBoundaryConditions)
                            {
                                row = MOD(row, pixels.rows());
                            }
                            else
                            {
                                if (pp.outOfBoundsAllowed)
                                {
                                    continue;
                                }
                                else
                                {
                                    match = false;
                                    break;
                                }
                            }
                        }

                        int col = j + pp.col;
                        if (col < 0 || col >= pixels.cols())
                        {
                            if (periodicBoundaryConditions)
                            {
                                col = MOD(col, pixels.cols());
                            }
                            else
                            {
                                if (pp.outOfBoundsAllowed)
                                {
                                    continue;
                                }
                                else
                                {
                                    match = false;
                                    break;
                                }
                            }
                        }

                        if (pixels(row, col) != pp.color)
                        {
                            if (pp.errorAllowed && errorCount < errorTolerance)
                            {
                                ++errorCount;
                            }
                            else
                            {
                                match = false;
                                break;
                            }
                        }
                    }

                    if (match)
                    {
                        // pattern matched at current position
                        pos.row = i;
                        pos.col = j;
                        return true;
                    }
                }
            }

            // no match found
            return false;
        }

        bool findPattern(const Pattern &pattern)
        {
            PatternPosition dummy;
            return findPattern(dummy, pattern);
        }

        int getHeight() const
        {
            return pixels.rows();
        }

        const Array<Scalar, Dynamic, 1> getNonPatternPixels(const PatternPosition &pos, const Pattern &pattern) const
        {
            Array<int, Dynamic, Dynamic> patternMask = Array<int, Dynamic, Dynamic>::Zero(getHeight(), getWidth());
            int patternPixelCount = 0;

            // cout << "pos = " << pos.row << "," << pos.col << endl;
            // cout << "pbc = " << periodicBoundaryConditions << endl;

            for (auto &pp : pattern.pixels)
            {
                // cout << "    pp = " << pp.row << "," << pp.col << endl;
                int row = pos.row + pp.row;
                if (row < 0 || row >= pixels.rows())
                {
                    if (periodicBoundaryConditions)
                    {
                        row = MOD(row, getHeight());
                    }
                    else
                    {
                        if (pp.outOfBoundsAllowed)
                        {
                            continue;
                        }
                        else
                        {
                            // error: pattern cannot occur
                            // at this position
                            return Array<Scalar, Dynamic, 1>();
                        }
                    }
                }
                // cout << "        row = " << row << endl;

                int col = pos.col + pp.col;
                if (col < 0 || col >= pixels.cols())
                {
                    if (periodicBoundaryConditions)
                    {
                        col = MOD(col, getWidth());
                    }
                    else
                    {
                        if (pp.outOfBoundsAllowed)
                        {
                            continue;
                        }
                        else
                        {
                            // error: pattern cannot occur
                            // at this position
                            return Array<Scalar, Dynamic, 1>();
                        }
                    }
                }
                // cout << "        col = " << col << endl;

                patternMask(row, col) = true;
                ++patternPixelCount;
            }

            Array<Scalar, Dynamic, 1> nonPatternPixels(getNumPixels() - patternPixelCount);

            int npc = 0;
            for (int i = 0; i < getHeight(); ++i)
            {
                for (int j = 0; j < getWidth(); ++j)
                {
                    if (! patternMask(i, j))
                    {
                        nonPatternPixels(npc) = pixels(i, j);
                        ++npc;
                    }
                }
            }

            return nonPatternPixels;
        }

        const size_t getNumPixels() const
        {
            return pixels.size();
        }

        Scalar getPixel(int row, int col) const
        {
            return pixels(MOD(row, getHeight()), MOD(col, getWidth()));
        }

        const Array<Scalar, Dynamic, Dynamic> &getPixels() const
        {
            return pixels;
        }

        /**
         * Calculate the image probability for a reference distribution
         * whose images consist of uniform distributions of the provided
         * pattern with the given activation probability of other
         * (non-pattern) pixels, i.e., the distribution underlying the
         * ImageBW::random(...) function.
         */
        Scalar getProbability(const Pattern &pattern, Scalar otherActProb) const
        {
            // uniform pattern distribution:
            Scalar prob = 1.0 / (getWidth() * getHeight());
            PatternPosition pos;
            if (findPattern(pos, pattern))
            {
                Array<Scalar, Dynamic, 1> nonPatternPixels = getNonPatternPixels(pos, pattern);
                for (int npi = 0; npi < nonPatternPixels.size(); ++npi)
                {
                    // activation probabilities for
                    // non-pattern pixels
                    if (nonPatternPixels(npi) > 0.0)
                    {
                        prob *= otherActProb;
                    }
                    else
                    {
                        prob *= (1.0 - otherActProb);
                    }
                }

                return prob;
            }
            else
            {
                // not a pattern image
                return 0.0;
            }
        }

        /**
         * Calculate the image probability for a reference distribution
         * whose images consist of distributions of the provided
         * patterns with uniform positions and the given activation
         * probability of other (non-pattern) pixels.
         */
        Scalar getProbability(const vector<Pattern> &patterns, const vector<Scalar> &patternProbabilities, Scalar otherActProb) const
        {
            for (size_t k = 0; k < patterns.size(); ++k)
            {
                Scalar patternProb = getProbability(patterns[k], otherActProb);
                if (patternProb > 0.0)
                {
                    return patternProb * patternProbabilities[k];
                }
            }

            return 0.0; // not an image showing any of the patterns
        }

        int getWidth() const
        {
            return pixels.cols();
        }

        void print()
        {
            for (int c = 0; c < getWidth() + 2; ++c)
            {
                printf("-");
            }
            printf("\n");

            for (int r = 0; r < getHeight(); ++r)
            {
                printf("|");
                for (int c = 0; c < getWidth(); ++c)
                {
                    if (getPixel(r, c) > 0.0)
                    {
                        printf("*");
                    }
                    else
                    {
                        printf(" ");
                    }
                }
                printf("|\n");
            }

            for (int c = 0; c < getWidth() + 2; ++c)
            {
                printf("-");
            }
            printf("\n");
        }

        static ImageBW random(int width, int height, const Pattern &pattern, Scalar otherProb = 0.5, bool periodicBoundaryConditions = true)
        {
            Array<Scalar, Dynamic, Dynamic> pixels(height, width);

            // generate random background
            for (int j = 0; j < width; ++j)
            {
                for (int i = 0; i < height; ++i)
                {
                    if (randd() < otherProb)
                    {
                        pixels(i, j) = 1.0;
                    }
                    else
                    {
                        pixels(i, j) = 0.0;
                    }
                }
            }

            // imprint pattern at random position
            if (periodicBoundaryConditions)
            {
                int i = (int) (randd() * height);
                int j = (int) (randd() * width);
                for (auto &pp : pattern.pixels)
                {
                    int row = MOD(i + pp.row, pixels.rows());
                    int col = MOD(j + pp.col, pixels.cols());
                    pixels(row, col) = pp.color;
                }
            }
            else
            {
                int rowMin, colMin, minHeight, minWidth;
                pattern.getPatternBounds(rowMin, colMin, minHeight, minWidth);

                int i = rowMin + (int) (randd() * (height - minHeight + 1));
                int j = colMin + (int) (randd() * (width - minWidth + 1));
                for (auto &pp : pattern.pixels)
                {
                    int row = i + pp.row;
                    int col = j + pp.col;
                    if (0 <= row && row < height && 0 <= col && col < width)
                    {
                        pixels(row, col) = pp.color;
                    }
                }
            }

            return ImageBW(pixels);
        }

        static vector<ImageBW> random(int numSamples, int width, int height, const Pattern &pattern, Scalar otherProb = 0.5, bool periodicBoundaryConditions = true)
        {
            vector<ImageBW> samples;
            for (int s = 0; s < numSamples; ++s)
            {
                samples.push_back(random(width, height, pattern, otherProb, periodicBoundaryConditions));
            }

            return samples;
        }

        static ImageBW random(int width, int height, const vector<Pattern> &patterns, const vector<Scalar> &patternProbabilities, Scalar otherProb = 0.5, bool periodicBoundaryConditions = true)
        {
            Scalar r = randd();
            Scalar cumProb = patternProbabilities[0];
            int index = 0;
            while (r >= cumProb)
            {
                cumProb += patternProbabilities[++index];
            }

            return random(width, height, patterns[index], otherProb, periodicBoundaryConditions);
        }

        static vector<ImageBW> random(int numSamples, int width, int height, const vector<Pattern> &patterns, const vector<Scalar> &patternProbabilities, Scalar otherProb = 0.5, bool periodicBoundaryConditions = true)
        {
            vector<ImageBW> samples;
            for (int s = 0; s < numSamples; ++s)
            {
                samples.push_back(random(width, height, patterns, patternProbabilities, otherProb, periodicBoundaryConditions));
            }

            return samples;
        }

        template<typename L>
        static Array<Scalar, Dynamic, Dynamic> flatten(const L &imageList)
        {
            if (imageList.size() <= 0)
            {
                return Array<Scalar, Dynamic, Dynamic>(0, 0);
            }

            int width = imageList[0].getWidth();
            int height = imageList[0].getHeight();
            Array<Scalar, Dynamic, Dynamic> flattenedImages(imageList.size(), height * width);

            for (int k = 0; k < (int) imageList.size(); ++k)
            {
                for (int j = 0; j < width; ++j)
                {
                    for (int i = 0; i < height; ++i)
                    {
                        flattenedImages(k, j * height + i) = imageList[k].getPixels()(i, j);
                    }
                }
            }

            return flattenedImages;
        }
};

#endif
