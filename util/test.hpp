/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Test routines.
 */

#ifndef _XMINIRBM_TEST_H
#define _XMINIRBM_TEST_H

#include "headers.hpp"

void test(const string &name);

#endif
