/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Collective header include file
 */

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <random>
#include <set>
#include <type_traits>
#include <vector>
#include <unistd.h>

/* OpenMP includes */
#ifdef _OPENMP
#include <omp.h>
#endif

/* dwamf includes */
#include "func/types.hpp"
#include "func/sets.hpp"
#include "conf/Configuration.hpp"
#include "data/DataIO.hpp"
#include "rand/numbers.hpp"
#include "rand/numbers-gsl-macros.hpp"
#include "rand/statistics.hpp"
#include "util/bitwise.hpp"
#include "util/files.hpp"
#include "util/strings.hpp"
#include "util/ProgressMonitor.hpp"
#include "util/LogManager.hpp"
using namespace dwamf;

/* Eigen includes */
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/unsupported/Eigen/FFT>
#include <eigen3/unsupported/Eigen/KroneckerProduct>
