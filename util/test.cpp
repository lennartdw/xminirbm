/**
 * Exact MiniRBM Learning
 * (c) 2021-22 RIKEN
 * Author: Lennart Dabelow
 *
 * Source/implementation for "test.hpp"
 */

#include "test.hpp"

#include "headers.hpp"

#include "util/bitwise.hpp"
#include "rand/IndexDistribution.hpp"
#include "func/matrices.hpp"
#include "traf/vectors.hpp"
#include "../datasets/prototypes.hpp"
#include "../datasets/ImageBW.hpp"
#include "../datasets/BinaryGaussianMixtureDistribution.hpp"
#include "../platforms/UnsupervisedPlatform.hpp"
#include "../networks/ExactBinaryRBM.hpp"

extern void experiment_modes(const Configuration&);

void test_cpp()
{
    vector<int> r = range_generate(10, 0, 9);
    cout << "r = ";
    for (int i = 0; i < (int) r.size(); ++i)
    {
        cout << r[i] << ", ";
    }
    cout << "\b\b  " << endl;
}

void test_imagebw()
{
    cout << "[test] 'ImageBW' class functionality" << endl;

    float dummy;
    ImageBW<float>::Pattern PATTERN_HOOK = datasets_pattern_hook<float>(dummy);

    ImageBW<float> testImg = ImageBW<float>::random(5, 5, PATTERN_HOOK, 0.1);
    cout << testImg.getPixels() << endl;

    ImageBW<float>::PatternPosition pos;
    if (testImg.findPattern(pos, PATTERN_HOOK))
    {
        cout << "Found pattern at " << pos.row << ", " << pos.col << endl;
    }
    else
    {
        cout << "Could not find pattern." << endl;
    }

    cout << "       test complete" << endl;
}

void test_x2rbm()
{
    cout << "[test] 'ExactBinaryRBM' class functionality" << endl;
    int batchSize = 10;
    float learningRate = 0.1;
    int numVisible = 25;
    int numHidden = 4;

    ExactBinaryRBM<float> rbm(numVisible, numHidden);
    UnsupervisedPlatform<float> platform(&rbm, batchSize, learningRate);

    Array<float, Dynamic, Dynamic> batch = Array<float, Dynamic, Dynamic>::Random(batchSize, numVisible);
    // rbm.trainingStep(batch);
    platform.train(batch);

    cout << "       test complete" << endl;
}

void test_eigen()
{
    cout << "[test] 'eigen' functionality" << endl;

    Matrix<float, Dynamic, Dynamic> vec(5, 4);
    vec << 1, 2, 3, 4,
           5, 6, 7, 8,
           9,10,11,12,
          13,14,15,16,
          17,18,19,20;
    cout << "vec = " << vec << endl;
    long int mask1 = 0b0011;
    long int mask2 = 0b1001;
    cout << "bwp = " << bitwise_product(mask1, vec, mask2);

    cout << "colwise mean = " << vec.colwise().mean() << endl;
    cout << "sum = " << vec.colwise().mean().sum() << endl;

    cout << "       test complete" << endl;
}

void test_distana()
{
    CategoricalDistributionAnalyzer<int, float> analyzer("testana",
        [](const Array<float, 1, Dynamic> &sample, void *params) -> int {
                return ROUND(sample(1));
            });
    Array<float, Dynamic, Dynamic> testData(5, 2);
    testData << 1, 2,
                3, 2,
                4, 3,
                1, 5,
                4, 2;
    for (int k = 0; k < testData.rows(); ++k)
    {
        float pprob = (k == 0 ? 6.0 : 1.0);
        analyzer.processSample(testData.row(k), pprob);
    }
    analyzer.normalize();
    analyzer.saveToFile("/home/lennart/Desktop/testana.dst");
}

void test_indexdist()
{
    vector<real_t> probs(3);
    probs[0] = 0.1;
    probs[1] = 0.6;
    probs[2] = 0.3;

    IndexDistribution dist(probs);

    for (int k = 0; k < 10; ++k)
    {
        cout << "[" << k << "] " << dist.getIntegerVariate() << endl;
    }
}

void test_setrank()
{
    vector<int> testSet = {1,2,3,6,7,20};
    cout << "rank = " << sets_rank(testSet) << endl;

    int testRank = 38799;
    int testK = 6;
    cout << "for rank " << testRank << ", " << testK << "-subset reads ";
    testSet = sets_subset_from_rank(testK, testRank);
    for (auto &c : testSet)
    {
        cout << c << ", ";
    }
    cout << endl;
}

void test_modes()
{
    int numTestUnits = 3;
    Matrix<real_t, Dynamic, 1> testProbs(1 << numTestUnits);

    testProbs(0b000) = 0.0;
    testProbs(0b001) = 0.2;
    testProbs(0b010) = 0.0;
    testProbs(0b011) = 0.4; // strong mode
    testProbs(0b100) = 0.2; // mode
    testProbs(0b101) = 0.1;
    testProbs(0b110) = 0.1;
    testProbs(0b111) = 0.0;

    DataIO::exportMatrixRaw("/tmp/test-dist.raw", testProbs);

    Configuration config;
    config.setValue("distribution_file", "/tmp/test-dist.raw");
    config.setValue("num_visible", 3);
    config.setValue("output_file", "/tmp/test-dist-modes.dst");
    experiment_modes(config);
}

void test_empgauss()
{
    size_t numUnits = 6;
    real_t stdDev = 1.0;
    unordered_map<string, real_t> stateProbsMap;
    stateProbsMap["000000"] = 0.2;
    stateProbsMap["100100"] = 0.15;
    stateProbsMap["111101"] = 0.05;
    stateProbsMap["101101"] = 0.2;
    stateProbsMap["111110"] = 0.2;
    stateProbsMap["111111"] = 0.1;
    stateProbsMap["000100"] = 0.1;

    BinaryGaussianMixtureDistribution dist(stateProbsMap, numUnits, stdDev);
    size_t numStates = ((size_t) 1) << numUnits;
    real_t probSum = 0.0;
    for (size_t state = 0; state < numStates; ++state)
    {
        string stateStr = to_base_string(state, 2, numUnits);
        probSum += dist.getProbability(stateStr);
    }
    cout << "prob. sum: " << probSum << endl;
}

void test_mnist()
{
    string fileName = "/run/media/dabelow/DAT2/MiniImage/data_MNIST/train-images.idx3-ubyte";
    cout << "import from '" << fileName << "'" << endl;
    boost::multi_array<unsigned char, 3> imgs;
    int r = DataIO::importMultiArrayIDX(fileName, imgs);
    cout << "code: " << r << endl;

    int imgIndex = 17;
    cout << "image: " << imgIndex << endl;
    for (int i = 0; i < (int) imgs.shape()[1]; ++i)
    {
        for (int j = 0; j < (int) imgs.shape()[2]; ++j)
        {
            if (imgs[imgIndex][i][j] < 128)
            {
                cout << " ";
            }
            else
            {
                cout << "*";
            }
        }
        cout << endl;
    }
}

void test_arrredux()
{
    Array<double, Dynamic, Dynamic> arr(2, 5);
    arr << 1, 5, 3, 4, 2,
           -1, -2, -3, -4, -5;

    cout << "test array:" << endl;
    cout << arr << endl;

    vector<string> trafSpecs;
    trafSpecs.push_back("max 0 1");
    trafSpecs.push_back("max 2 3 4");

    Array<double, Dynamic, Dynamic> trafArr = array_transform_rows(arr, vector_parse_transform< Array<double, 1, Dynamic> >(trafSpecs));
    cout << "traf array:" << endl;
    cout << trafArr << endl;
}

void test(const string &name)
{
    if (name == "x2rbm")
    {
        test_x2rbm();
    }
    else if (name == "eigen")
    {
        test_eigen();
    }
    else if (name == "cpp")
    {
        test_cpp();
    }
    else if (name == "imagebw")
    {
        test_imagebw();
    }
    else if (name == "distana")
    {
        test_distana();
    }
    else if (name == "indexdist")
    {
        test_indexdist();
    }
    else if (name == "setrank")
    {
        test_setrank();
    }
    else if (name == "modes")
    {
        test_modes();
    }
    else if (name == "mnist")
    {
        test_mnist();
    }
    else if (name == "arrredux")
    {
        test_arrredux();
    }
    else if (name == "empgauss")
    {
        test_empgauss();
    }
    else
    {
        STDLOG.printf_err("Unknown test routine '%s'.", name.c_str());
    }
}
